//
// Created by philipp on 12.05.18.
//

#include "../include/MessageLib/Client.h"
#include "../include/MessageLib/Server.h"
#include <iostream>

void printMessage(const message::Message & message)
{
    std::cout << "module: " << message.module
              << "\nkey: " << message.key
              << "\nvalue: " << message.value
              << "\n----------\n";
}

void fromLocal()
{
    using namespace message;
    using namespace std::chrono_literals;

    networking::Networking net;
    auto server = Server::create(net, 10207);
    auto client = Client::create(net, 10207);
    std::atomic<bool> running{true};

    server->run();
    for (std::size_t i = 0; i < 100; i++)
        *server << Message{"module", "key", std::to_string(i)};

    client->requestMessages(
        "127.0.0.1", 10s,
        [&](const auto & message) { printMessage(message); },
        [&](const auto & error)
        {
            if (error)
                std::cerr << "FAILED!\n";
            running = false;
        });

    while (running);
}

void fromCar()
{
    using namespace message;
    using namespace std::chrono_literals;
    networking::Networking net;
    auto client = Client::create(net, 10207);
    client->requestMessagesPeriodically(
        "127.0.0.1", 10ms, 3s,
        [](const auto & message) { printMessage(message); },
        [](const auto & error) { if (error) std::cerr << "Could not connect to car!\n"; });
    while (1);
}

int main(int argc, char ** argv)
{
    fromCar();
    return 0;
}