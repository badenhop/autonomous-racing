//
// Created by philipp on 12.05.18.
//

#include <iostream>
#include "../include/MessageLib/Parser.h"

void printMessage(const message::Message & message)
{
    std::cout << "module: " << message.module
              << "\nkey: " << message.key
              << "\nvalue: " << message.value
              << "\n----------\n";
}

int main(int argc, char ** argv)
{
    using namespace message;

    Parser parser;
    Message message;
    parser << R"({module: "MainNode" key: ")";
    if (parser >> message)
        printMessage(message);

    parser << R"(MyKey" value: "MyValue"}
               {module:"MavLink" key: "onInit" value: "START"})";
    while (parser >> message)
        printMessage(message);

    return 0;
}
