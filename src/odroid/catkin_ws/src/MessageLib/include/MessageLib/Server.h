//
// Created by philipp on 12.05.18.
//

#ifndef PC_MESSAGESENDER_H
#define PC_MESSAGESENDER_H

#include "NetworkingLib/Networking.h"
#include "NetworkingLib/ServiceServer.h"
#include "Message.h"
#include "StringService.h"

namespace message
{

class Server : public std::enable_shared_from_this<Server>
               , public networking::Busyable
{
private:
    struct PrivateTag
    {
    };

public:
    static constexpr std::size_t MAX_MESSAGE_SIZE{1024};

    using Ptr = std::shared_ptr<Server>;
    using ServiceServer = networking::service::Server<StringService>;

    static Ptr create(networking::Networking & net,
                      std::uint16_t port,
                      std::size_t maxBufferSize = std::numeric_limits<std::size_t>::max())
    { return std::make_shared<Server>(PrivateTag{}, net, port, maxBufferSize); }

    Server(PrivateTag, networking::Networking & net,
           std::uint16_t port,
           std::size_t maxBufferSize);

    void run();

    void stop();

    Server & operator<<(const Message & message);

private:
    struct AsyncState
    {
        AsyncState(Ptr self)
            : self(self), lock(*self)
        {}

        Ptr self;
        networking::BusyLock lock;
    };

    networking::Networking & net;
    std::uint16_t port;
    networking::service::Server<StringService>::Ptr server;
    std::mutex mutex;
    boost::asio::streambuf buffer;
    std::ostream ostream;
};

}

#endif //PC_MESSAGESENDER_H
