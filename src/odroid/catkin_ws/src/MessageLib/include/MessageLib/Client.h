//
// Created by philipp on 08.05.18.
//

#ifndef PC_LOGGER_H
#define PC_LOGGER_H

#include <memory>
#include <NetworkingLib/Networking.h>
#include <NetworkingLib/ServiceClient.h>
#include "Message.h"
#include "Parser.h"
#include "StringService.h"

namespace message
{

class Client
    : public std::enable_shared_from_this<Client>
      , public networking::Busyable
{
private:
    struct PrivateTag
    {
    };

public:
    using Ptr = std::shared_ptr<Client>;
    using ServiceClient = networking::service::Client<StringService>;
    using OnMessageProcessedCallback = std::function<void(const Message & message)>;
    using OnRequestFinishedCallback = std::function<void(const networking::error::ErrorCode & error)>;

    static Ptr create(networking::Networking & net, std::uint16_t port, std::size_t maxBufferSize=1024)
    { return std::make_shared<Client>(PrivateTag{}, net, port, maxBufferSize); }

    Client(PrivateTag, networking::Networking & net, std::uint16_t port, std::size_t maxBufferSize);

    void requestMessages(const std::string & host,
                         const networking::time::Duration & timeout,
                         const OnMessageProcessedCallback & onMessageProcessedCallback,
                         const OnRequestFinishedCallback & onRequestFinishedCallback = [](const auto & error) {});

    void requestMessagesPeriodically(const std::string & host,
                                     const networking::time::Duration & requestInterval,
                                     const networking::time::Duration & retryInterval,
                                     const OnMessageProcessedCallback & onMessageProcessedCallback,
                                     const OnRequestFinishedCallback & onRequestFinishedCallback =
                                     [](const auto & error) {});

    void stop();

private:
    struct AsyncState
    {
        using Ptr = std::shared_ptr<AsyncState>;

        AsyncState(Client::Ptr self,
                   const OnMessageProcessedCallback & onMessageProcessedCallback,
                   const OnRequestFinishedCallback & onRequestFinishedCallback)
            : lock(*self)
              , self(self)
              , onMessageProcessedCallback(onMessageProcessedCallback)
              , onRequestFinishedCallback(onRequestFinishedCallback)
        {}

        networking::BusyLock lock;
        Client::Ptr self;
        OnMessageProcessedCallback onMessageProcessedCallback;
        OnRequestFinishedCallback onRequestFinishedCallback;
    };

    struct PeriodicAsyncState
    {
        using Ptr = std::shared_ptr<PeriodicAsyncState>;

        PeriodicAsyncState(Client::Ptr self,
                           const std::string & host,
                           const networking::time::Duration & requestInterval,
                           const networking::time::Duration & retryInterval,
                           const OnMessageProcessedCallback & onMessageProcessedCallback,
                           const OnRequestFinishedCallback & onRequestFinishedCallback)
            : lock(*self)
              , self(self)
              , host(host)
              , requestInterval(requestInterval)
              , retryInterval(retryInterval)
              , onMessageProcessedCallback(onMessageProcessedCallback)
              , onRequestFinishedCallback(onRequestFinishedCallback)
        {}

        networking::BusyLock lock;
        Client::Ptr self;
        std::string host;
        networking::time::Duration requestInterval;
        networking::time::Duration retryInterval;
        OnMessageProcessedCallback onMessageProcessedCallback;
        OnRequestFinishedCallback onRequestFinishedCallback;
    };

    networking::Networking & net;
    std::uint16_t port;
    ServiceClient::Ptr client;
    Parser parser;
    networking::time::Timer::Ptr timer;
    std::atomic<bool> requesting{false};

    void processResponse(const std::string & response,
                         const OnMessageProcessedCallback & onMessageProcessedCallback);

    void scheduleNextRequest(PeriodicAsyncState::Ptr state, const networking::time::Duration & waitTime);

    void nextRequest(PeriodicAsyncState::Ptr state);
};

}

#endif //PC_LOGGER_H
