//
// Created by philipp on 12.05.18.
//

#ifndef PC_STRINGSERVICE_H
#define PC_STRINGSERVICE_H

namespace message
{

struct StringService
{
    using RequestMessage = std::string;
    using ResponseMessage = std::string;
};

}

#endif //PC_STRINGSERVICE_H
