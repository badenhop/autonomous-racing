//
// Created by philipp on 11.05.18.
//

#ifndef PC_MESSAGE_H
#define PC_MESSAGE_H

#include <string>
#include <utility>

namespace message
{

struct Message
{
    Message() = default;

    Message(std::string module, std::string key, std::string value)
        : module(std::move(module)), key(std::move(key)), value(std::move(value))
    {}

    std::string module;
    std::string key;
    std::string value;


};

}

#endif //PC_MESSAGE_H
