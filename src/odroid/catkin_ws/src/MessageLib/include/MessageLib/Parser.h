//
// Created by philipp on 11.05.18.
//

#ifndef PC_MESSAGEPARSER_H
#define PC_MESSAGEPARSER_H

#include <unordered_map>
#include "ParseState.h"
#include "Message.h"

namespace message
{

class Parser
{
public:
    Parser();

    Parser & operator<<(const std::string & input);

    bool operator>>(Message & message);

private:
    using MessageMap = std::unordered_map<std::string, std::string>;

    ParseState
        stateMessageBegin{"MessageBegin"},
        stateIdentifierBegin{"IdentifierBegin"},
        stateIdentifier{"Identifier"},
        stateIdentifierEnd{"IdentifierEnd"},
        stateContentBegin{"ContentBegin"},
        stateContent{"Content"},
        stateEscape{"Escape"},
        stateFinal{"Final"};

    ParseState::Condition conditionDefault = [](char c) { return true; };

    ParseState::Condition conditionWhitespace = [](char c) { return c == ' ' || c == '\t' || c == '\n' || c == '\r'; };

    ParseState::Condition conditionLetterOrDigit = [](char c) { return std::isalpha(c) || std::isdigit(c); };

    ParseState::Condition conditionMessageBegin = [](char c) { return c == '{'; };

    ParseState::Condition conditionMessageEnd = [](char c) { return c == '}'; };

    ParseState::Condition conditionColon = [](char c) { return c == ':'; };

    ParseState::Condition conditionQuote = [](char c) { return c == '\"'; };

    ParseState::Condition conditionEscape = [](char c) { return c == '\\'; };

    ParseState::Event eventResetMessage = [this](char c) { messageMap = MessageMap{}; };

    ParseState::Event eventResetIdentifier = [this](char c) { identifier = c; };

    ParseState::Event eventAccumulateIdentifier = [this](char c) { identifier += c; };

    ParseState::Event eventResetContent = [this](char c) { content = ""; };

    ParseState::Event eventAccumulateContent = [this](char c) { content += c; };

    ParseState::Event eventUpdateMessage = [this](char c) { messageMap[identifier] = content; };

    ParseState::Event eventDoNothing = [this](char c) {};

    std::string input;
    std::string identifier;
    std::string content;
    MessageMap messageMap;
    ParseState * currState;

    char popChar();
};

}

#endif //PC_MESSAGEPARSER_H
