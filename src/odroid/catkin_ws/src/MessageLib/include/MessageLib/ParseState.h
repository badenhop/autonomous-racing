//
// Created by philipp on 11.05.18.
//

#ifndef PC_STATE_H
#define PC_STATE_H

#include <string>
#include <functional>
#include <vector>

namespace message
{

class ParseState
{
public:
    class NoTransitionError : public std::exception
    {
        const char* what() const noexcept override
        { return "This state cannot take any transition!\n"; }
    };

    using Condition = std::function<bool(char)>;
    using Event = std::function<void(char)>;

    explicit ParseState(std::string label="")
        : label(std::move(label))
    {}

    ParseState * operator()(char c);

    void addTransition(ParseState & nextState, const Condition & condition, const Event & event)
    { transitions.emplace_back(nextState, condition, event); }

    std::string getLabel() const
    { return label; }

private:
    struct Transition
    {
        Transition(ParseState & nextState, const Condition & condition, const Event & event)
            : nextState(&nextState)
              , condition(condition)
              , event(event)
        {}

        ParseState * nextState;
        Condition condition;
        Event event;
    };

    std::string label;
    std::vector<Transition> transitions;
};

}

#endif //PC_STATE_H
