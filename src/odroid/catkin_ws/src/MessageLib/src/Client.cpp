//
// Created by philipp on 08.05.18.
//

#include "../include/MessageLib/Client.h"
#include <iostream>

namespace message
{

Client::Client(Client::PrivateTag, networking::Networking & net, std::uint16_t port, std::size_t maxBufferSize)
    : net(net), port(port)
{
    client = ServiceClient::create(net, maxBufferSize);
    timer = networking::time::Timer::create(net);
}

void Client::requestMessages(const std::string & host,
                             const networking::time::Duration & timeout,
                             const Client::OnMessageProcessedCallback & onMessageProcessedCallback,
                             const OnRequestFinishedCallback & onRequestFinishedCallback)
{
    using namespace std::chrono_literals;
    auto self = shared_from_this();
    auto state = std::make_shared<AsyncState>(self, onMessageProcessedCallback, onRequestFinishedCallback);
    std::string emptyRequest;
    client->asyncCall(
        emptyRequest, host, port, timeout,
        [state](const auto & error, const auto & responseMessage)
        {
            if (error)
            {
                state->onRequestFinishedCallback(error);
                return;
            }
            state->self->processResponse(responseMessage, state->onMessageProcessedCallback);
            state->onRequestFinishedCallback(error);
        });
}

void Client::requestMessagesPeriodically(const std::string & host,
                                         const networking::time::Duration & requestInterval,
                                         const networking::time::Duration & retryInterval,
                                         const OnMessageProcessedCallback & onMessageProcessedCallback,
                                         const OnRequestFinishedCallback & onRequestFinishedCallback)
{
    auto self = shared_from_this();
    auto state = std::make_shared<PeriodicAsyncState>(
        self, host, requestInterval, retryInterval, onMessageProcessedCallback, onRequestFinishedCallback);
    requesting = true;
    nextRequest(std::move(state));
}

void Client::stop()
{
    client->stop();
    timer->stop();
    requesting = false;
}

void Client::processResponse(const std::string & response,
                             const OnMessageProcessedCallback & onMessageProcessedCallback)
{
    if (!response.empty())
    {
        parser << response;
        Message message;
        while (parser >> message)
            onMessageProcessedCallback(message);
    }
}

void Client::nextRequest(PeriodicAsyncState::Ptr state)
{
    using namespace std::chrono_literals;

    if (!requesting)
        return;

    std::string emptyRequest;
    net.waitWhileBusy(*(client));
    client->asyncCall(
        emptyRequest, state->host, state->self->port, state->retryInterval,
        [state](const auto & error, const auto & responseMessage)
        {
            auto & self = state->self;
            if (error)
            {
                state->onRequestFinishedCallback(error);
                self->net.callLater([state] { state->self->nextRequest(state); });
                return;
            }
            self->processResponse(responseMessage, state->onMessageProcessedCallback);
            state->onRequestFinishedCallback(error);
            self->timer->startTimeout(
                state->requestInterval, [state] { state->self->nextRequest(state); });
        });
}

}

