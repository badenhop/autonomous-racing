//
// Created by philipp on 11.05.18.
//

#include "../include/MessageLib/ParseState.h"

namespace message
{

ParseState * ParseState::operator()(char c)
{
    for (auto & transition : transitions)
    {
        if (transition.condition(c))
        {
            transition.event(c);
            return transition.nextState;
        }
    }
    throw NoTransitionError{};
}


}