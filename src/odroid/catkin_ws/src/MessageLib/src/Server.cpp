//
// Created by philipp on 12.05.18.
//

#include "../include/MessageLib/Server.h"

namespace message
{

constexpr std::size_t Server::MAX_MESSAGE_SIZE;

Server::Server(Server::PrivateTag,
               networking::Networking & net,
               std::uint16_t port,
               std::size_t maxBufferSize)
    : net(net)
      , port(port)
      , buffer(maxBufferSize)
      , ostream(&buffer)
{
    server = networking::service::Server<StringService>::create(net, port, MAX_MESSAGE_SIZE);
}

void Server::run()
{
    auto self = shared_from_this();
    auto state = std::make_shared<AsyncState>(self);
    server->advertiseService(
        [state](const auto & clientEndpoint, auto & requestMessage, auto & responseMessage)
        {
            auto & self = state->self;
            std::lock_guard<std::mutex> lock{self->mutex};
            responseMessage = networking::utils::stringFromStreambuf(
                self->buffer, std::min(self->buffer.size(), MAX_MESSAGE_SIZE));
        });
}

void Server::stop()
{
    server->stop();
}

Server & Server::operator<<(const Message & message)
{
    std::lock_guard<std::mutex> lock{mutex};
    ostream << "{m:\"" << message.module
            << "\"k:\"" << message.key
            << "\"v:\"" << message.value << "\"}\n";
    return *this;
}


}