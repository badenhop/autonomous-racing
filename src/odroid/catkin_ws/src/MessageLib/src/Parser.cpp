//
// Created by philipp on 11.05.18.
//

#include "../include/MessageLib/Parser.h"

namespace message
{

Parser::Parser()
    : currState(&stateMessageBegin)
{
    stateMessageBegin.addTransition(stateIdentifierBegin, conditionMessageBegin, eventResetMessage);
    stateMessageBegin.addTransition(stateMessageBegin, conditionDefault, eventDoNothing);

    stateIdentifierBegin.addTransition(stateFinal, conditionMessageEnd, eventDoNothing);
    stateIdentifierBegin.addTransition(stateIdentifier, conditionLetterOrDigit, eventResetIdentifier);
    stateIdentifierBegin.addTransition(stateIdentifierBegin, conditionWhitespace, eventDoNothing);
    stateIdentifierBegin.addTransition(stateMessageBegin, conditionDefault, eventDoNothing);

    stateIdentifier.addTransition(stateIdentifier, conditionLetterOrDigit, eventAccumulateIdentifier);
    stateIdentifier.addTransition(stateContentBegin, conditionColon, eventDoNothing);
    stateIdentifier.addTransition(stateIdentifierEnd, conditionWhitespace, eventDoNothing);
    stateIdentifier.addTransition(stateMessageBegin, conditionDefault, eventDoNothing);

    stateIdentifierEnd.addTransition(stateIdentifierEnd, conditionWhitespace, eventDoNothing);
    stateIdentifierEnd.addTransition(stateContentBegin, conditionColon, eventDoNothing);
    stateIdentifierEnd.addTransition(stateMessageBegin, conditionDefault, eventDoNothing);

    stateContentBegin.addTransition(stateContentBegin, conditionWhitespace, eventDoNothing);
    stateContentBegin.addTransition(stateContent, conditionQuote, eventResetContent);
    stateContentBegin.addTransition(stateMessageBegin, conditionDefault, eventDoNothing);

    stateContent.addTransition(stateEscape, conditionEscape, eventDoNothing);
    stateContent.addTransition(stateIdentifierBegin, conditionQuote, eventUpdateMessage);
    stateContent.addTransition(stateContent, conditionDefault, eventAccumulateContent);

    stateEscape.addTransition(stateEscape, conditionDefault, eventAccumulateContent);

    stateFinal.addTransition(stateFinal, conditionDefault, eventDoNothing);
}

Parser & Parser::operator<<(const std::string & input)
{
    this->input += input;
    return *this;
}

bool Parser::operator>>(Message & message)
{
    while (!input.empty())
    {
        currState = (*currState)(popChar());
        if (currState == &stateFinal)
        {
            message = Message{messageMap.at("m"), messageMap.at("k"), messageMap.at("v")};
            currState = &stateMessageBegin;
            return true;
        }
    }
    return false;
}

char Parser::popChar()
{
    char c = input[0];
    input.erase(0, 1);
    return c;
}

}