//
// Created by philipp on 05.02.18.
//

#include "../include/PC2CarLib/CommandReceiver.h"

namespace pc2car
{

CommandReceiver::Ptr
CommandReceiver::create(networking::Networking & net,
                        float defaultSpeed)
{
    return std::make_shared<CommandReceiver>(
        PrivateTag{},
        net,
        defaultSpeed);
}

CommandReceiver::CommandReceiver(CommandReceiver::PrivateTag,
                                 networking::Networking & net,
                                 float defaultSpeed)
    : speed(defaultSpeed)
{
    receiver = DatagramReceiver::create(net, PORT, 50);
}

void CommandReceiver::receiveCommands(const CommandReceiver::OnCommandReceivedCallback & handler)
{
    this->handler = handler;
    receiveCommand();
}

void CommandReceiver::stop()
{
    receiver->stop();
}

void CommandReceiver::receiveCommand()
{
    auto self = shared_from_this();
    receiver->asyncReceive(
        networking::time::Duration::max(),
        [self](const auto & error, const auto & message, const auto & host, auto port)
        {
            if (error == networking::error::codes::ABORTED)
            {
                return;
            }
            else if (error)
            {
                self->receiveCommand();
                return;
            }

            try
            {
                using json = nlohmann::json;
                json j = json::parse(message);
                auto code = j.at(jsonNames::COMMAND).get<CommandCode>();
                self->setValue(code, j);
                self->handler(code);
            }
            catch (...)
            { /* Message invalid. */ }

            self->receiveCommand();
        });
}

void CommandReceiver::setValue(CommandCode code, nlohmann::json & j)
{
    using namespace commandCodes;
    auto & jsonVal = j.at(jsonNames::VALUE);

    switch (code)
    {
        case SET_SPEED:
            speed = jsonVal.get<float>();
            break;
    }
}


}