//
// Created by philipp on 22.01.18.
//

#ifndef PC2CAR_COMMAND_SENDER_H
#define PC2CAR_COMMAND_SENDER_H

class Client;

#include "PC2Car.h"
#include "NetworkingLib/Networking.h"
#include "NetworkingLib/DatagramSender.h"
#include "NetworkingLib/Error.h"
#include "json.hpp"

namespace pc2car
{

class CommandSender
{
public:
    using DatagramSender = networking::message::DatagramSender<std::string>;

    CommandSender(networking::Networking & net, const std::string & host)
        : host(host)
          , sender(DatagramSender::create(net))
    {}

    CommandSender(const CommandSender &) = delete;

    CommandSender & operator=(const CommandSender &) = delete;

    void setHost(const std::string & host)
    { this->host = host; }

    void setSpeed(float value)
    { send(commandCodes::SET_SPEED, value); }

private:
    std::string host;
    DatagramSender::Ptr sender;

    template<typename T>
    void send(CommandCode code, T value)
    {
        using json = nlohmann::json;
        using namespace jsonNames;
        auto j = json{{COMMAND, code},
                      {VALUE,   value}};

        sender->send(j.dump(), host, PORT, TIMEOUT);
    }
};

}

#endif // PC2CAR_COMMAND_SENDER_H
