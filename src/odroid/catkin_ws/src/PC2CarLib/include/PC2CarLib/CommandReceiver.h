//
// Created by philipp on 05.02.18.
//

#ifndef PC2CAR_COMMANDRECEIVER_H
#define PC2CAR_COMMANDRECEIVER_H

#include "PC2Car.h"
#include "NetworkingLib/DatagramReceiver.h"
#include "NetworkingLib/TimedValue.h"
#include "json.hpp"

namespace pc2car
{

class CommandReceiver : public std::enable_shared_from_this<CommandReceiver>
{
private:
    struct PrivateTag
    {
    };

public:
    using Ptr = std::shared_ptr<CommandReceiver>;
    using DatagramReceiver = networking::message::DatagramReceiver<std::string>;
    using OnCommandReceivedCallback = std::function<void(CommandCode)>;

    static Ptr create(
        networking::Networking & net,
        float defaultSpeed = 0.0f);

    CommandReceiver(PrivateTag,
                    networking::Networking & net,
                    float defaultSpeed);

    void receiveCommands(const OnCommandReceivedCallback & handler);

    void stop();

    networking::time::TimedValue<float> getSpeed()
    { return speed.getNonAtomicCopy(); }

private:
    DatagramReceiver::Ptr receiver;
    OnCommandReceivedCallback handler;

    networking::time::TimedAtomicValue<float> speed{false};

    void receiveCommand();

    void setValue(CommandCode code, nlohmann::json & j);
};

}

#endif //PC2CAR_COMMANDRECEIVER_H
