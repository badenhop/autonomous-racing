//
// Created by philipp on 05.02.18.
//

#ifndef PC2CAR_PC2CAR_H
#define PC2CAR_PC2CAR_H

#include <NetworkingLib/Time.h>

namespace pc2car
{

constexpr std::uint16_t PORT = 10100;
constexpr networking::time::Duration TIMEOUT = std::chrono::seconds(3);

using CommandCode = std::uint32_t;

namespace commandCodes
{

constexpr CommandCode ENABLE_LOGGING = 0x00000001;
constexpr CommandCode ENABLE_PLATOON = 0x00000002;
constexpr CommandCode ENABLE_RC_MODE = 0x00000003;
constexpr CommandCode SET_PS = 0x00000004;
constexpr CommandCode SET_IPD = 0x00000005;
constexpr CommandCode SET_SPEED = 0x00000006;

}

namespace jsonNames
{

constexpr char COMMAND[] = "command";
constexpr char VALUE[] = "value";

}

}

#endif //PC2CAR_PC2CAR_H
