//
// Created by philipp on 22.01.18.
//

#include <iostream>
#include "../include/PC2CarLib/CommandSender.h"
#include "../include/PC2CarLib/CommandReceiver.h"

int main(int argc, char ** argv)
{
    using namespace pc2car;

    networking::Networking net;
    CommandSender sender{net, "127.0.0.1"};
    auto receiver = CommandReceiver::create(net);

    // TODO: implement

    return 0;
}

