#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H
namespace car
{

class MissingParameters : public std::exception
{
    virtual const char * what() const throw()
    {
        return "Found too few paramters!";
    }
};

class FileNotFound : public std::runtime_error
{
public:
    explicit FileNotFound(const std::string & filename)
        : runtime_error("File not found: " + filename)
    {}
};

}

#endif
