#ifndef ENVIRONMENT_H
#define ENVIRONMENT_H

#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <logging/MessageOStream.h>

#include "NetworkingLib/Networking.h"
#include "VeloxProtocolLib/Connection.h"

#include "car/SetAngle.h"
#include "car/SetThrottle.h"

namespace car
{

class Stm : public nodelet::Nodelet
{
public:
    void onInit() override;

    Stm();

private:
    ros::NodeHandle nh;

    ros::Publisher currSpeedPublisher;
	ros::Publisher currAnglePublisher;

	ros::Subscriber setThrottleSubscriber;
	ros::Subscriber setAngleSubscriber;

	MessageOStream messageOStream;

	networking::Networking net;
	veloxProtocol::Connection::Ptr veloxConnection;

	float throttleGain{0.0};

	void readConfig();

    void onStmDataReceived();

	void setThrottleCallback(const SetThrottle::ConstPtr & msg);

	void setAngleCallback(const SetAngle::ConstPtr & msg);
};

}
#endif
