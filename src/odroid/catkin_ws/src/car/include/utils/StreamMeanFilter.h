#ifndef STREAMMEANFILTER_H
#define STREAMMEANFILTER_H

#include <vector>

namespace car
{
class StreamMeanFilter
{
public:
    explicit StreamMeanFilter(int windowSize);

    float moveWindow(float nextValue);

private:
    int windowSize;

    int currentIndex;

    double currentMean;

    std::vector<float> currentWindow;
};
    
}
#endif
