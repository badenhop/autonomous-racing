#ifndef STREAMMEDIANFILTER_H
#define STREAMMEDIANFILTER_H

#include <vector>

namespace car
{
class StreamMedianFilter
{
public:
    explicit StreamMedianFilter(int windowSize);

    int moveWindow(int nextValue);

private:
    std::vector<int> currentWindow;

    std::vector<int> sortedIndexList;

    int currentIndex;
};
    
}
#endif
