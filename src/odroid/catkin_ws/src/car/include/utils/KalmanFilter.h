#ifndef KALMANFILTER_H
#define KALMANFILTER_H

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <vector>
#include <fstream>
#include <stdio.h>

using namespace boost::numeric::ublas;
namespace car
{
class KalmanFilter
{
public:
    KalmanFilter(vector<double> x, matrix<double> P, matrix<double> F, matrix<double> Q, matrix<double> H, matrix<double> R, double *deltaT);

    KalmanFilter(int mesDimension, matrix<double> F, double *deltaT, std::string configFilePath);

    ~KalmanFilter();
    
    vector<double> nextIteration(vector<double> mesVec);

private:
    //Notation like in Kalamanfilter <Wikipedia>
    vector<double> x;           // current state (distance, velocity)^T
    matrix<double> P;           // covariance matrix
    matrix<double> F;           // dynamic model
    matrix<double> Q;           // process error
    matrix<double> H;           // mesure vector (refers to distance)
    matrix<double> R;           // measurement error

    double *dt;                 // delta t

    std::vector<std::string> getNextFileLine(std::ifstream *file);

    void readConfigFile(std::string configFilePath);

    void predict();

    void update(vector<double> mesVec);
    
    void print(std::string name, vector<double> toPrint);

    void print(std::string name, matrix<double> toPrint);
};
}
#endif
