#ifndef ULTRASONIC_H
#define ULTRASONIC_H

#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include "boost/thread.hpp"
#include "UltrasonicSensor.h"
#include "utils/StreamMedianFilter.h"
#include <thread>
#include <logging/MessageOStream.h>
#include "NetworkingLib/Timer.h"

namespace car
{

class Ultrasonic : public nodelet::Nodelet
{
public:
    Ultrasonic();

    void onInit() override;

private:
    ros::NodeHandle nh;
    MessageOStream messageOStream;
	ros::Publisher ussDistancePublisher;
    UltrasonicSensor::Ptr sensor;
    StreamMedianFilter streamMedianFilter;
    networking::Networking net;

    static int readDevId();
};

}
#endif
