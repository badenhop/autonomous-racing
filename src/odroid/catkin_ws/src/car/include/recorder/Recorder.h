//
// Created by philipp on 20.06.18.
//

#ifndef CAR_RECORDER_H
#define CAR_RECORDER_H

#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <logging/MessageOStream.h>
#include <atomic>
#include <opencv2/opencv.hpp>
#include <opencv2/core/mat.hpp>
#include <NetworkingLib/Time.h>
#include <camera/AsyncCapture.h>
#include <mutex>

#include "car/CurrSpeed.h"
#include "car/CurrAngle.h"
#include "car/SetThrottle.h"
#include "car/SetAngle.h"
#include "car/UssDistance.h"
#include "car/StopRecording.h"

namespace car
{

class Recorder : public nodelet::Nodelet
{
public:
	static constexpr unsigned int FPS{60};
	static const networking::time::Duration FRAME_TIME;

	void onInit() override;

	Recorder();

private:
	ros::NodeHandle nh;

	ros::Subscriber currSpeedSubscriber;
	ros::Subscriber currAngleSubscriber;
	ros::Subscriber setThrottleSubscriber;
	ros::Subscriber setAngleSubscriber;
	ros::Subscriber ussDistanceSubscriber;
	ros::Subscriber stopRecordingSubscriber;

	MessageOStream messageOStream;

	std::atomic<float> currSpeed{0.0f};
	std::atomic<float> currAngle{0.0f};
	std::atomic<float> setThrottle{0.0f};
	std::atomic<float> setAngle{0.0f};
	std::atomic<float> distance{0.0f};

	std::atomic<bool> recording{true};

	AsyncCapture capture;
	cv::Mat currFrame;
	std::mutex frameCopyMutex;
	std::atomic<bool> receivedFrame{false};

	void currSpeedCallback(const CurrSpeed::ConstPtr msg);

	void currAngleCallback(const CurrAngle::ConstPtr msg);

	void setThrottleCallback(const SetThrottle::ConstPtr msg);

	void setAngleCallback(const SetAngle::ConstPtr msg);

	void ussDistanceCallback(const UssDistance::ConstPtr msg);

	void stopRecordingCallback(const StopRecording::ConstPtr msg);

	void record();

	std::string createRecordingDirectory();

	void writeCsvRecord(std::ofstream & dataFile, const std::string & imgFilename);
};

}

#endif //CAR_RECORDER_H
