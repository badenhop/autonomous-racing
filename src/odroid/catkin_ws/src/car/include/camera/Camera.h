#ifndef CAMERA_H
#define CAMERA_H

#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <thread>
#include <logging/MessageOStream.h>

#include "opencv2/opencv.hpp"

namespace car
{
class Camera : public nodelet::Nodelet
{
public:
    Camera();

    void onInit() override;

private:
    ros::NodeHandle nh;
    MessageOStream messageOStream;
};
}
#endif
