//
// Created by philipp on 21.06.18.
//

#ifndef CAR_REMOTECONTROLMESSAGE_H
#define CAR_REMOTECONTROLMESSAGE_H

#include "NetworkingLib/Message.h"
#include <boost/algorithm/string.hpp>

namespace car
{

struct RemoteControlMessage
{
	RemoteControlMessage() = default;

	RemoteControlMessage(const std::string & key, const std::string & value)
		: key(key), value(value)
	{}

	std::string key;
	std::string value;
};

}

namespace networking
{
namespace message
{

template<>
struct Decoder<car::RemoteControlMessage>
{
	void operator()(car::RemoteControlMessage & message, const std::string & data) const
	{
		std::vector<std::string> split;
		boost::split(split, data, [](char c) {return c == '=';});
		message.key = split.at(0);
		message.value = split.at(1);
	};
};

}
}

#endif //CAR_REMOTECONTROLMESSAGE_H
