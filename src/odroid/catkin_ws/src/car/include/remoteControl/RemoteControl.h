//
// Created by philipp on 20.06.18.
//

#ifndef CAR_REMOTECONTROL_H
#define CAR_REMOTECONTROL_H

#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <logging/MessageOStream.h>

#include "NetworkingLib/DatagramReceiver.h"

#include "RemoteControlMessage.h"

namespace car
{

class RemoteControl : public nodelet::Nodelet
{
public:
	void onInit() override;

	RemoteControl();

private:
	ros::NodeHandle nh;

	ros::Publisher setThrottlePublisher;
	ros::Publisher setAnglePublisher;

	MessageOStream messageOStream;

	networking::Networking net;

	networking::message::DatagramReceiver<RemoteControlMessage>::Ptr messageReceiver;

	bool controlThrottle{false};
	bool controlSteeringAngle{false};

	void receiveMessages();

	void handleMessage(const RemoteControlMessage & message);

	void readConfig();
};

}

#endif //CAR_REMOTECONTROL_H
