//
// Created by philipp on 08.05.18.
//

#ifndef CAR_LOGGINGSTREAM_H
#define CAR_LOGGINGSTREAM_H

#include <nodelet/nodelet.h>
#include <ros/ros.h>
#include <string>
#include <utility>
#include "car/LogString.h"

namespace car
{

class MessageOStream
{
public:
    using Manipulator = MessageOStream & (*)(MessageOStream &);

    explicit MessageOStream(ros::NodeHandle & nh, std::string module)
        : module(std::move(module))
          , logStringPublisher(nh.advertise<LogString>("LogString", 1))
          , currStream{&ossValue}
    {}

    template<typename T>
    friend MessageOStream & operator<<(MessageOStream & stream, const T & data)
    {
        (*stream.currStream) << data;
        return stream;
    }

    friend MessageOStream & operator<<(MessageOStream & stream, Manipulator manipulator)
    {
        return manipulator(stream);
    }

    template<typename Key, typename Value>
    MessageOStream & write(const Key & key, const Value & value)
    {
        return *this << MessageOStream::key << key
                     << MessageOStream::value << value
                     << MessageOStream::flush;
    }

    static MessageOStream & flush(MessageOStream & stream)
    {
        LogString msg;

        msg.module = stream.module;
        msg.key = stream.ossKey.str();
        msg.value = stream.ossValue.str();

        std::cout << "{ module: \"" << msg.module << "\" key: \"" << msg.key << "\" value: \"" << msg.value << "\" }\n";

        stream.logStringPublisher.publish(msg);

        stream.ossValue = std::ostringstream{};
        stream.ossKey = std::ostringstream{};
        stream.currStream = &stream.ossValue;

        return stream;
    }

    static MessageOStream & value(MessageOStream & stream)
    {
        stream.currStream = &stream.ossValue;
        return stream;
    }

    static MessageOStream & key(MessageOStream & stream)
    {
        stream.currStream = &stream.ossKey;
        return stream;
    }

private:
    std::string module;
    ros::Publisher logStringPublisher;
    std::ostringstream ossKey;
    std::ostringstream ossValue;
    std::ostringstream * currStream;
};

}

#endif //CAR_LOGGINGSTREAM_H
