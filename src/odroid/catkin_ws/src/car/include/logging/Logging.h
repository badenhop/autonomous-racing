//
// Created by philipp on 22.03.18.
//

#ifndef MODULES_LOGGING_H
#define MODULES_LOGGING_H

#include <nodelet/nodelet.h>
#include <ros/ros.h>

#include <boost/asio.hpp>

#include "car/LogString.h"
#include "car/UssDistance.h"

#include "NetworkingLib/ServiceServer.h"
#include "MessageLib/Server.h"

namespace car
{
class Logging : public nodelet::Nodelet
{
public:
    static constexpr std::size_t PORT{10207};
    static constexpr std::size_t MAX_BUFFER_SIZE{std::numeric_limits<std::size_t>::max()};

    void onInit() override;

private:
    ros::NodeHandle nh;

    networking::Networking net;
    message::Server::Ptr messageServer;

	ros::Subscriber logStringSubscriber;
	ros::Subscriber  ussDistanceSubscriber;
	// TODO: add subscribersfor remaining topics

    void logStringCallback(const LogString::ConstPtr & inMsg);

    void ussDistanceCallback(const UssDistance::ConstPtr & inMsg);
};

}

#endif //MODULES_LOGGING_H
