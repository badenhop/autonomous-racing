//
// Created by philipp on 17.08.18.
//

#include "camera/AsyncCapture.h"

namespace car
{

AsyncCapture::AsyncCapture(int frameWidth, int frameHeight, int frameRate)
	: frameWidth(frameWidth), frameHeight(frameHeight), frameRate(frameRate)
{
	if (!gst_is_initialized())
		gst_init(nullptr, nullptr);

	createPipeline();
	ensureReadyState();
}

AsyncCapture::~AsyncCapture()
{
	stop();
	gst_object_unref(pipeline);
}

AsyncCapture::AsyncCapture(AsyncCapture && other) noexcept
	: pipeline(other.pipeline)
{
	other.pipeline = nullptr;
}

AsyncCapture & AsyncCapture::operator=(AsyncCapture && other) noexcept
{
	gst_object_unref(pipeline);
	pipeline = other.pipeline;
	other.pipeline = nullptr;
	return *this;
}

void AsyncCapture::start(const AsyncCapture::OnFrameCapturedCallback & callback)
{
	onFrameCapturedCallback = callback;
	GstAppSinkCallbacks callbacks = {nullptr, nullptr, appsinkFrameCallback};
	gst_app_sink_set_callbacks(GST_APP_SINK(appsink), &callbacks, this, nullptr);

	gst_element_set_state(pipeline, GST_STATE_PLAYING);
	gst_element_get_state(pipeline, nullptr, nullptr, GST_CLOCK_TIME_NONE);
}

void AsyncCapture::stop()
{
	gst_element_set_state(pipeline, GST_STATE_NULL);
	gst_element_get_state(pipeline, nullptr, nullptr, GST_CLOCK_TIME_NONE);
}

GstFlowReturn AsyncCapture::appsinkFrameCallback(GstAppSink * appsink, gpointer data)
{
	auto this_ = static_cast<AsyncCapture *>(data);
	auto sample = gst_app_sink_pull_sample(appsink);
	bool success{false};
	if (sample)
	{
		auto buffer = gst_sample_get_buffer(sample);
		auto caps = gst_sample_get_caps(sample);
		auto structure = gst_caps_get_structure(caps, 0);
		int width, height;
		gst_structure_get_int(structure, "width", &width);
		gst_structure_get_int(structure, "height", &height);
		GstMapInfo info;
		if (gst_buffer_map(buffer, &info, GST_MAP_READ))
		{
			const auto frameData = info.data;
			// 'data' now contains a pointer to readable image data
			cv::Mat frame{height, width, CV_8UC3, frameData};
			this_->onFrameCapturedCallback(frame);
			// unmap after use
			gst_buffer_unmap(buffer, &info);
			success = true;
		}
	}
	gst_sample_unref(sample);
	if (!success)
	{
		cv::Mat emptyFrame{};
		this_->onFrameCapturedCallback(emptyFrame);
	}
	return GST_FLOW_OK;
}

void AsyncCapture::createPipeline()
{
	pipeline = gst_pipeline_new("pipeline");

	source = gst_element_factory_make("tcambin", nullptr);
	if (!source)
		throw std::runtime_error("'tcambin' could not be initialized! Check tiscamera installation");

	inputCaps = gst_element_factory_make("capsfilter", nullptr);
	std::ostringstream oss;
	oss << "video/x-bayer, width=" << frameWidth
	    << ", height=" << frameHeight
	    << ", framerate=" << frameRate << "/1";
	g_object_set(inputCaps, "caps", gst_caps_from_string(oss.str().c_str()), nullptr);

	queue = gst_element_factory_make("queue", nullptr);
	g_object_set(queue, "leaky", true, "max-size-buffers", 2, nullptr);

	bayer2rgb = gst_element_factory_make("bayer2rgb", nullptr);

	convert = gst_element_factory_make("videoconvert", nullptr);

	appsink = gst_element_factory_make("appsink", nullptr);
	g_object_set(appsink, "caps", gst_caps_from_string("video/x-raw, format=BGR"), nullptr);

	assert(pipeline && source && inputCaps && queue && bayer2rgb && convert && appsink);

	gst_bin_add_many(
		GST_BIN(pipeline),
		source, inputCaps, queue, bayer2rgb, convert, appsink, nullptr);
	auto result = gst_element_link_many(
		source, inputCaps, queue, bayer2rgb, convert, appsink, nullptr);
	assert(result);
}

void AsyncCapture::ensureReadyState()
{
	GstState state;
	if ((gst_element_get_state(source, &state, nullptr, GST_CLOCK_TIME_NONE) == GST_STATE_CHANGE_SUCCESS) &&
	    state == GST_STATE_NULL)
	{
		gst_element_set_state(source, GST_STATE_READY);
		gst_element_get_state(source, nullptr, nullptr, GST_CLOCK_TIME_NONE);
	}
}

}