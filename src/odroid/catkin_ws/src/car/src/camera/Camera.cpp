#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>

#include "camera/Camera.h"

PLUGINLIB_EXPORT_CLASS(car::Camera, nodelet::Nodelet);

namespace car
{

Camera::Camera()
    : messageOStream(nh, "Camera")
{}

void Camera::onInit()
{
    messageOStream.write("onInit", "START");

    messageOStream.write("onInit", "END");
}
}
