#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <utils/Config.h>

#include "stm/Stm.h"
#include "car/CurrAngle.h"
#include "car/CurrSpeed.h"

PLUGINLIB_EXPORT_CLASS(car::Stm, nodelet::Nodelet);

namespace car
{

Stm::Stm()
    : messageOStream(nh, "Stm")
{}

void Stm::onInit()
{
    using namespace std::chrono_literals;

    messageOStream.write("onInit", "START");

	readConfig();

    currSpeedPublisher = nh.advertise<CurrSpeed>("CurrSpeed", 1);
    currAnglePublisher = nh.advertise<CurrAngle>("CurrAngle", 1);

    setThrottleSubscriber = nh.subscribe("SetThrottle", 1, &Stm::setThrottleCallback, this);
    setAngleSubscriber = nh.subscribe("SetAngle", 1, &Stm::setAngleCallback, this);

    veloxConnection = veloxProtocol::Connection::create(net);
    veloxConnection->open(
        "/dev/ttySAC0",
        [this]
        { onStmDataReceived(); },
        [this]
        { messageOStream.write("ERROR", "UART was closed"); });

    messageOStream.write("onInit", "END");
}

void Stm::onStmDataReceived()
{
    CurrSpeed speedMsg;
    speedMsg.speed = veloxConnection->getMeasuredSpeed().get();
    currSpeedPublisher.publish(speedMsg);

    CurrAngle angleMsg;
    angleMsg.angle = veloxConnection->getMeasuredSteeringAngle().get();
    currAnglePublisher.publish(angleMsg);
}

void Stm::setThrottleCallback(const SetThrottle::ConstPtr & msg)
{
	float throttle = std::max(std::min(msg->throttle * throttleGain, 1.0f), -1.0f);
    veloxConnection->setSpeed(throttle);
}

void Stm::setAngleCallback(const SetAngle::ConstPtr & msg)
{
    veloxConnection->setSteeringAngle(msg->angle);
}

void Stm::readConfig()
{
	auto j = config::open();
	throttleGain = j.at("throttleGain");
}

}
