#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <fstream>
#include <exceptions/Exceptions.h>
#include <utils/Config.h>

#include "ultrasonic/Ultrasonic.h"

#include "car/UssDistance.h"

PLUGINLIB_EXPORT_CLASS(car::Ultrasonic, nodelet::Nodelet
);

namespace car
{

Ultrasonic::Ultrasonic()
    : messageOStream(nh, "Ultrasonic")
      , streamMedianFilter(5)
{
    sensor = UltrasonicSensor::create(net, readDevId());
}

void Ultrasonic::onInit()
{
    messageOStream.write("onInit", "START");

    ussDistancePublisher = nh.advertise<UssDistance>("UssDistance", 1);
    sensor->start(
        [this](auto distance)
         {
             UssDistance msg;
             msg.distance = streamMedianFilter.moveWindow(distance);
             msg.timestamp = ros::Time::now();
             ussDistancePublisher.publish(msg);
         });

    messageOStream.write("onInit", "END");
}

int Ultrasonic::readDevId()
{
	auto j = config::open();
	std::stringstream ss;
	ss << std::hex << j.at("ultrasonicDeviceId").get<std::string>();
	int devId;
	ss >> devId;
	return devId;
}

}
