#include "ultrasonic/UltrasonicSensor.h"
#include "wiringPiI2C.h"

#include <iostream>

constexpr int UltrasonicSensor::SRF02_SDA;
constexpr int UltrasonicSensor::SRF02_SCL;
constexpr int UltrasonicSensor::DEVICE_ADDRESS1;
constexpr int UltrasonicSensor::DEVICE_ADDRESS2;
constexpr int UltrasonicSensor::DEVICE_ADDRESS3;
constexpr char UltrasonicSensor::DEVICE[];
constexpr int UltrasonicSensor::COMMAND_REGISTER;
constexpr int UltrasonicSensor::RESULT_HIGH_BYTE;
constexpr int UltrasonicSensor::RESULT_LOW_BYTE;
constexpr int UltrasonicSensor::RANGING_MODE_CM;
constexpr int UltrasonicSensor::DELAY;
constexpr int UltrasonicSensor::MAX_DISTANCE;

UltrasonicSensor::UltrasonicSensor(PrivateTag, networking::Networking & net, int devId)
    : net(net)
      , devId(devId)
      , fd(-1)
{
    timer = networking::time::Timer::create(net);
}

void UltrasonicSensor::start(const OnDistanceUpatedCallback & onDistanceUpatedCallback)
{
    auto self = shared_from_this();
    auto state = std::make_shared<AsyncState>(self, onDistanceUpatedCallback);

    fd = wiringPiI2CSetupInterface(DEVICE, devId);
    if (fd == -1)
        throw std::runtime_error{"Sensor not found!\n"};

    startRanging();
    timer->startPeriodicTimeout(
        std::chrono::milliseconds(DELAY),
        [state] { state->self->updateSensor(state); });
}

void UltrasonicSensor::stop()
{
    timer->stop();
}

void UltrasonicSensor::updateSensor(const AsyncState::Ptr & state)
{
    state->onDistanceUpdatedCallback(readDistance());
    startRanging();
}

int UltrasonicSensor::readDistance() const
{
    int distance = wiringPiI2CReadReg8(fd, RESULT_LOW_BYTE);
    distance += wiringPiI2CReadReg8(fd, RESULT_HIGH_BYTE) << 8;
    if (distance == 0)  // 0 means distance too far
        distance = 500;
    return std::min(distance, 500);
}

void UltrasonicSensor::startRanging() const
{
    wiringPiI2CWriteReg8(fd, COMMAND_REGISTER, RANGING_MODE_CM);
}
