//
// Created by philipp on 22.03.18.
//

#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <logging/MessageOStream.h>
#include "logging/Logging.h"

PLUGINLIB_EXPORT_CLASS(car::Logging, nodelet::Nodelet);

namespace car
{

constexpr std::size_t Logging::PORT;
constexpr std::size_t Logging::MAX_BUFFER_SIZE;

void Logging::onInit()
{
    messageServer = message::Server::create(net, PORT, MAX_BUFFER_SIZE);
    messageServer->run();
    *messageServer << message::Message{"Logging", "onInit", "START"};

    logStringSubscriber = nh.subscribe("LogString", 1000, &Logging::logStringCallback, this);
    ussDistanceSubscriber = nh.subscribe("UssDistance", 1, &Logging::ussDistanceCallback, this);

    *messageServer << message::Message{"Logging", "onInit", "END"};
}

void Logging::logStringCallback(const LogString::ConstPtr & inMsg)
{
    //*messageServer << message::Message{inMsg->module, inMsg->key, inMsg->value};
}

void Logging::ussDistanceCallback(const UssDistance::ConstPtr & inMsg)
{
    //*messageServer << message::Message{"ussData", "distance", std::to_string(inMsg->distance)};
}

}