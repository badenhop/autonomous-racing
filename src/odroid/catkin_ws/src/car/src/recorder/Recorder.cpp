//
// Created by philipp on 20.06.18.
//

#include <pluginlib/class_list_macros.h>
#include <thread>
#include "recorder/Recorder.h"
#include <boost/filesystem.hpp>

PLUGINLIB_EXPORT_CLASS(car::Recorder, nodelet::Nodelet);

namespace car
{

constexpr unsigned int Recorder::FPS;
const networking::time::Duration Recorder::FRAME_TIME = std::chrono::nanoseconds(1000000000 / Recorder::FPS);

Recorder::Recorder()
	: messageOStream(nh, "recorder")
	  , capture(744, 480, 60)
{}

void Recorder::onInit()
{
	messageOStream.write("onInit", "START");

	currSpeedSubscriber = nh.subscribe("CurrSpeed", 1, &Recorder::currSpeedCallback, this);
	currAngleSubscriber = nh.subscribe("CurrAngle", 1, &Recorder::currAngleCallback, this);
	setThrottleSubscriber = nh.subscribe("SetThrottle", 1, &Recorder::setThrottleCallback, this);
	setAngleSubscriber = nh.subscribe("SetAngle", 1, &Recorder::setAngleCallback, this);
	ussDistanceSubscriber = nh.subscribe("UssDistance", 1, &Recorder::ussDistanceCallback, this);
	stopRecordingSubscriber = nh.subscribe("StopRecording", 1, &Recorder::stopRecordingCallback, this);

	capture.start(
		[&](auto & frame)
		{
			std::lock_guard<std::mutex> lock{frameCopyMutex};
			currFrame = frame.clone();
			receivedFrame = true;
		});

	std::thread recorderThread{[&] { this->record(); }};
	recorderThread.detach();

	messageOStream.write("onInit", "END");
}

void Recorder::currSpeedCallback(const CurrSpeed::ConstPtr msg)
{
	currSpeed = msg->speed;
}

void Recorder::currAngleCallback(const CurrAngle::ConstPtr msg)
{
	currAngle = msg->angle;
}

void Recorder::setThrottleCallback(const SetThrottle::ConstPtr msg)
{
	setThrottle = msg->throttle;
}

void Recorder::setAngleCallback(const SetAngle::ConstPtr msg)
{
	setAngle = msg->angle;
}

void Recorder::ussDistanceCallback(const UssDistance::ConstPtr msg)
{
	distance = msg->distance;
}

void Recorder::stopRecordingCallback(const StopRecording::ConstPtr msg)
{
	recording = false;
}

void Recorder::record()
{
	auto dataDirName = createRecordingDirectory();

	std::ostringstream oss;
	oss << dataDirName << "/data.csv";
	std::ofstream dataFile{oss.str()};

	dataFile << "currSpeed;currAngle;setThrottle;setAngle;distance;img\n";

	auto last = networking::time::now();

	for (std::size_t counter = 0; recording; counter++)
	{
		// keep constant frame rate
		auto now = networking::time::now();
		auto lastFrameTime = now - last;
		if (lastFrameTime < FRAME_TIME)
			std::this_thread::sleep_for(FRAME_TIME - lastFrameTime);
		last = networking::time::now();

		while (!receivedFrame);
		cv::Mat frame{};
		{
			std::lock_guard<std::mutex> lock{frameCopyMutex};
			frame = currFrame;
		}
		if (frame.empty())
			continue;

		// save image
		oss = std::ostringstream{};
		oss << "img-" << counter << ".jpg";
		auto imgFilename = oss.str();
		oss = std::ostringstream{};
		oss << dataDirName << "/" << imgFilename;
		auto imgPath = oss.str();
		cv::imwrite(imgPath, frame);

		writeCsvRecord(dataFile, imgFilename);
	}

	messageOStream.write("MSG", "stopped recording");
}

std::string Recorder::createRecordingDirectory()
{
	std::ostringstream oss;
	auto time = std::time(nullptr);
	oss << std::getenv("HOME") << "/.car/recordings/data-" << std::put_time(std::localtime(&time), "%Y-%m-%d %H:%M:%S");
	auto dirName = oss.str();
	boost::filesystem::path dir{dirName};
	boost::filesystem::create_directory(dir);
	return dirName;
}

void Recorder::writeCsvRecord(std::ofstream & dataFile, const std::string & imgFilename)
{
	dataFile << currSpeed.load() << ","
	         << currAngle.load() << ","
	         << setThrottle.load() << ","
	         << setAngle.load() << ","
	         << distance.load() << ","
	         << imgFilename << "\n";
}

}