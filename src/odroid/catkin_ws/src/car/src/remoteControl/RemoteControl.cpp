//
// Created by philipp on 20.06.18.
//

#include <pluginlib/class_list_macros.h>
#include <utils/Config.h>
#include "remoteControl/RemoteControl.h"
#include "car/SetThrottle.h"
#include "car/SetAngle.h"

PLUGINLIB_EXPORT_CLASS(car::RemoteControl, nodelet::Nodelet);

namespace car
{

RemoteControl::RemoteControl()
	: messageOStream(nh, "remoteControl")
{}

void RemoteControl::onInit()
{
	messageOStream.write("onInit", "START");

	readConfig();

	setThrottlePublisher = nh.advertise<SetThrottle>("SetThrottle", 1);
	setAnglePublisher = nh.advertise<SetAngle>("SetAngle", 1);

	messageReceiver = networking::message::DatagramReceiver<RemoteControlMessage>::create(net, 10288);
	receiveMessages();

	messageOStream.write("onInit", "END");
}

void RemoteControl::receiveMessages()
{
	messageReceiver->asyncReceive(
		networking::time::Duration::max(),
		[&](const auto & error, const auto & message, const auto & host, auto port)
		{
			if (error)
				return;
			this->handleMessage(message);
			this->receiveMessages();
		});
}

void RemoteControl::handleMessage(const RemoteControlMessage & message)
{
	if (controlThrottle && message.key == "speed")
	{
		SetThrottle throttleMsg;
		throttleMsg.throttle = std::stof(message.value);
		setThrottlePublisher.publish(throttleMsg);
	}
	else if (controlSteeringAngle && message.key == "angle")
	{
		SetAngle angleMsg;
		angleMsg.angle = std::stof(message.value);
		setAnglePublisher.publish(angleMsg);
	}
}

void RemoteControl::readConfig()
{
	auto j = config::open();
	if (j.count("enableThrottleRemoteControl") > 0)
		controlThrottle = j.at("enableThrottleRemoteControl").get<bool>();
	if (j.count("enableSteeringAngleRemoteControl") > 0)
		controlSteeringAngle = j.at("enableSteeringAngleRemoteControl").get<bool>();
}

}