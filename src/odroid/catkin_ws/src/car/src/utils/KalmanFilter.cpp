#include "utils/KalmanFilter.h"
#include "exceptions/Exceptions.h"
#include <iterator>

using boost::numeric::ublas::prod;
using boost::numeric::ublas::trans;

namespace car
{
KalmanFilter::KalmanFilter(vector<double> x, matrix<double> P, matrix<double> F, matrix<double> Q, matrix<double> H, matrix<double> R, double *deltaT)
    : x(x)
    , P(P)
    , F(F)
    , Q(Q)
    , H(H)
    , R(R)
    , dt(deltaT)
    {}

KalmanFilter::KalmanFilter(int mesDimension, matrix<double> F, double *deltaT, std::string configFilePath)
    : x(F.size1())
    , P(F.size1(), F.size1())
    , F(F)
    , Q(F.size1(), F.size1())
    , H(mesDimension, F.size1())
    , R(mesDimension, mesDimension)
    , dt(deltaT)
    {   
        readConfigFile(configFilePath);
    }

KalmanFilter::~KalmanFilter() {}

std::vector<std::string> KalmanFilter::getNextFileLine(std::ifstream *file)
{
    // read a line
    std::string contentLine;
    std::getline(*file, contentLine);
        
    // split this line
    std::istringstream iss(contentLine);
    std::vector<std::string> words {std::istream_iterator<std::string>(iss), {} };
    return words;
}


void KalmanFilter::predict(){
    x = prod(F, x);
    P = prod( (matrix<double>) prod(F, P), trans(F)) + Q;
}

void KalmanFilter::update(vector<double> mesVec){
    // Innovation
    vector<double> y = mesVec - prod(H, x);

    // Residualkovarianz
    matrix<double> S = prod( (matrix<double>) prod(H, P), trans(H)) + R;

    // Kalman - Gain
    // TODO MATRIX Inversion for S necessary!
    matrix<double> K = (matrix<double>) prod(P, trans(H)) * (1.0/S(0,0));;
    
    // update the predecited values
    x = x + prod(K, y);
    P = P - prod( (matrix<double>) prod(K, S), trans(K) );
}

vector<double> KalmanFilter::nextIteration(vector <double> mesVec)
{
    // TODO HACK! Funktioniert nur, wenn dt nur auf (0,1) vorkommt.
    F(0,1) = *dt;
    predict();
    update(mesVec);
    return x;
}

void KalmanFilter::print(std::string name, vector<double> toPrint)
{
    std::cout << name << std::endl;
    int rows = toPrint.size();
    int i = 0;
    while (i < rows)
    {
        std::cout << toPrint(i++) << " ";
    }
    std::cout << std::endl;
}

void KalmanFilter::print(std::string name, matrix<double> toPrint)
{
    std::cout << name << std::endl;
    int rows = toPrint.size1();
    int cols = toPrint.size2();
    int i = 0;
    int j = 0;
    while (i < rows)
    {
        while (j < cols)
        {
            std::cout << toPrint(i, j++) << " ";
        }
        std::cout << std::endl;
        j = 0;
        i++;
    }
}

void KalmanFilter::readConfigFile(std::string configFilePath)
{
    // open config file
    std::ifstream configFile;
    configFile.open(configFilePath, std::ifstream::in);
    if (!configFile.is_open())
        throw FileNotFound{configFilePath};

    // read file
    while (!configFile.eof())
    {
        std::vector<std::string> words = getNextFileLine(&configFile);
        if ( words.size() == 3 ) // this line contains parameter[at (0)] and value [at (1)]
        {
            if ( words.at(0) == "//") { continue; }
            int rows = std::stoi(words.at(1));
            int cols = std::stoi(words.at(2));
            if (words.at(0) == "x") {
                int i = 0;
                while ( i < rows )
                {
                    words = getNextFileLine(&configFile);
                    x(i++) = std::stod(words.at(0));
                }
            } else if (words.at(0) == "P"){
                int i = 0;
                while ( i < rows )
                {
                    words = getNextFileLine(&configFile);
                    int j = 0;
                    while ( j < cols )
                    {
                        P(i, j) = std::stod(words.at(j));
                        j++;
                    }
                    i++;
                }
            } else if (words.at(0) == "Q"){
                int i = 0;
                while ( i < rows )
                {
                    words = getNextFileLine(&configFile);
                    int j = 0;
                    while ( j < cols )
                    {
                        Q(i, j) = std::stod(words.at(j));
                        j++;
                    }
                    i++;
                }
            } else if (words.at(0) == "H"){
                int i = 0;
                while ( i < rows )
                {
                    words = getNextFileLine(&configFile);
                    int j = 0;
                    while ( j < cols )
                    {
                        H(i, j) = std::stod(words.at(j));
                        j++;
                    }
                    i++;
                }
            } else if (words.at(0) == "R"){
                int i = 0;
                while ( i < rows )
                {
                    words = getNextFileLine(&configFile);
                    int j = 0;
                    while ( j < cols )
                    {
                        R(i, j) = std::stod(words.at(j));
                        j++;
                    }
                    i++;
                }
            }       
        }
    }
    configFile.close();
    return;
}
}
