//
// Created by philipp on 09.07.18.
//

#include "utils/Config.h"
#include <fstream>

namespace car
{
namespace config
{

nlohmann::json open()
{
	auto userHome = std::string{std::getenv("HOME")};
	auto configPath = userHome + "/.car/config.json";
	std::ifstream file{configPath};
	nlohmann::json j;
	file >> j;
	return j;
}

}
}