#include "utils/StreamMeanFilter.h"
#include <iostream>
#include <numeric>

namespace car
{
StreamMeanFilter::StreamMeanFilter(int windowSize)
    : windowSize(windowSize)
    , currentIndex(0)
    , currentMean(0.0)
    , currentWindow(windowSize, 0)
{}

float StreamMeanFilter::moveWindow(float nextValue)
{
    currentMean += nextValue - currentWindow.at(currentIndex);
    currentWindow.at(currentIndex++) = nextValue;
    currentIndex %= windowSize;
    return currentMean / windowSize;
}

}
