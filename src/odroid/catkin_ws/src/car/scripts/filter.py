from utils import *
import cv2
import numpy as np
import argparse
from merge import mkdir
from shutil import copyfile


last_img = None


def parse_line(line, out_csv, out_index, args):
    global last_img
    items = line.split(",")
    if len(items) < 6:
        raise ValueError("Invalid csv file!")
    in_index = int(items[5][4:].split(".")[0])

    src = args.in_dir + "/img-" + str(in_index) + ".jpg"
    dest = args.out_dir + "/img-" + str(out_index) + ".jpg"

    if in_index == 0:
        out_csv.write(line)
        last_img = load_image(args.in_dir, "img-0.jpg")
        print(src + " -> " + dest)
        copyfile(src, dest)
        return out_index + 1

    curr_img = load_image(args.in_dir, "img-" + str(in_index) + ".jpg")

    if (np.array_equal(last_img, curr_img)):
        return out_index
    
    last_img = curr_img
    
    for i in range(5):
        out_csv.write(items[i])
        out_csv.write(",")

    out_csv.write("img-" + str(out_index) + ".jpg\n")
    print(src + " -> " + dest)
    copyfile(src, dest)
    return out_index + 1


def main():
    parser = argparse.ArgumentParser(description="filter equal images in training data")
    parser.add_argument("-i", help="input directory", dest="in_dir", type=str)
    parser.add_argument("-o", help="output directory", dest="out_dir", type=str)
    args = parser.parse_args()

    in_fn = args.in_dir + "/data.csv"
    with open(in_fn, "r") as in_csv:
        mkdir(args.out_dir)
        out_fn = args.out_dir + "/data.csv"
        with open(out_fn, "w") as out_csv:
            in_csv.readline()
            out_csv.write("currSpeed,currAngle,setThrottle,setAngle,distance,img\n")
            out_index = 0
            for line in in_csv:
                if line.strip():
                    out_index = parse_line(line, out_csv, out_index, args)
            print("total number of training entries: {}".format(out_index))


if __name__ == "__main__":
    main()