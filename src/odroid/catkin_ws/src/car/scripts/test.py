from utils import *
import os
from keras.models import load_model
from keras import backend as K
import numpy as np
import cv2

model = load_model(get_config_dir() + "/models/model-080.h5")
image = load_image(get_config_dir() + "/recordings/data-2018-08-11 14_11_52", "img-405.jpg")
image = apply_contrast(image, 3)
#image = cv2.flip(image, 1)
image = preprocess_image(image)

prediction = model.predict(np.array([image]), batch_size=1)
angle = postprocess_angle(float(prediction[0]))
print("prediction: {}\nangle: {}".format(prediction, angle))

image = (image + 1.0) * 127.5
image = image.astype(np.uint8)
image = cv2.cvtColor(image, cv2.COLOR_YUV2RGB)
plt.imshow(image)
plt.show()
