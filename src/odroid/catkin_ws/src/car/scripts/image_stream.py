import cv2
from threading import Thread
import gi
import numpy as np
import numexpr as ne
import datetime
from utils import *
import sys

gi.require_version("Tcam", "0.1")
gi.require_version("Gst", "1.0")

from gi.repository import Tcam, Gst, GLib


class ImageStream:
    def __init__(self, contrast, crop_top=CROP_TOP, scale_width=IMAGE_WIDTH, scale_height=IMAGE_HEIGHT, frame_rate=60):
        Gst.init(sys.argv)  # init gstreamer
        self.contrast = contrast
        self.crop_top = crop_top
        self.scale_width = scale_width
        self.scale_height = scale_height
        self.frame_rate = frame_rate
        self.frame = None
        self.running = False
        self.init_pipeline()
        self.last_timestamp = datetime.datetime.now()
        self.last_frametime = 0
    
    def init_pipeline(self):
        source = Gst.ElementFactory.make("tcambin")
        input_caps = Gst.ElementFactory.make("capsfilter")
        input_caps.set_property("caps", Gst.Caps.from_string("video/x-bayer, framerate=" + str(self.frame_rate) + "/1"))
        queue = Gst.ElementFactory.make("queue")
        queue.set_property("leaky", True)
        queue.set_property("max-size-buffers", 2)
        bayer2rgb = Gst.ElementFactory.make("bayer2rgb")
        convert = Gst.ElementFactory.make("videoconvert")
        crop = Gst.ElementFactory.make("videocrop")
        crop.set_property("top", self.crop_top)
        balance = Gst.ElementFactory.make("videobalance")
        balance.set_property("contrast", self.contrast)
        scale = Gst.ElementFactory.make("videoscale")
        output = Gst.ElementFactory.make("appsink")
        output.set_property("caps", Gst.Caps.from_string("video/x-raw, format=RGB, width=" + str(self.scale_width) + ", height=" + str(self.scale_height)))
        output.set_property("emit-signals", True)
        output.connect("new-sample", self.on_new_sample)

        self.pipeline = Gst.Pipeline.new()
        self.pipeline.add(source)
        self.pipeline.add(input_caps)
        self.pipeline.add(queue)
        self.pipeline.add(bayer2rgb)
        self.pipeline.add(convert)
        self.pipeline.add(crop)
        self.pipeline.add(balance)
        self.pipeline.add(scale)
        self.pipeline.add(output)

        source.link(input_caps)
        input_caps.link(queue)
        queue.link(bayer2rgb)
        bayer2rgb.link(convert)
        convert.link(crop)
        crop.link(balance)
        balance.link(scale)
        scale.link(output)

        self.pipeline.set_state(Gst.State.READY)
        if self.pipeline.get_state(10 * Gst.SECOND)[0] != Gst.StateChangeReturn.SUCCESS:
            raise RuntimeError("Failed to start video stream.")

    def on_new_sample(self, sink):
        sample = sink.emit("pull-sample")
        if sample:
            buf = sample.get_buffer()

            caps = sample.get_caps()
            width = caps.get_structure(0).get_value("width")
            height = caps.get_structure(0).get_value("height")

            try:
                res, mapinfo = buf.map(Gst.MapFlags.READ)
                img_array = np.asarray(bytearray(mapinfo.data), dtype=np.uint8)

                # Performance-critical section here!
                # Keep this in mind if there's any change in the preprocessing!
                frame = img_array.reshape((height, width, 3))
                frame = cv2.cvtColor(frame, cv2.COLOR_RGB2YUV)
                self.frame = ne.evaluate("frame / 127.5 - 1.0")

                now = datetime.datetime.now()
                self.last_frametime = (now - self.last_timestamp).total_seconds()
                self.last_timestamp = now

            finally:
                buf.unmap(mapinfo)

        return Gst.FlowReturn.OK

    def start(self):
        self.pipeline.set_state(Gst.State.PLAYING)
        while self.frame is None:
            pass
        self.running = True
    
    def stop(self):
        self.pipeline.set_state(Gst.State.PAUSED)
        self.pipeline.set_state(Gst.State.READY)
        self.pipeline.set_state(Gst.State.NULL)
        self.frame = None
        self.running = False
        
    def read(self):
        return self.frame