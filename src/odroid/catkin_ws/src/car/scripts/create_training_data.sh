#!/bin/bash
configDir="F:/Dokumente/car"
json="${configDir}/trainingConfigs/merge.json"
python="python"

rm -r ${configDir}/recordings/merge
rm -r ${configDir}/trainingData
${python} merge.py -i "${json}" -o "${configDir}/recordings/merge"
${python} filter.py -i "${configDir}/recordings/merge" -o "${configDir}/trainingData"
