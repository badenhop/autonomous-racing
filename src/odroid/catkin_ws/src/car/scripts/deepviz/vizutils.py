import numpy as np
from matplotlib import pyplot as plt


def show_image(image, ax, grayscale = True, title=''):    

    print(image)

    if len(image.shape) == 2 or grayscale == True:
        if len(image.shape) == 3:
            image = np.sum(np.abs(image), axis=2)
            
        vmax = np.percentile(image, 99)
        vmin = np.min(image)

        ax.imshow(image, cmap=plt.cm.gray, vmin=vmin, vmax=vmax)
    else:
        image = image + 127.5
        image = image.astype('uint8')
        
        ax.imshow(image)
    
    
def load_image(file_path):
    im = PIL.Image.open(file_path)
    im = np.asarray(im)
    
    return im - 127.5