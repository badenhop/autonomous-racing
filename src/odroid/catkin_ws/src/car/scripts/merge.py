import os
from shutil import copyfile
import json
import argparse
from utils import *


def mkdir(directory):
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print ("Error: Creating directory. " +  directory)


def parse_line(line, out_csv, in_index, out_index, in_dir, out_dir, image_processing):
    items = line.split(",")
    if len(items) < 6:
        raise ValueError("Invalid csv file!")
    true_in_index = int(items[5][4:].split(".")[0])
    if in_index != true_in_index:
        raise ValueError("Invalid csv file!")
    for i in range(5):
        out_csv.write(items[i])
        out_csv.write(",")
    out_csv.write("img-" + str(out_index) + ".jpg\n")
    src = in_dir + "/img-" + str(in_index) + ".jpg"
    dest = out_dir + "/img-" + str(out_index) + ".jpg"
    print(src + " -> " + dest)
    if image_processing.omittable():
        copyfile(src, dest)
        return
    image = load_image(in_dir, "img-" + str(in_index) + ".jpg")
    image = image_processing.process(image)
    cv2.imwrite(dest, image)


class ImageProcessing:
    def __init__(self, contrast=1.0):
        self.contrast = contrast
    
    def omittable(self):
        return self.contrast == 1.0

    def process(self, image):
        return apply_contrast(image, self.contrast)


class Sequences:
    def __init__(self, j_sequences):
        self.sequences = []
        for j_sequence in j_sequences:
            start = int(j_sequence["start"])
            end = int(j_sequence["end"])
            if "contrast" in j_sequence:
                contrast = float(j_sequence["contrast"])
            else:
                contrast = 1.0
            if start < 0 or end < 0 or end < start:
                raise ValueError("Invalid sequences!")
            self.sequences.append((start, end, ImageProcessing(contrast=contrast)))
        self.sequences.sort()
        # check for no overlapping
        last = self.sequences[0]
        for i in range(1, len(self.sequences)):
            curr = self.sequences[i]
            if curr[0] < last[1]:
                raise ValueError("Invalid sequence!")

    def __contains__(self, index):
        return self.get_sequence(index) != None
    
    def get_sequence(self, index):
        for sequence in self.sequences:
            if index >= sequence[0] and index <= sequence[1]:
                return sequence
        return None
    

def main():
    parser = argparse.ArgumentParser(description="Merge training data.")
    parser.add_argument("-i", help="input json file", dest="json_file", type=str)
    parser.add_argument("-o", help="output directory", dest="out_dir", type=str)
    args = parser.parse_args()

    with open(args.json_file, "r") as jf:
        data = json.load(jf)
        mkdir(args.out_dir)
        out_fn = args.out_dir + "/data.csv"
        with open(out_fn, "w") as out_csv:
            out_csv.write("currSpeed,currAngle,setThrottle,setAngle,distance,img\n")
            out_index = 0
            recordings = data["recordings"]
            for recording in recordings:
                in_dir = get_config_dir() + "/recordings/" + recording["directory"]
                in_fn = in_dir + "/data.csv"
                sequences = Sequences(recording["sequences"])
                with open(in_fn, "r") as in_csv:
                    # read header
                    in_csv.readline()
                    in_index = 0
                    for line in in_csv:
                        if line.strip():
                            sequence = sequences.get_sequence(in_index)
                            if sequence is not None:
                                image_processing = sequence[2]
                                parse_line(line, out_csv, in_index, out_index, in_dir, args.out_dir, image_processing)
                                out_index += 1
                            in_index += 1
            print("total number of training entries: {}".format(out_index))

if __name__ == "__main__":
    main()