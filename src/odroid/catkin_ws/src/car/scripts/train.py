import numpy as np #matrix math
#for debugging, allows for reproducible (deterministic) results 
np.random.seed(0)
import pandas as pd # data analysis toolkit - create, read, update, delete datasets
from sklearn.cross_validation import train_test_split #to split out training and testing data 
#keras is a high level wrapper on top of tensorflow (machine learning library)
#The Sequential container is a linear stack of layers
from keras.models import Sequential, Model
import keras.regularizers
#popular optimization strategy that uses gradient descent 
from keras.optimizers import Adam
#to save our model periodically as checkpoints for loading later
from keras.callbacks import ModelCheckpoint, TensorBoard
#what types of layers do we want our model to have?
from keras.layers import Lambda, Convolution2D, MaxPooling2D, Dropout, Dense, Flatten, Input, BatchNormalization
from keras.initializers import TruncatedNormal
#helper class to define input shape and generate training images given image paths & steering angles
from utils import *
#for command line arguments
import argparse
#for reading files
import os
from time import time


def load_data(args):
    """
    Load training data and split it into training and validation set
    """
    #reads CSV file into a single dataframe variable
    data_df = pd.read_csv(os.path.join(os.getcwd(), args.data_dir, 'data.csv'), usecols=["setAngle", "setThrottle", "img"])

    #yay dataframes, we can select rows and columns by their names
    #we'll store the camera images as our input data
    X = data_df['img'].values
    #and our steering commands as our output data
    y = data_df[['setThrottle', 'setAngle']].values

    #now we can split the data into a training (80), testing(20), and validation set
    #thanks scikit learn
    X_train, X_valid, y_train, y_valid = train_test_split(X, y, test_size=args.test_size, random_state=0)

    return X_train, X_valid, y_train, y_valid


def build_model_nvidia(args):
    img_in = Input(shape=INPUT_SHAPE, name='img_in')

    x = img_in
    x = BatchNormalization()(x)
    x = Convolution2D(24, (5, 5), strides=(2, 2), activation='elu')(x)
    x = Convolution2D(36, (5, 5), strides=(2, 2), activation='elu')(x)
    x = Convolution2D(48, (5, 5), strides=(2, 2), activation='elu')(x)
    x = Convolution2D(64, (3, 3), activation='elu')(x)
    x = Convolution2D(64, (3, 3), activation='elu')(x)
    
    x = Dropout(0.5)(x)
    x = Flatten()(x)

    x = Dense(100, activation="elu")(x)
    x = Dense(50, activation="elu")(x)
    x = Dense(10, activation="elu")(x)
    angle_out = Dense(1, name="angle_out")(x)

    model = Model(inputs=[img_in], outputs=[angle_out])
    model.compile(optimizer='adam',
                  loss={'angle_out': 'mean_squared_error'})
    return model


def build_model_donkey(args):
    img_in = Input(shape=INPUT_SHAPE, name='img_in')
    x = img_in
    x = BatchNormalization()(x)
    x = Convolution2D(24, (5, 5), strides=(2, 2), activation='elu')(x)
    x = Convolution2D(32, (5, 5), strides=(2, 2), activation='elu')(x)
    x = Convolution2D(64, (5, 5), strides=(2, 2), activation='elu')(x)
    x = Convolution2D(64, (3, 3), strides=(2, 2), activation='elu')(x)
    x = Convolution2D(64, (3, 3), strides=(1, 1), activation='elu')(x)

    x = Flatten(name='flattened')(x)
    x = Dense(100, activation='linear')(x)
    x = Dropout(.1)(x)
    x = Dense(50, activation='linear')(x)
    x = Dropout(.1)(x)
    # categorical output of the angle
    angle_out = Dense(1, activation='linear', name='angle_out')(x)

    # continous output of throttle
    # throttle_out = Dense(1, activation='linear', name='throttle_out')(x)

    model = Model(inputs=[img_in], outputs=[angle_out])

    model.compile(optimizer='adam',
                  loss={'angle_out': 'mean_squared_error'},
                  loss_weights={'angle_out': 0.5})

    return model


def build_model_reduced(args):
    # Bad:
    # - 3. layer: filter = (3,3) and strides = (1, 1)
    # - 1. layer num_filters = 32 and 2. layer num_filters = 48
    # - kernel_regularizer=keras.regularizers.l2(0.001) to layer 1,2,3

    img_in = Input(shape=INPUT_SHAPE, name='img_in')

    x = img_in
    x = BatchNormalization()(x)
    x = Convolution2D(24, (5, 5), strides=(2, 2), activation='elu')(x)
    x = Convolution2D(32, (5, 5), strides=(2, 2), activation='elu')(x)
    x = Convolution2D(64, (5, 5), strides=(2, 2), activation='elu')(x)

    x = Flatten(name='flattened')(x)
    x = Dense(50, activation='linear')(x)
    # categorical output of the angle
    angle_out = Dense(1, activation='linear', name='angle_out')(x)

    model = Model(inputs=[img_in], outputs=[angle_out])

    model.compile(optimizer='adam',
                  loss={'angle_out': 'mean_squared_error'},
                  loss_weights={'angle_out': 0.5})

    return model


def train_model(model, args, X_train, X_valid, y_train, y_valid):
    """
    Train the model
    """
    #Saves the model after every epoch.
    #quantity to monitor, verbosity i.e logging mode (0 or 1), 
    #if save_best_only is true the latest best model according to the quantity monitored will not be overwritten.
    #mode: one of {auto, min, max}. If save_best_only=True, the decision to overwrite the current save file is
    # made based on either the maximization or the minimization of the monitored quantity. For val_acc, 
    #this should be max, for val_loss this should be min, etc. In auto mode, the direction is automatically
    # inferred from the name of the monitored quantity.
    checkpoint = ModelCheckpoint(get_config_dir() + '/models/model-{epoch:03d}.h5',
                                 monitor='val_loss',
                                 verbose=0,
                                 save_best_only=args.save_best_only,
                                 mode='auto')

    tensorboard = TensorBoard(log_dir=get_config_dir() + "/logs/{}_{}".format(args.log_label, time()))

    #calculate the difference between expected steering angle and actual steering angle
    #square the difference
    #add up all those differences for as many data points as we have
    #divide by the number of them
    #that value is our mean squared error! this is what we want to minimize via
    #gradient descent
    model.compile(loss='mean_squared_error', optimizer=Adam(lr=args.learning_rate))

    image_cache = ImageCache(args.data_dir, args.samples_per_epoch)
    print("Start loading images into cache...")
    image_cache.load_images()
    print("Finished!")

    #Fits the model on data generated batch-by-batch by a Python generator.

    #The generator is run in parallel to the model, for efficiency. 
    #For instance, this allows you to do real-time data augmentation on images on CPU in 
    #parallel to training your model on GPU.
    #so we reshape our data into their appropriate batches and train our model simulatenously
    steps_per_epoch = args.samples_per_epoch / args.batch_size
    model.fit_generator(BatchSequence(args.data_dir, X_train, y_train, args.batch_size, True, image_cache),
                        steps_per_epoch,
                        args.nb_epoch,
                        max_queue_size=args.max_queue_size,
                        validation_data=BatchSequence(args.data_dir, X_valid, y_valid, args.batch_size, False, image_cache),
                        validation_steps=len(X_valid) / args.batch_size,
                        callbacks=[checkpoint, tensorboard],
                        verbose=1,
                        workers=args.workers)

#for command line args
def s2b(s):
    """
    Converts a string to boolean value
    """
    s = s.lower()
    return s == 'true' or s == 'yes' or s == 'y' or s == '1'


def main():
    """
    Load train/validation data set and train the model
    """
    default_training_dir = get_config_dir() + "/trainingData"
    parser = argparse.ArgumentParser(description='Behavioral Cloning Training Program')
    parser.add_argument('-d', help='data directory',        dest='data_dir',          type=str,   default=default_training_dir)
    parser.add_argument('-t', help='test size fraction',    dest='test_size',         type=float, default=0.2)
    parser.add_argument('-k', help='drop out probability',  dest='keep_prob',         type=float, default=0.5)
    parser.add_argument('-n', help='number of epochs',      dest='nb_epoch',          type=int,   default=100)
    parser.add_argument('-s', help='samples per epoch',     dest='samples_per_epoch', type=int,   default=8635)
    parser.add_argument('-b', help='batch size',            dest='batch_size',        type=int,   default=40)
    parser.add_argument('-o', help='save best models only', dest='save_best_only',    type=s2b,   default='false')
    parser.add_argument('-l', help='learning rate',         dest='learning_rate',     type=float, default=6.0e-4)
    parser.add_argument('-x', help='tensorboard log label', dest='log_label',         type=str,   default='log')
    parser.add_argument('-w', help='number of workers',     dest='workers',           type=int,   default=8)
    parser.add_argument('-q', help='max_queue_size ',       dest='max_queue_size',    type=int,   default=1)
    args = parser.parse_args()

    #print parameters
    print('-' * 30)
    print('Parameters')
    print('-' * 30)
    for key, value in vars(args).items():
        print('{:<20} := {}'.format(key, value))
    print('-' * 30)

    #load data
    data = load_data(args)
    #build model
    model = build_model_nvidia(args)
    #train model on data, it saves as model.h5 
    train_model(model, args, *data)


if __name__ == '__main__':
    main()