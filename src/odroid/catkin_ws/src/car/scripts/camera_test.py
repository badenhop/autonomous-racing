from image_stream import ImageStream
from time import sleep
import cv2
import datetime
import numpy as np
from utils import *
from os.path import expanduser
from matplotlib import pyplot as plt


def main():
    stream = ImageStream(contrast=1.0)
    stream.start()

    times = np.array([])
    home = expanduser("~")

    # try:
    #     i = 0
    #     while True:
    #         iteration_start_time = datetime.datetime.now()

    #         frame = stream.read()

    #         iteration_duration = datetime.datetime.now() - iteration_start_time
    #         times = np.append(times, iteration_duration.total_seconds())
            
    #         #cv2.imwrite(home + "/tmp/rec2/img-" + str(i) + ".jpg", frame)
    #         #sleep(0.06)

    #         i += 1
    # except KeyboardInterrupt:
    #     print "average time: %f" % np.mean(times)
    # finally:        
    #     stream.stop()

    sleep(1)
    frame = stream.read()
    frame = (frame + 1) * 127.5
    frame = frame.astype(np.uint8)
    frame = cv2.cvtColor(frame, cv2.COLOR_YUV2RGB)
    plt.imshow(frame)
    plt.show()

if __name__ == "__main__":
    main()