import sys
sys.path.insert(0, "./deepviz")

from vis.visualization import visualize_saliency, visualize_cam
from keras import activations
from utils import *
from vis.utils import utils
from keras.models import load_model
from matplotlib import pyplot as plt
import numpy as np
import cv2
from deepviz.guided_backprop import GuidedBackprop
from deepviz.visual_backprop import VisualBackprop
from deepviz.integrated_gradients import IntegratedGradients
from deepviz.saliency import GradientSaliency
from deepviz.vizutils import show_image
from keras.models import Model


# model = load_model(get_config_dir() + "/models/model-001.h5")
# layer_idx = -2
# model.layers[layer_idx].activation = activations.linear
# model = utils.apply_modifications(model)

# model.summary()

# n = 33
# cols = 5
# rows = int(n / cols)
# if n % cols > 0:
#     rows += 1
# fig, ax = plt.subplots(rows, cols)

# for idx in range(n - 1):
#     grads = visualize_saliency(model, layer_idx, filter_indices=idx, seed_input=image, backprop_modifier="guided")
#     disp_idx = idx + 1
#     ax[int(disp_idx / cols), disp_idx % cols].imshow(grads, cmap="jet")

# image = (image + 1.0) * 127.5
# image = image.astype(np.uint8)
# image = cv2.cvtColor(image, cv2.COLOR_YUV2RGB)
# ax[0][0].imshow(image)

# --- deepviz ---
# input = model.input
# output = model.layers[-1].output
# model = Model(input, output)
# model.compile(optimizer='adam',
#                   loss={'angle_out': 'mean_squared_error'},
#                   loss_weights={'angle_out': 0.5})
# backprop = GradientSaliency(model)
# mask = backprop.get_mask(image)
# print(model.output)
# ax2.imshow(mask)

images = [
    # load_image(get_config_dir() + "/trainingData", "img-3300.jpg"),
    # load_image(get_config_dir() + "/trainingData", "img-3305.jpg"),
    load_image(get_config_dir() + "/trainingData", "img-3310.jpg"),
    # load_image(get_config_dir() + "/trainingData", "img-50.jpg"),
    # load_image(get_config_dir() + "/trainingData", "img-60.jpg"),
    load_image(get_config_dir() + "/trainingData", "img-70.jpg"),
    # load_image(get_config_dir() + "/trainingData", "img-4230.jpg"),
    load_image(get_config_dir() + "/trainingData", "img-4235.jpg"),
    # load_image(get_config_dir() + "/trainingData", "img-4240.jpg"),
    load_image(get_config_dir() + "/recordings/data-2018-07-27 11_28_35", "img-250.jpg"),
    # load_image(get_config_dir() + "/recordings/data-2018-07-27 11_28_35", "img-260.jpg"),
    apply_contrast(load_image(get_config_dir() + "/recordings/data-2018-08-12 15_53_56", "img-500.jpg"), 2),
    # apply_contrast(load_image(get_config_dir() + "/recordings/data-2018-08-12 15_53_56", "img-600.jpg"), 2),
    # apply_contrast(load_image(get_config_dir() + "/recordings/data-2018-08-12 15_53_56", "img-700.jpg"), 2),
    # apply_contrast(load_image(get_config_dir() + "/recordings/data-2018-08-12 15_59_01", "img-400.jpg"), 2),
    # apply_contrast(load_image(get_config_dir() + "/recordings/data-2018-08-12 15_59_01", "img-500.jpg"), 2),
    apply_contrast(load_image(get_config_dir() + "/recordings/data-2018-08-12 15_59_01", "img-600.jpg"), 2),
]

models = [
    load_model(get_config_dir() + "/models/nvidia batchnorm lr 6.0e4/model-001.h5"),
    load_model(get_config_dir() + "/models/nvidia batchnorm lr 6.0e4/model-010.h5"),
    load_model(get_config_dir() + "/models/nvidia batchnorm lr 6.0e4/model-020.h5"),
]

fig, axes = plt.subplots(len(images) * 2, len(models))

for j, model in enumerate(models):
    layer_idx = -2
    model.layers[layer_idx].activation = activations.linear
    model = utils.apply_modifications(model)
    for i, image in enumerate(images):
        image = preprocess_image(image)
        grads = visualize_saliency(model, layer_idx, filter_indices=None, seed_input=image, backprop_modifier="guided")
        axes[(2 * i) + 1, j].imshow(grads, cmap="jet")

        image = (image + 1.0) * 127.5
        image = image.astype(np.uint8)
        image = cv2.cvtColor(image, cv2.COLOR_YUV2RGB)
        axes[2 * i, j].imshow(image)

plt.show()
