import cv2, os
import numpy as np
import numexpr as ne
import matplotlib.image as mpimg
from os.path import expanduser
import json
import keras

IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNELS = 100, 372, 3
INPUT_SHAPE = (IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNELS)
CROP_TOP = 200


def get_config_dir():
    if os.name == "nt":
        return "G:/car"
    home = expanduser("~")
    return home + "/.car"


def open_config():
    f = open(get_config_dir() + "/config.json", "r")
    return json.load(f)


def load_image(data_dir, image_file):
    """
    Load BGR images from a file
    """
    return cv2.imread(os.path.join(data_dir, image_file.strip()))


def crop(image):
    """
    Crop the image (removing the sky at the top and the car front at the bottom)
    """
    return image[CROP_TOP:, :, :]


def resize(image):
    """
    Resize the image to the input shape used by the network model
    """
    return cv2.resize(image, (IMAGE_WIDTH, IMAGE_HEIGHT), cv2.INTER_AREA)


def normalize(image):
    return ne.evaluate("image / 127.5 - 1.0")


def convert(image):
    image = image.astype(np.uint8)
    return cv2.cvtColor(image, cv2.COLOR_BGR2YUV)


def bgr2yuv(image):
    """
    Convert the image from BGR to YUV (This is what the NVIDIA model does)
    """
    return cv2.cvtColor(image, cv2.COLOR_BGR2YUV)


def random_flip(image, angle):
    """
    Randomly flipt the image left <-> right, and adjust the steering angle.
    """
    if np.random.rand() < 0.5:
        image = cv2.flip(image, 1)
        angle = -angle
    return image, angle


def apply_contrast(image, ratio):
    image = image.astype(np.float64)
    image = np.minimum(image * ratio, 255)
    return image.astype(np.uint8)


def apply_brightness(image, ratio):
    image = image.astype(np.uint8)
    hsv = cv2.cvtColor(image, cv2.COLOR_BGR2HSV)
    hsv = hsv.astype(np.float64)
    hsv[:,:,2] = np.minimum(hsv[:,:,2] * ratio, 255)
    hsv = hsv.astype(np.uint8)
    return cv2.cvtColor(hsv, cv2.COLOR_HSV2BGR)


def random_brightness(image):
    """
    Randomly adjust brightness of the image.
    """
    # HSV (Hue, Saturation, Value) is also called HSB ('B' for Brightness).
    ratio = np.random.uniform(0.5, 2.0, 1)[0]
    return apply_brightness(image, ratio)


def augument(image, angle):
    """
    Generate an augumented image.
    """
    image, angle = random_flip(image, angle)
    image = random_brightness(image)
    return image, angle


def preprocess_image(image, do_crop=True, do_resize=True, do_convert=True, do_normalize=True):
    """
    Combine all preprocess functions into one
    """
    if do_crop:
        image = crop(image)
    if do_resize:
        image = resize(image)
    if do_convert:
        image = convert(image)
    if do_normalize:
        image = normalize(image)
    return image
    

def preprocess_angle(angle):
    return angle / 30


def preprocess_throttle(throttle):
    return throttle


def postprocess_angle(angle):
    return angle * 30


def postprocess_throttle(throttle):
    return throttle


def batch_generator(data_dir, X, y, batch_size, is_training):
    """
    Generate training image give image paths and associated angles and throttle values
    """
    images = np.empty([batch_size, IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNELS])
    angles = np.empty(batch_size)
    throttles = np.empty(batch_size)

    while True:
        permutation = np.random.permutation(X.shape[0])
        permutation_idx = 0
        while permutation_idx < len(permutation):
            for batch_idx in range(batch_size):
                sample_idx = permutation[permutation_idx % len(permutation)]
                image_path = X[sample_idx]
                throttle, angle = y[sample_idx]
                image = load_image(data_dir, image_path)
                # augumentation
                if is_training and np.random.rand() < 0.6:
                    image, angle = augument(image, angle)
                # add the image and steering angle to the batch
                images[batch_idx] = preprocess_image(image)
                angles[batch_idx] = preprocess_angle(angle)
                throttles[batch_idx] = preprocess_throttle(throttle)
                permutation_idx += 1
            yield images, angles


class BatchSequence(keras.utils.Sequence):
    def __init__(self, data_dir, X, y, batch_size, is_training, image_cache):
        self.data_dir = data_dir
        self.X = X
        self.y = y
        self.batch_size = batch_size
        self.is_training = is_training
        self.image_cache = image_cache
        self.next_permutation()

    def __len__(self):
        return int(np.floor(len(self.X) / float(self.batch_size)))

    def __getitem__(self, idx):
        images = np.empty([self.batch_size, IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNELS])
        angles = np.empty(self.batch_size)

        for batch_idx in range(self.batch_size):
            sample_idx = self.permutation[idx % len(self.permutation)]
            image_path = self.X[sample_idx]
            _, angle = self.y[sample_idx]
            image_idx = int(image_path[4:].split(".")[0])
            image = self.image_cache.get_image(image_idx)
            # augumentation
            if self.is_training and np.random.rand() < 0.6:
                image, angle = augument(image, angle)
            # add the image and steering angle to the batch
            images[batch_idx] = normalize(convert(image))
            angles[batch_idx] = preprocess_angle(angle)

        return images, angles

    def on_epoch_end(self):
        self.next_permutation()
    
    def next_permutation(self):
        self.permutation = np.random.permutation(self.X.shape[0])


class ImageCache:
    def __init__(self, image_dir, size):
        self.image_dir = image_dir
        self.size = size
        self.images = np.empty([self.size, IMAGE_HEIGHT, IMAGE_WIDTH, IMAGE_CHANNELS])

    def load_images(self):
        for i in range(self.size):
            image = load_image(self.image_dir, "img-" + str(i) + ".jpg")
            image = preprocess_image(image, do_convert=False, do_normalize=False)
            self.images[i] = image
    
    def get_image(self, idx):
        return self.images[idx]