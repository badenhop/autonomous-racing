from utils import *
import numpy as np
from matplotlib import pyplot as plt
import cv2


def show(images):
    fig, ax = plt.subplots(len(images))
    for i, image in enumerate(images):
        image = (image + 1.0) * 127.5
        image = image.astype(np.uint8)
        image = cv2.cvtColor(image, cv2.COLOR_YUV2RGB)
        ax[i].imshow(image)
    plt.show()

#
# show preprocessed image
#
orig = load_image(get_config_dir() + "/recordings/data-2018-08-07 16:21:33", "img-400.jpg")
image = preprocess_image(orig)
augumented = apply_brightness(orig, 1.0)
augumented = preprocess_image(augumented)
show([image, augumented])


#
#show difference in contrast
#
# orig = load_image(get_config_dir() + "/recordings/data-2018-06-28 18:04:51", "img-400.jpg")
# image = apply_contrast(image, 3)

# orig = cv2.cvtColor(orig, cv2.COLOR_BGR2RGB)
# image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)

# fig, axes = plt.subplots(2)
# axes[0].imshow(orig)
# axes[1].imshow(image)
# plt.show()


#
# display yuv parts
#
# orig = load_image("J:/trainingData", "img-740.jpg")
# yuv = cv2.cvtColor(orig, cv2.COLOR_RGB2YUV)
# yuv = orig
# y = yuv.copy()
# y[..., 1] = y[..., 0]
# y[..., 2] = y[..., 0]
# u = yuv.copy()
# u[..., 0] = u[..., 1]
# u[..., 2] = u[..., 1]
# v = yuv.copy()
# v[..., 0] = v[..., 2]
# v[..., 1] = v[..., 2]

# fig, ax = plt.subplots(4)
# ax[0].imshow(orig)
# ax[1].imshow(y)
# ax[2].imshow(u)
# ax[3].imshow(v)
# plt.show()

