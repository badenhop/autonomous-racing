from utils import *
import os
from keras.models import load_model
from keras import backend as K
import numpy as np
import cv2
from image_stream import ImageStream
from time import sleep

j = open_config()
model = load_model(get_config_dir() + "/models/" + j["model"])
stream = ImageStream(contrast=float(j["contrast"]))
stream.start()
sleep(1)
image = stream.read()
image = np.expand_dims(image, axis=0)
prediction = model.predict(image, batch_size=1)
angle = postprocess_angle(float(prediction[0]))
print "angle: %f" % angle
stream.stop()
