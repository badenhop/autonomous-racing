#!/usr/bin/env python
import rospy
from utils import *
from keras.models import load_model
from keras import backend as K
from car.msg import CurrAngle, CurrSpeed, SetAngle, SetThrottle
from threading import Thread, Lock
import cv2
import numpy as np
import signal
import datetime
from image_stream import ImageStream


class AutonomDrive:
    def __init__(self):
        self.running = True
        self.curr_angle = 0
        self.curr_speed = 0
        self.contrast = 1.0
        self.curr_angle_sub = rospy.Subscriber("CurrAngle", CurrAngle, self.curr_angle_callback, queue_size=1)
        self.curr_speed_sub = rospy.Subscriber("CurrSpeed", CurrSpeed, self.curr_speed_callback, queue_size=1)
        self.set_angle_pub = rospy.Publisher("SetAngle", SetAngle, queue_size=1)
        self.set_throttle_pub = rospy.Publisher("SetThrottle", SetThrottle, queue_size=1)
        self.model_path = None
        self.read_config()
        self.image_stream = ImageStream(self.contrast)
        self.model = load_model(self.model_path)
        rospy.init_node("autonom_drive", log_level=rospy.INFO)
        signal.signal(signal.SIGINT, self.signal_handler)

    def read_config(self):
        config = open_config()

        if "model" not in config:
            raise AttributeError("attribute 'model' is missing in config.json")
        model_filename = str(config["model"])
        self.model_path = get_config_dir() + "/models/" + model_filename
        
        if "numTensorflowThreads" in config:
            num_threads = int(config["numTensorflowThreads"])
            K.set_session(K.tf.Session(config=K.tf.ConfigProto(intra_op_parallelism_threads=num_threads, inter_op_parallelism_threads=num_threads)))
        
        if "contrast" in config:
            self.contrast = float(config["contrast"])
       
    def signal_handler(self, sig, frame):
        self.running = False
        self.image_stream.stop()

    def curr_angle_callback(self, msg):
        self.curr_angle = msg.angle

    def curr_speed_callback(self, msg):
        self.curr_speed = msg.speed

    def publish(self, angle):
        set_angle_msg = SetAngle()
        set_angle_msg.angle = angle
        self.set_angle_pub.publish(set_angle_msg)
        
        # set_throttle_msg = SetThrottle()
        # set_throttle_msg.throttle = throttle
        # self.set_throttle_pub.publish(set_throttle_msg)

    def run(self):
        self.image_stream.start()
        while self.running:
            if not self.image_stream.running:
                break

            image = self.image_stream.read()
            image = np.expand_dims(image, axis=0)

            prediction_start_time = datetime.datetime.now()

            prediction = self.model.predict(image, batch_size=1)
            angle = postprocess_angle(float(prediction[0]))
            #throttle = postprocess_throttle(float(prediction[1]))

            self.publish(angle)

            now = datetime.datetime.now()
            prediction_duration = now - prediction_start_time
            rospy.loginfo("\nprediction duration: %f\nframe processing duration: %f\n\n", prediction_duration.total_seconds(), self.image_stream.last_frametime)

  
if __name__ == "__main__":
    try:
        drive = AutonomDrive()
        drive.run()
    except rospy.ROSInterruptException:
        pass