cmake_minimum_required(VERSION 3.5.1)
project(VeloxProtocol)

set(VeloxProtocol_VERSION_MAJOR 0)
set(VeloxProtocol_VERSION_MINOR 1)

set(CMAKE_CXX_STANDARD 14)

# configure a header file to pass some of the CMake settings
# to the source code
configure_file(
        "${PROJECT_SOURCE_DIR}/src/VeloxProtocolLibConfig.h.in"
        "${PROJECT_BINARY_DIR}/VeloxProtocolLibConfig.h")

# so we can import the config header file
include_directories("${PROJECT_BINARY_DIR}")

# compiler flags
set(CMAKE_CXX_FLAGS -pthread)

set(SOURCE_FILES
        include/VeloxProtocolLib/mavlink/checksum.h
        include/VeloxProtocolLib/mavlink/mavlink_conversions.h
        include/VeloxProtocolLib/mavlink/mavlink_get_info.h
        include/VeloxProtocolLib/mavlink/mavlink_helpers.h
        include/VeloxProtocolLib/mavlink/mavlink_sha256.h
        include/VeloxProtocolLib/mavlink/mavlink_types.h
        include/VeloxProtocolLib/mavlink/protocol.h
        include/VeloxProtocolLib/mavlink/velox/mavlink.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_carcontrol.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_cmd_request_clocksync.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_cmd_request_msg.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_cmd_request_statechange.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_error.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_heartbeat.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_odometry.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_trajectory.h
        include/VeloxProtocolLib/mavlink/velox/testsuite.h
        include/VeloxProtocolLib/mavlink/velox/velox.h
        include/VeloxProtocolLib/mavlink/velox/version.h
        include/VeloxProtocolLib/Connection.h
        src/Connection.cpp)

set(PUBLIC_HEADER_FILES
        include/VeloxProtocolLib/mavlink/checksum.h
        include/VeloxProtocolLib/mavlink/mavlink_conversions.h
        include/VeloxProtocolLib/mavlink/mavlink_get_info.h
        include/VeloxProtocolLib/mavlink/mavlink_helpers.h
        include/VeloxProtocolLib/mavlink/mavlink_sha256.h
        include/VeloxProtocolLib/mavlink/mavlink_types.h
        include/VeloxProtocolLib/mavlink/protocol.h
        include/VeloxProtocolLib/mavlink/velox/mavlink.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_carcontrol.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_cmd_request_clocksync.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_cmd_request_msg.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_cmd_request_statechange.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_error.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_heartbeat.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_odometry.h
        include/VeloxProtocolLib/mavlink/velox/mavlink_msg_trajectory.h
        include/VeloxProtocolLib/mavlink/velox/testsuite.h
        include/VeloxProtocolLib/mavlink/velox/velox.h
        include/VeloxProtocolLib/mavlink/velox/version.h
        include/VeloxProtocolLib/Connection.h
        ${CMAKE_CURRENT_BINARY_DIR}/VeloxProtocolLibConfig.h)

foreach (HEADER ${PUBLIC_HEADER_FILES})
    set(PUBLIC_HEADER_FILES_COMBINED "${PUBLIC_HEADER_FILES_COMBINED}\\;${HEADER}")
endforeach ()

add_library(VeloxProtocolLib SHARED ${SOURCE_FILES})

# Specify public header files
set_target_properties(VeloxProtocolLib PROPERTIES PUBLIC_HEADER ${PUBLIC_HEADER_FILES_COMBINED})

set(LOCAL_INSTALL_DIR ${CMAKE_CURRENT_LIST_DIR}/../../install)

set(CMAKE_PREFIX_PATH "${LOCAL_INSTALL_DIR}")

# NetworkingLib
find_package(NetworkingLib REQUIRED)
target_include_directories(VeloxProtocolLib PUBLIC ${NetworkingLib_INCLUDE_DIRS})
target_link_libraries(VeloxProtocolLib NetworkingLib)

find_package(Boost REQUIRED COMPONENTS system regex)
link_libraries(${Boost_LIBRARIES})

set(INSTALL_PACKAGE ON)
if (INSTALL_PACKAGE)
    #############################
    # Specify install directories
    #############################
    set(CMAKE_INSTALL_PREFIX "${LOCAL_INSTALL_DIR}")
    set(INSTALL_LIB_DIR lib/VeloxProtocolLib)
    set(INSTALL_INCLUDE_DIR include/VeloxProtocolLib)
    set(INSTALL_CMAKE_DIR lib/VeloxProtocolLib/CMake)

    # Make relative paths absolute (needed later on)
    foreach (p LIB INCLUDE CMAKE)
        set(var INSTALL_${p}_DIR)
        if (NOT IS_ABSOLUTE "${${var}}")
            set(${var} "${CMAKE_INSTALL_PREFIX}/${${var}}")
        endif ()
    endforeach ()

    ####################################################
    # Create the *Config.cmake and *ConfigVersion files
    ####################################################
    # Get the relative path from the cmake install directory to the include install directory
    file(RELATIVE_PATH REL_INCLUDE_DIR "${INSTALL_CMAKE_DIR}" "${INSTALL_INCLUDE_DIR}")

    # Configure *Config.cmake.in for the install tree
    # Put it inside the cmake-files directory (which is located inside the binary directory)
    set(CONF_INCLUDE_DIRS "\${VELOX_PROTOCOL_LIB_CMAKE_DIR}/${REL_INCLUDE_DIR}/..")
    configure_file(VeloxProtocolLibConfig.cmake.in
            "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/VeloxProtocolLibConfig.cmake"
            @ONLY)

    # Configure *ConfigVersion.cmake.in for both build and install tree
    # Put it inside the binary directory
    configure_file(VeloxProtocolLibConfigVersion.cmake.in
            "${PROJECT_BINARY_DIR}/VeloxProtocolLibConfigVersion.cmake" @ONLY)

    ########################
    # Install VeloxProtocolLib
    ########################
    # Install library and header files
    install(TARGETS VeloxProtocolLib
            EXPORT VeloxProtocolLibTargets
            LIBRARY DESTINATION "${INSTALL_LIB_DIR}"
            ARCHIVE DESTINATION "${INSTALL_LIB_DIR}"
            RUNTIME DESTINATION "${INSTALL_LIB_DIR}") # for windows

    install(DIRECTORY "${CMAKE_CURRENT_LIST_DIR}/include/VeloxProtocolLib"
            DESTINATION "${CMAKE_INSTALL_PREFIX}/include")

    # Install the VeloxProtocolLibConfig.cmake and VeloxProtocolLibConfigVersion.cmake
    install(FILES
            "${PROJECT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/VeloxProtocolLibConfig.cmake"
            "${PROJECT_BINARY_DIR}/VeloxProtocolLibConfigVersion.cmake"
            DESTINATION "${INSTALL_CMAKE_DIR}")

    # Install the export set for use with the install-tree
    install(EXPORT VeloxProtocolLibTargets
            DESTINATION "${INSTALL_CMAKE_DIR}")
endif ()

#######
# Test
#######
add_executable(TerminalControl ${SOURCE_FILES} ${TEST_SOURCE_FILES}
        test/TerminalControl.cpp
        ${CMAKE_CURRENT_LIST_DIR}/../car/include/ultrasonic/UltrasonicSensor.h
        ${CMAKE_CURRENT_LIST_DIR}/../car/src/ultrasonic/UltrasonicSensor.cpp
        ${CMAKE_CURRENT_LIST_DIR}/../car/include/utils/StreamMeanFilter.h
        ${CMAKE_CURRENT_LIST_DIR}/../car/src/utils/StreamMeanFilter.cpp)
add_executable(SpeedMeasure ${SOURCE_FILES} ${TEST_SOURCE_FILES} test/SpeedMeasure.cpp)

# NetworkingLib
target_include_directories(TerminalControl PUBLIC ${NetworkingLib_INCLUDE_DIRS})
target_link_libraries(TerminalControl NetworkingLib libwiringPi.so)
target_include_directories(TerminalControl PUBLIC ${CMAKE_CURRENT_LIST_DIR}/../car/include)

target_include_directories(SpeedMeasure PUBLIC ${NetworkingLib_INCLUDE_DIRS})
target_link_libraries(SpeedMeasure NetworkingLib libwiringPi.so)