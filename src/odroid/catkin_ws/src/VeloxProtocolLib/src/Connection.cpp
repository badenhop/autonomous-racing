//
// Created by philipp on 20.04.18.
//

#include "../include/VeloxProtocolLib/Connection.h"

namespace veloxProtocol
{

const Connection::Callback Connection::DEFAULT_CALLBACK = []
{};

constexpr networking::time::Duration Connection::UPDATE_INTERVAL;
constexpr networking::time::Duration Connection::HEARTBEAT_INTERVAL;

void Connection::open(const std::string & uartSerialDevicePath,
                      const Connection::Callback & onUpdatedCallback,
                      const Connection::Callback & onClosedCallback)
{
    std::lock_guard<std::mutex> lock{mutex};

    auto self = shared_from_this();
    auto state = std::make_shared<AsyncState>(self);

    this->onUpdatedCallback = onUpdatedCallback;
    this->onClosedCallback = onClosedCallback;

    port.open(uartSerialDevicePath);
    port.set_option(boost::asio::serial_port_base::baud_rate{115200});

    transmitUpdateTimer = networking::time::Timer::create(net);
    transmitUpdateTimer->startPeriodicTimeout(
        UPDATE_INTERVAL,
        [state]
        { state->self->transmitUpdate(state); });

    transmitHeartbeatTimer = networking::time::Timer::create(net);
    transmitHeartbeatTimer->startPeriodicTimeout(
        HEARTBEAT_INTERVAL,
        [state]
        { state->self->transmitHeartbeat(state); });

    receive(state);

    // We will be sending a request message once to receive odometry updates permanently.
    transmitRequest(state);
}

bool Connection::isOpen() const noexcept
{
    return isBusy() && port.is_open();
}

void Connection::close()
{
    std::lock_guard<std::mutex> lock{mutex};

    if (!port.is_open())
        return;

    boost::system::error_code ignoredError;
    port.close(ignoredError);

    transmitUpdateTimer->stop();
    transmitHeartbeatTimer->stop();

    auto self = shared_from_this();
    // Guarantee that each callback is called on the networking thread.
    net.callLater(
        [self]
        { self->onClosedCallback(); });
}

void Connection::transmitRequest(AsyncState::Ptr state)
{
    mavlink_cmd_request_msg_t request;
    request.msgid = MAVLINK_MSG_ID_ODOMETRY;
    request.active = 1;
    request.period = 1; // odometry updates will be send every 1 * 10 ms
    mavlink_message_t message;
    mavlink_msg_cmd_request_msg_pack(mavlinkSystem.sysid, mavlinkSystem.compid, &message,
                                     request.msgid, request.active, request.period);
    transmitMessage(std::move(state), message);
}

void Connection::transmitUpdate(AsyncState::Ptr state)
{
    mavlink_carcontrol_t update;
    update.timestamp = 0; // timestamp doesn't seem to matter...
    update.vehspd = transmitSpeed.load();
    update.stang = transmitSteeringAngle.load();
    mavlink_message_t message;
    mavlink_msg_carcontrol_pack(mavlinkSystem.sysid, mavlinkSystem.compid, &message,
                                update.timestamp, update.vehspd, update.stang);
    transmitMessage(std::move(state), message);
}

void Connection::transmitHeartbeat(AsyncState::Ptr state)
{
    mavlink_message_t message;
    // The heartbeat message contains only data which will be ignored basically.
    mavlink_msg_heartbeat_pack(mavlinkSystem.sysid, mavlinkSystem.compid, &message, 0);
    transmitMessage(std::move(state), message);
}

void Connection::transmitMessage(AsyncState::Ptr state, const mavlink_message_t & message)
{
    uint8_t transmitBuffer[MAVLINK_MAX_PACKET_LEN];
    auto length = mavlink_msg_to_send_buffer(transmitBuffer, &message);
    try
    { boost::asio::write(port, boost::asio::buffer(transmitBuffer, length)); }
    catch (const boost::system::system_error & error)
    { state->self->close(); }
}

void Connection::receive(AsyncState::Ptr state)
{
    boost::asio::async_read(
        port, buffer, boost::asio::transfer_at_least(1),
        [state](const auto & error, std::size_t bytesTransferred)
        {
            if (error)
            {
                state->self->close();
                return;
            }

            mavlink_message_t message;
            mavlink_status_t status;
            auto & buffer = state->self->buffer;
            std::ptrdiff_t bytesToConsume = 0;
            auto begin = boost::asio::buffers_begin(buffer.data());
            for (auto i = begin; i != boost::asio::buffers_end(buffer.data()); ++i)
            {
                if (mavlink_parse_char(0, (uint8_t) *i, &message, &status))
                {
                    bytesToConsume = (i - begin) + 1;
                    state->self->handleMessage(message);
                }
            }
            buffer.consume((std::size_t) bytesToConsume);
            state->self->receive(state);
        });
}

void Connection::handleMessage(const mavlink_message_t & message)
{
    if (message.msgid == MAVLINK_MSG_ID_ODOMETRY)
    {
        measuredSpeed = mavlink_msg_odometry_get_vehspd_odom(&message);
        measuredSteeringAngle = mavlink_msg_odometry_get_stang(&message);
        measuredLeftWheelSpeed = mavlink_msg_odometry_get_xdist_odom(&message);
        measuredRightWheelSpeed = mavlink_msg_odometry_get_ydist_odom(&message);
        onUpdatedCallback();
    }
}

}