//
// Created by philipp on 20.04.18.
//

#ifndef MAVLINK_ODROID_VELOX_H
#define MAVLINK_ODROID_VELOX_H

#include <string>
#include "NetworkingLib/Networking.h"
#include "NetworkingLib/Timer.h"
#include "NetworkingLib/TimedValue.h"
#include <boost/asio/serial_port.hpp>
#include "mavlink/velox/mavlink.h"

namespace veloxProtocol
{

class Connection
    : public std::enable_shared_from_this<Connection>
      , public networking::Busyable
{
private:
    struct PrivateTag
    {
    };

public:
    using Ptr = std::shared_ptr<Connection>;
    using Callback = std::function<void()>;

    static constexpr networking::time::Duration UPDATE_INTERVAL = std::chrono::milliseconds{10};
    static constexpr networking::time::Duration HEARTBEAT_INTERVAL = std::chrono::milliseconds{100};
    static const Callback DEFAULT_CALLBACK;

    static Ptr create(networking::Networking & net)
    { return std::make_shared<Connection>(PrivateTag{}, net); }

    Connection(PrivateTag, networking::Networking & net)
        : net(net)
          , port(net.getIoService())
    {
        mavlinkSystem.sysid = 0;
        mavlinkSystem.compid = 0;
    }

    void open(const std::string & uartSerialDevicePath,
              const Callback & onUpdatedCallback,
              const Callback & onClosedCallback);

    bool isOpen() const noexcept;

    void close();

    void setSpeed(float speed)
    { transmitSpeed = speed; }

    void setSteeringAngle(float steeringAngle)
    { transmitSteeringAngle = steeringAngle; }

    networking::time::TimedValue<float> getMeasuredSpeed()
    { return measuredSpeed.getNonAtomicCopy(); }

    networking::time::TimedValue<float> getMeasuredSteeringAngle()
    { return measuredSteeringAngle.getNonAtomicCopy(); }

    networking::time::TimedValue<float> getMeasuredLeftWheelSpeed()
    { return measuredLeftWheelSpeed.getNonAtomicCopy(); }

    networking::time::TimedValue<float> getMeasuredRightWheelSpeed()
    { return measuredRightWheelSpeed.getNonAtomicCopy(); }

private:
    struct AsyncState
    {
        using Ptr = std::shared_ptr<AsyncState>;

        AsyncState(Connection::Ptr self)
            : busyLock(*self)
              , self(self)
        {}

        networking::BusyLock busyLock;
        Connection::Ptr self;
    };

    networking::Networking & net;
    boost::asio::serial_port port;

    Callback onUpdatedCallback{DEFAULT_CALLBACK};
    Callback onClosedCallback{DEFAULT_CALLBACK};

    networking::time::Timer::Ptr transmitUpdateTimer;
    networking::time::Timer::Ptr transmitHeartbeatTimer;

    networking::time::TimedAtomicValue<float> measuredSpeed;
    networking::time::TimedAtomicValue<float> measuredSteeringAngle;
    networking::time::TimedAtomicValue<float> measuredLeftWheelSpeed;
    networking::time::TimedAtomicValue<float> measuredRightWheelSpeed;

    std::atomic<float> transmitSpeed{0.0f};
    std::atomic<float> transmitSteeringAngle{0.0f};

    boost::asio::streambuf buffer;

    mavlink_system_t mavlinkSystem;

    // We want the open() and close() operations to be synchronized but we do not want the
    // actual opening of the port to be run on the networking thread so that we have instant feedback
    // whether our operation was successful or not.
    std::mutex mutex;

    void transmitUpdate(AsyncState::Ptr state);

    void transmitHeartbeat(AsyncState::Ptr state);

    void transmitRequest(AsyncState::Ptr state);

    void transmitMessage(AsyncState::Ptr state, const mavlink_message_t & message);

    void receive(AsyncState::Ptr state);

    void handleMessage(const mavlink_message_t & message);
};

}

#endif //MAVLINK_ODROID_VELOX_H
