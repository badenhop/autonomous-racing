#pragma once
// MESSAGE CMD_REQUEST_MSG PACKING

#define MAVLINK_MSG_ID_CMD_REQUEST_MSG 100

MAVPACKED(
typedef struct __mavlink_cmd_request_msg_t {
 uint8_t msgid; /*< message ID of the requested message*/
 uint8_t active; /*< active if TRUE*/
 uint8_t period; /*< send every x * 10 ms*/
}) mavlink_cmd_request_msg_t;

#define MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN 3
#define MAVLINK_MSG_ID_CMD_REQUEST_MSG_MIN_LEN 3
#define MAVLINK_MSG_ID_100_LEN 3
#define MAVLINK_MSG_ID_100_MIN_LEN 3

#define MAVLINK_MSG_ID_CMD_REQUEST_MSG_CRC 216
#define MAVLINK_MSG_ID_100_CRC 216



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_CMD_REQUEST_MSG { \
    100, \
    "CMD_REQUEST_MSG", \
    3, \
    {  { "msgid", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_cmd_request_msg_t, msgid) }, \
         { "active", NULL, MAVLINK_TYPE_UINT8_T, 0, 1, offsetof(mavlink_cmd_request_msg_t, active) }, \
         { "period", NULL, MAVLINK_TYPE_UINT8_T, 0, 2, offsetof(mavlink_cmd_request_msg_t, period) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_CMD_REQUEST_MSG { \
    "CMD_REQUEST_MSG", \
    3, \
    {  { "msgid", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_cmd_request_msg_t, msgid) }, \
         { "active", NULL, MAVLINK_TYPE_UINT8_T, 0, 1, offsetof(mavlink_cmd_request_msg_t, active) }, \
         { "period", NULL, MAVLINK_TYPE_UINT8_T, 0, 2, offsetof(mavlink_cmd_request_msg_t, period) }, \
         } \
}
#endif

/**
 * @brief Pack a cmd_request_msg message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param msgid message ID of the requested message
 * @param active active if TRUE
 * @param period send every x * 10 ms
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cmd_request_msg_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t msgid, uint8_t active, uint8_t period)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN];
    _mav_put_uint8_t(buf, 0, msgid);
    _mav_put_uint8_t(buf, 1, active);
    _mav_put_uint8_t(buf, 2, period);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN);
#else
    mavlink_cmd_request_msg_t packet;
    packet.msgid = msgid;
    packet.active = active;
    packet.period = period;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CMD_REQUEST_MSG;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CMD_REQUEST_MSG_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_CRC);
}

/**
 * @brief Pack a cmd_request_msg message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param msgid message ID of the requested message
 * @param active active if TRUE
 * @param period send every x * 10 ms
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cmd_request_msg_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t msgid,uint8_t active,uint8_t period)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN];
    _mav_put_uint8_t(buf, 0, msgid);
    _mav_put_uint8_t(buf, 1, active);
    _mav_put_uint8_t(buf, 2, period);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN);
#else
    mavlink_cmd_request_msg_t packet;
    packet.msgid = msgid;
    packet.active = active;
    packet.period = period;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CMD_REQUEST_MSG;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CMD_REQUEST_MSG_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_CRC);
}

/**
 * @brief Encode a cmd_request_msg struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param cmd_request_msg C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cmd_request_msg_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_cmd_request_msg_t* cmd_request_msg)
{
    return mavlink_msg_cmd_request_msg_pack(system_id, component_id, msg, cmd_request_msg->msgid, cmd_request_msg->active, cmd_request_msg->period);
}

/**
 * @brief Encode a cmd_request_msg struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param cmd_request_msg C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cmd_request_msg_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_cmd_request_msg_t* cmd_request_msg)
{
    return mavlink_msg_cmd_request_msg_pack_chan(system_id, component_id, chan, msg, cmd_request_msg->msgid, cmd_request_msg->active, cmd_request_msg->period);
}

/**
 * @brief Send a cmd_request_msg message
 * @param chan MAVLink channel to send the message
 *
 * @param msgid message ID of the requested message
 * @param active active if TRUE
 * @param period send every x * 10 ms
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_cmd_request_msg_send(mavlink_channel_t chan, uint8_t msgid, uint8_t active, uint8_t period)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN];
    _mav_put_uint8_t(buf, 0, msgid);
    _mav_put_uint8_t(buf, 1, active);
    _mav_put_uint8_t(buf, 2, period);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_MSG, buf, MAVLINK_MSG_ID_CMD_REQUEST_MSG_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_CRC);
#else
    mavlink_cmd_request_msg_t packet;
    packet.msgid = msgid;
    packet.active = active;
    packet.period = period;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_MSG, (const char *)&packet, MAVLINK_MSG_ID_CMD_REQUEST_MSG_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_CRC);
#endif
}

/**
 * @brief Send a cmd_request_msg message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_cmd_request_msg_send_struct(mavlink_channel_t chan, const mavlink_cmd_request_msg_t* cmd_request_msg)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_cmd_request_msg_send(chan, cmd_request_msg->msgid, cmd_request_msg->active, cmd_request_msg->period);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_MSG, (const char *)cmd_request_msg, MAVLINK_MSG_ID_CMD_REQUEST_MSG_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_CRC);
#endif
}

#if MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_cmd_request_msg_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t msgid, uint8_t active, uint8_t period)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint8_t(buf, 0, msgid);
    _mav_put_uint8_t(buf, 1, active);
    _mav_put_uint8_t(buf, 2, period);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_MSG, buf, MAVLINK_MSG_ID_CMD_REQUEST_MSG_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_CRC);
#else
    mavlink_cmd_request_msg_t *packet = (mavlink_cmd_request_msg_t *)msgbuf;
    packet->msgid = msgid;
    packet->active = active;
    packet->period = period;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_MSG, (const char *)packet, MAVLINK_MSG_ID_CMD_REQUEST_MSG_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN, MAVLINK_MSG_ID_CMD_REQUEST_MSG_CRC);
#endif
}
#endif

#endif

// MESSAGE CMD_REQUEST_MSG UNPACKING


/**
 * @brief Get field msgid from cmd_request_msg message
 *
 * @return message ID of the requested message
 */
static inline uint8_t mavlink_msg_cmd_request_msg_get_msgid(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Get field active from cmd_request_msg message
 *
 * @return active if TRUE
 */
static inline uint8_t mavlink_msg_cmd_request_msg_get_active(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  1);
}

/**
 * @brief Get field period from cmd_request_msg message
 *
 * @return send every x * 10 ms
 */
static inline uint8_t mavlink_msg_cmd_request_msg_get_period(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  2);
}

/**
 * @brief Decode a cmd_request_msg message into a struct
 *
 * @param msg The message to decode
 * @param cmd_request_msg C-struct to decode the message contents into
 */
static inline void mavlink_msg_cmd_request_msg_decode(const mavlink_message_t* msg, mavlink_cmd_request_msg_t* cmd_request_msg)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    cmd_request_msg->msgid = mavlink_msg_cmd_request_msg_get_msgid(msg);
    cmd_request_msg->active = mavlink_msg_cmd_request_msg_get_active(msg);
    cmd_request_msg->period = mavlink_msg_cmd_request_msg_get_period(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN? msg->len : MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN;
        memset(cmd_request_msg, 0, MAVLINK_MSG_ID_CMD_REQUEST_MSG_LEN);
    memcpy(cmd_request_msg, _MAV_PAYLOAD(msg), len);
#endif
}
