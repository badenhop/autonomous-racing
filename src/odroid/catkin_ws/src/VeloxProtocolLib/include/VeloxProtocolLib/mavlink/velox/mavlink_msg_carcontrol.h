#pragma once
// MESSAGE CARCONTROL PACKING

#define MAVLINK_MSG_ID_CARCONTROL 50

MAVPACKED(
typedef struct __mavlink_carcontrol_t {
 uint32_t timestamp; /*< timestamp*/
 float vehspd; /*< vehicle speed*/
 float stang; /*< steering angle*/
}) mavlink_carcontrol_t;

#define MAVLINK_MSG_ID_CARCONTROL_LEN 12
#define MAVLINK_MSG_ID_CARCONTROL_MIN_LEN 12
#define MAVLINK_MSG_ID_50_LEN 12
#define MAVLINK_MSG_ID_50_MIN_LEN 12

#define MAVLINK_MSG_ID_CARCONTROL_CRC 202
#define MAVLINK_MSG_ID_50_CRC 202



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_CARCONTROL { \
    50, \
    "CARCONTROL", \
    3, \
    {  { "timestamp", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_carcontrol_t, timestamp) }, \
         { "vehspd", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_carcontrol_t, vehspd) }, \
         { "stang", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_carcontrol_t, stang) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_CARCONTROL { \
    "CARCONTROL", \
    3, \
    {  { "timestamp", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_carcontrol_t, timestamp) }, \
         { "vehspd", NULL, MAVLINK_TYPE_FLOAT, 0, 4, offsetof(mavlink_carcontrol_t, vehspd) }, \
         { "stang", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_carcontrol_t, stang) }, \
         } \
}
#endif

/**
 * @brief Pack a carcontrol message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param timestamp timestamp
 * @param vehspd vehicle speed
 * @param stang steering angle
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_carcontrol_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t timestamp, float vehspd, float stang)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CARCONTROL_LEN];
    _mav_put_uint32_t(buf, 0, timestamp);
    _mav_put_float(buf, 4, vehspd);
    _mav_put_float(buf, 8, stang);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CARCONTROL_LEN);
#else
    mavlink_carcontrol_t packet;
    packet.timestamp = timestamp;
    packet.vehspd = vehspd;
    packet.stang = stang;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CARCONTROL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CARCONTROL;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CARCONTROL_MIN_LEN, MAVLINK_MSG_ID_CARCONTROL_LEN, MAVLINK_MSG_ID_CARCONTROL_CRC);
}

/**
 * @brief Pack a carcontrol message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param timestamp timestamp
 * @param vehspd vehicle speed
 * @param stang steering angle
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_carcontrol_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t timestamp,float vehspd,float stang)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CARCONTROL_LEN];
    _mav_put_uint32_t(buf, 0, timestamp);
    _mav_put_float(buf, 4, vehspd);
    _mav_put_float(buf, 8, stang);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CARCONTROL_LEN);
#else
    mavlink_carcontrol_t packet;
    packet.timestamp = timestamp;
    packet.vehspd = vehspd;
    packet.stang = stang;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CARCONTROL_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CARCONTROL;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CARCONTROL_MIN_LEN, MAVLINK_MSG_ID_CARCONTROL_LEN, MAVLINK_MSG_ID_CARCONTROL_CRC);
}

/**
 * @brief Encode a carcontrol struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param carcontrol C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_carcontrol_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_carcontrol_t* carcontrol)
{
    return mavlink_msg_carcontrol_pack(system_id, component_id, msg, carcontrol->timestamp, carcontrol->vehspd, carcontrol->stang);
}

/**
 * @brief Encode a carcontrol struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param carcontrol C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_carcontrol_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_carcontrol_t* carcontrol)
{
    return mavlink_msg_carcontrol_pack_chan(system_id, component_id, chan, msg, carcontrol->timestamp, carcontrol->vehspd, carcontrol->stang);
}

/**
 * @brief Send a carcontrol message
 * @param chan MAVLink channel to send the message
 *
 * @param timestamp timestamp
 * @param vehspd vehicle speed
 * @param stang steering angle
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_carcontrol_send(mavlink_channel_t chan, uint32_t timestamp, float vehspd, float stang)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CARCONTROL_LEN];
    _mav_put_uint32_t(buf, 0, timestamp);
    _mav_put_float(buf, 4, vehspd);
    _mav_put_float(buf, 8, stang);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CARCONTROL, buf, MAVLINK_MSG_ID_CARCONTROL_MIN_LEN, MAVLINK_MSG_ID_CARCONTROL_LEN, MAVLINK_MSG_ID_CARCONTROL_CRC);
#else
    mavlink_carcontrol_t packet;
    packet.timestamp = timestamp;
    packet.vehspd = vehspd;
    packet.stang = stang;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CARCONTROL, (const char *)&packet, MAVLINK_MSG_ID_CARCONTROL_MIN_LEN, MAVLINK_MSG_ID_CARCONTROL_LEN, MAVLINK_MSG_ID_CARCONTROL_CRC);
#endif
}

/**
 * @brief Send a carcontrol message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_carcontrol_send_struct(mavlink_channel_t chan, const mavlink_carcontrol_t* carcontrol)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_carcontrol_send(chan, carcontrol->timestamp, carcontrol->vehspd, carcontrol->stang);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CARCONTROL, (const char *)carcontrol, MAVLINK_MSG_ID_CARCONTROL_MIN_LEN, MAVLINK_MSG_ID_CARCONTROL_LEN, MAVLINK_MSG_ID_CARCONTROL_CRC);
#endif
}

#if MAVLINK_MSG_ID_CARCONTROL_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_carcontrol_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t timestamp, float vehspd, float stang)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, timestamp);
    _mav_put_float(buf, 4, vehspd);
    _mav_put_float(buf, 8, stang);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CARCONTROL, buf, MAVLINK_MSG_ID_CARCONTROL_MIN_LEN, MAVLINK_MSG_ID_CARCONTROL_LEN, MAVLINK_MSG_ID_CARCONTROL_CRC);
#else
    mavlink_carcontrol_t *packet = (mavlink_carcontrol_t *)msgbuf;
    packet->timestamp = timestamp;
    packet->vehspd = vehspd;
    packet->stang = stang;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CARCONTROL, (const char *)packet, MAVLINK_MSG_ID_CARCONTROL_MIN_LEN, MAVLINK_MSG_ID_CARCONTROL_LEN, MAVLINK_MSG_ID_CARCONTROL_CRC);
#endif
}
#endif

#endif

// MESSAGE CARCONTROL UNPACKING


/**
 * @brief Get field timestamp from carcontrol message
 *
 * @return timestamp
 */
static inline uint32_t mavlink_msg_carcontrol_get_timestamp(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field vehspd from carcontrol message
 *
 * @return vehicle speed
 */
static inline float mavlink_msg_carcontrol_get_vehspd(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  4);
}

/**
 * @brief Get field stang from carcontrol message
 *
 * @return steering angle
 */
static inline float mavlink_msg_carcontrol_get_stang(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Decode a carcontrol message into a struct
 *
 * @param msg The message to decode
 * @param carcontrol C-struct to decode the message contents into
 */
static inline void mavlink_msg_carcontrol_decode(const mavlink_message_t* msg, mavlink_carcontrol_t* carcontrol)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    carcontrol->timestamp = mavlink_msg_carcontrol_get_timestamp(msg);
    carcontrol->vehspd = mavlink_msg_carcontrol_get_vehspd(msg);
    carcontrol->stang = mavlink_msg_carcontrol_get_stang(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_CARCONTROL_LEN? msg->len : MAVLINK_MSG_ID_CARCONTROL_LEN;
        memset(carcontrol, 0, MAVLINK_MSG_ID_CARCONTROL_LEN);
    memcpy(carcontrol, _MAV_PAYLOAD(msg), len);
#endif
}
