#pragma once
// MESSAGE TRAJECTORY PACKING

#define MAVLINK_MSG_ID_TRAJECTORY 51

MAVPACKED(
typedef struct __mavlink_trajectory_t {
 uint32_t timestamp; /*< timestamp*/
 int16_t x[10]; /*< Lateral error*/
 uint16_t y[10]; /*< Longitudinal error*/
 int16_t theta[10]; /*< course angle error*/
 int16_t kappa[10]; /*< ground curvature*/
 int8_t v[10]; /*< nominal speed*/
}) mavlink_trajectory_t;

#define MAVLINK_MSG_ID_TRAJECTORY_LEN 94
#define MAVLINK_MSG_ID_TRAJECTORY_MIN_LEN 94
#define MAVLINK_MSG_ID_51_LEN 94
#define MAVLINK_MSG_ID_51_MIN_LEN 94

#define MAVLINK_MSG_ID_TRAJECTORY_CRC 4
#define MAVLINK_MSG_ID_51_CRC 4

#define MAVLINK_MSG_TRAJECTORY_FIELD_X_LEN 10
#define MAVLINK_MSG_TRAJECTORY_FIELD_Y_LEN 10
#define MAVLINK_MSG_TRAJECTORY_FIELD_THETA_LEN 10
#define MAVLINK_MSG_TRAJECTORY_FIELD_KAPPA_LEN 10
#define MAVLINK_MSG_TRAJECTORY_FIELD_V_LEN 10

#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_TRAJECTORY { \
    51, \
    "TRAJECTORY", \
    6, \
    {  { "timestamp", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_trajectory_t, timestamp) }, \
         { "x", NULL, MAVLINK_TYPE_INT16_T, 10, 4, offsetof(mavlink_trajectory_t, x) }, \
         { "y", NULL, MAVLINK_TYPE_UINT16_T, 10, 24, offsetof(mavlink_trajectory_t, y) }, \
         { "theta", NULL, MAVLINK_TYPE_INT16_T, 10, 44, offsetof(mavlink_trajectory_t, theta) }, \
         { "kappa", NULL, MAVLINK_TYPE_INT16_T, 10, 64, offsetof(mavlink_trajectory_t, kappa) }, \
         { "v", NULL, MAVLINK_TYPE_INT8_T, 10, 84, offsetof(mavlink_trajectory_t, v) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_TRAJECTORY { \
    "TRAJECTORY", \
    6, \
    {  { "timestamp", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_trajectory_t, timestamp) }, \
         { "x", NULL, MAVLINK_TYPE_INT16_T, 10, 4, offsetof(mavlink_trajectory_t, x) }, \
         { "y", NULL, MAVLINK_TYPE_UINT16_T, 10, 24, offsetof(mavlink_trajectory_t, y) }, \
         { "theta", NULL, MAVLINK_TYPE_INT16_T, 10, 44, offsetof(mavlink_trajectory_t, theta) }, \
         { "kappa", NULL, MAVLINK_TYPE_INT16_T, 10, 64, offsetof(mavlink_trajectory_t, kappa) }, \
         { "v", NULL, MAVLINK_TYPE_INT8_T, 10, 84, offsetof(mavlink_trajectory_t, v) }, \
         } \
}
#endif

/**
 * @brief Pack a trajectory message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param timestamp timestamp
 * @param x Lateral error
 * @param y Longitudinal error
 * @param theta course angle error
 * @param kappa ground curvature
 * @param v nominal speed
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_trajectory_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t timestamp, const int16_t *x, const uint16_t *y, const int16_t *theta, const int16_t *kappa, const int8_t *v)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TRAJECTORY_LEN];
    _mav_put_uint32_t(buf, 0, timestamp);
    _mav_put_int16_t_array(buf, 4, x, 10);
    _mav_put_uint16_t_array(buf, 24, y, 10);
    _mav_put_int16_t_array(buf, 44, theta, 10);
    _mav_put_int16_t_array(buf, 64, kappa, 10);
    _mav_put_int8_t_array(buf, 84, v, 10);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_TRAJECTORY_LEN);
#else
    mavlink_trajectory_t packet;
    packet.timestamp = timestamp;
    mav_array_memcpy(packet.x, x, sizeof(int16_t)*10);
    mav_array_memcpy(packet.y, y, sizeof(uint16_t)*10);
    mav_array_memcpy(packet.theta, theta, sizeof(int16_t)*10);
    mav_array_memcpy(packet.kappa, kappa, sizeof(int16_t)*10);
    mav_array_memcpy(packet.v, v, sizeof(int8_t)*10);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_TRAJECTORY_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_TRAJECTORY;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_TRAJECTORY_MIN_LEN, MAVLINK_MSG_ID_TRAJECTORY_LEN, MAVLINK_MSG_ID_TRAJECTORY_CRC);
}

/**
 * @brief Pack a trajectory message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param timestamp timestamp
 * @param x Lateral error
 * @param y Longitudinal error
 * @param theta course angle error
 * @param kappa ground curvature
 * @param v nominal speed
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_trajectory_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t timestamp,const int16_t *x,const uint16_t *y,const int16_t *theta,const int16_t *kappa,const int8_t *v)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TRAJECTORY_LEN];
    _mav_put_uint32_t(buf, 0, timestamp);
    _mav_put_int16_t_array(buf, 4, x, 10);
    _mav_put_uint16_t_array(buf, 24, y, 10);
    _mav_put_int16_t_array(buf, 44, theta, 10);
    _mav_put_int16_t_array(buf, 64, kappa, 10);
    _mav_put_int8_t_array(buf, 84, v, 10);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_TRAJECTORY_LEN);
#else
    mavlink_trajectory_t packet;
    packet.timestamp = timestamp;
    mav_array_memcpy(packet.x, x, sizeof(int16_t)*10);
    mav_array_memcpy(packet.y, y, sizeof(uint16_t)*10);
    mav_array_memcpy(packet.theta, theta, sizeof(int16_t)*10);
    mav_array_memcpy(packet.kappa, kappa, sizeof(int16_t)*10);
    mav_array_memcpy(packet.v, v, sizeof(int8_t)*10);
        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_TRAJECTORY_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_TRAJECTORY;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_TRAJECTORY_MIN_LEN, MAVLINK_MSG_ID_TRAJECTORY_LEN, MAVLINK_MSG_ID_TRAJECTORY_CRC);
}

/**
 * @brief Encode a trajectory struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param trajectory C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_trajectory_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_trajectory_t* trajectory)
{
    return mavlink_msg_trajectory_pack(system_id, component_id, msg, trajectory->timestamp, trajectory->x, trajectory->y, trajectory->theta, trajectory->kappa, trajectory->v);
}

/**
 * @brief Encode a trajectory struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param trajectory C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_trajectory_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_trajectory_t* trajectory)
{
    return mavlink_msg_trajectory_pack_chan(system_id, component_id, chan, msg, trajectory->timestamp, trajectory->x, trajectory->y, trajectory->theta, trajectory->kappa, trajectory->v);
}

/**
 * @brief Send a trajectory message
 * @param chan MAVLink channel to send the message
 *
 * @param timestamp timestamp
 * @param x Lateral error
 * @param y Longitudinal error
 * @param theta course angle error
 * @param kappa ground curvature
 * @param v nominal speed
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_trajectory_send(mavlink_channel_t chan, uint32_t timestamp, const int16_t *x, const uint16_t *y, const int16_t *theta, const int16_t *kappa, const int8_t *v)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_TRAJECTORY_LEN];
    _mav_put_uint32_t(buf, 0, timestamp);
    _mav_put_int16_t_array(buf, 4, x, 10);
    _mav_put_uint16_t_array(buf, 24, y, 10);
    _mav_put_int16_t_array(buf, 44, theta, 10);
    _mav_put_int16_t_array(buf, 64, kappa, 10);
    _mav_put_int8_t_array(buf, 84, v, 10);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TRAJECTORY, buf, MAVLINK_MSG_ID_TRAJECTORY_MIN_LEN, MAVLINK_MSG_ID_TRAJECTORY_LEN, MAVLINK_MSG_ID_TRAJECTORY_CRC);
#else
    mavlink_trajectory_t packet;
    packet.timestamp = timestamp;
    mav_array_memcpy(packet.x, x, sizeof(int16_t)*10);
    mav_array_memcpy(packet.y, y, sizeof(uint16_t)*10);
    mav_array_memcpy(packet.theta, theta, sizeof(int16_t)*10);
    mav_array_memcpy(packet.kappa, kappa, sizeof(int16_t)*10);
    mav_array_memcpy(packet.v, v, sizeof(int8_t)*10);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TRAJECTORY, (const char *)&packet, MAVLINK_MSG_ID_TRAJECTORY_MIN_LEN, MAVLINK_MSG_ID_TRAJECTORY_LEN, MAVLINK_MSG_ID_TRAJECTORY_CRC);
#endif
}

/**
 * @brief Send a trajectory message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_trajectory_send_struct(mavlink_channel_t chan, const mavlink_trajectory_t* trajectory)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_trajectory_send(chan, trajectory->timestamp, trajectory->x, trajectory->y, trajectory->theta, trajectory->kappa, trajectory->v);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TRAJECTORY, (const char *)trajectory, MAVLINK_MSG_ID_TRAJECTORY_MIN_LEN, MAVLINK_MSG_ID_TRAJECTORY_LEN, MAVLINK_MSG_ID_TRAJECTORY_CRC);
#endif
}

#if MAVLINK_MSG_ID_TRAJECTORY_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_trajectory_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t timestamp, const int16_t *x, const uint16_t *y, const int16_t *theta, const int16_t *kappa, const int8_t *v)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, timestamp);
    _mav_put_int16_t_array(buf, 4, x, 10);
    _mav_put_uint16_t_array(buf, 24, y, 10);
    _mav_put_int16_t_array(buf, 44, theta, 10);
    _mav_put_int16_t_array(buf, 64, kappa, 10);
    _mav_put_int8_t_array(buf, 84, v, 10);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TRAJECTORY, buf, MAVLINK_MSG_ID_TRAJECTORY_MIN_LEN, MAVLINK_MSG_ID_TRAJECTORY_LEN, MAVLINK_MSG_ID_TRAJECTORY_CRC);
#else
    mavlink_trajectory_t *packet = (mavlink_trajectory_t *)msgbuf;
    packet->timestamp = timestamp;
    mav_array_memcpy(packet->x, x, sizeof(int16_t)*10);
    mav_array_memcpy(packet->y, y, sizeof(uint16_t)*10);
    mav_array_memcpy(packet->theta, theta, sizeof(int16_t)*10);
    mav_array_memcpy(packet->kappa, kappa, sizeof(int16_t)*10);
    mav_array_memcpy(packet->v, v, sizeof(int8_t)*10);
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_TRAJECTORY, (const char *)packet, MAVLINK_MSG_ID_TRAJECTORY_MIN_LEN, MAVLINK_MSG_ID_TRAJECTORY_LEN, MAVLINK_MSG_ID_TRAJECTORY_CRC);
#endif
}
#endif

#endif

// MESSAGE TRAJECTORY UNPACKING


/**
 * @brief Get field timestamp from trajectory message
 *
 * @return timestamp
 */
static inline uint32_t mavlink_msg_trajectory_get_timestamp(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field x from trajectory message
 *
 * @return Lateral error
 */
static inline uint16_t mavlink_msg_trajectory_get_x(const mavlink_message_t* msg, int16_t *x)
{
    return _MAV_RETURN_int16_t_array(msg, x, 10,  4);
}

/**
 * @brief Get field y from trajectory message
 *
 * @return Longitudinal error
 */
static inline uint16_t mavlink_msg_trajectory_get_y(const mavlink_message_t* msg, uint16_t *y)
{
    return _MAV_RETURN_uint16_t_array(msg, y, 10,  24);
}

/**
 * @brief Get field theta from trajectory message
 *
 * @return course angle error
 */
static inline uint16_t mavlink_msg_trajectory_get_theta(const mavlink_message_t* msg, int16_t *theta)
{
    return _MAV_RETURN_int16_t_array(msg, theta, 10,  44);
}

/**
 * @brief Get field kappa from trajectory message
 *
 * @return ground curvature
 */
static inline uint16_t mavlink_msg_trajectory_get_kappa(const mavlink_message_t* msg, int16_t *kappa)
{
    return _MAV_RETURN_int16_t_array(msg, kappa, 10,  64);
}

/**
 * @brief Get field v from trajectory message
 *
 * @return nominal speed
 */
static inline uint16_t mavlink_msg_trajectory_get_v(const mavlink_message_t* msg, int8_t *v)
{
    return _MAV_RETURN_int8_t_array(msg, v, 10,  84);
}

/**
 * @brief Decode a trajectory message into a struct
 *
 * @param msg The message to decode
 * @param trajectory C-struct to decode the message contents into
 */
static inline void mavlink_msg_trajectory_decode(const mavlink_message_t* msg, mavlink_trajectory_t* trajectory)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    trajectory->timestamp = mavlink_msg_trajectory_get_timestamp(msg);
    mavlink_msg_trajectory_get_x(msg, trajectory->x);
    mavlink_msg_trajectory_get_y(msg, trajectory->y);
    mavlink_msg_trajectory_get_theta(msg, trajectory->theta);
    mavlink_msg_trajectory_get_kappa(msg, trajectory->kappa);
    mavlink_msg_trajectory_get_v(msg, trajectory->v);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_TRAJECTORY_LEN? msg->len : MAVLINK_MSG_ID_TRAJECTORY_LEN;
        memset(trajectory, 0, MAVLINK_MSG_ID_TRAJECTORY_LEN);
    memcpy(trajectory, _MAV_PAYLOAD(msg), len);
#endif
}
