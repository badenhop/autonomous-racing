#pragma once
// MESSAGE ODOMETRY PACKING

#define MAVLINK_MSG_ID_ODOMETRY 2

MAVPACKED(
typedef struct __mavlink_odometry_t {
 uint32_t timestamp_odom; /*< timestamp odometry*/
 uint32_t timestamp_stang; /*< timestamp steering angle*/
 float vehspd_odom; /*< vehicle speed*/
 float xdist_odom; /*< x distance*/
 float ydist_odom; /*< y distance*/
 float yawangle_odom; /*< yaw angle*/
 float stang; /*< current steering angle*/
}) mavlink_odometry_t;

#define MAVLINK_MSG_ID_ODOMETRY_LEN 28
#define MAVLINK_MSG_ID_ODOMETRY_MIN_LEN 28
#define MAVLINK_MSG_ID_2_LEN 28
#define MAVLINK_MSG_ID_2_MIN_LEN 28

#define MAVLINK_MSG_ID_ODOMETRY_CRC 9
#define MAVLINK_MSG_ID_2_CRC 9



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_ODOMETRY { \
    2, \
    "ODOMETRY", \
    7, \
    {  { "timestamp_odom", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_odometry_t, timestamp_odom) }, \
         { "timestamp_stang", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_odometry_t, timestamp_stang) }, \
         { "vehspd_odom", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_odometry_t, vehspd_odom) }, \
         { "xdist_odom", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_odometry_t, xdist_odom) }, \
         { "ydist_odom", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_odometry_t, ydist_odom) }, \
         { "yawangle_odom", NULL, MAVLINK_TYPE_FLOAT, 0, 20, offsetof(mavlink_odometry_t, yawangle_odom) }, \
         { "stang", NULL, MAVLINK_TYPE_FLOAT, 0, 24, offsetof(mavlink_odometry_t, stang) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_ODOMETRY { \
    "ODOMETRY", \
    7, \
    {  { "timestamp_odom", NULL, MAVLINK_TYPE_UINT32_T, 0, 0, offsetof(mavlink_odometry_t, timestamp_odom) }, \
         { "timestamp_stang", NULL, MAVLINK_TYPE_UINT32_T, 0, 4, offsetof(mavlink_odometry_t, timestamp_stang) }, \
         { "vehspd_odom", NULL, MAVLINK_TYPE_FLOAT, 0, 8, offsetof(mavlink_odometry_t, vehspd_odom) }, \
         { "xdist_odom", NULL, MAVLINK_TYPE_FLOAT, 0, 12, offsetof(mavlink_odometry_t, xdist_odom) }, \
         { "ydist_odom", NULL, MAVLINK_TYPE_FLOAT, 0, 16, offsetof(mavlink_odometry_t, ydist_odom) }, \
         { "yawangle_odom", NULL, MAVLINK_TYPE_FLOAT, 0, 20, offsetof(mavlink_odometry_t, yawangle_odom) }, \
         { "stang", NULL, MAVLINK_TYPE_FLOAT, 0, 24, offsetof(mavlink_odometry_t, stang) }, \
         } \
}
#endif

/**
 * @brief Pack a odometry message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param timestamp_odom timestamp odometry
 * @param timestamp_stang timestamp steering angle
 * @param vehspd_odom vehicle speed
 * @param xdist_odom x distance
 * @param ydist_odom y distance
 * @param yawangle_odom yaw angle
 * @param stang current steering angle
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_odometry_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint32_t timestamp_odom, uint32_t timestamp_stang, float vehspd_odom, float xdist_odom, float ydist_odom, float yawangle_odom, float stang)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ODOMETRY_LEN];
    _mav_put_uint32_t(buf, 0, timestamp_odom);
    _mav_put_uint32_t(buf, 4, timestamp_stang);
    _mav_put_float(buf, 8, vehspd_odom);
    _mav_put_float(buf, 12, xdist_odom);
    _mav_put_float(buf, 16, ydist_odom);
    _mav_put_float(buf, 20, yawangle_odom);
    _mav_put_float(buf, 24, stang);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ODOMETRY_LEN);
#else
    mavlink_odometry_t packet;
    packet.timestamp_odom = timestamp_odom;
    packet.timestamp_stang = timestamp_stang;
    packet.vehspd_odom = vehspd_odom;
    packet.xdist_odom = xdist_odom;
    packet.ydist_odom = ydist_odom;
    packet.yawangle_odom = yawangle_odom;
    packet.stang = stang;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ODOMETRY_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ODOMETRY;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_ODOMETRY_MIN_LEN, MAVLINK_MSG_ID_ODOMETRY_LEN, MAVLINK_MSG_ID_ODOMETRY_CRC);
}

/**
 * @brief Pack a odometry message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param timestamp_odom timestamp odometry
 * @param timestamp_stang timestamp steering angle
 * @param vehspd_odom vehicle speed
 * @param xdist_odom x distance
 * @param ydist_odom y distance
 * @param yawangle_odom yaw angle
 * @param stang current steering angle
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_odometry_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint32_t timestamp_odom,uint32_t timestamp_stang,float vehspd_odom,float xdist_odom,float ydist_odom,float yawangle_odom,float stang)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ODOMETRY_LEN];
    _mav_put_uint32_t(buf, 0, timestamp_odom);
    _mav_put_uint32_t(buf, 4, timestamp_stang);
    _mav_put_float(buf, 8, vehspd_odom);
    _mav_put_float(buf, 12, xdist_odom);
    _mav_put_float(buf, 16, ydist_odom);
    _mav_put_float(buf, 20, yawangle_odom);
    _mav_put_float(buf, 24, stang);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_ODOMETRY_LEN);
#else
    mavlink_odometry_t packet;
    packet.timestamp_odom = timestamp_odom;
    packet.timestamp_stang = timestamp_stang;
    packet.vehspd_odom = vehspd_odom;
    packet.xdist_odom = xdist_odom;
    packet.ydist_odom = ydist_odom;
    packet.yawangle_odom = yawangle_odom;
    packet.stang = stang;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_ODOMETRY_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_ODOMETRY;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_ODOMETRY_MIN_LEN, MAVLINK_MSG_ID_ODOMETRY_LEN, MAVLINK_MSG_ID_ODOMETRY_CRC);
}

/**
 * @brief Encode a odometry struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param odometry C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_odometry_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_odometry_t* odometry)
{
    return mavlink_msg_odometry_pack(system_id, component_id, msg, odometry->timestamp_odom, odometry->timestamp_stang, odometry->vehspd_odom, odometry->xdist_odom, odometry->ydist_odom, odometry->yawangle_odom, odometry->stang);
}

/**
 * @brief Encode a odometry struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param odometry C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_odometry_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_odometry_t* odometry)
{
    return mavlink_msg_odometry_pack_chan(system_id, component_id, chan, msg, odometry->timestamp_odom, odometry->timestamp_stang, odometry->vehspd_odom, odometry->xdist_odom, odometry->ydist_odom, odometry->yawangle_odom, odometry->stang);
}

/**
 * @brief Send a odometry message
 * @param chan MAVLink channel to send the message
 *
 * @param timestamp_odom timestamp odometry
 * @param timestamp_stang timestamp steering angle
 * @param vehspd_odom vehicle speed
 * @param xdist_odom x distance
 * @param ydist_odom y distance
 * @param yawangle_odom yaw angle
 * @param stang current steering angle
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_odometry_send(mavlink_channel_t chan, uint32_t timestamp_odom, uint32_t timestamp_stang, float vehspd_odom, float xdist_odom, float ydist_odom, float yawangle_odom, float stang)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_ODOMETRY_LEN];
    _mav_put_uint32_t(buf, 0, timestamp_odom);
    _mav_put_uint32_t(buf, 4, timestamp_stang);
    _mav_put_float(buf, 8, vehspd_odom);
    _mav_put_float(buf, 12, xdist_odom);
    _mav_put_float(buf, 16, ydist_odom);
    _mav_put_float(buf, 20, yawangle_odom);
    _mav_put_float(buf, 24, stang);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ODOMETRY, buf, MAVLINK_MSG_ID_ODOMETRY_MIN_LEN, MAVLINK_MSG_ID_ODOMETRY_LEN, MAVLINK_MSG_ID_ODOMETRY_CRC);
#else
    mavlink_odometry_t packet;
    packet.timestamp_odom = timestamp_odom;
    packet.timestamp_stang = timestamp_stang;
    packet.vehspd_odom = vehspd_odom;
    packet.xdist_odom = xdist_odom;
    packet.ydist_odom = ydist_odom;
    packet.yawangle_odom = yawangle_odom;
    packet.stang = stang;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ODOMETRY, (const char *)&packet, MAVLINK_MSG_ID_ODOMETRY_MIN_LEN, MAVLINK_MSG_ID_ODOMETRY_LEN, MAVLINK_MSG_ID_ODOMETRY_CRC);
#endif
}

/**
 * @brief Send a odometry message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_odometry_send_struct(mavlink_channel_t chan, const mavlink_odometry_t* odometry)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_odometry_send(chan, odometry->timestamp_odom, odometry->timestamp_stang, odometry->vehspd_odom, odometry->xdist_odom, odometry->ydist_odom, odometry->yawangle_odom, odometry->stang);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ODOMETRY, (const char *)odometry, MAVLINK_MSG_ID_ODOMETRY_MIN_LEN, MAVLINK_MSG_ID_ODOMETRY_LEN, MAVLINK_MSG_ID_ODOMETRY_CRC);
#endif
}

#if MAVLINK_MSG_ID_ODOMETRY_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_odometry_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint32_t timestamp_odom, uint32_t timestamp_stang, float vehspd_odom, float xdist_odom, float ydist_odom, float yawangle_odom, float stang)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint32_t(buf, 0, timestamp_odom);
    _mav_put_uint32_t(buf, 4, timestamp_stang);
    _mav_put_float(buf, 8, vehspd_odom);
    _mav_put_float(buf, 12, xdist_odom);
    _mav_put_float(buf, 16, ydist_odom);
    _mav_put_float(buf, 20, yawangle_odom);
    _mav_put_float(buf, 24, stang);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ODOMETRY, buf, MAVLINK_MSG_ID_ODOMETRY_MIN_LEN, MAVLINK_MSG_ID_ODOMETRY_LEN, MAVLINK_MSG_ID_ODOMETRY_CRC);
#else
    mavlink_odometry_t *packet = (mavlink_odometry_t *)msgbuf;
    packet->timestamp_odom = timestamp_odom;
    packet->timestamp_stang = timestamp_stang;
    packet->vehspd_odom = vehspd_odom;
    packet->xdist_odom = xdist_odom;
    packet->ydist_odom = ydist_odom;
    packet->yawangle_odom = yawangle_odom;
    packet->stang = stang;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_ODOMETRY, (const char *)packet, MAVLINK_MSG_ID_ODOMETRY_MIN_LEN, MAVLINK_MSG_ID_ODOMETRY_LEN, MAVLINK_MSG_ID_ODOMETRY_CRC);
#endif
}
#endif

#endif

// MESSAGE ODOMETRY UNPACKING


/**
 * @brief Get field timestamp_odom from odometry message
 *
 * @return timestamp odometry
 */
static inline uint32_t mavlink_msg_odometry_get_timestamp_odom(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  0);
}

/**
 * @brief Get field timestamp_stang from odometry message
 *
 * @return timestamp steering angle
 */
static inline uint32_t mavlink_msg_odometry_get_timestamp_stang(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint32_t(msg,  4);
}

/**
 * @brief Get field vehspd_odom from odometry message
 *
 * @return vehicle speed
 */
static inline float mavlink_msg_odometry_get_vehspd_odom(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  8);
}

/**
 * @brief Get field xdist_odom from odometry message
 *
 * @return x distance
 */
static inline float mavlink_msg_odometry_get_xdist_odom(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  12);
}

/**
 * @brief Get field ydist_odom from odometry message
 *
 * @return y distance
 */
static inline float mavlink_msg_odometry_get_ydist_odom(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  16);
}

/**
 * @brief Get field yawangle_odom from odometry message
 *
 * @return yaw angle
 */
static inline float mavlink_msg_odometry_get_yawangle_odom(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  20);
}

/**
 * @brief Get field stang from odometry message
 *
 * @return current steering angle
 */
static inline float mavlink_msg_odometry_get_stang(const mavlink_message_t* msg)
{
    return _MAV_RETURN_float(msg,  24);
}

/**
 * @brief Decode a odometry message into a struct
 *
 * @param msg The message to decode
 * @param odometry C-struct to decode the message contents into
 */
static inline void mavlink_msg_odometry_decode(const mavlink_message_t* msg, mavlink_odometry_t* odometry)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    odometry->timestamp_odom = mavlink_msg_odometry_get_timestamp_odom(msg);
    odometry->timestamp_stang = mavlink_msg_odometry_get_timestamp_stang(msg);
    odometry->vehspd_odom = mavlink_msg_odometry_get_vehspd_odom(msg);
    odometry->xdist_odom = mavlink_msg_odometry_get_xdist_odom(msg);
    odometry->ydist_odom = mavlink_msg_odometry_get_ydist_odom(msg);
    odometry->yawangle_odom = mavlink_msg_odometry_get_yawangle_odom(msg);
    odometry->stang = mavlink_msg_odometry_get_stang(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_ODOMETRY_LEN? msg->len : MAVLINK_MSG_ID_ODOMETRY_LEN;
        memset(odometry, 0, MAVLINK_MSG_ID_ODOMETRY_LEN);
    memcpy(odometry, _MAV_PAYLOAD(msg), len);
#endif
}
