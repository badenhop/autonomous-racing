/** @file
 *  @brief MAVLink comm protocol generated from velox.xml
 *  @see http://mavlink.org
 */
#pragma once
#ifndef MAVLINK_VELOX_H
#define MAVLINK_VELOX_H

#ifndef MAVLINK_H
    #error Wrong include order: MAVLINK_VELOX.H MUST NOT BE DIRECTLY USED. Include mavlink.h from the same directory instead or set ALL AND EVERY defines from MAVLINK.H manually accordingly, including the #define MAVLINK_H call.
#endif

#undef MAVLINK_THIS_XML_IDX
#define MAVLINK_THIS_XML_IDX 0

#ifdef __cplusplus
extern "C" {
#endif

// MESSAGE LENGTHS AND CRCS

#ifndef MAVLINK_MESSAGE_LENGTHS
#define MAVLINK_MESSAGE_LENGTHS {}
#endif

#ifndef MAVLINK_MESSAGE_CRCS
#define MAVLINK_MESSAGE_CRCS {{0, 85, 2, 0, 0, 0}, {1, 77, 8, 0, 0, 0}, {2, 9, 28, 0, 0, 0}, {50, 202, 12, 0, 0, 0}, {51, 4, 94, 0, 0, 0}, {100, 216, 3, 0, 0, 0}, {101, 229, 1, 0, 0, 0}, {102, 119, 1, 0, 0, 0}}
#endif

#include "../protocol.h"

#define MAVLINK_ENABLED_VELOX

// ENUM DEFINITIONS


/** @brief  */
#ifndef HAVE_ENUM_MAV_COMPONENT
#define HAVE_ENUM_MAV_COMPONENT
typedef enum MAV_COMPONENT
{
   MAV_COMP_ID_STM=0, /*  | */
   MAV_COMP_ID_ADAS=1, /*  | */
   MAV_COMPONENT_ENUM_END=2, /*  | */
} MAV_COMPONENT;
#endif

/** @brief  */
#ifndef HAVE_ENUM_SYSTEM_STATE
#define HAVE_ENUM_SYSTEM_STATE
typedef enum SYSTEM_STATE
{
   SYSTEM_STATE_INITIALIZING=0, /*  | */
   SYSTEM_STATE_IDLE=1, /*  | */
   SYSTEM_STATE_RUNNING_EXT=2, /*  | */
   SYSTEM_STATE_RUNNING_RC=3, /*  | */
   SYSTEM_STATE_EMERGENCY=4, /*  | */
   SYSTEM_STATE_NO_REQUEST=255, /* workaround value for requestedState variable when no state change is requested | */
   SYSTEM_STATE_ENUM_END=256, /*  | */
} SYSTEM_STATE;
#endif

// MAVLINK VERSION

#ifndef MAVLINK_VERSION
#define MAVLINK_VERSION 1
#endif

#if (MAVLINK_VERSION == 0)
#undef MAVLINK_VERSION
#define MAVLINK_VERSION 1
#endif

// MESSAGE DEFINITIONS
#include "./mavlink_msg_heartbeat.h"
#include "./mavlink_msg_error.h"
#include "./mavlink_msg_odometry.h"
#include "./mavlink_msg_carcontrol.h"
#include "./mavlink_msg_trajectory.h"
#include "./mavlink_msg_cmd_request_msg.h"
#include "./mavlink_msg_cmd_request_statechange.h"
#include "./mavlink_msg_cmd_request_clocksync.h"

// base include


#undef MAVLINK_THIS_XML_IDX
#define MAVLINK_THIS_XML_IDX 0

#if MAVLINK_THIS_XML_IDX == MAVLINK_PRIMARY_XML_IDX
# define MAVLINK_MESSAGE_INFO {MAVLINK_MESSAGE_INFO_HEARTBEAT, MAVLINK_MESSAGE_INFO_ERROR, MAVLINK_MESSAGE_INFO_ODOMETRY, MAVLINK_MESSAGE_INFO_CARCONTROL, MAVLINK_MESSAGE_INFO_TRAJECTORY, MAVLINK_MESSAGE_INFO_CMD_REQUEST_MSG, MAVLINK_MESSAGE_INFO_CMD_REQUEST_STATECHANGE, MAVLINK_MESSAGE_INFO_CMD_REQUEST_CLOCKSYNC}
# if MAVLINK_COMMAND_24BIT
#  include "../mavlink_get_info.h"
# endif
#endif

#ifdef __cplusplus
}
#endif // __cplusplus
#endif // MAVLINK_VELOX_H
