import QtQuick 2.5
import QtQuick.Window 2.2
import QtQuick.Controls 1.2

ApplicationWindow {
    visible: true
    width: 500
    height: 250
    title: qsTr("Command Gui")

    // --- host ---
    Label {
        id: hostLabel
        text: "Host:"

        anchors.left: parent.left
        anchors.leftMargin: 30
        anchors.top: parent.top
        anchors.topMargin: 30
    }

    TextField {
        id: hostTextField
        objectName: "hostTextField"
        text: "127.0.0.1"

        anchors.left: hostLabel.right
        anchors.leftMargin: 150
        anchors.verticalCenter: hostLabel.verticalCenter
    }

    Button {
        id: setHostButton
        objectName: "setHostButton"
        text: "set"

        anchors.left: hostTextField.right
        anchors.leftMargin: 10
        anchors.verticalCenter: hostTextField.verticalCenter
    }

    // --- enablePlatoon ---
    Button {
        id: enablePlatoonButton
        objectName: "enablePlatoonButton"
        text: "Enable Platoon"

        anchors.left: hostLabel.left
        anchors.top: hostLabel.bottom
        anchors.topMargin: 20
    }

    Button {
        id: disablePlatoonButton
        objectName: "disablePlatoonButton"
        text: "Disable Platoon"

        anchors.left: enablePlatoonButton.right
        anchors.leftMargin: 15
        anchors.verticalCenter: enablePlatoonButton.verticalCenter
    }

    // --- platoonSpeed ---
    Label {
        id: platoonSpeedLabel
        text: "Platoon Speed:"

        anchors.left: hostLabel.left
        anchors.top: enablePlatoonButton.bottom
        anchors.topMargin: 20
    }

    TextField {
        id: platoonSpeedTextField
        objectName: "platoonSpeedTextField"
        text: "0"

        anchors.left: hostTextField.left
        anchors.verticalCenter: platoonSpeedLabel.verticalCenter
    }

    Button {
        id: setPlatoonSpeedButton
        objectName: "setPlatoonSpeedButton"
        text: "set"

        anchors.left: platoonSpeedTextField.right
        anchors.leftMargin: 10
        anchors.verticalCenter: platoonSpeedTextField.verticalCenter
    }

    // --- innerPlatoonDistance ---
    Label {
        id: innerPlatoonDistanceLabel
        text: "Inner Platoon Distance:"

        anchors.left: hostLabel.left
        anchors.top: platoonSpeedLabel.bottom
        anchors.topMargin: 20
    }

    TextField {
        id: innerPlatoonDistanceTextField
        objectName: "innerPlatoonDistanceTextField"
        text: "0"

        anchors.left: hostTextField.left
        anchors.verticalCenter: innerPlatoonDistanceLabel.verticalCenter
    }

    Button {
        id: setInnerPlatoonDistanceButton
        objectName: "setInnerPlatoonDistanceButton"
        text: "set"

        anchors.left: innerPlatoonDistanceTextField.right
        anchors.leftMargin: 10
        anchors.verticalCenter: innerPlatoonDistanceTextField.verticalCenter
    }

    // --- speed ---
    Label {
        id: speedLabel
        text: "ACC Desired Speed:"

        anchors.left: hostLabel.left
        anchors.top: innerPlatoonDistanceLabel.bottom
        anchors.topMargin: 20
    }

    TextField {
        id: speedTextField
        objectName: "speedTextField"
        text: "0"

        anchors.left: hostTextField.left
        anchors.verticalCenter: speedLabel.verticalCenter
    }

    Button {
        id: setPlatoonButton
        objectName: "setSpeedButton"
        text: "set"

        anchors.left: speedTextField.right
        anchors.leftMargin: 10
        anchors.verticalCenter: speedTextField.verticalCenter
    }
}
