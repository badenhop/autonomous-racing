import QtQuick 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.2
import car.pcgui 1.0

ApplicationWindow {
    id: window
    visible: true
    title: "Message Gui"

    toolBar: ToolBar {
        height: 180

        Label {
            id: moduleLabel
            text: "Module: "
            anchors.left: parent.left
            anchors.leftMargin: 10
            anchors.top: parent.top
            anchors.topMargin: 20
        }

        TextField {
            id: moduleSearchBox

            placeholderText: "Search..."
            inputMethodHints: Qt.ImhNoPredictiveText

            anchors.left: moduleLabel.right
            anchors.leftMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 10
            anchors.verticalCenter: moduleLabel.verticalCenter
        }

        Label {
            id: keyLabel
            text: "Key: "
            anchors.left: moduleLabel.left
            anchors.top: moduleLabel.bottom
            anchors.topMargin: 20
        }

        TextField {
            id: keySearchBox

            placeholderText: "Search..."
            inputMethodHints: Qt.ImhNoPredictiveText

            anchors.left: moduleSearchBox.left
            anchors.right: moduleSearchBox.right
            anchors.verticalCenter: keyLabel.verticalCenter
        }

        CheckBox {
            id: mostRecentCheckBox
            text: "show only most recent values"
            checked: false

            anchors.left: moduleLabel.left
            anchors.top: keyLabel.bottom
            anchors.topMargin: 20
        }

        Button {
            id: clearButton
            text: "Clear"
            onClicked: {
                messages_.clear();
            }

            anchors.left: mostRecentCheckBox.right
            anchors.leftMargin: 20
            anchors.verticalCenter: mostRecentCheckBox.verticalCenter
        }

        Label {
            id: hostLabel
            text: "Host:"

            anchors.left: moduleLabel.left
            anchors.top: mostRecentCheckBox.bottom
            anchors.topMargin: 20
        }

        TextField {
            id: hostTextField
            objectName: "hostTextField"
            text: "127.0.0.1"

            anchors.left: hostLabel.right
            anchors.leftMargin: 20
            anchors.verticalCenter: hostLabel.verticalCenter
        }

        Button {
            id: connectButton
            objectName: "connectButton"
            text: "Connect"

            anchors.left: hostTextField.right
            anchors.leftMargin: 20
            anchors.verticalCenter: hostTextField.verticalCenter
        }

        Label {
            id: statusLabel
            text: "Status:"

            anchors.left: connectButton.right
            anchors.leftMargin: 20
            anchors.verticalCenter: connectButton.verticalCenter
        }

        Label {
            id: statusTextLabel
            objectName: "statusTextLabel"

            text: "Disconnected"
            color: "red"

            anchors.left: statusLabel.right
            anchors.leftMargin: 20
            anchors.verticalCenter: statusLabel.verticalCenter
        }
    }

    TableView {
        id: tableView

        frameVisible: false
        sortIndicatorVisible: true

        anchors.fill: parent

        Layout.minimumWidth: 400
        Layout.minimumHeight: 240
        Layout.preferredWidth: 600
        Layout.preferredHeight: 400

        TableViewColumn {
            id: moduleColumn
            title: "Module"
            role: "module"
            movable: false
            resizable: true
            width: tableView.viewport.width / 3
        }

        TableViewColumn {
            id: keyColumn
            title: "Key"
            role: "key"
            movable: false
            resizable: true
            width: tableView.viewport.width / 3
        }

        TableViewColumn {
            id: valueColumn
            title: "Value"
            role: "value"
            movable: false
            resizable: true
            width: tableView.viewport.width / 3
        }

        model: filterKeyModel

        SortFilterProxyModel {
            id: filterKeyModel
            source: filterModuleModel

            //sortOrder: tableView.sortIndicatorOrder
            sortCaseSensitivity: Qt.CaseInsensitive
            sortRole: "key"

            filterString: "*" + keySearchBox.text + "*"
            filterSyntax: SortFilterProxyModel.Wildcard
            filterCaseSensitivity: Qt.CaseInsensitive
        }

        SortFilterProxyModel {
            id: filterModuleModel
            source: mostRecentModel

            //sortOrder: tableView.sortIndicatorOrder
            sortCaseSensitivity: Qt.CaseInsensitive
            sortRole: "module"

            filterString: "*" + moduleSearchBox.text + "*"
            filterSyntax: SortFilterProxyModel.Wildcard
            filterCaseSensitivity: Qt.CaseInsensitive
        }

        MostRecentMessageFilterProxyModel {
            id: mostRecentModel
            source: messageModel
            enabled: mostRecentCheckBox.checked
        }

        MessageModel {
            id: messageModel
            messageCollection: messages_
        }
    }
}
