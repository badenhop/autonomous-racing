#include "include/MessageGui/MessageModel.h"
#include "include/MessageGui/MessageRole.h"

MessageModel::MessageModel(QObject * parent)
    : QAbstractListModel(parent)
      , messageCollection(nullptr)
{
}

int MessageModel::rowCount(const QModelIndex & parent) const
{
    // For list models only the root node (an invalid parent) should return the list's size. For all
    // other (valid) parents, rowCount() should return 0 so that it does not become a tree model.
    if (parent.isValid() || !messageCollection)
        return 0;

    return messageCollection->getSize();
}

QVariant MessageModel::data(const QModelIndex & index, int role) const
{
    if (!index.isValid() || !messageCollection)
        return QVariant{};

    auto message = messageCollection->getItem(index.row());
    switch (role)
    {
        case messageRole::id:
            return QVariant{message.id};

        case messageRole::module:
            return QVariant{QString::fromStdString(message.data.module)};

        case messageRole::key:
            return QVariant{QString::fromStdString(message.data.key)};

        case messageRole::value:
            return QVariant{QString::fromStdString(message.data.value)};
    }

    return QVariant{};
}

QHash<int, QByteArray> MessageModel::roleNames() const
{
    return messageRole::roleNames();
}

MessageCollection * MessageModel::getMessageCollection() const noexcept
{
    return messageCollection;
}

void MessageModel::setMessageCollection(MessageCollection * newMessageCollection)
{
    beginResetModel();

    if (messageCollection)
    {
        messageCollection->disconnect(this);
        removeRows(0, rowCount());
    }

    messageCollection = newMessageCollection;

    if (messageCollection)
    {
        insertRows(0, messageCollection->getSize());

        connect(messageCollection, &MessageCollection::preMessagesInserted, this, [this](int count)
        {
            int index = messageCollection->getSize();
            beginInsertRows(QModelIndex{}, index, index + count - 1);
        });

        connect(messageCollection, &MessageCollection::postMessagesInserted, this, [this]
        { endInsertRows(); });

        connect(messageCollection, &MessageCollection::preMessagesCleared, this, [this]()
        { beginRemoveRows(QModelIndex{}, 0, rowCount()); });

        connect(messageCollection, &MessageCollection::postMessagesCleared, this, [this]
        { endRemoveRows(); });
    }

    endResetModel();
}
