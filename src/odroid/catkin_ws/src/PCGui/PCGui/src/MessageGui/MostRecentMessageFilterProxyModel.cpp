//
// Created by philipp on 15.05.18.
//

#include "include/MessageGui/MostRecentMessageFiltertProxyModel.h"
#include "include/MessageGui/MessageRole.h"
#include <include/Utils.h>
#include <QDebug>

MostRecentMessageFilterProxyModel::MostRecentMessageFilterProxyModel(QObject * parent)
    : QSortFilterProxyModel(parent)
{
    connect(this, SIGNAL(rowsInserted(QModelIndex, int, int)), this, SIGNAL(countChanged()));
    connect(this, SIGNAL(rowsRemoved(QModelIndex, int, int)), this, SIGNAL(countChanged()));
}

QObject * MostRecentMessageFilterProxyModel::source() const
{
    return sourceModel();
}

void MostRecentMessageFilterProxyModel::setSource(QObject * source)
{
    beginResetModel();

    if (this->source())
        disconnect(this->source());

    setSourceModel(qobject_cast<QAbstractItemModel *>(source));

    connect(sourceModel(), SIGNAL(rowsInserted(QModelIndex, int, int)),
            this, SLOT(onRowsInserted(const QModelIndex &, int, int)));
    connect(sourceModel(), SIGNAL(rowsRemoved(QModelIndex, int, int)),
            this, SLOT(onRowsRemoved(const QModelIndex &, int, int)));

    endResetModel();
}

int MostRecentMessageFilterProxyModel::count() const
{
    return rowCount();
}

void MostRecentMessageFilterProxyModel::enable(bool flag)
{
    enabledFlag = flag;
    invalidateFilter();
}

bool MostRecentMessageFilterProxyModel::enabled() const noexcept
{
    return enabledFlag;
}

QHash<int, QByteArray> MostRecentMessageFilterProxyModel::roleNames() const
{
    return messageRole::roleNames();
}

void MostRecentMessageFilterProxyModel::onRowsInserted(const QModelIndex & parent, int first, int last)
{
    for (int i = first; i <= last; i++)
    {
        auto key = messageKeyFromRow(i, parent);
        mostRecent[key] = idFromRow(i, parent);
    }
    invalidateFilter();
}

void MostRecentMessageFilterProxyModel::onRowsRemoved(const QModelIndex & parent, int first, int last)
{
    mostRecent.clear();
    invalidateFilter();
}

MessageKey MostRecentMessageFilterProxyModel::messageKeyFromRow(int row, const QModelIndex & parent) const
{
    auto model = sourceModel();
    auto module = model->data(model->index(row, 0, parent), messageRole::module).toString().toStdString();
    auto key = model->data(model->index(row, 0, parent), messageRole::key).toString().toStdString();
    return MessageKey{module, key};
}

int MostRecentMessageFilterProxyModel::idFromRow(int row, const QModelIndex & parent) const
{
    auto model = sourceModel();
    return model->data(model->index(row, 0, parent), messageRole::id).toInt();
}

bool MostRecentMessageFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex & sourceParent) const
{
    if (!enabled())
        return true;

    auto key = messageKeyFromRow(sourceRow, sourceParent);
    if (!utils::contains(mostRecent, key))
        return true;

    auto id = idFromRow(sourceRow, sourceParent);
    return id == mostRecent.at(key);
}
