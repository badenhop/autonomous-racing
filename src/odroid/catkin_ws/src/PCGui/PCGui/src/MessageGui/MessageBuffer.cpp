//
// Created by philipp on 13.05.18.
//

#include "include/MessageGui/MessageBuffer.h"

MessageBuffer::MessageBuffer(std::size_t initCapacity, QObject * parent)
    : MessageCollection(parent)
{
    items.reserve(initCapacity);
}

Message & MessageBuffer::getItem(int index)
{
    return items[index];
}

int MessageBuffer::getSize() const noexcept
{
    return size;
}

void MessageBuffer::insert(const Message & message)
{
    items.push_back(message);
}

void MessageBuffer::commit()
{
    auto messagesToCommit = (int) items.size() - size;
    emit preMessagesInserted(messagesToCommit);
    size = (int) items.size();
    emit postMessagesInserted();
}

void MessageBuffer::clear()
{
    emit preMessagesCleared();
    items.clear();
    emit postMessagesCleared();
}
