#include "include/QtQuickControlsApplication.h"
#include "include/MessageGui/SortFilterProxyModel.h"
#include <QtQml/qqmlapplicationengine.h>
#include <QtGui/qsurfaceformat.h>
#include <QtQml/qqml.h>
#include <QtQml/QQmlContext>
#include <include/MessageGui/MessageModel.h>
#include "NetworkingLib/Networking.h"
#include "MessageLib/Client.h"
#include "include/MessageGui/SignalProducer.h"
#include "include/MessageGui/SignalConsumer.h"
#include <QDebug>
#include "include/MessageGui/MostRecentMessageFiltertProxyModel.h"

static int currId = 0;

void registerTypes()
{
    qmlRegisterType<SortFilterProxyModel>("car.pcgui", 1, 0, "SortFilterProxyModel");
    qmlRegisterType<MostRecentMessageFilterProxyModel>("car.pcgui", 1, 0, "MostRecentMessageFilterProxyModel");
    qmlRegisterType<MessageModel>("car.pcgui", 1, 0, "MessageModel");
    qRegisterMetaType<Message>();
    qmlRegisterUncreatableType<MessageCollection>(
        "car.pcgui", 1, 0, "MessageCollection", QStringLiteral("MessageCollection should not be created in QML"));
    qmlRegisterUncreatableType<MessageBuffer>(
        "car.pcgui", 1, 0, "MessageBuffer", QStringLiteral("MessageBuffer should not be created in QML"));
    qmlRegisterUncreatableType<SignalConsumer>(
        "car.pcgui", 1, 0, "SignalConsumer", QStringLiteral("SignalConsumer should not be created in QML"));
}

void configureSurfaceFormat()
{
    if (QCoreApplication::arguments().contains(QLatin1String("--coreprofile")))
    {
        QSurfaceFormat fmt;
        fmt.setVersion(4, 4);
        fmt.setProfile(QSurfaceFormat::CoreProfile);
        QSurfaceFormat::setDefaultFormat(fmt);
    }
}

void requestMessages(message::Client::Ptr & messageClient,
                     SignalProducer & producer,
                     const std::string & host)
{
    using namespace std::chrono_literals;
    messageClient->requestMessagesPeriodically(
        host, 500ms, 3s,
        [&producer](const auto & message)
        {
            producer.emitMessageProduced(Message{currId++, message});
        },
        [&producer](const auto & error)
        {
            if (error)
            {
                producer.emitDisconnected();
            }
            else
            {
                producer.emitConnected();
                producer.emitMessagesCommitted();
            }
        });
}

int main(int argc, char * argv[])
{
    networking::Networking net;
    auto messageClient = message::Client::create(net, 10207, 500000);
    MessageBuffer messages{10000};
    SignalProducer producer;
    SignalConsumer consumer;

    registerTypes();
    QtQuickControlsApplication app{argc, argv};
    configureSurfaceFormat();
    QQmlApplicationEngine engine{};

    engine.rootContext()->setContextProperty(QStringLiteral("messages_"), &messages);
    engine.rootContext()->setContextProperty(QStringLiteral("consumer_"), &consumer);

    engine.load(QUrl("qrc:/MessageGui.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    auto root = engine.rootObjects()[0];
    auto hostTextField = root->findChild<QObject *>("hostTextField");
    auto connectButton = root->findChild<QObject *>("connectButton");
    auto statusTextLabel = root->findChild<QObject *>("statusTextLabel");

    QObject::connect(&producer, SIGNAL(messageProduced(Message)), &messages, SLOT(insert(const Message &)));
    QObject::connect(&producer, SIGNAL(messagesCommitted()), &messages, SLOT(commit()));
    QObject::connect(&producer, SIGNAL(connected()), &consumer, SLOT(showConnected()));
    QObject::connect(&producer, SIGNAL(disconnected()), &consumer, SLOT(showDisconnected()));
    QObject::connect(connectButton, SIGNAL(clicked()), &consumer, SLOT(doConnect()));

    consumer.setOnShowConnected(
        [statusTextLabel]
        {
            statusTextLabel->setProperty("text", QStringLiteral("Connected"));
            statusTextLabel->setProperty("color", QStringLiteral("green"));
        });

    consumer.setOnShowDisconnected(
        [statusTextLabel]
        {
            statusTextLabel->setProperty("text", QStringLiteral("Disconnected"));
            statusTextLabel->setProperty("color", QStringLiteral("red"));
        });

    consumer.setOnDoConnect(
        [hostTextField, &messageClient, &producer, &net]
        {
            auto host = hostTextField->property("text").toString().toStdString();
            messageClient->stop();
            net.waitWhileBusy(*messageClient);
            requestMessages(messageClient, producer, host);
        });

    requestMessages(messageClient, producer, "127.0.0.1");

    return app.exec();
}
