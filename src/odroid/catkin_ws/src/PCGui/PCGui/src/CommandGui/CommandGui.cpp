#include "include/QtQuickControlsApplication.h"
#include <QQmlApplicationEngine>
#include <QtGui/qsurfaceformat.h>
#include <QtQml/qqml.h>
#include <QtQml/QQmlContext>
#include <NetworkingLib/Networking.h>
#include "../include/CommandGui/SignalConsumer.h"
#include "PC2CarLib/CommandSender.h"

void registerTypes()
{
    qmlRegisterUncreatableType<SignalConsumer>(
        "car.pcgui", 1, 0, "SignalConsumer", QStringLiteral("SignalConsumer should not be created in QML"));
}

void configureSurfaceFormat()
{
    if (QCoreApplication::arguments().contains(QLatin1String("--coreprofile")))
    {
        QSurfaceFormat fmt;
        fmt.setVersion(4, 4);
        fmt.setProfile(QSurfaceFormat::CoreProfile);
        QSurfaceFormat::setDefaultFormat(fmt);
    }
}

int main(int argc, char * argv[])
{
    SignalConsumer consumer;
    networking::Networking net;
    pc2car::CommandSender commandSender{net, "127.0.0.1"};

    registerTypes();
    QtQuickControlsApplication app{argc, argv};
    configureSurfaceFormat();
    QQmlApplicationEngine engine{};

    engine.rootContext()->setContextProperty(QStringLiteral("consumer"), &consumer);

    engine.load(QUrl("qrc:/CommandGui.qml"));
    if (engine.rootObjects().isEmpty())
        return -1;

    auto root = engine.rootObjects()[0];
    auto hostTextField = root->findChild<QObject *>("hostTextField");
    auto setHostButton = root->findChild<QObject *>("setHostButton");
    auto enablePlatoonButton = root->findChild<QObject *>("enablePlatoonButton");
    auto disablePlatoonButton = root->findChild<QObject *>("disablePlatoonButton");
    auto platoonSpeedTextField = root->findChild<QObject *>("platoonSpeedTextField");
    auto setPlatoonSpeedButton = root->findChild<QObject *>("setPlatoonSpeedButton");
    auto innerPlatoonDistanceTextField = root->findChild<QObject *>("innerPlatoonDistanceTextField");
    auto setInnerPlatoonDistanceButton = root->findChild<QObject *>("setInnerPlatoonDistanceButton");
    auto speedTextField = root->findChild<QObject *>("speedTextField");
    auto setSpeedButton = root->findChild<QObject *>("setSpeedButton");

    QObject::connect(setHostButton, SIGNAL(clicked()), &consumer, SLOT(hostChanged()));
    QObject::connect(enablePlatoonButton, SIGNAL(clicked()), &consumer, SLOT(enablePlatoon()));
    QObject::connect(disablePlatoonButton, SIGNAL(clicked()), &consumer, SLOT(disablePlatoon()));
    QObject::connect(setPlatoonSpeedButton, SIGNAL(clicked()), &consumer, SLOT(platoonSpeedChanged()));
    QObject::connect(setInnerPlatoonDistanceButton, SIGNAL(clicked()), &consumer, SLOT(innerPlatoonDistanceChanged()));
    QObject::connect(setSpeedButton, SIGNAL(clicked()), &consumer, SLOT(speedChanged()));

    consumer.setOnHostChanged(
        [&commandSender, &hostTextField]
        {
            auto host = hostTextField->property("text").toString().toStdString();
            commandSender.setHost(host);
        });

    consumer.setOnEnablePlatoon(
        [&commandSender]
        {
            commandSender.enablePlatoon(true);
        });

    consumer.setOnDisablePlatoon(
        [&commandSender]
        {
            commandSender.enablePlatoon(false);
        });

    consumer.setOnPlatoonSpeedChanged(
        [&commandSender, &platoonSpeedTextField]
        {
            auto platoonSpeed = platoonSpeedTextField->property("text").toFloat();
            commandSender.setPlatoonSpeed(platoonSpeed);
        });

    consumer.setOnInnerPlatoonDistanceChanged(
        [&commandSender, &innerPlatoonDistanceTextField]
        {
            auto innerPlatoonDistance = innerPlatoonDistanceTextField->property("text").toFloat();
            commandSender.setInnerPlatoonDistance(innerPlatoonDistance);
        });

    consumer.setOnSpeedChanged(
        [&commandSender, &speedTextField]
        {
            auto speed = speedTextField->property("text").toFloat();
            commandSender.setSpeed(speed);
        });

    return app.exec();
}
