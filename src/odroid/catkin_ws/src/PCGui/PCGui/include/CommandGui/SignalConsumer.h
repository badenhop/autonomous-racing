//
// Created by philipp on 16.05.18.
//

#ifndef PCGUI_SIGNALCONSUMER_H
#define PCGUI_SIGNALCONSUMER_H

#include <QObject>
#include <functional>

class SignalConsumer : public QObject
{
Q_OBJECT

public:

    using Callback = std::function<void()>;

    explicit SignalConsumer(QObject * parent = nullptr) : QObject(parent)
    {}

    void setOnHostChanged(const Callback & callback)
    { onHostChanged = callback; }

    void setOnEnablePlatoon(const Callback & callback)
    { onEnablePlatoon = callback; }

    void setOnDisablePlatoon(const Callback & callback)
    { onDisablePlatoon = callback; }

    void setOnPlatoonSpeedChanged(const Callback & callback)
    { onPlatoonSpeedChanged = callback; }

    void setOnInnerPlatoonDistanceChanged(const Callback & callback)
    { onInnerPlatoonDistanceChanged = callback; }

    void setOnSpeedChanged(const Callback & callback)
    { onSpeedChanged = callback; }

public slots:

    void hostChanged()
    { onHostChanged(); };

    void enablePlatoon()
    { onEnablePlatoon(); }

    void disablePlatoon()
    { onDisablePlatoon(); }

    void platoonSpeedChanged()
    { onPlatoonSpeedChanged(); }

    void innerPlatoonDistanceChanged()
    { onInnerPlatoonDistanceChanged(); }

    void speedChanged()
    { onSpeedChanged(); }

private:

    Callback onHostChanged = [] {};
    Callback onEnablePlatoon = [] {};
    Callback onDisablePlatoon = [] {};
    Callback onPlatoonSpeedChanged = [] {};
    Callback onInnerPlatoonDistanceChanged = [] {};
    Callback onSpeedChanged = [] {};
};

#endif //PCGUI_SIGNALCONSUMER_H
