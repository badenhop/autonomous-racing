//
// Created by philipp on 15.05.18.
//

#ifndef PCGUI_UTILS_H
#define PCGUI_UTILS_H

#include <unordered_map>
#include <vector>

namespace utils
{

template<template<typename...> class Map, typename Key, typename Value>
std::vector<Key> keys(const Map<Key, Value> & map)
{
    std::vector<Key> mapKeys;
    for (const auto & i : map)
        mapKeys.emplace_back(i.first);
    return mapKeys;
};

template<typename Container, typename Key>
bool contains(const Container & container, const Key & key)
{
    return container.find(key) != container.end();
};

}

#endif //PCGUI_UTILS_H
