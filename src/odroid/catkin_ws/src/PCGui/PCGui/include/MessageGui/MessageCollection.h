
//
// Created by philipp on 15.05.18.
//

#ifndef PCGUI_MESSAGECOLLECTION_H
#define PCGUI_MESSAGECOLLECTION_H

#include <QObject>
#include "Message.h"

class MessageCollection : public QObject
{
Q_OBJECT

public:
    explicit MessageCollection(QObject * parent = nullptr) : QObject(parent)
    {}

    virtual Message & getItem(int index) = 0;

    virtual int getSize() const noexcept = 0;

signals:

    void preMessagesInserted(int count);

    void postMessagesInserted();

    void preMessagesCleared();

    void postMessagesCleared();

};

#endif //PCGUI_MESSAGECOLLECTION_H
