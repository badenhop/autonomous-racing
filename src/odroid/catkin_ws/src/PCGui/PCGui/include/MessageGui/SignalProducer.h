//
// Created by philipp on 15.05.18.
//

#ifndef PCGUI_SIGNALPRODUCER_H
#define PCGUI_SIGNALPRODUCER_H

#include <QObject>

class SignalProducer : public QObject
{
Q_OBJECT

public:
    explicit SignalProducer(QObject * parent = nullptr)
    {}

    void emitConnected()
    { emit connected(); }

    void emitDisconnected()
    { emit disconnected(); }

    void emitMessageProduced(const Message & message)
    { emit messageProduced(message); }

    void emitMessagesCommitted()
    { emit messagesCommitted(); }

signals:

    void connected();

    void disconnected();

    void messageProduced(Message);

    void messagesCommitted();
};

#endif //PCGUI_SIGNALPRODUCER_H
