//
// Created by philipp on 15.05.18.
//

#ifndef PCGUI_SIGNALCONSUMER_H
#define PCGUI_SIGNALCONSUMER_H

#include <QObject>
#include <functional>

class SignalConsumer : public QObject
{
Q_OBJECT

public:
    using Callback = std::function<void()>;

    explicit SignalConsumer(QObject * parent = nullptr)
    {}

    void setOnShowConnected(const Callback & callback)
    { onShowConnected = callback; }

    void setOnShowDisconnected(const Callback & callback)
    { onShowDisconnected = callback; }

    void setOnDoConnect(const Callback & callback)
    { onDoConnect = callback; }

public slots:

    void showConnected()
    { onShowConnected(); };

    void showDisconnected()
    { onShowDisconnected(); }

    void doConnect()
    { onDoConnect(); }

private:
    Callback onShowConnected = [] {};
    Callback onShowDisconnected = [] {};
    Callback onDoConnect = [] {};
};

#endif //PCGUI_SIGNALCONSUMER_H
