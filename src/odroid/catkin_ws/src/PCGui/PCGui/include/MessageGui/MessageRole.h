//
// Created by philipp on 15.05.18.
//

#ifndef PCGUI_MESSAGEROLES_H
#define PCGUI_MESSAGEROLES_H

#include <QHash>

namespace messageRole
{

enum
{
    id = Qt::UserRole,
    module,
    key,
    value
};

inline QHash<int, QByteArray> roleNames()
{
    QHash<int, QByteArray> names;
    names[id] = "id";
    names[module] = "module";
    names[key] = "key";
    names[value] = "value";
    return names;
}

}

#endif //PCGUI_MESSAGEROLES_H
