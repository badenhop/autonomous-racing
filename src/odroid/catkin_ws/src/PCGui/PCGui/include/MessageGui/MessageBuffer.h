//
// Created by philipp on 13.05.18.
//

#ifndef PCGUI_MESSAGELIST_H
#define PCGUI_MESSAGELIST_H

#include "MessageCollection.h"
#include <vector>

class MessageBuffer : public MessageCollection
{
Q_OBJECT

public:
    MessageBuffer(std::size_t initCapacity=10, QObject * parent = nullptr);

    Message & getItem(int index) override;

    int getSize() const noexcept override;

public slots:

    void insert(const Message & message);

    void commit();

    void clear();

private:
    std::vector<Message> items;
    int size{0};
};

#endif //PCGUI_MESSAGELIST_H
