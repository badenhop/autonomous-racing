#ifndef MESSAGEMODEL_H
#define MESSAGEMODEL_H

#include <QAbstractListModel>
#include "MessageLib/Message.h"
#include "MessageBuffer.h"

class MessageModel : public QAbstractListModel
{
Q_OBJECT

Q_PROPERTY(MessageCollection * messageCollection READ getMessageCollection WRITE setMessageCollection)

public:
    explicit MessageModel(QObject * parent = nullptr);

    int rowCount(const QModelIndex & parent = QModelIndex()) const override;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const override;

    QHash<int, QByteArray> roleNames() const override;

    MessageCollection * getMessageCollection() const noexcept;

    void setMessageCollection(MessageCollection * newMessageCollection);

private:
    MessageCollection * messageCollection;
};

#endif // MESSAGEMODEL_H