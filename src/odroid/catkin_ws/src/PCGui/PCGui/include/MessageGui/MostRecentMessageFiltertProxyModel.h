//
// Created by philipp on 15.05.18.
//

#ifndef PCGUI_MOSTRECENTFILTERTPROXYMODEL_H
#define PCGUI_MOSTRECENTFILTERTPROXYMODEL_H

#include <QtCore/QSortFilterProxyModel>
#include <unordered_map>
#include "MessageLib/Message.h"

struct MessageKey
{
    MessageKey(const std::string & module, const std::string & key)
        : module(module), key(key)
    {}

    std::string module;
    std::string key;
};

namespace std
{

template<>
struct hash<MessageKey>
{
    std::size_t operator()(const MessageKey & key) const
    {
        return std::hash<std::string>{}(key.module + "#" + key.key);
    }
};

template<>
struct equal_to<MessageKey>
{
    bool operator()(const MessageKey & lhs, const MessageKey & rhs) const noexcept
    {
        return lhs.module == rhs.module && lhs.key == rhs.key;
    }
};

}

class MostRecentMessageFilterProxyModel : public QSortFilterProxyModel
{
Q_OBJECT

Q_PROPERTY(int count READ count NOTIFY countChanged)
Q_PROPERTY(QObject * source READ source WRITE setSource)
Q_PROPERTY(bool enabled READ enabled WRITE enable )

public:
    explicit MostRecentMessageFilterProxyModel(QObject * parent = nullptr);

    QObject * source() const;

    void setSource(QObject * source);

    int count() const;

    void enable(bool flag);

    bool enabled() const noexcept;

signals:

    void countChanged();

protected slots:

    void onRowsInserted(const QModelIndex & parent, int first, int last);

    void onRowsRemoved(const QModelIndex & parent, int first, int last);

protected:

    bool filterAcceptsRow(int sourceRow, const QModelIndex &sourceParent) const;

    QHash<int, QByteArray> roleNames() const;

private:

    MessageKey messageKeyFromRow(int row, const QModelIndex & parent) const;

    int idFromRow(int row, const QModelIndex & parent) const;

    std::unordered_map<MessageKey, int> mostRecent;
    bool enabledFlag{false};
};

#endif //PCGUI_MOSTRECENTFILTERTPROXYMODEL_H
