//
// Created by philipp on 15.05.18.
//

#ifndef PCGUI_MESSAGE_H
#define PCGUI_MESSAGE_H

#include "MessageLib/Message.h"
#include <QMetaType>

struct Message
{
    Message() = default;

    Message(int id, const message::Message & data)
        : id(id), data(data)
    {}

    int id;
    message::Message data;
};

Q_DECLARE_METATYPE(Message);

#endif //PCGUI_MESSAGE_H
