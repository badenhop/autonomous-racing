/** *****************************************************************************************
 * Unit in charge:
 * @file	errorHandler.c
 * @author	adelhoefer
 * $Date:: 2017-04-20 13:17:57 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev::                   $
 * $Author:: adelhoefer   		$
 *
 * ----------------------------------------------------------
 *
 * @brief	This modul provides basic error handling
 ******************************************************************************************/


/************* Includes *******************************************************************/
#include "../errorHandler.h"
#include "sys/util/io.h"
/************* Public typedefs ************************************************************/

/************* input signal pointer********************************************************/
uint64_t* pErrorRegister_errorHandler;

/************* Macros and constants *******************************************************/

/************* Private function prototypes *************************************************/
void clearErrorRegister(void);

/************* Public function prototypes *************************************************/

int8_t errorHandler_init(void){
	*pErrorRegister_errorHandler = 0;
	return 0;
}


void errorHandler_setError(enum ERRORTYPE error){
	irq_disable();
	*pErrorRegister_errorHandler |= (1 << error);
	irq_enable();
}

void errorHandler_clearError(enum ERRORTYPE error){
	irq_disable();
	*pErrorRegister_errorHandler &= ~(1 << error);
	irq_enable();
}

void errorHandler_printErrors(void){
	uint8_t errorIndex;
	uint64_t errorRegister;

	irq_disable();
	errorRegister = *pErrorRegister_errorHandler;
	irq_enable();

	if(errorRegister == 0){
		return;
	}

	for(errorIndex = 0; errorIndex < 64; errorIndex ++){
		/* Error Occured --> print */
		uint64_t errorOccuredIndex = errorRegister & (1<< (uint64_t) errorIndex);
		if(0 != errorOccuredIndex){
			io_printf("Error: %s\n", strErrorList[errorIndex-1]);
		}
	}

}


