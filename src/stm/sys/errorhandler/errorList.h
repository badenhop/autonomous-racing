/*******************************************************************************************
* @brief provides a list of possible probable errors
* These are handled in errorHandling.c module
* Auto-Generated by VBA Tool - see ddp2
* DDP2 --> \EC_B01\Documents\00_Projekte\01_Vehicle ECU\03_SW\11_ErrorHandling
******************************************************************************************/

#ifndef ERRORLIST_H_
#define ERRORLIST_H_

/************* External Definitions *******************************************************/
extern const char* strErrorList[];

/************* Macros and constants *******************************************************/

enum ERRORTYPE{
	ERROR_BATMON_BattEcuLowVoltage = 1,
	ERROR_BATMON_BattMotorLowVoltage = 2,
	ERROR_WENC_WencFLDeffect = 3,
	ERROR_WENC_WencFRDeffect = 4,
	ERROR_WENC_WencRLDeffect = 5,
	ERROR_WENC_WencRRDeffect = 6,
	ERROR_MAVLINK_Timeout = 7,
	ERROR_RX24F_InputVoltageError = 8,
	ERROR_RX24F_ServoAngleLimitReached = 9,
	ERROR_RX24F_ServoOverheating = 10,
	ERROR_RX24F_Range = 11,
	ERROR_RX24F_ChecksumRx24F = 12,
	ERROR_RX24F_OverTorque = 13,
	ERROR_RX24F_Instruktion = 14,
	ERROR_RX24F_ChecksumSTM = 15,
	ERROR_RX24F_NOSYNCBYTES = 16,
	ERROR_MAVLINK_XMLVersionsDiffer = 17,
};

#endif /* ERRORLIST_H_ */