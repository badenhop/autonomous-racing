/** *****************************************************************************************
 * Unit in charge:
 * @file	errorHandler.h
 * @author	adelhoefer
 * $Date:: 2017-04-20 13:17:57 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev::                   $
 * $Author:: adelhoefer   		$
 *
 * ----------------------------------------------------------
 *
 * @brief	This modul provides basic error handling
 ******************************************************************************************/


#ifndef ERRORHANDLER_H_
#define ERRORHANDLER_H_

/************* Includes *******************************************************************/

#include "errorList.h"
#include "brd/startup/stm32f4xx.h"
#include "sys/sigp/sigp.h"
#include "per/irq.h"

/************* Public typedefs ************************************************************/

/************* Macros and constants *******************************************************/

/************* Public function prototypes *************************************************/
/**
 *	@brief initialize error Register
 */
int8_t errorHandler_init(void);

/**
 *	@brief set specific error from Error Regeister
 */
void errorHandler_setError(enum ERRORTYPE error);


/**
 *	@brief clear specific error from Error Regeister
 */
void errorHandler_clearError(enum ERRORTYPE error);

/**
 *	@brief print occured errors of the whole system
*/
void errorHandler_printErrors(void);

#endif /* ERRORHANDLER_H_ */
