/**
*****************************************************************************
**
**  File        : stubs.h
**
**  Abstract    : Declarations for replacement functions for functions that do not yet exist,
**  			  as well as constants, type definitions etc.
**
**  Functions   : <constantly changing>
**
**  Environment : Atollic TrueSTUDIO(R)
**                STMicroelectronics STM32F4xx Standard Peripherals Library
**
**  Distribution: The file is distributed �as is,� without any warranty
**                of any kind.
**
**  Author		: Bernhard Kaiser
**
**
** ----------------------------------------------------------
** SVN information of last commit:
** $Rev:: 2158                  $
** $Author:: mbrieske           $
**
** ----------------------------------------------------------
**
*****************************************************************************
*/

#ifndef STUBS_H_
#define STUBS_H_

#include "brd/types.h"
#include "sys/paramsys/paramsys.h"
#include "sys/sched/scheduler.h"

// global typedefs

typedef void (* func_ptr_t)(void);


// some useful global macros (for later incorporation in global macro header)

#define ABS(N) (((N) < 0) ? (-(N)) : (N))	/* absolute value of any integer type */
#define MIN(A,B) (((A) < (B)) ? (A) : (B))	/* minimum of two values of any int type */
#define MAX(A,B) (((A) > (B)) ? (A) : (B)) 	/* maximum of two values of any int type */
// #define DIV_ROUND(A, B) (((A) + (((A) > 0) || ((A) == 0) ? (B) / 2 : -(B) / 2)) / (B))
											/* Division with correct rounding (comparation
											   > 0 or == 0 instead >= 0 avoids warning when used
											   on unsigned types.

											   TAKE CARE OF TYPE OVERFLOWS WHEN USING THIS MACRO!
											   A + (B/2) might be too large!
											*/




// #define INIT(A) A = A ## _INI				/* to simplify inits from xxini.h */

/* For Boolean Logic */

//#define TRUE -1
//#define FALSE 0


/* For Bit Logic */

#define B0      0x0001
#define B1      0x0002
#define B2      0x0004
#define B3      0x0008
#define B4      0x0010
#define B5      0x0020
#define B6      0x0040
#define B7      0x0080
#define B8      0x0100
#define B9      0x0200
#define B10     0x0400
#define B11     0x0800
#define B12     0x1000
#define B13     0x2000
#define B14     0x4000
#define B15     0x8000
#define B16 0x00010000
#define B17 0x00020000
#define B18 0x00040000
#define B19 0x00080000
#define B20 0x00100000
#define B21 0x00200000
#define B22 0x00400000
#define B23 0x00800000
#define B24 0x01000000
#define B25 0x02000000
#define B26 0x04000000
#define B27 0x08000000
#define B28 0x10000000
#define B29 0x20000000
#define B30 0x40000000
#define B31 0x80000000


// missing macros

#define portDISABLE_INTERRUPTS() disableInterrupts();
#define portENABLE_INTERRUPTS() enableInterrupts();


// global variables (to be moved from here)


uint8_t uxCriticalNesting; // for nesting of critical sections (= interrupt disabled sections); needed by scheduler or RTOS

typedef uint16_t interrupt_ena_status_t;						// for storing old interrupt enable status when disabling interrupts in order to allow later restore

interrupt_ena_status_t disableInterrupts(void);			// disalbe interrupts, but allow remembering old status
void restoreInterrupts(interrupt_ena_status_t oldStatus);// restore former interrupt enable situation
void enableInterrupts(void);								// enable all interrupts without regard to former situation




// Dummy stuff for test purposes

void test(void);
void vTaskToggleLED3(void);
void vTaskToggleLED4(void);
void vTaskToggleLED5(void);
void vTaskToggleLED6(void);

void delay(uint32_t waitTimeMs);


/* Type for void function pointer */
typedef void (* func_ptr_t)(void);






/* Functions for character checking/transformation (may overload library functions) */

//static inline bool isdigit (char a)
//{return (a >= '0' && a <= '9');}
//
//static inline bool isxdigit (char a)
//{return (a >= 'a' && a <= 'f')  || (a >= 'A' && a <= 'F') || (a >= '0' && a <= '9');}
//
//static inline bool isalpha (char a)
//{return (a >= 'a' && a <= 'z') || (a >= 'A' && a <= 'Z');}
//
static inline char toUpper (char a) {return ((a >= 'a' && a <= 'z') ? (a - 'a' + 'A') : a);}


/* Functions for ASCII <-> HEX conversion (unprotected, use isxdigit() if needed!) */

static inline uint8_t asciiToHex(char ch)
{
if (ch >= '0' && ch <= '9')
	ch = ch - '0';

else if (ch >= 'A' && ch <= 'F')
	ch = ch - 'A' + 10;

else if (ch >= 'a' && ch <= 'f')
	ch = ch - 'a' + 10;

else
	ch = 0xFF;

return ch;
}

static inline uint8_t asciiToOneByte(char upper, char lower)
{
return (asciiToHex(upper) << 4) + asciiToHex(lower);
}


static inline char hexToAscii(uint8_t c)
{
static const char asciiHexDigits[] = "0123456789ABCDEF";
return asciiHexDigits[c & 0x0F];
}



//Takeover constant for one aspects of parameter type (signedness) --> to be completed or changed later!!!
#define PAR_SIGNED					0x0020
#define PAR_FLOAT					0x0030  //FRY...FLOAT



// FRY... �bernommen in paramsys.h
//static inline param_id_t decToParID (uint16_t in) {param_id_t out; out.grp = in/100; out.index = in%100; return out;}




int16_t parDisplayAttrRead(param_id_t param, param_display_attr_t * dest);


void taskList(void);


// Typedefs, Constants and Functions that will later be provided by SW Component ??? (Serial Comm Handler)

typedef enum
		{
			NO_PARITY = 0,
			ODD = 1,
			EVEN = 2,
		} PARITY;

typedef enum
		{
			POLLED,
			HALF_DUPLEX,
			FULL_DUPLEX,
		} SCI_MODE;



typedef enum
		{
			RS232_PORT,
		} USART_PORT;

typedef enum
		{
				MONITOR,
		} SER_COMM_PROT;


uint16_t SCI_Init(USART_PORT port, uint32_t baudRate, uint8_t dataLen, uint8_t nrStopBits, PARITY parity,
									SCI_MODE mode, SER_COMM_PROT commProt);

// Typedefs, Constants and Functions that will later be provided by SW Component SCHED (Scheduler)






// Typedefs, Constants and Functions that will later be provided by SW Component EVENTMGR (Event and Fault Manager)

void EVENTMGR_resetAllFaults(void);

// Typedefs, Constants and Functions for testing and playing purposes
//void appInit(void);
void vInitLedsForDummyApp(void);
void myDummyAppInit(void);

#endif /* STUBS_H_ */
