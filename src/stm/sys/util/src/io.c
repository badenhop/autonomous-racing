/** *****************************************************************************************
 * Unit in charge:
 * @file	io.c
 * @author	Bjoern Hepner
 * $Date:: 2016-04-05 17:36:40 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:

 * $Rev:: 1934                  $
 * $Author:: reinhard           $
 * 
 * ----------------------------------------------------------
 *
 * @brief Sets an alias for printf and scanf for formatted output and input ready.
 *
 * @code
 *  int8_t recBuf[12], sendBuf[12];
 *  uint32_t recBufSize = 12, sendBufSize = 12;
 *	int8_t charResult;
 *	int32_t intResult;
 *	int32_t hexToIntResult;
 *	float32_t floatResult;
 *  float32_t fNumber = 789.684684, fNumber2 = 78945.12, fNumber3 = 7.54655;
 *	int8_t stringResult[32];
 *
 *  uart_t uart;
 *	io_init(&uart,recBuf,recBufSize,sendBuf,sendBufSize);
 *
 *	io_scanf("%c %i %s %f %x", &charResult, &intResult, &stringResult, &floatResult, &hexToIntResult);
 *	io_printf("%c %i %s %f %x \n", charResult, intResult, stringResult, floatResult, hexToIntResult);
 *
 *	io_printf("Foobar %f Foobar2 %f Foobar3 %f",fNumber,fNumber2,fNumber3);
 *
 *	io_getline(stringResult);
 *	io_printf("\nResult: %s\n",stringResult);
 *	while (1) {
 *	}
 * @endcode
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include <stdarg.h>
#include <stdio.h>
#include <math.h>

#include "sys/util/io.h"

/************* Private typedefs ***********************************************************/


/************* Macros and constants *******************************************************/
#define INPUT_SIZE 64							/**< Number of characters that can be read. */
#define DEFAULT_DECIMAL_PLACES 6				/**< Is required for the float conversion. */

/**
 * \defgroup errMessIO Input Output error messages
 */
/*@{*/
#define IO_PRINTF_WRONG_FORMAT_SPECIFIER 	-1 	/**< Wrong format specifier indicated. */
#define IO_SCANF_INPUT_SIZE_FULL 			-2 	/**< Input Puffer (value[]) is full. */
#define IO_SCANF_WRONG_FORMAT_SPECIFIER 	-3 	/**< Wrong format specifier indicated. */
#define IO_GETLINE_INPUT_SIZE_FULL			-4  /**< Input Puffer (value[]) is full. */
/*@}*/

/************* Private static variables ***************************************************/
static uart_t uart;
static bool_t existTab;
static int8_t reverseCommand1[30];
static int8_t reverseCommand2[30];
static int8_t reverseCommand3[30];
static int8_t lengthCommand1;
static int8_t lengthCommand2;
static int8_t lengthCommand3;


/************* Private function prototypes ************************************************/

/**
 *
 *  @brief Convert integer to ascii
 *
 *  @param [in,out] **pBuf - memory
 *  @param [in] d - Start address in memory
 *  @param [in] base - Base number of.
 *  @param [in] length - The length is required in the memory or the length of the number
 *  @param [in] fillupZero - If the output is output with zeros.
 *
 *  @return void
 *
 */
void io_itoa(int8_t** pBuf, uint32_t d, int32_t base, int32_t length, bool_t fillupZero);

/**
 *
 * 	@brief Convert float to ascii
 *
 *  @param [in,out] **pBuf - memory buffer
 *  @param [in] f - Start address in memory
 *  @param [in] length - The length is required in the memory or the length of the number
 *  @param [in] fillupZero - If the output is output with zeros.
 *
 * 	@return void
 *
 */
void io_ftoa(int8_t** pBuf, float32_t f, int32_t length, bool_t fillupZero);

/**
 *
 *  @brief 	Writes arguments va to buffer buf according to format pFmt
 *
 *  @param [in,out] *pBuf - memory buffer
 *  @param [in,out] *pFmt -  Data for output
 *  @param [in] va - list with output parameters
 *
 *  @return  Length of string
 *  @return IO_PRINTF_WRONG_FORMAT_SPECIFIER
 *
 */
int32_t io_formatstring(int8_t* pBuf, const int8_t* pFmt, va_list va);

/**
 *
 *  @brief 	Calculate maximum length of the resulting string from the
 *            format string and va_list va
 *
 *  @param [in,out] *pFmt -  Data for output
 *  @param [in] va - list with output parameters
 *
 *  @return	Maximum length
 *
 */
int32_t io_formatlength(const int8_t* pFmt, va_list va);

/**
 *
 *  @brief 	Calculate maximum length of the resulting string from the
 *            format string and va_list va
 *
 *  @param [in,out] **pFmt - Data for output
 *  @param [in,out] *pLength - Complete length of the output stream with zeros.
 *  @param [in,out] *pFillupZero - If the output is output with zeros.
 *
 *  @return	Maximum length
 *
 */
int32_t io_getLength(const int8_t** pFmt, int32_t* pLength, bool_t* pFillupZero);

/************* Public functions ***********************************************************/
int8_t io_init(uart_t* pUart, int8_t* pRecBuf, uint32_t recBufSize, int8_t* pSendBuf, uint32_t sendBufSize, uint32_t *pSendBufTimestamp, uint32_t *pRecBufTimestamp)
{
//	uart.baudrate = 57600;
//	uart.baudrate = 941176;
//	uart.baudrate = 9600;
	uart.baudrate = 115200;
#ifdef Auto
	uart.channel  = UART_CH5;
#endif
#ifdef Board
	uart.channel  = UART_CH4;
#endif

	uart_init(&uart, pRecBuf, recBufSize, pSendBuf, sendBufSize, pSendBufTimestamp, pRecBufTimestamp);
	return 0;
}

int8_t io_printf( const char_t* pFmt, ...)
{
	int32_t length = 0;
	int32_t tmp = 0;
	va_list va;
	va_start(va, pFmt);
	length = io_formatlength((int8_t*) pFmt, va);
	va_end(va);
	{
		int8_t buf[length];
		va_start(va, pFmt);
		length = io_formatstring(buf,(int8_t*) pFmt, va);
		while (tmp < length) {
			tmp += uart_writeString(&uart, buf + tmp, length);
		}
		va_end(va);
	}
	return tmp;
}


int8_t io_scanf(const char_t* pFmt, ...)
{
	va_list va;
	int8_t data = 0, n, i = 0;
	char_t value[INPUT_SIZE];
	bool_t firstString = 1;

	do {
		n = 0;
		n = uart_readByte(&uart, &data);
		if (n > 0) {
			uart_writeByte(&uart, data);
			value[i] = data;
			i++;
		}
		if (i == INPUT_SIZE) {
			return IO_SCANF_INPUT_SIZE_FULL; /* Eingabe Puffer (value[]) ist voll */
		}

	} while (data != '\r'); /* Eingabe im Terminal beendet. */
	value[i - 1] = '\0'; /* Am Ende des char Array das CR mit \0 ersetzen. */

	if (n > 0) {
		va_start(va, pFmt);
		char_t *pEnd = (char_t *)value;
		while (*pFmt) {
			if (*pFmt == '%') {
				++pFmt;
				switch (*pFmt) {
					case 'c':
					{
						int8_t* pListC = va_arg(va, int8_t*);
						if (*pEnd != value[0]) {
							pEnd++; /* Leerzeichen �berspringen wenn es nicht an 1. Stelle vom Array ist */
						}
						*pListC = *pEnd;
						pEnd++;
						pFmt++;
						break;
					}
					case 'd':
					case 'i':
					case 'u':
					{
						int32_t *pListInt = va_arg(va, int32_t*);
						*pListInt = strtol(pEnd, &pEnd, 10); /* char array konvertieren nach int */
						pFmt++;
						break;
					}
					case 's':
					{

						int8_t *pListString = va_arg(va, int8_t*);
						if(firstString == 0) {
							pEnd++;
						}
						if(*pEnd == ' ') {
							pEnd++;
						}

						while ((*pEnd != ' ') && (*pEnd != '\0')) { /* schreibt solange an die pListString Adresse bis das Null-Zeichen bei pEnd auftaucht */
							*pListString = *pEnd;
							pListString++;
							pEnd++;
						}
						*pListString = '\0'; /* setzt am Ende des gelesenen Strings das Null-Zeichen */
						pFmt++;
						firstString = 0;
						break;
					}
					case 'x':
					case 'X':
					{
						int32_t *pListHex = va_arg(va, int32_t*);
						*pListHex = strtol(pEnd, &pEnd, 16);
						pFmt++;
						break;
					}
					case 'f': /* floating point */
					case 'e':
					case 'g':
					{
						float32_t *pListF32 = va_arg(va, float32_t*);
						*pListF32 = strtod(pEnd, &pEnd); /* char array konvertieren nach float */
						pFmt++;
						break;
					}
					default:
						/* Error ausgeben */
						return IO_SCANF_WRONG_FORMAT_SPECIFIER;
				}
			}
			if (*pFmt != '\0') { /* damit der pFmt Pointer nicht �ber den Adressbereich der abgelegten Variablen zeigt */
				++pFmt;
			}
		}
	}
	return 0;
}

int8_t io_getchar(void)
{
	int8_t data = -1, n = 0;

	n = uart_readByte(&uart, &data);
	if (n < 1) {
		data = n;
	}
	return data; /* Eingelesenes Zeichen, oder Error zur�ck geben. */
}

int8_t io_getline(int8_t *pBuf, int8_t size)
{
	int8_t i, charCount = 0;
	int8_t* pUartAdr = NULL;

	if(size > uart.sendring.size) {
		return -1; //Fehler - size darf nicht gr��er sein als die size vom Ringbuffer
	}

	 uart_readByte(&uart, pUartAdr);

	for(i=0; i<=size; i++) {

		if(((*pUartAdr == '\n') || (*pUartAdr == '\r')) && (i < 1)) { //
			pUartAdr++;
		}

		*pBuf = *pUartAdr;
		charCount++;

		if((*pBuf == '\n') || (*pBuf == '\r')) { /* Determine lines end with CR. */
			return charCount;
		}
		pBuf++;
		pUartAdr++;
	}

	return charCount;
}

int8_t io_getlineTerminal(int8_t *pResolution)
{
	int8_t data = 0, n, i = 0, nrTab = 1, x;// lengthTmp = 0;// test variable
	int8_t value[INPUT_SIZE];

	do {

		n = 0;
		n = uart_readByte(&uart, &data);

		if(data == '\t') { 	/* TAB push ? */
			n = 0;	
	//		uart_writeString(&uart,'\0',lengthTmp);
			if(existTab == TRUE) { /* If true as soon as something else will enter except TAB. */

				/* todo
				 *  Sobald ein l�ngerer String eingeben wird und danach ein k�rzrer kommt(TAB) wird nicht alles vom
				 *  l�ngeren string gel�scht.
				 */
/* --> lengthTmp never used! */
				/* How long was the longest input string? Is required to completely delete the */
				/* previous entry, and jump on since the beginning of the line. */
				/*
				if ((lengthCommand1 >= lengthCommand2) && (lengthCommand1 >= lengthCommand3)) {
					lengthTmp = lengthCommand1;

				} else if (lengthCommand2 >= lengthCommand3) {
					lengthTmp = lengthCommand2;

				} else { //kann nur l�nge3 am groessten sein
					lengthTmp = lengthCommand3;

				}

				uart_writeByte(&uart, '\r');
				*/
//				uart_writeString(&uart,'\0',lengthTmp);
/*				for(x=0;x<lengthTmp;x++) {
					uart_writeByte(&uart, '\0');
				}
*/
				switch(nrTab) { /* The last commands repeatedly spend. */
					case 1:
					{
						uart_writeString(&uart, reverseCommand1, lengthCommand1); /* The last command */
						for(x=0;x<lengthCommand1-1;x++) {
							value[x] = reverseCommand1[x];
						}
						i = lengthCommand1-1;
						break;
					}
					case 2:
					{
						uart_writeString(&uart, reverseCommand2, lengthCommand2); /* The penultimate command */
						for(x=0;x<lengthCommand2-1;x++) {
							value[x] = reverseCommand2[x];
						}
						i = lengthCommand2-1;
						break;
					}
					case 3:
					{
						uart_writeString(&uart, reverseCommand3, lengthCommand3); /* The antepenultimate command */
						for(x=0;x<lengthCommand3-1;x++) {
							value[x] = reverseCommand3[x];
						}
						i = lengthCommand3-1;
						break;
					}
					default:
						break;
				}
				nrTab++;
				if(nrTab > 3) {
					nrTab = 1;
				}
			}
			data = '\0';
		}
		if (n > 0) {
			uart_writeByte(&uart, data);
			value[i] = data;
			i++;
		}

		if(value[i-1] == '\177') { /* Backspace */
			if (i >= 2) {
				i = i - 2;
			}
		}

		if (i == INPUT_SIZE) {
			return IO_GETLINE_INPUT_SIZE_FULL;
		}

	} while (data != '\r');

	/* Special conversion for the terminal module. */
	value[i - 1] = ' ';
	value[i] = '\0';
	value[i+1] = '\0';
	value[i+2] = ' ';

	existTab = TRUE; /* A command was entered for the first time. It can now be used TAB. */


	int8_t *pEnd = value;

	/* The command should only be realized if it is not to the same as before. */
	if(reverseCommand1 != value) {

		/* The last command entered will appear when using TAB as the first. */
		/* This instruction is always one displaced rearward when a new command has been entered. */
		lengthCommand3 = lengthCommand2;
		lengthCommand2 = lengthCommand1;
		lengthCommand1 = i;

		for(i=0;i<=29;i++) {
			reverseCommand3[i] = reverseCommand2[i];
			reverseCommand2[i] = reverseCommand1[i];
			reverseCommand1[i] = value[i];
		}
	}

	while (*pEnd != '\r') {
		*pResolution = *pEnd;
		pResolution++;
		pEnd++;
	}

	return 0;
}

/************* Private functions **********************************************************/

void io_itoa(int8_t** pBuf, uint32_t d, int32_t base, int32_t length, bool_t fillupZero)
{
	char_t fillupChar;
	if(fillupZero == TRUE) {
		fillupChar = '0';
	} else {
		fillupChar = ' ';
	}
	int32_t tmp = (log10(d) / log10(base)) + 1;
	for (int32_t i = 0; i < length - tmp; i++) {
		*((*pBuf)++) = fillupChar;
	}
	int32_t div = 1;
	while (d / div >= base) {
		div *= base;
	}

	while (div != 0) {
		int32_t num = d / div;
		d = d % div;
		div /= base;
		if (num > 9)
			*((*pBuf)++) = (num - 10) + 'a';
		else
			*((*pBuf)++) = num + '0';
	}
}

void io_ftoa(int8_t** pBuf, float32_t f, int32_t length, bool_t fillupZero)
{
	if (f < 0.0) {
		*((*pBuf)++) = '-';
		f *= -1;
	}

	if (length == 0) {
		length = DEFAULT_DECIMAL_PLACES;
	}
	uint32_t preFrac = (uint32_t)f;
	io_itoa(pBuf, preFrac, 10, 0, 0);
	*((*pBuf)++) = '.';
	int32_t desiredLength = pow(10, length);
	uint32_t frac = round((f - preFrac) * desiredLength);

	io_itoa(pBuf, frac, 10, length, 1);
}

int32_t io_formatstring(int8_t* pBuf, const int8_t* pFmt, va_list va)
{
	int8_t *start_buf = pBuf;
	bool_t fillupZero = FALSE;
	bool_t lengthSpecified = FALSE;
	int32_t length;
	while (*pFmt) {
		if (*pFmt == '%') {
			lengthSpecified = FALSE;
			fillupZero = FALSE;
			length = 0;
		}
		/* Character needs formating? */
		if (*pFmt == '%' || lengthSpecified == TRUE) {
			switch (*(++pFmt)) {
				case '.':
				case '0':
				case '1':
				case '2':
				case '3':
				case '4':
				case '5':
				case '6':
				case '7':
				case '8':
				case '9':
					io_getLength(&pFmt, &length, &fillupZero);
					lengthSpecified = TRUE;
					break;
				case 'c':
					for (uint32_t i = 0; i < length - 1; i++) {
						if (fillupZero == TRUE) {
							*pBuf++ = '0';
						} else {
							*pBuf++ = ' ';
						}
					}
					*pBuf++ = va_arg(va, int32_t);
					lengthSpecified = FALSE;
					pFmt++;
					break;
				case 'd':
				case 'i':
				{
					int32_t val = va_arg(va, int32_t);
					if (val < 0) {
						val *= -1;
						*pBuf++ = '-';
					}
					io_itoa(&pBuf, val, 10, length, fillupZero);
					lengthSpecified = FALSE;
					pFmt++;
				}
					break;
				case 'f':
				{
					io_ftoa(&pBuf, va_arg(va, double), length, fillupZero);
					lengthSpecified = FALSE;
					pFmt++;
					break;
				}
				case 's':
				{
					int8_t *arg = va_arg(va, int8_t*);
					while (*arg) {
						*pBuf++ = *arg++;
					}
				}
					lengthSpecified = FALSE;
					pFmt++;
					break;
				case 'u':
					io_itoa(&pBuf, va_arg(va, uint32_t), 10, length, fillupZero);
					lengthSpecified = FALSE;
					pFmt++;
					break;
				case 'x':
				case 'X':
					io_itoa(&pBuf, va_arg(va, int32_t), 16, length, fillupZero);
					lengthSpecified = FALSE;
					pFmt++;
					break;
				case '%':
					*pBuf++ = '%';
					lengthSpecified = FALSE;
					pFmt++;
					break;
				default:
					return IO_PRINTF_WRONG_FORMAT_SPECIFIER;
			}
		}
		/* Else just copy */
		else {
			*pBuf++ = *pFmt++;
		}
	}
	*pBuf = 0;

	return (int32_t)(pBuf - start_buf);
}

int32_t io_getLength(const int8_t** pFmt, int32_t* pLength, bool_t* pFillupZero)
{
	switch (**pFmt) {
		case '.':
		*pFillupZero = TRUE;
		*pLength = 0;
		break;
		case '0':
			if (*pLength == 0) {
				*pFillupZero = TRUE;
			} else {
				*pLength = *pLength * 10;
			}
			break;
		case '1':
			*pLength = *pLength * 10 + 1;
			break;
		case '2':
			*pLength = *pLength * 10 + 2;
			break;
		case '3':
			*pLength = *pLength * 10 + 3;
			break;
		case '4':
			*pLength = *pLength * 10 + 4;
			break;
		case '5':
			*pLength = *pLength * 10 + 5;
			break;
		case '6':
			*pLength = *pLength * 10 + 6;
			break;
		case '7':
			*pLength = *pLength * 10 + 7;
			break;
		case '8':
			*pLength = *pLength * 10 + 8;
			break;
		case '9':
			*pLength = *pLength * 10 + 9;
			break;
		default:
			return -1;
	}
	return 0;
}

int32_t io_formatlength(const int8_t* pFmt, va_list va)
{
	int32_t length = 0;
	while (*pFmt) {
		if (*pFmt == '%') {
			++pFmt;
			switch (*pFmt) {
				case 'c':
					va_arg(va, int32_t);
					++length;
					break;
				case 'd':
				case 'i':
				case 'u':
					/* 32 bits integer is max 11 characters with minus sign */
					length += 11;
					va_arg(va, int32_t);
					break;
				case 'f':
				{
					float_t tmp = va_arg(va, double_t);
					tmp = log10(tmp); /* log10 is the amount of digits minus the first */
					length += (tmp + 9); /* amount of digits + highest digit + minus sign + decimal point + 6 decimal places */
				}
					break;
				case 's':
				{
					int8_t * str = va_arg(va, int8_t *);
					while (*str++)
						++length;
				}
					break;
				case 'x':
				case 'X':
					/* 32 bits integer as hex is max 8 characters */
					length += 8;
					va_arg(va, uint32_t);
					break;
				default:
					++length;
					break;
			}
		} else {
			++length;
		}
		++pFmt;
	}
	return length;
}


