/** *****************************************************************************************
 * Unit in charge:
 * @file	ringbuf.c
 * @author	Matthias Reinhard
 * $Date:: 2016-11-22 17:15:32 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2060                  $
 * $Author:: mbrieske           $
 * 
 * ----------------------------------------------------------
 *
 * @brief  Provides the ring buffer.
 *
 ******************************************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "sys/util/ringbuf.h"
#include "sys/systime/systime.h"
#include "per/irq.h"

/**
 * \defgroup errMessRB ring buffer error messages
 */
/*@{*/
#define RINGBUF_NOBUF_INIT 		-1		/**< Ring buffer could not be initialized.*/
#define RINGBUF_NOT_ENOUGHT_SPACE	-2	/**< Not enough space in ringBuffer */
#define RINGBUF_ERR_NULLPOINTER	-3		/**< Ring buffer or initialized data for ring buffer does not exist. */
/*@}*/

int8_t ringbuf_init(ringbuf_t *pRing, ringbuf_init_t *pInitData)
{
	if(pRing == NULL || pInitData == NULL) {
		return RINGBUF_ERR_NULLPOINTER;
	}
	pRing->wIndex 		= 0; /* head */
	pRing->rIndex 		= 0; /* tail */
	pRing->count 		= 0;
	pRing->size 		= pInitData->size; 		/* Ringgr��e */
	pRing->pData 		= pInitData->memory;
	pRing->pTimestamp 	= pInitData->timestamp;
	return 0;
}

int8_t ringbuf_write(ringbuf_t *pRingBuf, int8_t data) /* einzelnes Zeichen in ringBuffer */
{
	if(pRingBuf == NULL) {
		return RINGBUF_NOBUF_INIT;	/* Keine ringBuffer gr��e initialisiert */
	}
	if(pRingBuf->count == pRingBuf->size) {
		io_printf("P");
		return 0;	/* Error - not enough space in ringBuffer */
	}

	irq_disable();
	pRingBuf->pData[pRingBuf->wIndex] = data;
	pRingBuf->pTimestamp[pRingBuf->wIndex] = systime_getSysTime();
	pRingBuf->wIndex = (pRingBuf->wIndex + 1) % pRingBuf->size;
	pRingBuf->count++;
	irq_enable();

	return 1;
}

int32_t ringbuf_writeString(ringbuf_t *pRingBuf, int8_t *pData, uint16_t size)
{
	uint8_t i;
	uint8_t tmp_length = pRingBuf->size - pRingBuf->count;

	if(pRingBuf == NULL) {
	//	io_printf("ER1\n");
		return RINGBUF_NOBUF_INIT;	//Keine ringBuffer gr��e initialisiert
	}
	if(0 >= tmp_length) { //Kann in den ringBuffer noch was rein schreiben ohne das nicht gelesene Werte �berschrieben werden.
	//	io_printf("ER1\n");
		return 0;	//Error - not enough space in ringBuffer
	}
	if(tmp_length < size) { // ringBuffer kann nicht die kompletten Daten beschreiben sondern nur ein Teil
	//	io_printf("ER3\n");
		size = tmp_length;
	}

	irq_disable();
	for(i = 0; i < size; i++) {
		pRingBuf->pData[pRingBuf->wIndex] = pData[i];
		pRingBuf->pTimestamp[pRingBuf->wIndex] = systime_getSysTime();
		pRingBuf->wIndex = (pRingBuf->wIndex + 1) % pRingBuf->size;
		pRingBuf->count++;
	}
	irq_enable();
	return size;

/*	Haukes Version:
	uint8_t i = 0;
	int8_t n = 0;
	do {
		n = ringBuf_write(pRingBuf, pData[i++]);
		if (n < 0) {
			return n;
		}
	} while (n > 0 && i < size);
	return i;    */
}


int8_t ringbuf_read(ringbuf_t *pRingBuf, int8_t *pData)
{
	if(pRingBuf == NULL) {
		return RINGBUF_NOBUF_INIT;
	}
	if(pRingBuf->count == 0) {
		return 0; //es gibt nichts zu lesen
	}
	irq_disable();
	*pData = pRingBuf->pData[pRingBuf->rIndex];
	pRingBuf->rIndex = (pRingBuf->rIndex + 1) % pRingBuf->size;
	pRingBuf->count--;
	irq_enable();
	return 1;
}

/* Read + timestamp */

int8_t ringbuf_read_timed(ringbuf_t *pRingBuf, timed_byte_t *pData)
{
	if(pRingBuf == NULL) {
		return RINGBUF_NOBUF_INIT;
	}
	if(pRingBuf->count == 0) {
		return 0; /* es gibt nichts zu lesen */
	}
	irq_disable();
	pData->data = pRingBuf->pData[pRingBuf->rIndex];
	pData->timestamp = pRingBuf->pTimestamp[pRingBuf->rIndex];
	pRingBuf->rIndex = (pRingBuf->rIndex + 1) % pRingBuf->size;
	pRingBuf->count--;
	irq_enable();
	return 1;
}

int32_t ringbuf_readString(ringbuf_t *pRingBuf, int8_t *pData, uint16_t maxSize)
{
	uint8_t i, tmpCount;

	if(pRingBuf == NULL) {
		return RINGBUF_NOBUF_INIT;
	}
	if(pRingBuf->count == NULL) {
		return 0; /* es gibt nichts zu lesen */
	}

	if(pRingBuf->count > maxSize) {
		tmpCount = maxSize;
	}
	else {
		tmpCount = pRingBuf->count;
	}
	irq_disable();
	for(i = 0; i < tmpCount; i++) {
		pData[i] = pRingBuf->pData[pRingBuf->rIndex];
		pRingBuf->rIndex = ((pRingBuf->rIndex + 1) % pRingBuf->size);
		pRingBuf->count--;
	}
	irq_enable();
	return tmpCount;
}
