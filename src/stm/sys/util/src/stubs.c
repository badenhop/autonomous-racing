/**
*****************************************************************************
**
**  File        : stubs.c
**
**  Abstract    : Stubs for replacing functions, macros and data that do not yet exist.
**
**  Functions   : <constantly changing>
**
**  Environment : Atollic TrueSTUDIO(R)
**                STMicroelectronics STM32F4xx Standard Peripherals Library
**
**  Distribution: The file is distributed �as is,� without any warranty
**                of any kind.
**
**  Author		: Bernhard Kaiser
**
**
**
*****************************************************************************
*/

/* Includes */
#include "brd/startup/stm32f4xx.h"
// #include "stm32f4_discovery.h"

//#include "../../brd/types.h"

#include "brd/discovery/stm32f4_discovery.h"
#include "brd/cmsis/core_cmInstr.h"
#include "sys/util/io.h"
#include "sys/util/stubs.h"
#include "sys/sched/scheduler.h"
#include "sys/systime/systime.h"


/* Private macro */
/* Private variables */
/* Private function prototypes */
/* Private functions */

/* Global Variables */


static const char strMonitorTasklist[] = "\n              ***** Tasklist *****			\n\
																	                    \n\
Available Tasks are:																	\n\
====================																	\n\
																						\n\
   0 - Toggle LED 3												                        \n\
   1 - Toggle LED 4																		\n\
   2 - Toggle LED 5																		\n\
   3 - Toggle LED 6																		\n\
   4 - *not defined yet*										                        \n\
   5 - *not defined yet*																\n\
\n";



int16_t parDisplayAttrRead(param_id_t param, param_display_attr_t *dest)
{

	if (param.grp >= 1 && param.grp <= 5)    //FRY ... Integer
	{
		*dest = PAR_SIGNED;
	}

	else									//FRY ... Floating Point
	{
		*dest = PAR_FLOAT;
	}

	return 0;
}



// Typedefs, Constants and Functions that will later be provided by SW Component ??? (Serial Comm Handler)


//uint16_t SCI_Init(USART_PORT port, uint32_t baudRate, uint8_t dataLen, uint8_t nrStopBits, PARITY parity,
//							SCI_MODE mode, SER_COMM_PROT commProt)
//{
//	return 0;
//}

// Functions that will later be provided by SW Component EVENTMGR (Event and Fault Manager)



/****************************************************************************

 void taskList(void)

 This function print all available tasks.

 Arguments:			-
 Returns:			-
 Reads Globals:		-
 Changes Globals:	-
 Called by:			cmd_TL
 Calls:				strMonitorTasklist
 Uses result from:	-
 Provides data to:	-

 ****************************************************************************/

void taskList(void){
	io_printf("\n%s", strMonitorTasklist);
	return;
}



void EVENTMGR_resetAllFaults(void)
{
	io_printf(" >>> Reset All Faults - Not working yet");
	return;
}

// Dummy stuff for test purposes


void test(void)
{
	io_printf("Test\n");
	return;
}

void delay(uint32_t waitTimeMs)		// blocking wait
{
	uint32_t starttime = systime_getSysTime();
	while (systime_getSysTime() <= starttime + waitTimeMs)
			{
				__NOP();	// do nothing
			}

}

// Functions for port to specific platform; to be adapted and moved to another file

interrupt_ena_status_t disableInterrupts(void)
{
	// place appropriate code here!
	return 0; // this is just dummy!
}

void restoreInterrupts(interrupt_ena_status_t oldStatus)
{
	// place appropriate code here!
}

void enableInterrupts(void)
{
	// place appropriate code here!
}

// Dummy functions for Testing Purposes

void vInitLedsForDummyApp(void) /* Initializes 4 LEDs on eval board for playing around */
{
	  /* Initialize LEDs */
	  STM_EVAL_LEDInit(LED3);
	  STM_EVAL_LEDInit(LED4);
	  STM_EVAL_LEDInit(LED5);
	  STM_EVAL_LEDInit(LED6);
}


void vTaskToggleLED3(void)
{
	STM_EVAL_LEDToggle(LED3);
}

void vTaskToggleLED4(void)
{
	STM_EVAL_LEDToggle(LED4);
}

void vTaskToggleLED5(void)
{
	STM_EVAL_LEDToggle(LED5);
}

void vTaskToggleLED6(void)
{
	STM_EVAL_LEDToggle(LED6);
}

void myDummyAppInit(void)
{
	vInitLedsForDummyApp(); // for now, init 4 LEDs for playing around
	sched_registerTask(vTaskToggleLED3, "ToggleLED3",1);
	sched_registerTask(vTaskToggleLED4, "ToggleLED4",2);
	sched_registerTask(vTaskToggleLED5, "ToggleLED5",3);
	sched_registerTask(vTaskToggleLED6, "ToggleLED6",4);
	sched_activateTask(1);  // start one first task
	return;
}

