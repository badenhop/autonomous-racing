/** *****************************************************************************************
 * Unit in charge:
 * @file	bitarithmetics.h
 * @author	Bernhard Kaiser <bernhard.kaiser@berner-mattner.com>
 * @author	Hauke Petersen <mail@haukepetersen.de>
 * $Date:: 2014-05-08 18:05:37 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 449                   $
 * $Author:: wandji-kwuntchou   $
 * 
 * ----------------------------------------------------------
 *
 * @brief 	A collection of useful macros for bit arithmetics
 *
 ******************************************************************************************/

#ifndef BITARITHMETICS_H_
#define BITARITHMETICS_H_

/************* Includes *******************************************************************/


/************* Public typedefs ************************************************************/


/************* Macros and constants *******************************************************/
#define BITARITHMETICS_SET(INTEGERVAR,NR) ((INTEGERVAR) |= 1UL << (NR))
#define BITARITHMETICS_CLR(INTEGERVAR,NR) ((INTEGERVAR) &= ~(1UL << (NR)))
#define BITARITHMETICS_TEST(INTEGERVAR,NR) ((INTEGERVAR) & 1UL << (NR))

/************* Public function prototypes *************************************************/


#endif /* BITARITHMETICS_H_ */
