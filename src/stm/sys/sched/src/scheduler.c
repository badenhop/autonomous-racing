/** *****************************************************************************************
 * Unit in charge:
 * @file	scheduler.c
 * @author	Bernhard Kaiser <bernhard.kaiser@berner-mattner.com>
 * @author	Hauke Petersen <mail@haukepetersen.de>
 * $Date:: 2015-02-19 14:03:20 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1312                  $
 * $Author:: neumerkel          $
 * 
 * ----------------------------------------------------------
 *
 * @brief 	The file contains the implementation for a primitive scheduler base on periodic, fixed length time slots.
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include <string.h>

#include "sys/sched/scheduler.h"

#include "sys/util/bitarithmetics.h"
#include "per/irq.h"
#include "brd/startup/stm32f4xx_it.h"
#include "brd/startup/system_stm32f4xx.h"

/************* Private typedefs ***********************************************************/
typedef struct {
	 void(*taskCode)(void);		// pointer to the task code
	 const char *taskname;				// name of a task (for debug purposes)
	 uint8_t timeLevelMask;				// mask that assigns task to one out of 8 time levels
	 volatile uint8_t taskSstatus;		// status bits of a task
//	 uint8_t taskNr;					// number of task (must be identical to pos in task table!
} task_t;

/************* Macros and constants *******************************************************/
// Task attributes
#define TASK_ASYNC		(1 << 0)
#define TASK_ENABLE		(1 << 1)
#define TASK_PENDING 	(1 << 2)
#define TASK_RUNNING	(1 << 3)

#define TRUE				1
#define FALSE				0

#define CT_END (taskTable + numOfTasks)
#define TIMELEVEL(i) (1 << i)
//#define COUNTERINIT(i) (((i) / PRESCALE) - 1)
#define COUNTERINIT(i) ((i) - 1)

/************* Private static variables ***************************************************/
static uint8_t enabled = FALSE;
static uint32_t schedTime;								// the current scheduler time (will overflow every once in a while)
static uint8_t numOfTasks = 0;							// the number of currently registered tasks
static task_t taskTable[SCHED_MAX_TASKS];				// table containing the task data for all tasks in the system
static uint8_t tids[SCHED_MAX_TASKS];					// mapping between task IDs and entries in the task table

static volatile uint16_t counter[SCHED_NUMOF_TIMELEVELS];		// counters for each time level

static uint16_t timelevels[SCHED_NUMOF_TIMELEVELS] =		// reload-values for counters
															// enter here time intervals in
											       			// milliseconds (must be a multiple
															// of PRESCALE)
		{
			COUNTERINIT(SCHED_DEFAULT_TIMELEVEL_0),				/* Timelevel 0 */
			COUNTERINIT(SCHED_DEFAULT_TIMELEVEL_1),				/* Timelevel 1 */
			COUNTERINIT(SCHED_DEFAULT_TIMELEVEL_2),				/* Timelevel 2 */
			COUNTERINIT(SCHED_DEFAULT_TIMELEVEL_3),				/* Timelevel 3 */
			COUNTERINIT(SCHED_DEFAULT_TIMELEVEL_4),				/* Timelevel 4 */
			COUNTERINIT(SCHED_DEFAULT_TIMELEVEL_5),				/* Timelevel 5 */
			COUNTERINIT(SCHED_DEFAULT_TIMELEVEL_6),				/* Timelevel 6 */
			COUNTERINIT(SCHED_DEFAULT_TIMELEVEL_7)				/* Timelevel 7 */
		};


/************* Private function prototypes ************************************************/
/**
 * @brief	Activate a given task
 *
 * @param[in] pTask		A pointer to the task to activate
 */
static inline void activate(task_t *pTaskPtr);

/**
 * @brief	Suspend a given task
 *
 * @param[in] pTask		A pointer to the task to suspend
 */
static inline void suspend(task_t* pTaskPtr);

/**
 * @brief	Get the status of the given task
 *
 * @param[in] pTask		Pointer to the task who's status is of interest
 * @return The
 */
static inline uint8_t status(task_t* pTaskPtr);

/**
 * @brief	This function issues a fault message in case of an activation of a
 *			task that has not been finished before or is still pending and kills that task
 *
 *	TODO: implement
 *
 * @parem[in]	pActTask	the currently active task
 */
void secondActivation(task_t *pActTask);				// issues a fault message

/**
 * @brief	Insert a task at a given position into the task table, present tasks will be shiftet down the table.
 *
 * @param[in] i				Insert task into this position
 * @param[in] tid			The task ID of the new task
 * @param[in] fnct			Function pointer to the tasks code
 * @param[in] descriptor	Clear text descriptor of the task
 * @param[in] timelevel		The desired timelevel of the new task
 */
void insertTaskAt(uint8_t i, uint8_t tid, void(*fnct)(void), const char *descriptor, uint8_t timelevel);



/************* Public functions ***********************************************************/
int8_t sched_init(void)
{
	// to be checked and adapted (does enable the interrupt as well, which was not on purpose
	SysTick_Config(168000); 		// 1 ms if clock frequency 168 MHz
	SystemCoreClockUpdate();

	// initialize the task structure to zero
	memset(taskTable, 0, SCHED_MAX_TASKS * sizeof(task_t));

	return 0;
}

int8_t sched_start(void)
{
	enabled = TRUE;
	return 0;
}

int8_t sched_stop(void)
{
	enabled = FALSE;
	return 0;
}

uint32_t sched_getSchedTime(void)
{
	return schedTime;
}

// DEPRECATED?
//sched_task_t sched_getTask(uint8_t tid)
//{
//	return sched_taskTable[tid];
//}

int8_t sched_taskStatus(uint8_t tid)
{
	if(tid < numOfTasks) {
		tid = tids[tid];
		return status(taskTable + tid) & (TASK_ENABLE | TASK_RUNNING | TASK_PENDING);
	} else { 								// if task number >= NR_OF_TASKS task does not exist
		return SCHED_TASK_NOT_EXIST;
	}
}

int8_t sched_activateTask (uint8_t tid)
{
	if(tid < numOfTasks) {
		tid = tids[tid];
		activate(taskTable + tid);
		return 0;
	} else {
		return -1;
	}
}

int8_t sched_suspendTask (uint8_t tid)
{
	if(tid < numOfTasks) {
		tid = tids[tid];
		suspend(taskTable + tid);
		return 0;
	} else {
		return 1;
	}
}

int8_t sched_registerTask(void(*fnct)(void), const char *descriptor, uint8_t timelevel)
{
	uint8_t tid;
	uint8_t i;

	// see if maximum number of tasks is reached
	if (numOfTasks == SCHED_MAX_TASKS) {
		return -1;
	}

	// insert task into task table
	irq_disable();
	tid = numOfTasks;
	for (i = 0; i < numOfTasks; i++) {
		if (taskTable[i].timeLevelMask > TIMELEVEL(timelevel)) {
			insertTaskAt(i, tid, fnct, descriptor, timelevel);
			return tid;
		}
	}
	insertTaskAt(i, tid, fnct, descriptor, timelevel);
	irq_enable();

	return tid;
}

int8_t sched_setTimelevel(uint8_t timelevel, uint16_t period)
{
	if (timelevel < SCHED_NUMOF_TIMELEVELS) {
		timelevels[timelevel] = COUNTERINIT(period);
		return 0;
	} else {
		return -1;
	}
}




/************* Private functions **********************************************************/
static inline void activate(task_t *pTask)
{
	pTask->taskSstatus |= TASK_ENABLE;
}

static inline void suspend(task_t* pTask)
{
	pTask->taskSstatus &= ~(TASK_ENABLE | TASK_PENDING);
}

static inline uint8_t status(task_t* pTask)
{
	return pTask->taskSstatus;
}

void secondActivation(task_t *pActTask)
{
	for (;;) {
		// TODO fault handling
	}
	//kill(pActTask);
	//g_uiDiagnosis = pActTask - sched_taskTable + 20000;		// note task number (leave entries from 0 to 20000 to other services)
	//prompt("Second Activation");
}

void insertTaskAt(uint8_t i, uint8_t tid, void(*fnct)(void), const char *descriptor, uint8_t timelevel)
{
	for (uint8_t j = numOfTasks; j > i; j--) {
		memcpy(&taskTable[j], &taskTable[j - 1], sizeof(task_t));
	}
	for (uint8_t j = 0; j < numOfTasks; j++) {
		if (tids[j] >= i) {
			tids[j]++;
		}
	}
	taskTable[i].taskCode = fnct;
	taskTable[i].taskSstatus = 0;
	taskTable[i].taskname = descriptor;
	taskTable[i].timeLevelMask = TIMELEVEL(timelevel);
	tids[tid] = i;
	numOfTasks++;
}



/* Interrupt Handler ---------------------------------------------------------*/

/**
 *  @brief Scheduler entry function = Sys Tick Interrupt Handler (Name is predefined by STM-lib)
 *
 */
void SysTick_Handler(void)
{
	task_t *pActTask; 		// points to one task table entry
	uint8_t pendMsk;		// used to note current time level of THIS INCARNATION (scheduler is reentrant = can interrupt its former incarnations!)
	uint8_t i; 				// generic loop counter

	// increment the system time
	schedTime++;

	// only proceed, if the scheduler is enabled
	if (enabled == FALSE) {
		return;
	}

	// mark time levels to be activated
	irq_disable();
	pendMsk = 0;
	for (i = 0; i < SCHED_NUMOF_TIMELEVELS; i++) {				// for all time levels
		if (counter[i] == 0) {									// if time for timelevel i
			counter[i] = timelevels[i]; 						// restart time counter
			BITARITHMETICS_SET(pendMsk, i);						// note level i as pending
		}
		counter[i]--;
	}
	irq_enable();

	// mark pending tasks in control table and check for second activation
	irq_disable();
	for (pActTask = taskTable; pActTask < CT_END; pActTask++) {
		if (pActTask->taskSstatus & TASK_ENABLE) {
			if (pActTask->timeLevelMask & pendMsk) {			// check if task is pending
				if (pActTask->taskSstatus & TASK_PENDING) {
					secondActivation(pActTask); 				// if already pending -> fault!
				} else {
					pActTask->taskSstatus |= TASK_PENDING;		// set task pending
				}
			} else if (pActTask->taskSstatus & TASK_ASYNC) {	// if enabled and async
				pActTask->taskSstatus |= TASK_PENDING;
			}
		}
	}
	irq_enable(); // re-allow interrupts, including by other incarnations of scheduler


	// execute pending tasks in control table until running task encountered
	irq_disable();
	for (pActTask = taskTable; pActTask < CT_END; pActTask++) {
		if (pActTask->taskSstatus & TASK_RUNNING) {						// return (=go back to interrupted task called
			break; 														// from a former incarnation of scheduler)
		}
		if (pActTask->taskSstatus & TASK_PENDING) {
			pActTask->taskSstatus |= TASK_RUNNING; 						// mark task as running
			irq_enable();
			// execute the next task
			pActTask->taskCode();
			irq_disable();
			pActTask->taskSstatus &= ~(TASK_RUNNING | TASK_PENDING);	// task no longer running nor pending
		} // of if
	}
	irq_enable();
}
