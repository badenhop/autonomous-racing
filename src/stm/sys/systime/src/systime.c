/** *****************************************************************************************
 * Unit in charge:
 * @file	systime.c
 * @author	neumerkel
 * $Date:: 2015-08-18 14:28:01 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1643                  $
 * $Author:: Yoga        $
 *
 * ----------------------------------------------------------
 *
 * @brief This system module calculates the system time.
 * It uses the TIM5 periphery and increments the timer in the inrerrupt handler all 100�s.
 *
 ******************************************************************************************/


/************* Includes *******************************************************************/
#include "../systime.h"

#include "per/gpio.h"


/************* Global variables ***********************************************************/


/************* Private typedefs ***********************************************************/


/************* Macros and constants *******************************************************/
#define PriorityGroup_4		4 			/**< 4 bits for pre-emption priority
  *                                			 0 bits for subpriority*/

/************* Private static variables ***************************************************/
static uint32_t sysTime = 0;

/************* Private function prototypes ************************************************/
void timerInit(void);

/************* Public functions ***********************************************************/

int8_t systime_init(void)
{

	sysTime = 0;
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_4);
	timerInit();

	return 0;
}

uint32_t systime_getSysTime(void) {
	sysTime = TIM_GetCounter(TIM5);
	return sysTime;
}

void systime_reset() {
	sysTime = 0;

	return;
}

/************* Private functions **********************************************************/
void timerInit(void) {
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM5, ENABLE);

	TIM_TimeBaseInitTypeDef TIM_InitStructure;
	TIM_InitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_InitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_InitStructure.TIM_Prescaler = 84-1;
	TIM_InitStructure.TIM_Period = 0xFFFFFFFF;     /* timeout after 655 360 us (FFFFh = 16 bit) */
	TIM_TimeBaseInit(TIM5, &TIM_InitStructure);

	TIM_Cmd(TIM5, ENABLE);    /* enable timer */
}
