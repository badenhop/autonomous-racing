/** *****************************************************************************************
 * Unit in charge:
 * @file	systime.h
 * @author	neumerkel
 * $Date:: 2015-08-18 14:28:01 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1643                  $
 * $Author:: neumerkel			$
 *
 * ----------------------------------------------------------
 *
 * @brief This system module calculates the system time.
 * It uses the TIM5 periphery and increments the timer in the inrerrupt handler all 100�s.
 *
 ******************************************************************************************/


#ifndef SYSTIME_H_
#define SYSTIME_H_

/************* Includes *******************************************************************/
#include "brd/stm32f4xx_tim.h"
/************* Public typedefs ************************************************************/

/************* Macros and constants *******************************************************/

/************* Public function prototypes *************************************************/

int8_t systime_init(void);

uint32_t systime_getSysTime(void);

void systime_reset(void);

#endif /* SYSTIME_H_ */
