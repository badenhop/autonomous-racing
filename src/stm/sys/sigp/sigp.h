
/** *****************************************************************************************
 * Unit in charge:
 * @file	sigp.h
 * @author	Wandji-Kwuntchou
 * $Date:: 2017-09-06 12:43:27 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2158                  $
 * $Author:: mbrieske           $
 *
 * ----------------------------------------------------------
 *
 * @brief TODO What does the unit?
 *
 ******************************************************************************************/


#ifndef SIGP_H_
#define SIGP_H_

/************* Includes *******************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "brd/types.h"
/************* Public typedefs ************************************************************/

typedef struct heartbeat {
	uint8_t state; /*< system state*/
	uint8_t mavlink_version; /*< MAVLink version*/
	uint32_t lastReceiveTimestamp;
} heartbeat_t;

typedef struct carcontrol {
	float32_t vehSpdTgtExt;
	float32_t stAngTgtExt;
	uint32_t lastReceiveTimestamp;
} carcontrol_t;

typedef struct trajectory {
	 uint32_t timestamp; /*< timestamp*/
	 int16_t x[10]; /*< Lateral error*/
	 uint16_t y[10]; /*< Longitudinal error*/
	 int16_t theta[10]; /*< course angle error*/
	 int16_t kappa[10]; /*< ground curvature*/
	 int8_t v[10]; /*< nominal speed*/
	 uint32_t lastReceiveTimestamp;
} trajectory_t;

/************* Macros and constants *******************************************************/
#define SIGP_INIT_FAILED     -1              /**< The connection of the signal pool failed*/

/************* Public function prototypes *************************************************/
/*
 * @brief This function initialize the signal pool by connecting all the signals used.
 * For this purpose, it uses the macro signalConnect(module, variable) which connects the
 * private statics variables defined in the pool with the corresponding pointer defined in
 * the module (dri, dev, or app).
 *
 * @param [in] void
 *
 * @return SIGP_INIT_FAILED in case of error and 0 in case of successful initialization
 */
int8_t sigp_init(void);

#endif /* SIGP_H_ */
