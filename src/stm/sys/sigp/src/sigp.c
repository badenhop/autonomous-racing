/********************************************************************************************
 * Unit in charge:
 * @file    sigp.c
 * @author  Freyer / Wandji-Wuntchou
 * $Date: 2015-04-23 10:02:35 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2213                  $
 * $Author:: mbrieske                   $
 *
 * ----------------------------------------------------------
 *
 * @brief Application specific initializations (e.g. of periphery such as ADC, PWM for motor control)
 *
 ******************************************************************************************/



/************* Includes *******************************************************************/

#include <stdio.h>
#include "app/config.h"
#include "sys/sigp/sigp.h"

/************* Private typedefs ***********************************************************/



/************* Macros and constants *******************************************************/

#define signalConnect(module, signalName) \
		extern void* p##signalName##_##module;\
		p##signalName##_##module = &var##signalName

/************* Private static variables ***************************************************/

/* rx24f */

static float32_t varStAngTgtPiCtrl;

/*clocksync*/
static uint32_t varSyncOffset;

/*stm*/
static uint8_t varState;
static uint8_t varRequestedState;

/*mavlink*/
static heartbeat_t varHeartbeat;
static carcontrol_t varCarControl;
static trajectory_t varTrajectory;

/* rcrec */
static uint16_t varRcState;
static uint16_t varRcChannel1;
static uint16_t varRcChannel2;
static uint16_t varRcChannel3;
static uint16_t varRcChannel4;
static uint16_t varRcChannel5;
static uint16_t varRcChannel6;
static uint16_t varRcChannel7;
static uint16_t varRcChannel8;

/* servo */
static float32_t varMotTrqTgtSrv;
static float32_t varStAngTgt;
static float32_t varSrvAngVolt;
static float32_t varSrvAngAct;
static bool_t varRcOverrideStangEnabled;

/* stangproc */
static uint32_t varStAngTimestamp;
static float32_t varStAngAct;
static float32_t varStAngTgtSel;

/* distproc */
static uint32_t varDistUs;

/* wenc */
static int32_t varWhlTicksFL;
static int32_t varWhlTicksFR;
static int32_t varWhlTicksRL;
static int32_t varWhlTicksRR;
static int8_t varCarDirection;

/* mpu9150 */
static int32_t varAccelRawX;
static int32_t varAccelRawY;
static int32_t varAccelRawZ;
static float32_t varAccelGeeX;
static float32_t varAccelGeeY;
static float32_t varAccelGeeZ;
static int32_t varGyroRawX;
static int32_t varGyroRawY;
static int32_t varGyroRawZ;
static float32_t varGyroDpsX;
static float32_t varGyroDpsY;
static float32_t varGyroDpsZ;

/*	errorHandler	*/
static uint64_t varErrorRegister;

/* odom */
static uint32_t varOdomTimestamp;
static float32_t varVehXDistOdom;
static float32_t varVehYDistOdom;
static float32_t varVehYawAngOdom;
static float32_t varVehSpdOdom;
static int8_t varVehDirOdom;
static float32_t varLeftWheelSpeedOdom;
static float32_t varRightWheelSpeedOdom;

/************* Private function prototypes ************************************************/



/************* Public functions ***********************************************************/

int8_t sigp_init(void){

	int8_t errorFlag;

	/*clocksync*/
	signalConnect(clocksync, SyncOffset);

	/*stm*/
	signalConnect(stm, State);
	signalConnect(stm, RequestedState);
	signalConnect(stm, SyncOffset);
	signalConnect(stm, ErrorRegister);
	signalConnect(stm, RcState);
	signalConnect(stm, RcChannel5);

	/*errorHandler*/
	signalConnect(errorHandler, ErrorRegister);

	/*mavlink*/
	signalConnect(mavlink, SyncOffset);
	signalConnect(mavlink, Heartbeat);
	signalConnect(mavlink, State);
	signalConnect(mavlink, RequestedState);
	signalConnect(mavlink, ErrorRegister);

	/*mavlink odometry*/
	signalConnect(mavlink, OdomTimestamp);
	signalConnect(mavlink, StAngTimestamp);
	signalConnect(mavlink, VehSpdOdom);
	signalConnect(mavlink, VehXDistOdom);
	signalConnect(mavlink, VehYDistOdom);
	signalConnect(mavlink, VehYawAngOdom);
	signalConnect(mavlink, StAngAct);
	signalConnect(mavlink, LeftWheelSpeedOdom);
	signalConnect(mavlink, RightWheelSpeedOdom);

	/*mavlink carcontrol*/
	signalConnect(mavlink, CarControl);

	/*mavlink trajectory*/
	signalConnect(mavlink, Trajectory);

	/* latctrl */
	signalConnect(latctrl, Trajectory);

	/* rcrec */
	signalConnect(rcrec, RcState);
	signalConnect(rcrec, RcChannel1);
	signalConnect(rcrec, RcChannel2);
	signalConnect(rcrec, RcChannel3);
	signalConnect(rcrec, RcChannel4);
	signalConnect(rcrec, RcChannel5);
	signalConnect(rcrec, RcChannel6);
	signalConnect(rcrec, RcChannel7);
	signalConnect(rcrec, RcChannel8);

	/* servo */

	signalConnect(servo, StAngTgt);
	signalConnect(servo, MotTrqTgtSrv);
	signalConnect(servo, SrvAngVolt);
	signalConnect(servo, SrvAngAct);
	signalConnect(servo, RcOverrideStangEnabled);
	signalConnect(servo, RcChannel4);

	/* stangproc */
	signalConnect(stangproc, StAngTimestamp);
	signalConnect(stangproc, StAngTgt);
	signalConnect(stangproc, StAngAct);
	signalConnect(stangproc, StAngTgtSel);
	signalConnect(stangproc, SrvAngAct);

	/* stangsel */
	signalConnect(stangsel, State);
	signalConnect(stangsel, CarControl);
	signalConnect(stangsel, StAngTgtSel);
	signalConnect(stangsel, RcOverrideStangEnabled);

	/* rx24f device */
	signalConnect(rx24f, StAngTgtPiCtrl);
	signalConnect(rx24f, StAngTgtSel);
	signalConnect(rx24f, SrvAngAct);
	signalConnect(rx24f, RcOverrideStangEnabled);
	signalConnect(rx24f, RcChannel4);

	/* rx24f pi controller */
	signalConnect(rx24fctrl, StAngTgtSel);
	signalConnect(rx24fctrl, StAngTgtPiCtrl);
	signalConnect(rx24fctrl, SrvAngAct);

	/* vehspdctrl */
	signalConnect(vehspdctrl, State);
	signalConnect(vehspdctrl, VehSpdOdom);
	signalConnect(vehspdctrl, CarControl);
	signalConnect(vehspdctrl, RcChannel1);

	/* SRF02T */
	signalConnect(srf02t, DistUs);

	/* wenc */
	signalConnect(wenc, WhlTicksFL);
	signalConnect(wenc, WhlTicksFR);
	signalConnect(wenc, WhlTicksRL);
	signalConnect(wenc, WhlTicksRR);
	signalConnect(wenc, CarDirection);

	/* mpu9150 */
	signalConnect(mpu9150, AccelRawX);
	signalConnect(mpu9150, AccelRawY);
	signalConnect(mpu9150, AccelRawZ);
	signalConnect(mpu9150, GyroRawX);
	signalConnect(mpu9150, GyroRawY);
	signalConnect(mpu9150, GyroRawZ);
	signalConnect(mpu9150, AccelGeeX);
	signalConnect(mpu9150, AccelGeeY);
	signalConnect(mpu9150, AccelGeeZ);
	signalConnect(mpu9150, GyroDpsX);
	signalConnect(mpu9150, GyroDpsY);
	signalConnect(mpu9150, GyroDpsZ);

	/* odom */
	signalConnect(odom, WhlTicksFL);
	signalConnect(odom, WhlTicksFR);
	signalConnect(odom, WhlTicksRL);
	signalConnect(odom, WhlTicksRR);
	signalConnect(odom, CarDirection);
	signalConnect(odom, VehSpdOdom);
	signalConnect(odom, VehDirOdom);
	signalConnect(odom, VehXDistOdom);
	signalConnect(odom, VehYDistOdom);
	signalConnect(odom, VehYawAngOdom);
	signalConnect(odom, OdomTimestamp);
	signalConnect(odom, LeftWheelSpeedOdom);
	signalConnect(odom, RightWheelSpeedOdom);

	errorFlag = 1;
	if(errorFlag)
	{
		return 0;
	}
	else return SIGP_INIT_FAILED;
}

/************* Private functions **********************************************************/

