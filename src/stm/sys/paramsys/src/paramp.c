/********************************************************************************************
 * Unit in charge:
 * @file    paramp.c
 * @author  Freyer
 * $Date: 2015-09-14 15:55:45 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2213                  $
 * $Author:: mbrieske                   $
 *
 * ----------------------------------------------------------
 *
 * @brief Parameterpool V1.0
 *  This modul connect the parameters of the Parametersystem with the pointer in the moduls / apps / driver.
 *
 ******************************************************************************************/



/************* Includes *******************************************************************/

#include <stdio.h>

#include "sys/paramsys/paramsys.h"

extern paramSysGrp_t grpStruct[];

/************* Private typedefs ***********************************************************/



/************* Macros and constants *******************************************************/

#define paramConnect(module, paramName, Group, Index) \
     extern void* p##paramName##_##module;\
     p##paramName##_##module = &grpStruct[Group].startAddress[Index-1].paramActValue;

/************* Private static variables ***************************************************/



/************* Private function prototypes ************************************************/
int8_t paramsys_initConnect(void) {

    int8_t err = 0;

    /* servo */
    paramConnect(servo, InitAngSrv1, 15, 1);
    paramConnect(servo, MinAbsAngSrv1, 15, 2);
    paramConnect(servo, MaxAbsAngSrv1, 15, 3);
    paramConnect(servo, InitAngSrv2, 15, 4);
    paramConnect(servo, MinAbsAngSrv2, 15, 5);
    paramConnect(servo, MaxAbsAngSrv2, 15, 6);
    paramConnect(servo, SrvAngVoltMin, 15, 7);
    paramConnect(servo, SrvAngVoltMax, 15, 8);

    /* stangproc */
    paramConnect(stangproc, StAngMax, 18, 1);
    paramConnect(stangproc, StAngMin, 18, 2);
    paramConnect(stangproc, MinAbsAngSrv1, 15, 2);
    paramConnect(stangproc, MaxAbsAngSrv1, 15, 3);
    paramConnect(stangproc, StAngTgtFailSafe, 5, 3);

    return err;
}

/************* Private functions **********************************************************/

