/********************************************************************************************
 * Unit in charge:
 * @file    paramlist.c
 * @author  Automatisch via VBA erzeugt
 * $Date: 2015-09-14 15:55:38 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2022                  $
 * $Author:: Yoga               $
 *
 * ----------------------------------------------------------
 *
 * @brief Application specific initializations (e.g. of periphery such as ADC, PWM for motor control)
 *
 ******************************************************************************************/


/************* Includes *******************************************************************/

#include <string.h>

#include "sys/paramsys/paramsys.h"
#include "sys/paramsys/paramlist.h"
#include "sys/util/io.h"

/************* Private typedefs ***********************************************************/



/************* Macros and constants *******************************************************/



/************* Private static variables ***************************************************/



/************* Private function prototypes ************************************************/



/************* Public functions ***********************************************************/



/************* Private functions **********************************************************/


/* initialization of the  parameter groups (fill the groups with data) */
int16_t paramsys_setupGrp(paramSysGrp_t grpStructArray[]) {

int16_t err = 0;

/* 0101 */
strcpy(grpStructArray[1].startAddress[0].name, "STM32F4");            /*Name of Parameter*/
       grpStructArray[1].startAddress[0].numb = 101;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[0].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[1].startAddress[0].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[1].startAddress[0].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[0].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[0].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[0].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[1].startAddress[0].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[1].startAddress[0].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[1].startAddress[0].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[1].startAddress[0].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[0].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0102 */
strcpy(grpStructArray[1].startAddress[1].name, "uCVariant");          /*Name of Parameter*/
       grpStructArray[1].startAddress[1].numb = 102;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[1].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[1].startAddress[1].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[1].startAddress[1].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[1].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[1].varianz = PARAM_VARIABLE;    /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[1].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[1].startAddress[1].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[1].startAddress[1].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[1].startAddress[1].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[1].startAddress[1].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[1].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0103 */
strcpy(grpStructArray[1].startAddress[2].name, "SizeRAM");            /*Name of Parameter*/
       grpStructArray[1].startAddress[2].numb = 103;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[2].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[1].startAddress[2].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[1].startAddress[2].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[2].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[2].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[2].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[1].startAddress[2].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[1].startAddress[2].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[1].startAddress[2].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[1].startAddress[2].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[2].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0104 */
strcpy(grpStructArray[1].startAddress[3].name, "SizeFlash");          /*Name of Parameter*/
       grpStructArray[1].startAddress[3].numb = 104;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[3].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[1].startAddress[3].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[1].startAddress[3].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[3].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[3].varianz = PARAM_VARIABLE;    /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[3].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[1].startAddress[3].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[1].startAddress[3].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[1].startAddress[3].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[1].startAddress[3].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[3].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0105 */
strcpy(grpStructArray[1].startAddress[4].name, "RunFromFlash");       /*Name of Parameter*/
       grpStructArray[1].startAddress[4].numb = 105;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[4].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[1].startAddress[4].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[1].startAddress[4].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[4].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[4].varianz = PARAM_VARIABLE;    /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[4].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[1].startAddress[4].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[1].startAddress[4].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[1].startAddress[4].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[1].startAddress[4].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[4].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0106 */
strcpy(grpStructArray[1].startAddress[5].name, "reserved1");          /*Name of Parameter*/
       grpStructArray[1].startAddress[5].numb = 106;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[5].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[1].startAddress[5].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[1].startAddress[5].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[5].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[5].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[5].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[1].startAddress[5].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[1].startAddress[5].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[1].startAddress[5].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[1].startAddress[5].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[5].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0107 */
strcpy(grpStructArray[1].startAddress[6].name, "reserved2");          /*Name of Parameter*/
       grpStructArray[1].startAddress[6].numb = 107;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[6].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[1].startAddress[6].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[1].startAddress[6].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[6].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[6].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[6].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[1].startAddress[6].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[1].startAddress[6].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[1].startAddress[6].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[1].startAddress[6].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[6].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0108 */
strcpy(grpStructArray[1].startAddress[7].name, "reserved3");          /*Name of Parameter*/
       grpStructArray[1].startAddress[7].numb = 108;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[7].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[1].startAddress[7].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[1].startAddress[7].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[7].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[7].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[7].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[1].startAddress[7].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[1].startAddress[7].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[1].startAddress[7].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[1].startAddress[7].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[7].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0109 */
strcpy(grpStructArray[1].startAddress[8].name, "reserved4");          /*Name of Parameter*/
       grpStructArray[1].startAddress[8].numb = 109;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[8].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[1].startAddress[8].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[1].startAddress[8].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[8].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[8].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[8].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[1].startAddress[8].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[1].startAddress[8].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[1].startAddress[8].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[1].startAddress[8].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[8].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0110 */
strcpy(grpStructArray[1].startAddress[9].name, "EvalBrdType");        /*Name of Parameter*/
       grpStructArray[1].startAddress[9].numb = 110;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[9].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[1].startAddress[9].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[1].startAddress[9].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[9].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[9].varianz = PARAM_VARIABLE;    /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[9].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[1].startAddress[9].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[1].startAddress[9].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[1].startAddress[9].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[1].startAddress[9].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[9].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0111 */
strcpy(grpStructArray[1].startAddress[10].name, "EvalBrdVer");        /*Name of Parameter*/
       grpStructArray[1].startAddress[10].numb = 111;   /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[10].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[1].startAddress[10].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[1].startAddress[10].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[10].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[10].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[10].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[1].startAddress[10].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[1].startAddress[10].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[1].startAddress[10].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[1].startAddress[10].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[10].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/* 0112 */
strcpy(grpStructArray[1].startAddress[11].name, "CarrierBrdType");    /*Name of Parameter*/
       grpStructArray[1].startAddress[11].numb = 112;   /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[11].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[1].startAddress[11].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[1].startAddress[11].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[11].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[11].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[11].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[1].startAddress[11].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[1].startAddress[11].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[1].startAddress[11].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[1].startAddress[11].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[11].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/* 0113 */
strcpy(grpStructArray[1].startAddress[12].name, "CarrierBrdVer");     /*Name of Parameter*/
       grpStructArray[1].startAddress[12].numb = 113;   /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[12].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[1].startAddress[12].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[1].startAddress[12].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[12].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[12].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[12].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[1].startAddress[12].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[1].startAddress[12].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[1].startAddress[12].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[1].startAddress[12].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[12].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/* 0114 */
strcpy(grpStructArray[1].startAddress[13].name, "ApplHWType");        /*Name of Parameter*/
       grpStructArray[1].startAddress[13].numb = 114;   /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[13].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[1].startAddress[13].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[1].startAddress[13].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[13].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[13].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[13].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[1].startAddress[13].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[1].startAddress[13].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[1].startAddress[13].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[1].startAddress[13].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[13].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/* 0115 */
strcpy(grpStructArray[1].startAddress[14].name, "ApplHWVer");         /*Name of Parameter*/
       grpStructArray[1].startAddress[14].numb = 115;   /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[14].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[1].startAddress[14].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[1].startAddress[14].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[14].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[14].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[14].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[1].startAddress[14].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[1].startAddress[14].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[1].startAddress[14].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[1].startAddress[14].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[14].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/* 0116 */
strcpy(grpStructArray[1].startAddress[15].name, "MasterCtrlType");    /*Name of Parameter*/
       grpStructArray[1].startAddress[15].numb = 116;   /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[15].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[1].startAddress[15].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[1].startAddress[15].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[15].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[15].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[15].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[1].startAddress[15].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[1].startAddress[15].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[1].startAddress[15].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[1].startAddress[15].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[15].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/* 0117 */
strcpy(grpStructArray[1].startAddress[16].name, "MasterCtrlVer");     /*Name of Parameter*/
       grpStructArray[1].startAddress[16].numb = 117;   /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[1].startAddress[16].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[1].startAddress[16].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[1].startAddress[16].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[1].startAddress[16].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[1].startAddress[16].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[1].startAddress[16].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[1].startAddress[16].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[1].startAddress[16].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[1].startAddress[16].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[1].startAddress[16].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[1].startAddress[16].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/* 0201 */
strcpy(grpStructArray[2].startAddress[0].name, "SysSWVer");           /*Name of Parameter*/
       grpStructArray[2].startAddress[0].numb = 201;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[2].startAddress[0].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[2].startAddress[0].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[2].startAddress[0].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[2].startAddress[0].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[2].startAddress[0].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[2].startAddress[0].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[2].startAddress[0].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[2].startAddress[0].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[2].startAddress[0].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[2].startAddress[0].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[2].startAddress[0].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0202 */
strcpy(grpStructArray[2].startAddress[1].name, "BootloaderVer");      /*Name of Parameter*/
       grpStructArray[2].startAddress[1].numb = 202;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[2].startAddress[1].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[2].startAddress[1].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[2].startAddress[1].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[2].startAddress[1].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[2].startAddress[1].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[2].startAddress[1].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[2].startAddress[1].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[2].startAddress[1].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[2].startAddress[1].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[2].startAddress[1].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[2].startAddress[1].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0203 */
strcpy(grpStructArray[2].startAddress[2].name, "HWSWConfName");       /*Name of Parameter*/
       grpStructArray[2].startAddress[2].numb = 203;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[2].startAddress[2].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[2].startAddress[2].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[2].startAddress[2].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[2].startAddress[2].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[2].startAddress[2].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[2].startAddress[2].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[2].startAddress[2].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[2].startAddress[2].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[2].startAddress[2].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[2].startAddress[2].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[2].startAddress[2].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0204 */
strcpy(grpStructArray[2].startAddress[3].name, "HWSWConfVer");        /*Name of Parameter*/
       grpStructArray[2].startAddress[3].numb = 204;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[2].startAddress[3].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[2].startAddress[3].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[2].startAddress[3].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[2].startAddress[3].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[2].startAddress[3].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[2].startAddress[3].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[2].startAddress[3].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[2].startAddress[3].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[2].startAddress[3].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[2].startAddress[3].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[2].startAddress[3].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0205 */
strcpy(grpStructArray[2].startAddress[4].name, "ParSysVer");          /*Name of Parameter*/
       grpStructArray[2].startAddress[4].numb = 205;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[2].startAddress[4].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[2].startAddress[4].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[2].startAddress[4].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[2].startAddress[4].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[2].startAddress[4].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[2].startAddress[4].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[2].startAddress[4].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[2].startAddress[4].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[2].startAddress[4].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[2].startAddress[4].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[2].startAddress[4].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0206 */
strcpy(grpStructArray[2].startAddress[5].name, "ApplSWName");         /*Name of Parameter*/
       grpStructArray[2].startAddress[5].numb = 206;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[2].startAddress[5].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[2].startAddress[5].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[2].startAddress[5].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[2].startAddress[5].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[2].startAddress[5].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[2].startAddress[5].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[2].startAddress[5].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[2].startAddress[5].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[2].startAddress[5].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[2].startAddress[5].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[2].startAddress[5].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0207 */
strcpy(grpStructArray[2].startAddress[6].name, "ApplSWVer");          /*Name of Parameter*/
       grpStructArray[2].startAddress[6].numb = 207;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[2].startAddress[6].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[2].startAddress[6].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[2].startAddress[6].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[2].startAddress[6].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[2].startAddress[6].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[2].startAddress[6].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[2].startAddress[6].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[2].startAddress[6].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[2].startAddress[6].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[2].startAddress[6].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[2].startAddress[6].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0301 */
strcpy(grpStructArray[3].startAddress[0].name, "BatVoltageNom");      /*Name of Parameter*/
       grpStructArray[3].startAddress[0].numb = 301;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[3].startAddress[0].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[3].startAddress[0].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[3].startAddress[0].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[3].startAddress[0].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[3].startAddress[0].varianz = PARAM_VARIABLE;    /*is the Parameter variable or fix?*/
       grpStructArray[3].startAddress[0].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[3].startAddress[0].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[3].startAddress[0].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[3].startAddress[0].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[3].startAddress[0].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[3].startAddress[0].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0302 */
strcpy(grpStructArray[3].startAddress[1].name, "BatVoltageAct");      /*Name of Parameter*/
       grpStructArray[3].startAddress[1].numb = 302;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[3].startAddress[1].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[3].startAddress[1].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[3].startAddress[1].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[3].startAddress[1].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[3].startAddress[1].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[3].startAddress[1].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[3].startAddress[1].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[3].startAddress[1].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[3].startAddress[1].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[3].startAddress[1].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[3].startAddress[1].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0303 */
strcpy(grpStructArray[3].startAddress[2].name, "EnvTempAct");         /*Name of Parameter*/
       grpStructArray[3].startAddress[2].numb = 303;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[3].startAddress[2].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[3].startAddress[2].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[3].startAddress[2].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[3].startAddress[2].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[3].startAddress[2].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[3].startAddress[2].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[3].startAddress[2].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[3].startAddress[2].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[3].startAddress[2].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[3].startAddress[2].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[3].startAddress[2].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0304 */
strcpy(grpStructArray[3].startAddress[3].name, "BrdTempAct");         /*Name of Parameter*/
       grpStructArray[3].startAddress[3].numb = 304;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[3].startAddress[3].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[3].startAddress[3].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[3].startAddress[3].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[3].startAddress[3].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[3].startAddress[3].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[3].startAddress[3].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[3].startAddress[3].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[3].startAddress[3].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[3].startAddress[3].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[3].startAddress[3].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[3].startAddress[3].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0305 */
strcpy(grpStructArray[3].startAddress[4].name, "OperEnvType");        /*Name of Parameter*/
       grpStructArray[3].startAddress[4].numb = 305;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[3].startAddress[4].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[3].startAddress[4].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[3].startAddress[4].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[3].startAddress[4].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[3].startAddress[4].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[3].startAddress[4].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[3].startAddress[4].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[3].startAddress[4].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[3].startAddress[4].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[3].startAddress[4].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[3].startAddress[4].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0401 */
strcpy(grpStructArray[4].startAddress[0].name, "OffsetADC");          /*Name of Parameter*/
       grpStructArray[4].startAddress[0].numb = 401;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[4].startAddress[0].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[4].startAddress[0].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[4].startAddress[0].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[4].startAddress[0].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[4].startAddress[0].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[4].startAddress[0].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[4].startAddress[0].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[4].startAddress[0].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[4].startAddress[0].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[4].startAddress[0].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[4].startAddress[0].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0501 */
strcpy(grpStructArray[5].startAddress[0].name, "MotTrqTgtFailSafe");  /*Name of Parameter*/
       grpStructArray[5].startAddress[0].numb = 501;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[5].startAddress[0].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[5].startAddress[0].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[5].startAddress[0].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[5].startAddress[0].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[5].startAddress[0].varianz = PARAM_VARIABLE;    /*is the Parameter variable or fix?*/
       grpStructArray[5].startAddress[0].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[5].startAddress[0].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[5].startAddress[0].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[5].startAddress[0].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[5].startAddress[0].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[5].startAddress[0].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0502 */
strcpy(grpStructArray[5].startAddress[1].name, "VegSpdTgtFailSafe");  /*Name of Parameter*/
       grpStructArray[5].startAddress[1].numb = 502;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[5].startAddress[1].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[5].startAddress[1].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[5].startAddress[1].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[5].startAddress[1].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[5].startAddress[1].varianz = PARAM_VARIABLE;    /*is the Parameter variable or fix?*/
       grpStructArray[5].startAddress[1].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[5].startAddress[1].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[5].startAddress[1].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[5].startAddress[1].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[5].startAddress[1].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[5].startAddress[1].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0503 */
strcpy(grpStructArray[5].startAddress[2].name, "StAngTgtFailSafe");   /*Name of Parameter*/
       grpStructArray[5].startAddress[2].numb = 503;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[5].startAddress[2].variType = PARAM_INT;        /*Variable type of Parameter*/
       grpStructArray[5].startAddress[2].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[5].startAddress[2].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[5].startAddress[2].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[5].startAddress[2].varianz = PARAM_VARIABLE;    /*is the Parameter variable or fix?*/
       grpStructArray[5].startAddress[2].paramDefValue.valueInt = 0;  /*Default value of Parameter*/
       grpStructArray[5].startAddress[2].paramMinValue.valueInt = 0;  /*Min Value of Parameter*/
       grpStructArray[5].startAddress[2].paramActValue.valueInt = 0;  /*Active Value of Parameter*/
       grpStructArray[5].startAddress[2].paramMaxValue.valueInt = 0;  /*Max Value of Parameter*/
       grpStructArray[5].startAddress[2].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[5].startAddress[2].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0504 */
strcpy(grpStructArray[5].startAddress[3].name, "OperationModFailSafe");             /*Name of Parameter*/
       grpStructArray[5].startAddress[3].numb = 504;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[5].startAddress[3].variType = PARAM_UINT;       /*Variable type of Parameter*/
       grpStructArray[5].startAddress[3].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[5].startAddress[3].sign = PARAM_UNSIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[5].startAddress[3].change = PARAM_CHANGEABLE;   /*is the Parameter changeable during operation?*/
       grpStructArray[5].startAddress[3].varianz = PARAM_VARIABLE;    /*is the Parameter variable or fix?*/
       grpStructArray[5].startAddress[3].paramDefValue.valueUint = 128;             /*Default value of Parameter*/
       grpStructArray[5].startAddress[3].paramMinValue.valueUint = 128;             /*Min Value of Parameter*/
       grpStructArray[5].startAddress[3].paramActValue.valueUint = 128;             /*Active Value of Parameter*/
       grpStructArray[5].startAddress[3].paramMaxValue.valueUint = 128;             /*Max Value of Parameter*/
       grpStructArray[5].startAddress[3].digits = 3;    /*Number of digits for decimal Parameter*/
       grpStructArray[5].startAddress[3].decPlace = 0;  /*Number of digits behind the comma for float Parameter*/

/* 0901 */
strcpy(grpStructArray[9].startAddress[0].name, "AmpFactorU");         /*Name of Parameter*/
       grpStructArray[9].startAddress[0].numb = 901;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[9].startAddress[0].variType = PARAM_FLOAT;      /*Variable type of Parameter*/
       grpStructArray[9].startAddress[0].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[9].startAddress[0].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[9].startAddress[0].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[9].startAddress[0].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[9].startAddress[0].paramDefValue.valueFloat = -0.016619461;   /*Default value of Parameter*/
       grpStructArray[9].startAddress[0].paramMinValue.valueFloat = -0.1;           /*Min Value of Parameter*/
       grpStructArray[9].startAddress[0].paramActValue.valueFloat = -0.016619461;   /*Active Value of Parameter*/
       grpStructArray[9].startAddress[0].paramMaxValue.valueFloat = 0.1;            /*Max Value of Parameter*/
       grpStructArray[9].startAddress[0].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[9].startAddress[0].decPlace = 9;  /*Number of digits behind the comma for float Parameter*/

/* 0902 */
strcpy(grpStructArray[9].startAddress[1].name, "AmpFactorV");         /*Name of Parameter*/
       grpStructArray[9].startAddress[1].numb = 902;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[9].startAddress[1].variType = PARAM_FLOAT;      /*Variable type of Parameter*/
       grpStructArray[9].startAddress[1].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[9].startAddress[1].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[9].startAddress[1].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[9].startAddress[1].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[9].startAddress[1].paramDefValue.valueFloat = -0.015729056;   /*Default value of Parameter*/
       grpStructArray[9].startAddress[1].paramMinValue.valueFloat = -0.1;           /*Min Value of Parameter*/
       grpStructArray[9].startAddress[1].paramActValue.valueFloat = -0.015729056;   /*Active Value of Parameter*/
       grpStructArray[9].startAddress[1].paramMaxValue.valueFloat = 0.1;            /*Max Value of Parameter*/
       grpStructArray[9].startAddress[1].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[9].startAddress[1].decPlace = 9;  /*Number of digits behind the comma for float Parameter*/

/* 0903 */
strcpy(grpStructArray[9].startAddress[2].name, "AmpFactorW");         /*Name of Parameter*/
       grpStructArray[9].startAddress[2].numb = 903;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[9].startAddress[2].variType = PARAM_FLOAT;      /*Variable type of Parameter*/
       grpStructArray[9].startAddress[2].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[9].startAddress[2].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[9].startAddress[2].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[9].startAddress[2].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[9].startAddress[2].paramDefValue.valueFloat = -0.016353225;   /*Default value of Parameter*/
       grpStructArray[9].startAddress[2].paramMinValue.valueFloat = -0.1;           /*Min Value of Parameter*/
       grpStructArray[9].startAddress[2].paramActValue.valueFloat = -0.016353225;   /*Active Value of Parameter*/
       grpStructArray[9].startAddress[2].paramMaxValue.valueFloat = 0.1;            /*Max Value of Parameter*/
       grpStructArray[9].startAddress[2].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[9].startAddress[2].decPlace = 9;  /*Number of digits behind the comma for float Parameter*/

/* 0904 */
strcpy(grpStructArray[9].startAddress[3].name, "AmpFactorSin");       /*Name of Parameter*/
       grpStructArray[9].startAddress[3].numb = 904;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[9].startAddress[3].variType = PARAM_FLOAT;      /*Variable type of Parameter*/
       grpStructArray[9].startAddress[3].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[9].startAddress[3].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[9].startAddress[3].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[9].startAddress[3].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[9].startAddress[3].paramDefValue.valueFloat = 0.00054061912;  /*Default value of Parameter*/
       grpStructArray[9].startAddress[3].paramMinValue.valueFloat = 0;              /*Min Value of Parameter*/
       grpStructArray[9].startAddress[3].paramActValue.valueFloat = 0.00054061912;  /*Active Value of Parameter*/
       grpStructArray[9].startAddress[3].paramMaxValue.valueFloat = 1;              /*Max Value of Parameter*/
       grpStructArray[9].startAddress[3].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[9].startAddress[3].decPlace = 11; /*Number of digits behind the comma for float Parameter*/

/* 0905 */
strcpy(grpStructArray[9].startAddress[4].name, "AmpFactorCos");       /*Name of Parameter*/
       grpStructArray[9].startAddress[4].numb = 905;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[9].startAddress[4].variType = PARAM_FLOAT;      /*Variable type of Parameter*/
       grpStructArray[9].startAddress[4].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[9].startAddress[4].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[9].startAddress[4].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[9].startAddress[4].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[9].startAddress[4].paramDefValue.valueFloat = 0.00056789914;  /*Default value of Parameter*/
       grpStructArray[9].startAddress[4].paramMinValue.valueFloat = 0;              /*Min Value of Parameter*/
       grpStructArray[9].startAddress[4].paramActValue.valueFloat = 0.00056789914;  /*Active Value of Parameter*/
       grpStructArray[9].startAddress[4].paramMaxValue.valueFloat = 1.01;           /*Max Value of Parameter*/
       grpStructArray[9].startAddress[4].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[9].startAddress[4].decPlace = 11; /*Number of digits behind the comma for float Parameter*/

/* 0906 */
strcpy(grpStructArray[9].startAddress[5].name, "OffsetFactorU");      /*Name of Parameter*/
       grpStructArray[9].startAddress[5].numb = 906;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[9].startAddress[5].variType = PARAM_FLOAT;      /*Variable type of Parameter*/
       grpStructArray[9].startAddress[5].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[9].startAddress[5].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[9].startAddress[5].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[9].startAddress[5].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[9].startAddress[5].paramDefValue.valueFloat = 31.83384;       /*Default value of Parameter*/
       grpStructArray[9].startAddress[5].paramMinValue.valueFloat = 25.1;           /*Min Value of Parameter*/
       grpStructArray[9].startAddress[5].paramActValue.valueFloat = 31.83384;       /*Active Value of Parameter*/
       grpStructArray[9].startAddress[5].paramMaxValue.valueFloat = 40.1;           /*Max Value of Parameter*/
       grpStructArray[9].startAddress[5].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[9].startAddress[5].decPlace = 8;  /*Number of digits behind the comma for float Parameter*/

/* 0907 */
strcpy(grpStructArray[9].startAddress[6].name, "OffsetFactorV");      /*Name of Parameter*/
       grpStructArray[9].startAddress[6].numb = 907;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[9].startAddress[6].variType = PARAM_FLOAT;      /*Variable type of Parameter*/
       grpStructArray[9].startAddress[6].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[9].startAddress[6].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[9].startAddress[6].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[9].startAddress[6].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[9].startAddress[6].paramDefValue.valueFloat = 30.1283084;     /*Default value of Parameter*/
       grpStructArray[9].startAddress[6].paramMinValue.valueFloat = 25.1;           /*Min Value of Parameter*/
       grpStructArray[9].startAddress[6].paramActValue.valueFloat = 30.1283084;     /*Active Value of Parameter*/
       grpStructArray[9].startAddress[6].paramMaxValue.valueFloat = 40.1;           /*Max Value of Parameter*/
       grpStructArray[9].startAddress[6].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[9].startAddress[6].decPlace = 8;  /*Number of digits behind the comma for float Parameter*/

/* 0908 */
strcpy(grpStructArray[9].startAddress[7].name, "OffsetFactorW");      /*Name of Parameter*/
       grpStructArray[9].startAddress[7].numb = 908;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[9].startAddress[7].variType = PARAM_FLOAT;      /*Variable type of Parameter*/
       grpStructArray[9].startAddress[7].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[9].startAddress[7].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[9].startAddress[7].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[9].startAddress[7].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[9].startAddress[7].paramDefValue.valueFloat = 31.32387619;    /*Default value of Parameter*/
       grpStructArray[9].startAddress[7].paramMinValue.valueFloat = 25.1;           /*Min Value of Parameter*/
       grpStructArray[9].startAddress[7].paramActValue.valueFloat = 31.32387619;    /*Active Value of Parameter*/
       grpStructArray[9].startAddress[7].paramMaxValue.valueFloat = 40.1;           /*Max Value of Parameter*/
       grpStructArray[9].startAddress[7].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[9].startAddress[7].decPlace = 8;  /*Number of digits behind the comma for float Parameter*/

/* 0909 */
strcpy(grpStructArray[9].startAddress[8].name, "OffsetFactorSin");    /*Name of Parameter*/
       grpStructArray[9].startAddress[8].numb = 909;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[9].startAddress[8].variType = PARAM_FLOAT;      /*Variable type of Parameter*/
       grpStructArray[9].startAddress[8].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[9].startAddress[8].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[9].startAddress[8].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[9].startAddress[8].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[9].startAddress[8].paramDefValue.valueFloat = 2320.1105;      /*Default value of Parameter*/
       grpStructArray[9].startAddress[8].paramMinValue.valueFloat = 2000.1;         /*Min Value of Parameter*/
       grpStructArray[9].startAddress[8].paramActValue.valueFloat = 2320.1105;      /*Active Value of Parameter*/
       grpStructArray[9].startAddress[8].paramMaxValue.valueFloat = 2500.5;         /*Max Value of Parameter*/
       grpStructArray[9].startAddress[8].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[9].startAddress[8].decPlace = 6;  /*Number of digits behind the comma for float Parameter*/

/* 0910 */
strcpy(grpStructArray[9].startAddress[9].name, "OffsetFactorCos");    /*Name of Parameter*/
       grpStructArray[9].startAddress[9].numb = 910;    /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[9].startAddress[9].variType = PARAM_FLOAT;      /*Variable type of Parameter*/
       grpStructArray[9].startAddress[9].unit = PARAM_VOLT;           /*Unit of Parameter*/
       grpStructArray[9].startAddress[9].sign = PARAM_SIGNED;         /*is the Parameter signed or unsigned?*/
       grpStructArray[9].startAddress[9].change = PARAM_NOTCHANGEABLE;              /*is the Parameter changeable during operation?*/
       grpStructArray[9].startAddress[9].varianz = PARAM_NOTVARIABLE; /*is the Parameter variable or fix?*/
       grpStructArray[9].startAddress[9].paramDefValue.valueFloat = 2382.3305;      /*Default value of Parameter*/
       grpStructArray[9].startAddress[9].paramMinValue.valueFloat = 2000.1;         /*Min Value of Parameter*/
       grpStructArray[9].startAddress[9].paramActValue.valueFloat = 2382.3305;      /*Active Value of Parameter*/
       grpStructArray[9].startAddress[9].paramMaxValue.valueFloat = 2500.5;         /*Max Value of Parameter*/
       grpStructArray[9].startAddress[9].digits = 1;    /*Number of digits for decimal Parameter*/
       grpStructArray[9].startAddress[9].decPlace = 6;  /*Number of digits behind the comma for float Parameter*/

/*1001 */
strcpy(grpStructArray[10].startAddress[0].name, "MotType");           /*Name of Parameter*/
       grpStructArray[10].startAddress[0].numb = 1001;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[10].startAddress[0].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[10].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[10].startAddress[0].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[10].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[10].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[10].startAddress[0].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[10].startAddress[0].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[10].startAddress[0].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[10].startAddress[0].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[10].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[10].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1002 */
strcpy(grpStructArray[10].startAddress[1].name, "MotTrqConstant");    /*Name of Parameter*/
       grpStructArray[10].startAddress[1].numb = 1002;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[10].startAddress[1].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[10].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[10].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[10].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[10].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[10].startAddress[1].paramDefValue.valueFloat = 0.001895;      /*Default value of Parameter*/
       grpStructArray[10].startAddress[1].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[10].startAddress[1].paramActValue.valueFloat = 0.001895;      /*Active Value of Parameter*/
       grpStructArray[10].startAddress[1].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[10].startAddress[1].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[10].startAddress[1].decPlace = 6; /*Number of digits behind the comma for float Parameter*/

/*1003 */
strcpy(grpStructArray[10].startAddress[2].name, "MotWindingResist");  /*Name of Parameter*/
       grpStructArray[10].startAddress[2].numb = 1003;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[10].startAddress[2].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[10].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[10].startAddress[2].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[10].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[10].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[10].startAddress[2].paramDefValue.valueFloat = 0.002;         /*Default value of Parameter*/
       grpStructArray[10].startAddress[2].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[10].startAddress[2].paramActValue.valueFloat = 0.002;         /*Active Value of Parameter*/
       grpStructArray[10].startAddress[2].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[10].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[10].startAddress[2].decPlace = 3; /*Number of digits behind the comma for float Parameter*/

/*1004 */
strcpy(grpStructArray[10].startAddress[3].name, "MotWindingInduct");  /*Name of Parameter*/
       grpStructArray[10].startAddress[3].numb = 1004;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[10].startAddress[3].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[10].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[10].startAddress[3].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[10].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[10].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[10].startAddress[3].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[10].startAddress[3].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[10].startAddress[3].paramActValue.valueFloat = 0.00001;       /*Active Value of Parameter*/
       grpStructArray[10].startAddress[3].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[10].startAddress[3].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[10].startAddress[3].decPlace = 5; /*Number of digits behind the comma for float Parameter*/

/*1005 */
strcpy(grpStructArray[10].startAddress[4].name, "CrossInduct");       /*Name of Parameter*/
       grpStructArray[10].startAddress[4].numb = 1005;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[10].startAddress[4].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[10].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[10].startAddress[4].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[10].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[10].startAddress[4].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[10].startAddress[4].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[10].startAddress[4].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[10].startAddress[4].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[10].startAddress[4].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[10].startAddress[4].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[10].startAddress[4].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1006 */
strcpy(grpStructArray[10].startAddress[5].name, "MotPolePairs");      /*Name of Parameter*/
       grpStructArray[10].startAddress[5].numb = 1006;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[10].startAddress[5].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[10].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[10].startAddress[5].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[10].startAddress[5].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[10].startAddress[5].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[10].startAddress[5].paramDefValue.valueUint = 1;              /*Default value of Parameter*/
       grpStructArray[10].startAddress[5].paramMinValue.valueUint = 0;              /*Min Value of Parameter*/
       grpStructArray[10].startAddress[5].paramActValue.valueUint = 1;              /*Active Value of Parameter*/
       grpStructArray[10].startAddress[5].paramMaxValue.valueUint = 1;              /*Max Value of Parameter*/
       grpStructArray[10].startAddress[5].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[10].startAddress[5].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1007 */
strcpy(grpStructArray[10].startAddress[6].name, "Inertia");           /*Name of Parameter*/
       grpStructArray[10].startAddress[6].numb = 1007;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[10].startAddress[6].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[10].startAddress[6].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[10].startAddress[6].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[10].startAddress[6].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[10].startAddress[6].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[10].startAddress[6].paramDefValue.valueUint = 2;              /*Default value of Parameter*/
       grpStructArray[10].startAddress[6].paramMinValue.valueUint = 0;              /*Min Value of Parameter*/
       grpStructArray[10].startAddress[6].paramActValue.valueUint = 2;              /*Active Value of Parameter*/
       grpStructArray[10].startAddress[6].paramMaxValue.valueUint = 2;              /*Max Value of Parameter*/
       grpStructArray[10].startAddress[6].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[10].startAddress[6].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1008 */
strcpy(grpStructArray[10].startAddress[7].name, "MotCurrentMax");     /*Name of Parameter*/
       grpStructArray[10].startAddress[7].numb = 1008;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[10].startAddress[7].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[10].startAddress[7].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[10].startAddress[7].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[10].startAddress[7].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[10].startAddress[7].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[10].startAddress[7].paramDefValue.valueFloat = 20;            /*Default value of Parameter*/
       grpStructArray[10].startAddress[7].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[10].startAddress[7].paramActValue.valueFloat = 20;            /*Active Value of Parameter*/
       grpStructArray[10].startAddress[7].paramMaxValue.valueFloat = 60;            /*Max Value of Parameter*/
       grpStructArray[10].startAddress[7].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[10].startAddress[7].decPlace = 1; /*Number of digits behind the comma for float Parameter*/

/*1009 */
strcpy(grpStructArray[10].startAddress[8].name, "MotCurrentMin");     /*Name of Parameter*/
       grpStructArray[10].startAddress[8].numb = 1009;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[10].startAddress[8].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[10].startAddress[8].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[10].startAddress[8].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[10].startAddress[8].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[10].startAddress[8].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[10].startAddress[8].paramDefValue.valueFloat = -20;           /*Default value of Parameter*/
       grpStructArray[10].startAddress[8].paramMinValue.valueFloat = -60;           /*Min Value of Parameter*/
       grpStructArray[10].startAddress[8].paramActValue.valueFloat = -20;           /*Active Value of Parameter*/
       grpStructArray[10].startAddress[8].paramMaxValue.valueFloat = 0;             /*Max Value of Parameter*/
       grpStructArray[10].startAddress[8].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[10].startAddress[8].decPlace = 1; /*Number of digits behind the comma for float Parameter*/

/*1101 */
strcpy(grpStructArray[11].startAddress[0].name, "Ph1OffsetCal");      /*Name of Parameter*/
       grpStructArray[11].startAddress[0].numb = 1101;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[0].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[11].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[11].startAddress[0].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[0].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[11].startAddress[0].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[11].startAddress[0].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[11].startAddress[0].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[11].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1102 */
strcpy(grpStructArray[11].startAddress[1].name, "Ph1MaxRaw");         /*Name of Parameter*/
       grpStructArray[11].startAddress[1].numb = 1102;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[1].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[11].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[11].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[1].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[11].startAddress[1].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[11].startAddress[1].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[11].startAddress[1].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[11].startAddress[1].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1103 */
strcpy(grpStructArray[11].startAddress[2].name, "Ph1RatedCur");       /*Name of Parameter*/
       grpStructArray[11].startAddress[2].numb = 1103;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[2].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[11].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[11].startAddress[2].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[2].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[11].startAddress[2].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[11].startAddress[2].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[11].startAddress[2].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[11].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1104 */
strcpy(grpStructArray[11].startAddress[3].name, "Ph1RngChkEna");      /*Name of Parameter*/
       grpStructArray[11].startAddress[3].numb = 1104;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[3].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[11].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[11].startAddress[3].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[3].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[11].startAddress[3].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[11].startAddress[3].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[11].startAddress[3].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[11].startAddress[3].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1105 */
strcpy(grpStructArray[11].startAddress[4].name, "CurPh1RngChkHi");    /*Name of Parameter*/
       grpStructArray[11].startAddress[4].numb = 1105;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[4].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[11].startAddress[4].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[4].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[4].paramDefValue.valueUint = 3753;           /*Default value of Parameter*/
       grpStructArray[11].startAddress[4].paramMinValue.valueUint = 3482;           /*Min Value of Parameter*/
       grpStructArray[11].startAddress[4].paramActValue.valueUint = 3753;           /*Active Value of Parameter*/
       grpStructArray[11].startAddress[4].paramMaxValue.valueUint = 4095;           /*Max Value of Parameter*/
       grpStructArray[11].startAddress[4].digits = 4;   /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[4].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1106 */
strcpy(grpStructArray[11].startAddress[5].name, "CurPh1RngChkLo");    /*Name of Parameter*/
       grpStructArray[11].startAddress[5].numb = 1106;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[5].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[11].startAddress[5].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[5].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[5].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[5].paramDefValue.valueUint = 342;            /*Default value of Parameter*/
       grpStructArray[11].startAddress[5].paramMinValue.valueUint = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[5].paramActValue.valueUint = 342;            /*Active Value of Parameter*/
       grpStructArray[11].startAddress[5].paramMaxValue.valueUint = 696;            /*Max Value of Parameter*/
       grpStructArray[11].startAddress[5].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[5].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1107 */
strcpy(grpStructArray[11].startAddress[6].name, "reserved Ph1");      /*Name of Parameter*/
       grpStructArray[11].startAddress[6].numb = 1107;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[6].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[11].startAddress[6].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[11].startAddress[6].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[6].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[6].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[6].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[11].startAddress[6].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[11].startAddress[6].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[11].startAddress[6].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[11].startAddress[6].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[6].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1108 */
strcpy(grpStructArray[11].startAddress[7].name, "reserved Ph1");      /*Name of Parameter*/
       grpStructArray[11].startAddress[7].numb = 1108;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[7].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[11].startAddress[7].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[11].startAddress[7].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[7].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[7].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[7].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[11].startAddress[7].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[11].startAddress[7].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[11].startAddress[7].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[11].startAddress[7].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[7].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1109 */
strcpy(grpStructArray[11].startAddress[8].name, "reserved Ph1");      /*Name of Parameter*/
       grpStructArray[11].startAddress[8].numb = 1109;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[8].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[11].startAddress[8].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[11].startAddress[8].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[8].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[8].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[8].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[11].startAddress[8].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[11].startAddress[8].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[11].startAddress[8].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[11].startAddress[8].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[8].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1110 */
strcpy(grpStructArray[11].startAddress[9].name, "reserved Ph1");      /*Name of Parameter*/
       grpStructArray[11].startAddress[9].numb = 1110;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[9].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[11].startAddress[9].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[11].startAddress[9].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[9].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[9].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[9].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[11].startAddress[9].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[11].startAddress[9].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[11].startAddress[9].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[11].startAddress[9].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[9].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1111 */
strcpy(grpStructArray[11].startAddress[10].name, "reserved Ph1");     /*Name of Parameter*/
       grpStructArray[11].startAddress[10].numb = 1111; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[10].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[10].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[10].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[10].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[10].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[10].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[10].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[10].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[10].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[10].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[10].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1112 */
strcpy(grpStructArray[11].startAddress[11].name, "Ph2OffsetCal");     /*Name of Parameter*/
       grpStructArray[11].startAddress[11].numb = 1112; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[11].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[11].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[11].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[11].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[11].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[11].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[11].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[11].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[11].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[11].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[11].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1113 */
strcpy(grpStructArray[11].startAddress[12].name, "Ph2MaxRaw");        /*Name of Parameter*/
       grpStructArray[11].startAddress[12].numb = 1113; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[12].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[12].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[12].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[12].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[12].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[12].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[12].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[12].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[12].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[12].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[12].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1114 */
strcpy(grpStructArray[11].startAddress[13].name, "Ph2RatedCur");      /*Name of Parameter*/
       grpStructArray[11].startAddress[13].numb = 1114; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[13].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[13].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[13].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[13].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[13].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[13].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[13].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[13].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[13].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[13].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[13].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1115 */
strcpy(grpStructArray[11].startAddress[14].name, "Ph2RngChkEna");     /*Name of Parameter*/
       grpStructArray[11].startAddress[14].numb = 1115; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[14].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[14].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[14].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[14].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[14].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[14].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[14].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[14].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[14].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[14].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[14].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1116 */
strcpy(grpStructArray[11].startAddress[15].name, "CurPh2RngChkHi");   /*Name of Parameter*/
       grpStructArray[11].startAddress[15].numb = 1116; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[15].variType = PARAM_UINT;     /*Variable type of Parameter*/
       grpStructArray[11].startAddress[15].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[15].sign = PARAM_UNSIGNED;     /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[15].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[15].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[15].paramDefValue.valueUint = 3753;          /*Default value of Parameter*/
       grpStructArray[11].startAddress[15].paramMinValue.valueUint = 3482;          /*Min Value of Parameter*/
       grpStructArray[11].startAddress[15].paramActValue.valueUint = 3753;          /*Active Value of Parameter*/
       grpStructArray[11].startAddress[15].paramMaxValue.valueUint = 4095;          /*Max Value of Parameter*/
       grpStructArray[11].startAddress[15].digits = 4;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[15].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1117 */
strcpy(grpStructArray[11].startAddress[16].name, "CurPh2RngChkLo");   /*Name of Parameter*/
       grpStructArray[11].startAddress[16].numb = 1117; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[16].variType = PARAM_UINT;     /*Variable type of Parameter*/
       grpStructArray[11].startAddress[16].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[16].sign = PARAM_UNSIGNED;     /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[16].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[16].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[16].paramDefValue.valueUint = 342;           /*Default value of Parameter*/
       grpStructArray[11].startAddress[16].paramMinValue.valueUint = 0;             /*Min Value of Parameter*/
       grpStructArray[11].startAddress[16].paramActValue.valueUint = 342;           /*Active Value of Parameter*/
       grpStructArray[11].startAddress[16].paramMaxValue.valueUint = 696;           /*Max Value of Parameter*/
       grpStructArray[11].startAddress[16].digits = 3;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[16].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1118 */
strcpy(grpStructArray[11].startAddress[17].name, "<reserved Ph2>");   /*Name of Parameter*/
       grpStructArray[11].startAddress[17].numb = 1118; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[17].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[17].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[17].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[17].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[17].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[17].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[17].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[17].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[17].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[17].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[17].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1119 */
strcpy(grpStructArray[11].startAddress[18].name, "<reserved Ph2>");   /*Name of Parameter*/
       grpStructArray[11].startAddress[18].numb = 1119; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[18].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[18].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[18].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[18].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[18].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[18].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[18].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[18].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[18].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[18].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[18].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1120 */
strcpy(grpStructArray[11].startAddress[19].name, "<reserved Ph2>");   /*Name of Parameter*/
       grpStructArray[11].startAddress[19].numb = 1120; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[19].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[19].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[19].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[19].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[19].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[19].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[19].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[19].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[19].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[19].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[19].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1121 */
strcpy(grpStructArray[11].startAddress[20].name, "<reserved Ph2>");   /*Name of Parameter*/
       grpStructArray[11].startAddress[20].numb = 1121; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[20].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[20].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[20].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[20].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[20].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[20].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[20].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[20].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[20].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[20].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[20].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1122 */
strcpy(grpStructArray[11].startAddress[21].name, "<reserved Ph2>");   /*Name of Parameter*/
       grpStructArray[11].startAddress[21].numb = 1122; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[21].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[21].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[21].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[21].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[21].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[21].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[21].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[21].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[21].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[21].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[21].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1123 */
strcpy(grpStructArray[11].startAddress[22].name, "Ph3OffsetCal");     /*Name of Parameter*/
       grpStructArray[11].startAddress[22].numb = 1123; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[22].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[22].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[22].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[22].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[22].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[22].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[22].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[22].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[22].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[22].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[22].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1124 */
strcpy(grpStructArray[11].startAddress[23].name, "Ph3MaxRaw");        /*Name of Parameter*/
       grpStructArray[11].startAddress[23].numb = 1124; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[23].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[23].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[23].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[23].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[23].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[23].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[23].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[23].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[23].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[23].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[23].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1125 */
strcpy(grpStructArray[11].startAddress[24].name, "Ph3RatedCur");      /*Name of Parameter*/
       grpStructArray[11].startAddress[24].numb = 1125; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[24].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[24].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[24].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[24].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[24].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[24].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[24].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[24].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[24].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[24].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[24].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1126 */
strcpy(grpStructArray[11].startAddress[25].name, "Ph3RngChkEna");     /*Name of Parameter*/
       grpStructArray[11].startAddress[25].numb = 1126; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[25].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[25].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[25].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[25].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[25].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[25].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[25].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[25].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[25].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[25].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[25].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1127 */
strcpy(grpStructArray[11].startAddress[26].name, "CurPh3RngChkHi");   /*Name of Parameter*/
       grpStructArray[11].startAddress[26].numb = 1127; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[26].variType = PARAM_UINT;     /*Variable type of Parameter*/
       grpStructArray[11].startAddress[26].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[26].sign = PARAM_UNSIGNED;     /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[26].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[26].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[26].paramDefValue.valueUint = 3753;          /*Default value of Parameter*/
       grpStructArray[11].startAddress[26].paramMinValue.valueUint = 3482;          /*Min Value of Parameter*/
       grpStructArray[11].startAddress[26].paramActValue.valueUint = 3753;          /*Active Value of Parameter*/
       grpStructArray[11].startAddress[26].paramMaxValue.valueUint = 4095;          /*Max Value of Parameter*/
       grpStructArray[11].startAddress[26].digits = 4;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[26].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1128 */
strcpy(grpStructArray[11].startAddress[27].name, "CurPh3RngChkLo");   /*Name of Parameter*/
       grpStructArray[11].startAddress[27].numb = 1128; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[27].variType = PARAM_UINT;     /*Variable type of Parameter*/
       grpStructArray[11].startAddress[27].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[27].sign = PARAM_UNSIGNED;     /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[27].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[27].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[27].paramDefValue.valueUint = 342;           /*Default value of Parameter*/
       grpStructArray[11].startAddress[27].paramMinValue.valueUint = 0;             /*Min Value of Parameter*/
       grpStructArray[11].startAddress[27].paramActValue.valueUint = 342;           /*Active Value of Parameter*/
       grpStructArray[11].startAddress[27].paramMaxValue.valueUint = 696;           /*Max Value of Parameter*/
       grpStructArray[11].startAddress[27].digits = 3;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[27].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1129 */
strcpy(grpStructArray[11].startAddress[28].name, "<reserved Ph3>");   /*Name of Parameter*/
       grpStructArray[11].startAddress[28].numb = 1129; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[28].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[28].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[28].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[28].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[28].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[28].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[28].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[28].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[28].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[28].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[28].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1130 */
strcpy(grpStructArray[11].startAddress[29].name, "<reserved Ph3>");   /*Name of Parameter*/
       grpStructArray[11].startAddress[29].numb = 1130; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[29].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[29].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[29].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[29].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[29].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[29].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[29].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[29].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[29].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[29].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[29].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1131 */
strcpy(grpStructArray[11].startAddress[30].name, "<reserved Ph3>");   /*Name of Parameter*/
       grpStructArray[11].startAddress[30].numb = 1131; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[30].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[30].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[30].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[30].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[30].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[30].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[30].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[30].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[30].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[30].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[30].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1132 */
strcpy(grpStructArray[11].startAddress[31].name, "<reserved Ph3>");   /*Name of Parameter*/
       grpStructArray[11].startAddress[31].numb = 1132; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[31].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[31].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[31].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[31].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[31].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[31].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[31].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[31].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[31].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[31].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[31].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1133 */
strcpy(grpStructArray[11].startAddress[32].name, "<reserved Ph3>");   /*Name of Parameter*/
       grpStructArray[11].startAddress[32].numb = 1133; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[32].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[32].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[32].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[32].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[32].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[32].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[32].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[32].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[32].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[32].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[32].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1134 */
strcpy(grpStructArray[11].startAddress[33].name, "<reserved Ph3>");   /*Name of Parameter*/
       grpStructArray[11].startAddress[33].numb = 1134; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[33].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[33].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[33].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[33].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[33].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[33].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[33].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[33].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[11].startAddress[33].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[11].startAddress[33].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[33].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1135 */
strcpy(grpStructArray[11].startAddress[34].name, "CurrentLimit");     /*Name of Parameter*/
       grpStructArray[11].startAddress[34].numb = 1135; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[34].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[11].startAddress[34].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[34].sign = PARAM_UNSIGNED;     /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[34].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[34].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[34].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[11].startAddress[34].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[11].startAddress[34].paramActValue.valueInt = 30;             /*Active Value of Parameter*/
       grpStructArray[11].startAddress[34].paramMaxValue.valueInt = 30;             /*Max Value of Parameter*/
       grpStructArray[11].startAddress[34].digits = 2;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[34].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*1136 */
strcpy(grpStructArray[11].startAddress[35].name, "TolCurSum");        /*Name of Parameter*/
       grpStructArray[11].startAddress[35].numb = 1136; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[11].startAddress[35].variType = PARAM_FLOAT;    /*Variable type of Parameter*/
       grpStructArray[11].startAddress[35].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[11].startAddress[35].sign = PARAM_UNSIGNED;     /*is the Parameter signed or unsigned?*/
       grpStructArray[11].startAddress[35].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[11].startAddress[35].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[11].startAddress[35].paramDefValue.valueFloat = 5.9;          /*Default value of Parameter*/
       grpStructArray[11].startAddress[35].paramMinValue.valueFloat = 5.89;         /*Min Value of Parameter*/
       grpStructArray[11].startAddress[35].paramActValue.valueFloat = 5.9;          /*Active Value of Parameter*/
       grpStructArray[11].startAddress[35].paramMaxValue.valueFloat = 5.91;         /*Max Value of Parameter*/
       grpStructArray[11].startAddress[35].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[11].startAddress[35].decPlace = 2;              /*Number of digits behind the comma for float Parameter*/

/*1201 */
strcpy(grpStructArray[12].startAddress[0].name, "FocPICtrlFluxProp"); /*Name of Parameter*/
       grpStructArray[12].startAddress[0].numb = 1201;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[12].startAddress[0].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[12].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[12].startAddress[0].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[12].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[12].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[12].startAddress[0].paramDefValue.valueFloat = 0.5;           /*Default value of Parameter*/
       grpStructArray[12].startAddress[0].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[12].startAddress[0].paramActValue.valueFloat = 0.5;           /*Active Value of Parameter*/
       grpStructArray[12].startAddress[0].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[12].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[12].startAddress[0].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1202 */
strcpy(grpStructArray[12].startAddress[1].name, "FocPICtrlFluxIntg"); /*Name of Parameter*/
       grpStructArray[12].startAddress[1].numb = 1202;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[12].startAddress[1].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[12].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[12].startAddress[1].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[12].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[12].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[12].startAddress[1].paramDefValue.valueUint = 600;            /*Default value of Parameter*/
       grpStructArray[12].startAddress[1].paramMinValue.valueUint = 0;              /*Min Value of Parameter*/
       grpStructArray[12].startAddress[1].paramActValue.valueUint = 600;            /*Active Value of Parameter*/
       grpStructArray[12].startAddress[1].paramMaxValue.valueUint = 600;            /*Max Value of Parameter*/
       grpStructArray[12].startAddress[1].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[12].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1203 */
strcpy(grpStructArray[12].startAddress[2].name, "FocPICtrlTrqProp");  /*Name of Parameter*/
       grpStructArray[12].startAddress[2].numb = 1203;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[12].startAddress[2].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[12].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[12].startAddress[2].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[12].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[12].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[12].startAddress[2].paramDefValue.valueFloat = 0.5;           /*Default value of Parameter*/
       grpStructArray[12].startAddress[2].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[12].startAddress[2].paramActValue.valueFloat = 0.5;           /*Active Value of Parameter*/
       grpStructArray[12].startAddress[2].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[12].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[12].startAddress[2].decPlace = 1; /*Number of digits behind the comma for float Parameter*/

/*1204 */
strcpy(grpStructArray[12].startAddress[3].name, "FocPICtrlTrqIntg");  /*Name of Parameter*/
       grpStructArray[12].startAddress[3].numb = 1204;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[12].startAddress[3].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[12].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[12].startAddress[3].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[12].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[12].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[12].startAddress[3].paramDefValue.valueFloat = 600;           /*Default value of Parameter*/
       grpStructArray[12].startAddress[3].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[12].startAddress[3].paramActValue.valueFloat = 600;           /*Active Value of Parameter*/
       grpStructArray[12].startAddress[3].paramMaxValue.valueFloat = 600;           /*Max Value of Parameter*/
       grpStructArray[12].startAddress[3].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[12].startAddress[3].decPlace = 1; /*Number of digits behind the comma for float Parameter*/

/*1205 */
strcpy(grpStructArray[12].startAddress[4].name, "RotAngOffsetCorrection");          /*Name of Parameter*/
       grpStructArray[12].startAddress[4].numb = 1205;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[12].startAddress[4].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[12].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[12].startAddress[4].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[12].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[12].startAddress[4].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[12].startAddress[4].paramDefValue.valueFloat = -2;            /*Default value of Parameter*/
       grpStructArray[12].startAddress[4].paramMinValue.valueFloat = -3.14;         /*Min Value of Parameter*/
       grpStructArray[12].startAddress[4].paramActValue.valueFloat = -2;            /*Active Value of Parameter*/
       grpStructArray[12].startAddress[4].paramMaxValue.valueFloat = 3.14;          /*Max Value of Parameter*/
       grpStructArray[12].startAddress[4].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[12].startAddress[4].decPlace = 4; /*Number of digits behind the comma for float Parameter*/

/*1301 */
strcpy(grpStructArray[13].startAddress[0].name, "NumRcChannel");      /*Name of Parameter*/
       grpStructArray[13].startAddress[0].numb = 1301;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[13].startAddress[0].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[13].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[13].startAddress[0].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[13].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[13].startAddress[0].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[13].startAddress[0].paramDefValue.valueUint = 7;              /*Default value of Parameter*/
       grpStructArray[13].startAddress[0].paramMinValue.valueUint = 1;              /*Min Value of Parameter*/
       grpStructArray[13].startAddress[0].paramActValue.valueUint = 7;              /*Active Value of Parameter*/
       grpStructArray[13].startAddress[0].paramMaxValue.valueUint = 8;              /*Max Value of Parameter*/
       grpStructArray[13].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[13].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1302 */
strcpy(grpStructArray[13].startAddress[1].name, "RotSpdMaxTrqRc");    /*Name of Parameter*/
       grpStructArray[13].startAddress[1].numb = 1302;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[13].startAddress[1].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[13].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[13].startAddress[1].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[13].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[13].startAddress[1].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[13].startAddress[1].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[13].startAddress[1].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[13].startAddress[1].paramActValue.valueFloat = 0.008;         /*Active Value of Parameter*/
       grpStructArray[13].startAddress[1].paramMaxValue.valueFloat = 0.02;          /*Max Value of Parameter*/
       grpStructArray[13].startAddress[1].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[13].startAddress[1].decPlace = 3; /*Number of digits behind the comma for float Parameter*/

/*1303 */
strcpy(grpStructArray[13].startAddress[2].name, "RotSpdMinTrqRc");    /*Name of Parameter*/
       grpStructArray[13].startAddress[2].numb = 1303;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[13].startAddress[2].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[13].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[13].startAddress[2].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[13].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[13].startAddress[2].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[13].startAddress[2].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[13].startAddress[2].paramMinValue.valueFloat = -0.02;         /*Min Value of Parameter*/
       grpStructArray[13].startAddress[2].paramActValue.valueFloat = -0.008;        /*Active Value of Parameter*/
       grpStructArray[13].startAddress[2].paramMaxValue.valueFloat = 0;             /*Max Value of Parameter*/
       grpStructArray[13].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[13].startAddress[2].decPlace = 3; /*Number of digits behind the comma for float Parameter*/

/*1401 */
strcpy(grpStructArray[14].startAddress[0].name, "RotSpdCtrlProp");    /*Name of Parameter*/
       grpStructArray[14].startAddress[0].numb = 1401;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[14].startAddress[0].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[14].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[14].startAddress[0].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[14].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[14].startAddress[0].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[14].startAddress[0].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[14].startAddress[0].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[14].startAddress[0].paramActValue.valueFloat = 50;            /*Active Value of Parameter*/
       grpStructArray[14].startAddress[0].paramMaxValue.valueFloat = 1000;          /*Max Value of Parameter*/
       grpStructArray[14].startAddress[0].digits = 4;   /*Number of digits for decimal Parameter*/
       grpStructArray[14].startAddress[0].decPlace = 6; /*Number of digits behind the comma for float Parameter*/

/*1402 */
strcpy(grpStructArray[14].startAddress[1].name, "RotSpdCtrlIntg");    /*Name of Parameter*/
       grpStructArray[14].startAddress[1].numb = 1402;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[14].startAddress[1].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[14].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[14].startAddress[1].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[14].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[14].startAddress[1].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[14].startAddress[1].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[14].startAddress[1].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[14].startAddress[1].paramActValue.valueFloat = 150;           /*Active Value of Parameter*/
       grpStructArray[14].startAddress[1].paramMaxValue.valueFloat = 1000;          /*Max Value of Parameter*/
       grpStructArray[14].startAddress[1].digits = 4;   /*Number of digits for decimal Parameter*/
       grpStructArray[14].startAddress[1].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1403 */
strcpy(grpStructArray[14].startAddress[2].name, "RotSpdCtrlTimeIntrvl");            /*Name of Parameter*/
       grpStructArray[14].startAddress[2].numb = 1403;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[14].startAddress[2].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[14].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[14].startAddress[2].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[14].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[14].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[14].startAddress[2].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[14].startAddress[2].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[14].startAddress[2].paramActValue.valueFloat = 0.005;         /*Active Value of Parameter*/
       grpStructArray[14].startAddress[2].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[14].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[14].startAddress[2].decPlace = 3; /*Number of digits behind the comma for float Parameter*/

/*1404 */
strcpy(grpStructArray[14].startAddress[3].name, "RotSpdCtrlMinTrq");  /*Name of Parameter*/
       grpStructArray[14].startAddress[3].numb = 1404;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[14].startAddress[3].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[14].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[14].startAddress[3].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[14].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[14].startAddress[3].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[14].startAddress[3].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[14].startAddress[3].paramMinValue.valueFloat = -0.055;        /*Min Value of Parameter*/
       grpStructArray[14].startAddress[3].paramActValue.valueFloat = -0.0379;       /*Active Value of Parameter*/
       grpStructArray[14].startAddress[3].paramMaxValue.valueFloat = 0;             /*Max Value of Parameter*/
       grpStructArray[14].startAddress[3].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[14].startAddress[3].decPlace = 4; /*Number of digits behind the comma for float Parameter*/

/*1405 */
strcpy(grpStructArray[14].startAddress[4].name, "RotSpdCtrlMaxTrq");  /*Name of Parameter*/
       grpStructArray[14].startAddress[4].numb = 1405;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[14].startAddress[4].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[14].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[14].startAddress[4].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[14].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[14].startAddress[4].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[14].startAddress[4].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[14].startAddress[4].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[14].startAddress[4].paramActValue.valueFloat = 0.0379;        /*Active Value of Parameter*/
       grpStructArray[14].startAddress[4].paramMaxValue.valueFloat = 0.055;         /*Max Value of Parameter*/
       grpStructArray[14].startAddress[4].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[14].startAddress[4].decPlace = 4; /*Number of digits behind the comma for float Parameter*/

/*1406 */
strcpy(grpStructArray[14].startAddress[5].name, "RotSpdCtrlOutGain"); /*Name of Parameter*/
       grpStructArray[14].startAddress[5].numb = 1406;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[14].startAddress[5].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[14].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[14].startAddress[5].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[14].startAddress[5].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[14].startAddress[5].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[14].startAddress[5].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[14].startAddress[5].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[14].startAddress[5].paramActValue.valueFloat = 0.0000001895;  /*Active Value of Parameter*/
       grpStructArray[14].startAddress[5].paramMaxValue.valueFloat = 1000;          /*Max Value of Parameter*/
       grpStructArray[14].startAddress[5].digits = 4;   /*Number of digits for decimal Parameter*/
       grpStructArray[14].startAddress[5].decPlace = 7; /*Number of digits behind the comma for float Parameter*/

/*1501 */
strcpy(grpStructArray[15].startAddress[0].name, "InitAngSrv1");       /*Name of Parameter*/
       grpStructArray[15].startAddress[0].numb = 1501;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[15].startAddress[0].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[15].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[15].startAddress[0].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[15].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[15].startAddress[0].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[15].startAddress[0].paramDefValue.valueFloat = 90;            /*Default value of Parameter*/
       grpStructArray[15].startAddress[0].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[15].startAddress[0].paramActValue.valueFloat = 90;            /*Active Value of Parameter*/
       grpStructArray[15].startAddress[0].paramMaxValue.valueFloat = 180;           /*Max Value of Parameter*/
       grpStructArray[15].startAddress[0].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[15].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1502 */
strcpy(grpStructArray[15].startAddress[1].name, "MinAbsAngSrv1");     /*Name of Parameter*/
       grpStructArray[15].startAddress[1].numb = 1502;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[15].startAddress[1].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[15].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[15].startAddress[1].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[15].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[15].startAddress[1].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[15].startAddress[1].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[15].startAddress[1].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[15].startAddress[1].paramActValue.valueFloat = 0;             /*Active Value of Parameter*/
       grpStructArray[15].startAddress[1].paramMaxValue.valueFloat = 180;           /*Max Value of Parameter*/
       grpStructArray[15].startAddress[1].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[15].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1503 */
strcpy(grpStructArray[15].startAddress[2].name, "MaxAbsAngSrv1");     /*Name of Parameter*/
       grpStructArray[15].startAddress[2].numb = 1503;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[15].startAddress[2].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[15].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[15].startAddress[2].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[15].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[15].startAddress[2].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[15].startAddress[2].paramDefValue.valueFloat = 180;           /*Default value of Parameter*/
       grpStructArray[15].startAddress[2].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[15].startAddress[2].paramActValue.valueFloat = 180;           /*Active Value of Parameter*/
       grpStructArray[15].startAddress[2].paramMaxValue.valueFloat = 180;           /*Max Value of Parameter*/
       grpStructArray[15].startAddress[2].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[15].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1504 */
strcpy(grpStructArray[15].startAddress[3].name, "InitAngSrv2");       /*Name of Parameter*/
       grpStructArray[15].startAddress[3].numb = 1504;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[15].startAddress[3].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[15].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[15].startAddress[3].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[15].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[15].startAddress[3].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[15].startAddress[3].paramDefValue.valueFloat = 90;            /*Default value of Parameter*/
       grpStructArray[15].startAddress[3].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[15].startAddress[3].paramActValue.valueFloat = 90;            /*Active Value of Parameter*/
       grpStructArray[15].startAddress[3].paramMaxValue.valueFloat = 180;           /*Max Value of Parameter*/
       grpStructArray[15].startAddress[3].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[15].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1505 */
strcpy(grpStructArray[15].startAddress[4].name, "MinAbsAngSrv2");     /*Name of Parameter*/
       grpStructArray[15].startAddress[4].numb = 1505;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[15].startAddress[4].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[15].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[15].startAddress[4].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[15].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[15].startAddress[4].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[15].startAddress[4].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[15].startAddress[4].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[15].startAddress[4].paramActValue.valueFloat = 0;             /*Active Value of Parameter*/
       grpStructArray[15].startAddress[4].paramMaxValue.valueFloat = 180;           /*Max Value of Parameter*/
       grpStructArray[15].startAddress[4].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[15].startAddress[4].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1506 */
strcpy(grpStructArray[15].startAddress[5].name, "MaxAbsAngSrv2");     /*Name of Parameter*/
       grpStructArray[15].startAddress[5].numb = 1506;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[15].startAddress[5].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[15].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[15].startAddress[5].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[15].startAddress[5].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[15].startAddress[5].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[15].startAddress[5].paramDefValue.valueFloat = 180;           /*Default value of Parameter*/
       grpStructArray[15].startAddress[5].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[15].startAddress[5].paramActValue.valueFloat = 180;           /*Active Value of Parameter*/
       grpStructArray[15].startAddress[5].paramMaxValue.valueFloat = 180;           /*Max Value of Parameter*/
       grpStructArray[15].startAddress[5].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[15].startAddress[5].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1507 */
strcpy(grpStructArray[15].startAddress[6].name, "SrvAngVoltMin");     /*Name of Parameter*/
       grpStructArray[15].startAddress[6].numb = 1507;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[15].startAddress[6].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[15].startAddress[6].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[15].startAddress[6].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[15].startAddress[6].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[15].startAddress[6].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[15].startAddress[6].paramDefValue.valueFloat = 0.69;         /*Default value of Parameter*/
       grpStructArray[15].startAddress[6].paramMinValue.valueFloat = 0.6;           /*Min Value of Parameter*/
       grpStructArray[15].startAddress[6].paramActValue.valueFloat = 0.7;          /*Active Value of Parameter*/
       grpStructArray[15].startAddress[6].paramMaxValue.valueFloat = 0.75;           /*Max Value of Parameter*/
       grpStructArray[15].startAddress[6].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[15].startAddress[6].decPlace = 3; /*Number of digits behind the comma for float Parameter*/

/*1508 */
strcpy(grpStructArray[15].startAddress[7].name, "SrvAngVoltMax");     /*Name of Parameter*/
       grpStructArray[15].startAddress[7].numb = 1508;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[15].startAddress[7].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[15].startAddress[7].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[15].startAddress[7].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[15].startAddress[7].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[15].startAddress[7].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[15].startAddress[7].paramDefValue.valueFloat = 1.33;          /*Default value of Parameter*/
       grpStructArray[15].startAddress[7].paramMinValue.valueFloat = 1.3;           /*Min Value of Parameter*/
       grpStructArray[15].startAddress[7].paramActValue.valueFloat = 1.36;          /*Active Value of Parameter*/
       grpStructArray[15].startAddress[7].paramMaxValue.valueFloat = 1.5;           /*Max Value of Parameter*/
       grpStructArray[15].startAddress[7].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[15].startAddress[7].decPlace = 3; /*Number of digits behind the comma for float Parameter*/

/*1601 */
strcpy(grpStructArray[16].startAddress[0].name, "SpurGear");          /*Name of Parameter*/
       grpStructArray[16].startAddress[0].numb = 1601;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[16].startAddress[0].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[16].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[16].startAddress[0].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[16].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[16].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[16].startAddress[0].paramDefValue.valueUint = 48;             /*Default value of Parameter*/
       grpStructArray[16].startAddress[0].paramMinValue.valueUint = 0;              /*Min Value of Parameter*/
       grpStructArray[16].startAddress[0].paramActValue.valueUint = 48;             /*Active Value of Parameter*/
       grpStructArray[16].startAddress[0].paramMaxValue.valueUint = 48;             /*Max Value of Parameter*/
       grpStructArray[16].startAddress[0].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[16].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1602 */
strcpy(grpStructArray[16].startAddress[1].name, "PinionGear");        /*Name of Parameter*/
       grpStructArray[16].startAddress[1].numb = 1602;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[16].startAddress[1].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[16].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[16].startAddress[1].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[16].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[16].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[16].startAddress[1].paramDefValue.valueUint = 13;             /*Default value of Parameter*/
       grpStructArray[16].startAddress[1].paramMinValue.valueUint = 0;              /*Min Value of Parameter*/
       grpStructArray[16].startAddress[1].paramActValue.valueUint = 13;             /*Active Value of Parameter*/
       grpStructArray[16].startAddress[1].paramMaxValue.valueUint = 17;             /*Max Value of Parameter*/
       grpStructArray[16].startAddress[1].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[16].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1603 */
strcpy(grpStructArray[16].startAddress[2].name, "Differential");      /*Name of Parameter*/
       grpStructArray[16].startAddress[2].numb = 1603;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[16].startAddress[2].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[16].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[16].startAddress[2].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[16].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[16].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[16].startAddress[2].paramDefValue.valueFloat = 4.3;           /*Default value of Parameter*/
       grpStructArray[16].startAddress[2].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[16].startAddress[2].paramActValue.valueFloat = 4.3;           /*Active Value of Parameter*/
       grpStructArray[16].startAddress[2].paramMaxValue.valueFloat = 5.1;           /*Max Value of Parameter*/
       grpStructArray[16].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[16].startAddress[2].decPlace = 1; /*Number of digits behind the comma for float Parameter*/

/*1604 */
strcpy(grpStructArray[16].startAddress[3].name, "WheelDiameter");     /*Name of Parameter*/
       grpStructArray[16].startAddress[3].numb = 1604;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[16].startAddress[3].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[16].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[16].startAddress[3].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[16].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[16].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[16].startAddress[3].paramDefValue.valueFloat = 0.11;          /*Default value of Parameter*/
       grpStructArray[16].startAddress[3].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[16].startAddress[3].paramActValue.valueFloat = 0.11;          /*Active Value of Parameter*/
       grpStructArray[16].startAddress[3].paramMaxValue.valueFloat = 0.2;           /*Max Value of Parameter*/
       grpStructArray[16].startAddress[3].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[16].startAddress[3].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1701 */
strcpy(grpStructArray[17].startAddress[0].name, "MotTrqMax");         /*Name of Parameter*/
       grpStructArray[17].startAddress[0].numb = 1701;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[17].startAddress[0].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[17].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[17].startAddress[0].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[17].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[17].startAddress[0].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[17].startAddress[0].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[17].startAddress[0].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[17].startAddress[0].paramActValue.valueFloat = 1.5;           /*Active Value of Parameter*/
       grpStructArray[17].startAddress[0].paramMaxValue.valueFloat = 2;             /*Max Value of Parameter*/
       grpStructArray[17].startAddress[0].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[17].startAddress[0].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1702 */
strcpy(grpStructArray[17].startAddress[1].name, "MotTrqMin");         /*Name of Parameter*/
       grpStructArray[17].startAddress[1].numb = 1702;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[17].startAddress[1].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[17].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[17].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[17].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[17].startAddress[1].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[17].startAddress[1].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[17].startAddress[1].paramMinValue.valueFloat = -2;            /*Min Value of Parameter*/
       grpStructArray[17].startAddress[1].paramActValue.valueFloat = -1.5;          /*Active Value of Parameter*/
       grpStructArray[17].startAddress[1].paramMaxValue.valueFloat = 0;             /*Max Value of Parameter*/
       grpStructArray[17].startAddress[1].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[17].startAddress[1].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1801 */
strcpy(grpStructArray[18].startAddress[0].name, "StAngMax");          /*Name of Parameter*/
       grpStructArray[18].startAddress[0].numb = 1801;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[18].startAddress[0].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[18].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[18].startAddress[0].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[18].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[18].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[18].startAddress[0].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[18].startAddress[0].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[18].startAddress[0].paramActValue.valueFloat = 29;            /*Active Value of Parameter*/
       grpStructArray[18].startAddress[0].paramMaxValue.valueFloat = 30;            /*Max Value of Parameter*/
       grpStructArray[18].startAddress[0].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[18].startAddress[0].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1802 */
strcpy(grpStructArray[18].startAddress[1].name, "StAngMin");          /*Name of Parameter*/
       grpStructArray[18].startAddress[1].numb = 1802;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[18].startAddress[1].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[18].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[18].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[18].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[18].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[18].startAddress[1].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[18].startAddress[1].paramMinValue.valueFloat = -30;           /*Min Value of Parameter*/
       grpStructArray[18].startAddress[1].paramActValue.valueFloat = -29;           /*Active Value of Parameter*/
       grpStructArray[18].startAddress[1].paramMaxValue.valueFloat = 0;             /*Max Value of Parameter*/
       grpStructArray[18].startAddress[1].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[18].startAddress[1].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1901 */
strcpy(grpStructArray[19].startAddress[0].name, "LatDifGain");        /*Name of Parameter*/
       grpStructArray[19].startAddress[0].numb = 1901;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[19].startAddress[0].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[19].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[19].startAddress[0].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[19].startAddress[0].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[19].startAddress[0].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[19].startAddress[0].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[19].startAddress[0].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[19].startAddress[0].paramActValue.valueFloat = 0;             /*Active Value of Parameter*/
       grpStructArray[19].startAddress[0].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[19].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[19].startAddress[0].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1902 */
strcpy(grpStructArray[19].startAddress[1].name, "LatIntGain");        /*Name of Parameter*/
       grpStructArray[19].startAddress[1].numb = 1902;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[19].startAddress[1].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[19].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[19].startAddress[1].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[19].startAddress[1].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[19].startAddress[1].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[19].startAddress[1].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[19].startAddress[1].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[19].startAddress[1].paramActValue.valueFloat = 0;             /*Active Value of Parameter*/
       grpStructArray[19].startAddress[1].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[19].startAddress[1].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[19].startAddress[1].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1903 */
strcpy(grpStructArray[19].startAddress[2].name, "LatProGain");        /*Name of Parameter*/
       grpStructArray[19].startAddress[2].numb = 1903;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[19].startAddress[2].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[19].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[19].startAddress[2].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[19].startAddress[2].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[19].startAddress[2].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[19].startAddress[2].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[19].startAddress[2].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[19].startAddress[2].paramActValue.valueFloat = 0;             /*Active Value of Parameter*/
       grpStructArray[19].startAddress[2].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[19].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[19].startAddress[2].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1904 */
strcpy(grpStructArray[19].startAddress[3].name, "LatNFilter");        /*Name of Parameter*/
       grpStructArray[19].startAddress[3].numb = 1904;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[19].startAddress[3].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[19].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[19].startAddress[3].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[19].startAddress[3].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[19].startAddress[3].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[19].startAddress[3].paramDefValue.valueFloat = 100;           /*Default value of Parameter*/
       grpStructArray[19].startAddress[3].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[19].startAddress[3].paramActValue.valueFloat = 100;           /*Active Value of Parameter*/
       grpStructArray[19].startAddress[3].paramMaxValue.valueFloat = 255;           /*Max Value of Parameter*/
       grpStructArray[19].startAddress[3].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[19].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1905 */
strcpy(grpStructArray[19].startAddress[4].name, "AngDifGain");        /*Name of Parameter*/
       grpStructArray[19].startAddress[4].numb = 1905;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[19].startAddress[4].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[19].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[19].startAddress[4].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[19].startAddress[4].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[19].startAddress[4].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[19].startAddress[4].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[19].startAddress[4].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[19].startAddress[4].paramActValue.valueFloat = 0;             /*Active Value of Parameter*/
       grpStructArray[19].startAddress[4].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[19].startAddress[4].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[19].startAddress[4].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1906 */
strcpy(grpStructArray[19].startAddress[5].name, "AngIntGain");        /*Name of Parameter*/
       grpStructArray[19].startAddress[5].numb = 1906;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[19].startAddress[5].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[19].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[19].startAddress[5].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[19].startAddress[5].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[19].startAddress[5].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[19].startAddress[5].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[19].startAddress[5].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[19].startAddress[5].paramActValue.valueFloat = 0;             /*Active Value of Parameter*/
       grpStructArray[19].startAddress[5].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[19].startAddress[5].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[19].startAddress[5].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1907 */
strcpy(grpStructArray[19].startAddress[6].name, "AngProGain");        /*Name of Parameter*/
       grpStructArray[19].startAddress[6].numb = 1907;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[19].startAddress[6].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[19].startAddress[6].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[19].startAddress[6].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[19].startAddress[6].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[19].startAddress[6].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[19].startAddress[6].paramDefValue.valueFloat = 0.05;          /*Default value of Parameter*/
       grpStructArray[19].startAddress[6].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[19].startAddress[6].paramActValue.valueFloat = 0.05;          /*Active Value of Parameter*/
       grpStructArray[19].startAddress[6].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[19].startAddress[6].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[19].startAddress[6].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1908 */
strcpy(grpStructArray[19].startAddress[7].name, "AngNFilter");        /*Name of Parameter*/
       grpStructArray[19].startAddress[7].numb = 1908;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[19].startAddress[7].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[19].startAddress[7].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[19].startAddress[7].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[19].startAddress[7].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[19].startAddress[7].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[19].startAddress[7].paramDefValue.valueFloat = 100;           /*Default value of Parameter*/
       grpStructArray[19].startAddress[7].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[19].startAddress[7].paramActValue.valueFloat = 100;           /*Active Value of Parameter*/
       grpStructArray[19].startAddress[7].paramMaxValue.valueFloat = 255;           /*Max Value of Parameter*/
       grpStructArray[19].startAddress[7].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[19].startAddress[7].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*1909 */
strcpy(grpStructArray[19].startAddress[8].name, "AngRateLimRising");  /*Name of Parameter*/
       grpStructArray[19].startAddress[8].numb = 1909;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[19].startAddress[8].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[19].startAddress[8].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[19].startAddress[8].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[19].startAddress[8].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[19].startAddress[8].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[19].startAddress[8].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[19].startAddress[8].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[19].startAddress[8].paramActValue.valueFloat = 0.1;           /*Active Value of Parameter*/
       grpStructArray[19].startAddress[8].paramMaxValue.valueFloat = 1;             /*Max Value of Parameter*/
       grpStructArray[19].startAddress[8].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[19].startAddress[8].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*1910 */
strcpy(grpStructArray[19].startAddress[9].name, "AngRateLimFalling"); /*Name of Parameter*/
       grpStructArray[19].startAddress[9].numb = 1910;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[19].startAddress[9].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[19].startAddress[9].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[19].startAddress[9].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[19].startAddress[9].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[19].startAddress[9].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[19].startAddress[9].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[19].startAddress[9].paramMinValue.valueFloat = -1;            /*Min Value of Parameter*/
       grpStructArray[19].startAddress[9].paramActValue.valueFloat = -0.1;          /*Active Value of Parameter*/
       grpStructArray[19].startAddress[9].paramMaxValue.valueFloat = 0;             /*Max Value of Parameter*/
       grpStructArray[19].startAddress[9].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[19].startAddress[9].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*2001 */
strcpy(grpStructArray[20].startAddress[0].name, "VehLength");         /*Name of Parameter*/
       grpStructArray[20].startAddress[0].numb = 2001;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[20].startAddress[0].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[20].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[20].startAddress[0].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[20].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[20].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[20].startAddress[0].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[20].startAddress[0].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[20].startAddress[0].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[20].startAddress[0].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[20].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[20].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2002 */
strcpy(grpStructArray[20].startAddress[1].name, "VehWidth");          /*Name of Parameter*/
       grpStructArray[20].startAddress[1].numb = 2002;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[20].startAddress[1].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[20].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[20].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[20].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[20].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[20].startAddress[1].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[20].startAddress[1].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[20].startAddress[1].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[20].startAddress[1].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[20].startAddress[1].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[20].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2003 */
strcpy(grpStructArray[20].startAddress[2].name, "VehHight");          /*Name of Parameter*/
       grpStructArray[20].startAddress[2].numb = 2003;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[20].startAddress[2].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[20].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[20].startAddress[2].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[20].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[20].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[20].startAddress[2].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[20].startAddress[2].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[20].startAddress[2].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[20].startAddress[2].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[20].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[20].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2004 */
strcpy(grpStructArray[20].startAddress[3].name, "VehMass");           /*Name of Parameter*/
       grpStructArray[20].startAddress[3].numb = 2004;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[20].startAddress[3].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[20].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[20].startAddress[3].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[20].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[20].startAddress[3].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[20].startAddress[3].paramDefValue.valueFloat = 0;             /*Default value of Parameter*/
       grpStructArray[20].startAddress[3].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[20].startAddress[3].paramActValue.valueFloat = 1;             /*Active Value of Parameter*/
       grpStructArray[20].startAddress[3].paramMaxValue.valueFloat = 6;             /*Max Value of Parameter*/
       grpStructArray[20].startAddress[3].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[20].startAddress[3].decPlace = 1; /*Number of digits behind the comma for float Parameter*/

/*2005 */
strcpy(grpStructArray[20].startAddress[4].name, "DrivetrainType");    /*Name of Parameter*/
       grpStructArray[20].startAddress[4].numb = 2005;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[20].startAddress[4].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[20].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[20].startAddress[4].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[20].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[20].startAddress[4].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[20].startAddress[4].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[20].startAddress[4].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[20].startAddress[4].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[20].startAddress[4].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[20].startAddress[4].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[20].startAddress[4].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2006 */
strcpy(grpStructArray[20].startAddress[5].name, "WheelHex");          /*Name of Parameter*/
       grpStructArray[20].startAddress[5].numb = 2006;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[20].startAddress[5].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[20].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[20].startAddress[5].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[20].startAddress[5].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[20].startAddress[5].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[20].startAddress[5].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[20].startAddress[5].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[20].startAddress[5].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[20].startAddress[5].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[20].startAddress[5].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[20].startAddress[5].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2007 */
strcpy(grpStructArray[20].startAddress[6].name, "WheelBaseFront");    /*Name of Parameter*/
       grpStructArray[20].startAddress[6].numb = 2007;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[20].startAddress[6].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[20].startAddress[6].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[20].startAddress[6].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[20].startAddress[6].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[20].startAddress[6].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[20].startAddress[6].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[20].startAddress[6].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[20].startAddress[6].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[20].startAddress[6].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[20].startAddress[6].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[20].startAddress[6].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2008 */
strcpy(grpStructArray[20].startAddress[7].name, "WheelBaseRear");     /*Name of Parameter*/
       grpStructArray[20].startAddress[7].numb = 2008;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[20].startAddress[7].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[20].startAddress[7].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[20].startAddress[7].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[20].startAddress[7].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[20].startAddress[7].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[20].startAddress[7].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[20].startAddress[7].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[20].startAddress[7].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[20].startAddress[7].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[20].startAddress[7].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[20].startAddress[7].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2009 */
strcpy(grpStructArray[20].startAddress[8].name, "Gauge");             /*Name of Parameter*/
       grpStructArray[20].startAddress[8].numb = 2009;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[20].startAddress[8].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[20].startAddress[8].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[20].startAddress[8].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[20].startAddress[8].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[20].startAddress[8].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[20].startAddress[8].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[20].startAddress[8].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[20].startAddress[8].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[20].startAddress[8].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[20].startAddress[8].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[20].startAddress[8].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2101 */
strcpy(grpStructArray[21].startAddress[0].name, "MotRotSpdMax");      /*Name of Parameter*/
       grpStructArray[21].startAddress[0].numb = 2101;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[0].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[21].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[21].startAddress[0].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[0].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[0].paramDefValue.valueFloat = 6320.43;       /*Default value of Parameter*/
       grpStructArray[21].startAddress[0].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[21].startAddress[0].paramActValue.valueFloat = 6320.43;       /*Active Value of Parameter*/
       grpStructArray[21].startAddress[0].paramMaxValue.valueFloat = 6320.43;       /*Max Value of Parameter*/
       grpStructArray[21].startAddress[0].digits = 4;   /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[0].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*2102 */
strcpy(grpStructArray[21].startAddress[1].name, "MotRotSpdMin");      /*Name of Parameter*/
       grpStructArray[21].startAddress[1].numb = 2102;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[1].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[21].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[21].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[1].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[1].paramDefValue.valueFloat = -6320.43;      /*Default value of Parameter*/
       grpStructArray[21].startAddress[1].paramMinValue.valueFloat = -6320.43;      /*Min Value of Parameter*/
       grpStructArray[21].startAddress[1].paramActValue.valueFloat = -6320.43;      /*Active Value of Parameter*/
       grpStructArray[21].startAddress[1].paramMaxValue.valueFloat = 0;             /*Max Value of Parameter*/
       grpStructArray[21].startAddress[1].digits = 4;   /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[1].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*2103 */
strcpy(grpStructArray[21].startAddress[2].name, "AccelMax");          /*Name of Parameter*/
       grpStructArray[21].startAddress[2].numb = 2103;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[2].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[21].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[21].startAddress[2].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[2].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[2].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[21].startAddress[2].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[21].startAddress[2].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[21].startAddress[2].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[21].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2104 */
strcpy(grpStructArray[21].startAddress[3].name, "DecelMax");          /*Name of Parameter*/
       grpStructArray[21].startAddress[3].numb = 2104;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[3].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[21].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[21].startAddress[3].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[3].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[3].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[21].startAddress[3].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[21].startAddress[3].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[21].startAddress[3].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[21].startAddress[3].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2105 */
strcpy(grpStructArray[21].startAddress[4].name, "PowNom");            /*Name of Parameter*/
       grpStructArray[21].startAddress[4].numb = 2105;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[4].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[21].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[21].startAddress[4].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[4].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[4].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[4].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[21].startAddress[4].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[21].startAddress[4].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[21].startAddress[4].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[21].startAddress[4].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[4].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2106 */
strcpy(grpStructArray[21].startAddress[5].name, "WheelCirconf");      /*Name of Parameter*/
       grpStructArray[21].startAddress[5].numb = 2106;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[5].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[21].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[21].startAddress[5].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[5].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[5].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[5].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[21].startAddress[5].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[21].startAddress[5].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[21].startAddress[5].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[21].startAddress[5].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[5].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2107 */
strcpy(grpStructArray[21].startAddress[6].name, "GripNom");           /*Name of Parameter*/
       grpStructArray[21].startAddress[6].numb = 2107;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[6].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[21].startAddress[6].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[21].startAddress[6].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[6].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[6].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[6].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[21].startAddress[6].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[21].startAddress[6].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[21].startAddress[6].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[21].startAddress[6].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[6].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2108 */
strcpy(grpStructArray[21].startAddress[7].name, "PivotPoint");        /*Name of Parameter*/
       grpStructArray[21].startAddress[7].numb = 2108;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[7].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[21].startAddress[7].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[21].startAddress[7].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[7].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[7].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[7].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[21].startAddress[7].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[21].startAddress[7].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[21].startAddress[7].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[21].startAddress[7].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[7].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2109 */
strcpy(grpStructArray[21].startAddress[8].name, "PivotHigh");         /*Name of Parameter*/
       grpStructArray[21].startAddress[8].numb = 2109;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[8].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[21].startAddress[8].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[21].startAddress[8].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[8].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[8].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[8].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[21].startAddress[8].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[21].startAddress[8].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[21].startAddress[8].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[21].startAddress[8].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[8].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2110 */
strcpy(grpStructArray[21].startAddress[9].name, "Caster");            /*Name of Parameter*/
       grpStructArray[21].startAddress[9].numb = 2110;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[9].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[21].startAddress[9].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[21].startAddress[9].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[9].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[9].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[9].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[21].startAddress[9].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[21].startAddress[9].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[21].startAddress[9].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[21].startAddress[9].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[9].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2111 */
strcpy(grpStructArray[21].startAddress[10].name, "SteeringSys");      /*Name of Parameter*/
       grpStructArray[21].startAddress[10].numb = 2111; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[10].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[21].startAddress[10].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[10].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[10].change = PARAM_CHANGEABLE; /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[10].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[10].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[21].startAddress[10].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[21].startAddress[10].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[21].startAddress[10].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[21].startAddress[10].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[10].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*2112 */
strcpy(grpStructArray[21].startAddress[11].name, "FrontShocks");      /*Name of Parameter*/
       grpStructArray[21].startAddress[11].numb = 2112; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[11].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[21].startAddress[11].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[11].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[11].change = PARAM_CHANGEABLE; /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[11].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[11].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[21].startAddress[11].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[21].startAddress[11].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[21].startAddress[11].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[21].startAddress[11].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[11].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*2113 */
strcpy(grpStructArray[21].startAddress[12].name, "RearShocks");       /*Name of Parameter*/
       grpStructArray[21].startAddress[12].numb = 2113; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[12].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[21].startAddress[12].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[12].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[12].change = PARAM_CHANGEABLE; /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[12].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[12].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[21].startAddress[12].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[21].startAddress[12].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[21].startAddress[12].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[21].startAddress[12].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[12].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*2114 */
strcpy(grpStructArray[21].startAddress[13].name, "ChassisBraces");    /*Name of Parameter*/
       grpStructArray[21].startAddress[13].numb = 2114; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[13].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[21].startAddress[13].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[13].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[13].change = PARAM_CHANGEABLE; /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[13].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[13].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[21].startAddress[13].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[21].startAddress[13].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[21].startAddress[13].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[21].startAddress[13].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[13].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*2115 */
strcpy(grpStructArray[21].startAddress[14].name, "CamberFront");      /*Name of Parameter*/
       grpStructArray[21].startAddress[14].numb = 2115; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[14].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[21].startAddress[14].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[14].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[14].change = PARAM_CHANGEABLE; /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[14].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[14].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[21].startAddress[14].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[21].startAddress[14].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[21].startAddress[14].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[21].startAddress[14].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[14].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*2116 */
strcpy(grpStructArray[21].startAddress[15].name, "CamberRear");       /*Name of Parameter*/
       grpStructArray[21].startAddress[15].numb = 2116; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[15].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[21].startAddress[15].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[15].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[15].change = PARAM_CHANGEABLE; /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[15].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[15].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[21].startAddress[15].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[21].startAddress[15].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[21].startAddress[15].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[21].startAddress[15].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[15].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*2117 */
strcpy(grpStructArray[21].startAddress[16].name, "AntiRollBarFront"); /*Name of Parameter*/
       grpStructArray[21].startAddress[16].numb = 2117; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[16].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[21].startAddress[16].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[16].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[16].change = PARAM_CHANGEABLE; /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[16].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[16].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[21].startAddress[16].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[21].startAddress[16].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[21].startAddress[16].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[21].startAddress[16].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[16].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*2118 */
strcpy(grpStructArray[21].startAddress[17].name, "AntiRollBarRear");  /*Name of Parameter*/
       grpStructArray[21].startAddress[17].numb = 2118; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[17].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[21].startAddress[17].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[17].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[17].change = PARAM_CHANGEABLE; /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[17].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[17].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[21].startAddress[17].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[21].startAddress[17].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[21].startAddress[17].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[21].startAddress[17].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[17].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*2119 */
strcpy(grpStructArray[21].startAddress[18].name, "ZeroSpdLim");       /*Name of Parameter*/
       grpStructArray[21].startAddress[18].numb = 2119; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[18].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[21].startAddress[18].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[18].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[18].change = PARAM_CHANGEABLE; /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[18].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[18].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[21].startAddress[18].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[21].startAddress[18].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[21].startAddress[18].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[21].startAddress[18].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[18].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*2120 */
strcpy(grpStructArray[21].startAddress[19].name, "FrCStiff");         /*Name of Parameter*/
       grpStructArray[21].startAddress[19].numb = 2120; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[19].variType = PARAM_UINT;     /*Variable type of Parameter*/
       grpStructArray[21].startAddress[19].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[19].sign = PARAM_UNSIGNED;     /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[19].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[19].varianz = PARAM_VARIABLE;  /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[19].paramDefValue.valueUint = 25;            /*Default value of Parameter*/
       grpStructArray[21].startAddress[19].paramMinValue.valueUint = 10;            /*Min Value of Parameter*/
       grpStructArray[21].startAddress[19].paramActValue.valueUint = 25;            /*Active Value of Parameter*/
       grpStructArray[21].startAddress[19].paramMaxValue.valueUint = 99;            /*Max Value of Parameter*/
       grpStructArray[21].startAddress[19].digits = 2;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[19].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*2121 */
strcpy(grpStructArray[21].startAddress[20].name, "RrCStiff");         /*Name of Parameter*/
       grpStructArray[21].startAddress[20].numb = 2121; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[20].variType = PARAM_UINT;     /*Variable type of Parameter*/
       grpStructArray[21].startAddress[20].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[20].sign = PARAM_UNSIGNED;     /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[20].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[20].varianz = PARAM_VARIABLE;  /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[20].paramDefValue.valueUint = 40;            /*Default value of Parameter*/
       grpStructArray[21].startAddress[20].paramMinValue.valueUint = 10;            /*Min Value of Parameter*/
       grpStructArray[21].startAddress[20].paramActValue.valueUint = 40;            /*Active Value of Parameter*/
       grpStructArray[21].startAddress[20].paramMaxValue.valueUint = 99;            /*Max Value of Parameter*/
       grpStructArray[21].startAddress[20].digits = 2;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[20].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*2122 */
strcpy(grpStructArray[21].startAddress[21].name, "YawInert  ");       /*Name of Parameter*/
       grpStructArray[21].startAddress[21].numb = 2122; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[21].variType = PARAM_FLOAT;    /*Variable type of Parameter*/
       grpStructArray[21].startAddress[21].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[21].sign = PARAM_UNSIGNED;     /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[21].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[21].varianz = PARAM_VARIABLE;  /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[21].paramDefValue.valueFloat = 0.088;        /*Default value of Parameter*/
       grpStructArray[21].startAddress[21].paramMinValue.valueFloat = 0;            /*Min Value of Parameter*/
       grpStructArray[21].startAddress[21].paramActValue.valueFloat = 0.088;        /*Active Value of Parameter*/
       grpStructArray[21].startAddress[21].paramMaxValue.valueFloat = 9;            /*Max Value of Parameter*/
       grpStructArray[21].startAddress[21].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[21].decPlace = 3;              /*Number of digits behind the comma for float Parameter*/

/*2123 */
strcpy(grpStructArray[21].startAddress[22].name, "LenCoMRrAx");       /*Name of Parameter*/
       grpStructArray[21].startAddress[22].numb = 2123; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[22].variType = PARAM_UINT;     /*Variable type of Parameter*/
       grpStructArray[21].startAddress[22].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[22].sign = PARAM_UNSIGNED;     /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[22].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[22].varianz = PARAM_VARIABLE;  /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[22].paramDefValue.valueUint = 0,157;         /*Default value of Parameter*/
       grpStructArray[21].startAddress[22].paramMinValue.valueUint = 0;             /*Min Value of Parameter*/
       grpStructArray[21].startAddress[22].paramActValue.valueUint = 0,157;         /*Active Value of Parameter*/
       grpStructArray[21].startAddress[22].paramMaxValue.valueUint = 1;             /*Max Value of Parameter*/
       grpStructArray[21].startAddress[22].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[22].decPlace = 3;              /*Number of digits behind the comma for float Parameter*/

/*2124 */
strcpy(grpStructArray[21].startAddress[23].name, "LenCoMFrAx ");      /*Name of Parameter*/
       grpStructArray[21].startAddress[23].numb = 2124; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[21].startAddress[23].variType = PARAM_UINT;     /*Variable type of Parameter*/
       grpStructArray[21].startAddress[23].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[21].startAddress[23].sign = PARAM_UNSIGNED;     /*is the Parameter signed or unsigned?*/
       grpStructArray[21].startAddress[23].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[21].startAddress[23].varianz = PARAM_VARIABLE;  /*is the Parameter variable or fix?*/
       grpStructArray[21].startAddress[23].paramDefValue.valueUint = 0,173;         /*Default value of Parameter*/
       grpStructArray[21].startAddress[23].paramMinValue.valueUint = 0;             /*Min Value of Parameter*/
       grpStructArray[21].startAddress[23].paramActValue.valueUint = 0,173;         /*Active Value of Parameter*/
       grpStructArray[21].startAddress[23].paramMaxValue.valueUint = 1;             /*Max Value of Parameter*/
       grpStructArray[21].startAddress[23].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[21].startAddress[23].decPlace = 3;              /*Number of digits behind the comma for float Parameter*/

/*2201 */
strcpy(grpStructArray[22].startAddress[0].name, "RcCtrlEna");         /*Name of Parameter*/
       grpStructArray[22].startAddress[0].numb = 2201;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[22].startAddress[0].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[22].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[22].startAddress[0].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[22].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[22].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[22].startAddress[0].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[22].startAddress[0].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[22].startAddress[0].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[22].startAddress[0].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[22].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[22].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2202 */
strcpy(grpStructArray[22].startAddress[1].name, "TrajCtrlEna");       /*Name of Parameter*/
       grpStructArray[22].startAddress[1].numb = 2202;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[22].startAddress[1].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[22].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[22].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[22].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[22].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[22].startAddress[1].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[22].startAddress[1].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[22].startAddress[1].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[22].startAddress[1].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[22].startAddress[1].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[22].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2203 */
strcpy(grpStructArray[22].startAddress[2].name, "ADASCtrlEna");       /*Name of Parameter*/
       grpStructArray[22].startAddress[2].numb = 2203;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[22].startAddress[2].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[22].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[22].startAddress[2].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[22].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[22].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[22].startAddress[2].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[22].startAddress[2].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[22].startAddress[2].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[22].startAddress[2].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[22].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[22].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2204 */
strcpy(grpStructArray[22].startAddress[3].name, "EmeStoppMod");       /*Name of Parameter*/
       grpStructArray[22].startAddress[3].numb = 2204;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[22].startAddress[3].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[22].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[22].startAddress[3].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[22].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[22].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[22].startAddress[3].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[22].startAddress[3].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[22].startAddress[3].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[22].startAddress[3].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[22].startAddress[3].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[22].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2301 */
strcpy(grpStructArray[23].startAddress[0].name, "BatType");           /*Name of Parameter*/
       grpStructArray[23].startAddress[0].numb = 2301;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[23].startAddress[0].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[23].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[23].startAddress[0].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[23].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[23].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[23].startAddress[0].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[23].startAddress[0].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[23].startAddress[0].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[23].startAddress[0].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[23].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[23].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2302 */
strcpy(grpStructArray[23].startAddress[1].name, "VoltageNom");        /*Name of Parameter*/
       grpStructArray[23].startAddress[1].numb = 2302;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[23].startAddress[1].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[23].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[23].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[23].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[23].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[23].startAddress[1].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[23].startAddress[1].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[23].startAddress[1].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[23].startAddress[1].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[23].startAddress[1].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[23].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2303 */
strcpy(grpStructArray[23].startAddress[2].name, "AlertThreshold");    /*Name of Parameter*/
       grpStructArray[23].startAddress[2].numb = 2303;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[23].startAddress[2].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[23].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[23].startAddress[2].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[23].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[23].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[23].startAddress[2].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[23].startAddress[2].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[23].startAddress[2].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[23].startAddress[2].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[23].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[23].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2304 */
strcpy(grpStructArray[23].startAddress[3].name, "Undervoltage");      /*Name of Parameter*/
       grpStructArray[23].startAddress[3].numb = 2304;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[23].startAddress[3].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[23].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[23].startAddress[3].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[23].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[23].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[23].startAddress[3].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[23].startAddress[3].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[23].startAddress[3].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[23].startAddress[3].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[23].startAddress[3].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[23].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2305 */
strcpy(grpStructArray[23].startAddress[4].name, "NumOfCells");        /*Name of Parameter*/
       grpStructArray[23].startAddress[4].numb = 2305;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[23].startAddress[4].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[23].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[23].startAddress[4].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[23].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[23].startAddress[4].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[23].startAddress[4].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[23].startAddress[4].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[23].startAddress[4].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[23].startAddress[4].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[23].startAddress[4].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[23].startAddress[4].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2306 */
strcpy(grpStructArray[23].startAddress[5].name, "CellVoltage");       /*Name of Parameter*/
       grpStructArray[23].startAddress[5].numb = 2306;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[23].startAddress[5].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[23].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[23].startAddress[5].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[23].startAddress[5].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[23].startAddress[5].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[23].startAddress[5].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[23].startAddress[5].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[23].startAddress[5].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[23].startAddress[5].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[23].startAddress[5].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[23].startAddress[5].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2401 */
strcpy(grpStructArray[24].startAddress[0].name, "I2cSlaveAdr");       /*Name of Parameter*/
       grpStructArray[24].startAddress[0].numb = 2401;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[24].startAddress[0].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[24].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[24].startAddress[0].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[24].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[24].startAddress[0].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[24].startAddress[0].paramDefValue.valueInt = 6; /*Default value of Parameter*/
       grpStructArray[24].startAddress[0].paramMinValue.valueInt = 2; /*Min Value of Parameter*/
       grpStructArray[24].startAddress[0].paramActValue.valueInt = 6; /*Active Value of Parameter*/
       grpStructArray[24].startAddress[0].paramMaxValue.valueInt = 124;             /*Max Value of Parameter*/
       grpStructArray[24].startAddress[0].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[24].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2402 */
strcpy(grpStructArray[24].startAddress[1].name, "USdistCalibr");      /*Name of Parameter*/
       grpStructArray[24].startAddress[1].numb = 2402;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[24].startAddress[1].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[24].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[24].startAddress[1].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[24].startAddress[1].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[24].startAddress[1].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[24].startAddress[1].paramDefValue.valueFloat = 1.02;          /*Default value of Parameter*/
       grpStructArray[24].startAddress[1].paramMinValue.valueFloat = 0;             /*Min Value of Parameter*/
       grpStructArray[24].startAddress[1].paramActValue.valueFloat = 1.02;          /*Active Value of Parameter*/
       grpStructArray[24].startAddress[1].paramMaxValue.valueFloat = 10;            /*Max Value of Parameter*/
       grpStructArray[24].startAddress[1].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[24].startAddress[1].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*2403 */
strcpy(grpStructArray[24].startAddress[2].name, "UsdistTemp");        /*Name of Parameter*/
       grpStructArray[24].startAddress[2].numb = 2403;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[24].startAddress[2].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[24].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[24].startAddress[2].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[24].startAddress[2].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[24].startAddress[2].varianz = PARAM_VARIABLE;   /*is the Parameter variable or fix?*/
       grpStructArray[24].startAddress[2].paramDefValue.valueInt = 16;              /*Default value of Parameter*/
       grpStructArray[24].startAddress[2].paramMinValue.valueInt = -100;            /*Min Value of Parameter*/
       grpStructArray[24].startAddress[2].paramActValue.valueInt = 16;              /*Active Value of Parameter*/
       grpStructArray[24].startAddress[2].paramMaxValue.valueInt = 200;             /*Max Value of Parameter*/
       grpStructArray[24].startAddress[2].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[24].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2501 */
strcpy(grpStructArray[25].startAddress[0].name, "SinRngChkHi");       /*Name of Parameter*/
       grpStructArray[25].startAddress[0].numb = 2501;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[25].startAddress[0].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[25].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[25].startAddress[0].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[25].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[25].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[25].startAddress[0].paramDefValue.valueUint = 3753;           /*Default value of Parameter*/
       grpStructArray[25].startAddress[0].paramMinValue.valueUint = 3752;           /*Min Value of Parameter*/
       grpStructArray[25].startAddress[0].paramActValue.valueUint = 3753;           /*Active Value of Parameter*/
       grpStructArray[25].startAddress[0].paramMaxValue.valueUint = 3754;           /*Max Value of Parameter*/
       grpStructArray[25].startAddress[0].digits = 4;   /*Number of digits for decimal Parameter*/
       grpStructArray[25].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2502 */
strcpy(grpStructArray[25].startAddress[1].name, "SinRngChkLo");       /*Name of Parameter*/
       grpStructArray[25].startAddress[1].numb = 2502;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[25].startAddress[1].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[25].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[25].startAddress[1].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[25].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[25].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[25].startAddress[1].paramDefValue.valueUint = 342;            /*Default value of Parameter*/
       grpStructArray[25].startAddress[1].paramMinValue.valueUint = 341;            /*Min Value of Parameter*/
       grpStructArray[25].startAddress[1].paramActValue.valueUint = 342;            /*Active Value of Parameter*/
       grpStructArray[25].startAddress[1].paramMaxValue.valueUint = 343;            /*Max Value of Parameter*/
       grpStructArray[25].startAddress[1].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[25].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2503 */
strcpy(grpStructArray[25].startAddress[2].name, "CosRngChkHi");       /*Name of Parameter*/
       grpStructArray[25].startAddress[2].numb = 2503;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[25].startAddress[2].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[25].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[25].startAddress[2].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[25].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[25].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[25].startAddress[2].paramDefValue.valueUint = 3753;           /*Default value of Parameter*/
       grpStructArray[25].startAddress[2].paramMinValue.valueUint = 3752;           /*Min Value of Parameter*/
       grpStructArray[25].startAddress[2].paramActValue.valueUint = 3753;           /*Active Value of Parameter*/
       grpStructArray[25].startAddress[2].paramMaxValue.valueUint = 3754;           /*Max Value of Parameter*/
       grpStructArray[25].startAddress[2].digits = 4;   /*Number of digits for decimal Parameter*/
       grpStructArray[25].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2504 */
strcpy(grpStructArray[25].startAddress[3].name, "CosRngChkLo");       /*Name of Parameter*/
       grpStructArray[25].startAddress[3].numb = 2504;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[25].startAddress[3].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[25].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[25].startAddress[3].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[25].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[25].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[25].startAddress[3].paramDefValue.valueUint = 342;            /*Default value of Parameter*/
       grpStructArray[25].startAddress[3].paramMinValue.valueUint = 0;              /*Min Value of Parameter*/
       grpStructArray[25].startAddress[3].paramActValue.valueUint = 342;            /*Active Value of Parameter*/
       grpStructArray[25].startAddress[3].paramMaxValue.valueUint = 343;            /*Max Value of Parameter*/
       grpStructArray[25].startAddress[3].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[25].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2505 */
strcpy(grpStructArray[25].startAddress[4].name, "SinCosPlaus");       /*Name of Parameter*/
       grpStructArray[25].startAddress[4].numb = 2505;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[25].startAddress[4].variType = PARAM_FLOAT;     /*Variable type of Parameter*/
       grpStructArray[25].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[25].startAddress[4].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[25].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[25].startAddress[4].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[25].startAddress[4].paramDefValue.valueFloat = 0.2;           /*Default value of Parameter*/
       grpStructArray[25].startAddress[4].paramMinValue.valueFloat = 0.19;          /*Min Value of Parameter*/
       grpStructArray[25].startAddress[4].paramActValue.valueFloat = 0.2;           /*Active Value of Parameter*/
       grpStructArray[25].startAddress[4].paramMaxValue.valueFloat = 0.21;          /*Max Value of Parameter*/
       grpStructArray[25].startAddress[4].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[25].startAddress[4].decPlace = 2; /*Number of digits behind the comma for float Parameter*/

/*2601 */
strcpy(grpStructArray[26].startAddress[0].name, "DiagCurPh1RngChkHi");              /*Name of Parameter*/
       grpStructArray[26].startAddress[0].numb = 2601;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[26].startAddress[0].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[26].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[26].startAddress[0].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[26].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[26].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[26].startAddress[0].paramDefValue.valueUint = 3413;           /*Default value of Parameter*/
       grpStructArray[26].startAddress[0].paramMinValue.valueUint = 3412;           /*Min Value of Parameter*/
       grpStructArray[26].startAddress[0].paramActValue.valueUint = 3413;           /*Active Value of Parameter*/
       grpStructArray[26].startAddress[0].paramMaxValue.valueUint = 3414;           /*Max Value of Parameter*/
       grpStructArray[26].startAddress[0].digits = 4;   /*Number of digits for decimal Parameter*/
       grpStructArray[26].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2602 */
strcpy(grpStructArray[26].startAddress[1].name, "DiagCurPh1RngChkLo");              /*Name of Parameter*/
       grpStructArray[26].startAddress[1].numb = 2602;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[26].startAddress[1].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[26].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[26].startAddress[1].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[26].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[26].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[26].startAddress[1].paramDefValue.valueUint = 50;             /*Default value of Parameter*/
       grpStructArray[26].startAddress[1].paramMinValue.valueUint = 49;             /*Min Value of Parameter*/
       grpStructArray[26].startAddress[1].paramActValue.valueUint = 50;             /*Active Value of Parameter*/
       grpStructArray[26].startAddress[1].paramMaxValue.valueUint = 51;             /*Max Value of Parameter*/
       grpStructArray[26].startAddress[1].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[26].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2603 */
strcpy(grpStructArray[26].startAddress[2].name, "DiagCurPh2RngChkHi");              /*Name of Parameter*/
       grpStructArray[26].startAddress[2].numb = 2603;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[26].startAddress[2].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[26].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[26].startAddress[2].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[26].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[26].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[26].startAddress[2].paramDefValue.valueUint = 3413;           /*Default value of Parameter*/
       grpStructArray[26].startAddress[2].paramMinValue.valueUint = 3412;           /*Min Value of Parameter*/
       grpStructArray[26].startAddress[2].paramActValue.valueUint = 3413;           /*Active Value of Parameter*/
       grpStructArray[26].startAddress[2].paramMaxValue.valueUint = 3414;           /*Max Value of Parameter*/
       grpStructArray[26].startAddress[2].digits = 4;   /*Number of digits for decimal Parameter*/
       grpStructArray[26].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2604 */
strcpy(grpStructArray[26].startAddress[3].name, "DiagCurPh2RngChkLo");              /*Name of Parameter*/
       grpStructArray[26].startAddress[3].numb = 2604;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[26].startAddress[3].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[26].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[26].startAddress[3].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[26].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[26].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[26].startAddress[3].paramDefValue.valueUint = 50;             /*Default value of Parameter*/
       grpStructArray[26].startAddress[3].paramMinValue.valueUint = 49;             /*Min Value of Parameter*/
       grpStructArray[26].startAddress[3].paramActValue.valueUint = 50;             /*Active Value of Parameter*/
       grpStructArray[26].startAddress[3].paramMaxValue.valueUint = 51;             /*Max Value of Parameter*/
       grpStructArray[26].startAddress[3].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[26].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2605 */
strcpy(grpStructArray[26].startAddress[4].name, "DiagCurPh3RngChkHi");              /*Name of Parameter*/
       grpStructArray[26].startAddress[4].numb = 2605;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[26].startAddress[4].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[26].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[26].startAddress[4].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[26].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[26].startAddress[4].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[26].startAddress[4].paramDefValue.valueUint = 3413;           /*Default value of Parameter*/
       grpStructArray[26].startAddress[4].paramMinValue.valueUint = 3412;           /*Min Value of Parameter*/
       grpStructArray[26].startAddress[4].paramActValue.valueUint = 3413;           /*Active Value of Parameter*/
       grpStructArray[26].startAddress[4].paramMaxValue.valueUint = 3414;           /*Max Value of Parameter*/
       grpStructArray[26].startAddress[4].digits = 4;   /*Number of digits for decimal Parameter*/
       grpStructArray[26].startAddress[4].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*2606 */
strcpy(grpStructArray[26].startAddress[5].name, "DiagCurPh3RngChkLo");              /*Name of Parameter*/
       grpStructArray[26].startAddress[5].numb = 2606;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[26].startAddress[5].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[26].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[26].startAddress[5].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[26].startAddress[5].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[26].startAddress[5].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[26].startAddress[5].paramDefValue.valueUint = 50;             /*Default value of Parameter*/
       grpStructArray[26].startAddress[5].paramMinValue.valueUint = 49;             /*Min Value of Parameter*/
       grpStructArray[26].startAddress[5].paramActValue.valueUint = 50;             /*Active Value of Parameter*/
       grpStructArray[26].startAddress[5].paramMaxValue.valueUint = 51;             /*Max Value of Parameter*/
       grpStructArray[26].startAddress[5].digits = 2;   /*Number of digits for decimal Parameter*/
       grpStructArray[26].startAddress[5].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3001 */
strcpy(grpStructArray[30].startAddress[0].name, "MotType");           /*Name of Parameter*/
       grpStructArray[30].startAddress[0].numb = 3001;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[30].startAddress[0].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[30].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[30].startAddress[0].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[30].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[30].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[30].startAddress[0].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[30].startAddress[0].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[30].startAddress[0].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[30].startAddress[0].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[30].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[30].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3002 */
strcpy(grpStructArray[30].startAddress[1].name, "FieldConstant");     /*Name of Parameter*/
       grpStructArray[30].startAddress[1].numb = 3002;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[30].startAddress[1].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[30].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[30].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[30].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[30].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[30].startAddress[1].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[30].startAddress[1].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[30].startAddress[1].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[30].startAddress[1].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[30].startAddress[1].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[30].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3003 */
strcpy(grpStructArray[30].startAddress[2].name, "WindingResist");     /*Name of Parameter*/
       grpStructArray[30].startAddress[2].numb = 3003;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[30].startAddress[2].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[30].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[30].startAddress[2].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[30].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[30].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[30].startAddress[2].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[30].startAddress[2].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[30].startAddress[2].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[30].startAddress[2].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[30].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[30].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3004 */
strcpy(grpStructArray[30].startAddress[3].name, "WindingInduct");     /*Name of Parameter*/
       grpStructArray[30].startAddress[3].numb = 3004;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[30].startAddress[3].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[30].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[30].startAddress[3].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[30].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[30].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[30].startAddress[3].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[30].startAddress[3].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[30].startAddress[3].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[30].startAddress[3].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[30].startAddress[3].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[30].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3005 */
strcpy(grpStructArray[30].startAddress[4].name, "CrossInduct");       /*Name of Parameter*/
       grpStructArray[30].startAddress[4].numb = 3005;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[30].startAddress[4].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[30].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[30].startAddress[4].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[30].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[30].startAddress[4].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[30].startAddress[4].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[30].startAddress[4].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[30].startAddress[4].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[30].startAddress[4].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[30].startAddress[4].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[30].startAddress[4].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3006 */
strcpy(grpStructArray[30].startAddress[5].name, "PolePairs");         /*Name of Parameter*/
       grpStructArray[30].startAddress[5].numb = 3006;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[30].startAddress[5].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[30].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[30].startAddress[5].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[30].startAddress[5].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[30].startAddress[5].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[30].startAddress[5].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[30].startAddress[5].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[30].startAddress[5].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[30].startAddress[5].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[30].startAddress[5].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[30].startAddress[5].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3007 */
strcpy(grpStructArray[30].startAddress[6].name, "Inertia");           /*Name of Parameter*/
       grpStructArray[30].startAddress[6].numb = 3007;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[30].startAddress[6].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[30].startAddress[6].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[30].startAddress[6].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[30].startAddress[6].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[30].startAddress[6].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[30].startAddress[6].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[30].startAddress[6].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[30].startAddress[6].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[30].startAddress[6].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[30].startAddress[6].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[30].startAddress[6].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3201 */
strcpy(grpStructArray[32].startAddress[0].name, "Coeff");             /*Name of Parameter*/
       grpStructArray[32].startAddress[0].numb = 3201;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[32].startAddress[0].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[32].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[32].startAddress[0].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[32].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[32].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[32].startAddress[0].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[32].startAddress[0].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[32].startAddress[0].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[32].startAddress[0].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[32].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[32].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3202 */
strcpy(grpStructArray[32].startAddress[1].name, "AddValue");          /*Name of Parameter*/
       grpStructArray[32].startAddress[1].numb = 3202;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[32].startAddress[1].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[32].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[32].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[32].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[32].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[32].startAddress[1].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[32].startAddress[1].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[32].startAddress[1].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[32].startAddress[1].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[32].startAddress[1].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[32].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3203 */
strcpy(grpStructArray[32].startAddress[2].name, "Noise");             /*Name of Parameter*/
       grpStructArray[32].startAddress[2].numb = 3203;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[32].startAddress[2].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[32].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[32].startAddress[2].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[32].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[32].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[32].startAddress[2].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[32].startAddress[2].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[32].startAddress[2].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[32].startAddress[2].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[32].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[32].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3204 */
strcpy(grpStructArray[32].startAddress[3].name, "ArraySize");         /*Name of Parameter*/
       grpStructArray[32].startAddress[3].numb = 3204;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[32].startAddress[3].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[32].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[32].startAddress[3].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[32].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[32].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[32].startAddress[3].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[32].startAddress[3].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[32].startAddress[3].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[32].startAddress[3].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[32].startAddress[3].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[32].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3301 */
strcpy(grpStructArray[33].startAddress[0].name, "MotConst");          /*Name of Parameter*/
       grpStructArray[33].startAddress[0].numb = 3301;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[33].startAddress[0].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[33].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[33].startAddress[0].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[33].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[33].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[33].startAddress[0].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[33].startAddress[0].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[33].startAddress[0].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[33].startAddress[0].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[33].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[33].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3302 */
strcpy(grpStructArray[33].startAddress[1].name, "FreqPWM");           /*Name of Parameter*/
       grpStructArray[33].startAddress[1].numb = 3302;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[33].startAddress[1].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[33].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[33].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[33].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[33].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[33].startAddress[1].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[33].startAddress[1].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[33].startAddress[1].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[33].startAddress[1].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[33].startAddress[1].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[33].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3303 */
strcpy(grpStructArray[33].startAddress[2].name, "ResolutionPWM");     /*Name of Parameter*/
       grpStructArray[33].startAddress[2].numb = 3303;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[33].startAddress[2].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[33].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[33].startAddress[2].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[33].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[33].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[33].startAddress[2].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[33].startAddress[2].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[33].startAddress[2].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[33].startAddress[2].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[33].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[33].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3304 */
strcpy(grpStructArray[33].startAddress[3].name, "ModePWM");           /*Name of Parameter*/
       grpStructArray[33].startAddress[3].numb = 3304;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[33].startAddress[3].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[33].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[33].startAddress[3].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[33].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[33].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[33].startAddress[3].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[33].startAddress[3].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[33].startAddress[3].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[33].startAddress[3].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[33].startAddress[3].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[33].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3305 */
strcpy(grpStructArray[33].startAddress[4].name, "DCMotOffset");       /*Name of Parameter*/
       grpStructArray[33].startAddress[4].numb = 3305;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[33].startAddress[4].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[33].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[33].startAddress[4].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[33].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[33].startAddress[4].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[33].startAddress[4].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[33].startAddress[4].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[33].startAddress[4].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[33].startAddress[4].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[33].startAddress[4].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[33].startAddress[4].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3306 */
strcpy(grpStructArray[33].startAddress[5].name, "DCMotFactorI");      /*Name of Parameter*/
       grpStructArray[33].startAddress[5].numb = 3306;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[33].startAddress[5].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[33].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[33].startAddress[5].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[33].startAddress[5].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[33].startAddress[5].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[33].startAddress[5].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[33].startAddress[5].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[33].startAddress[5].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[33].startAddress[5].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[33].startAddress[5].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[33].startAddress[5].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3307 */
strcpy(grpStructArray[33].startAddress[6].name, "DCMotFactorU");      /*Name of Parameter*/
       grpStructArray[33].startAddress[6].numb = 3307;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[33].startAddress[6].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[33].startAddress[6].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[33].startAddress[6].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[33].startAddress[6].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[33].startAddress[6].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[33].startAddress[6].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[33].startAddress[6].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[33].startAddress[6].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[33].startAddress[6].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[33].startAddress[6].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[33].startAddress[6].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3308 */
strcpy(grpStructArray[33].startAddress[7].name, "RefValueADC");       /*Name of Parameter*/
       grpStructArray[33].startAddress[7].numb = 3308;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[33].startAddress[7].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[33].startAddress[7].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[33].startAddress[7].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[33].startAddress[7].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[33].startAddress[7].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[33].startAddress[7].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[33].startAddress[7].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[33].startAddress[7].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[33].startAddress[7].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[33].startAddress[7].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[33].startAddress[7].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3309 */
strcpy(grpStructArray[33].startAddress[8].name, "ResolutionADC");     /*Name of Parameter*/
       grpStructArray[33].startAddress[8].numb = 3309;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[33].startAddress[8].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[33].startAddress[8].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[33].startAddress[8].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[33].startAddress[8].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[33].startAddress[8].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[33].startAddress[8].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[33].startAddress[8].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[33].startAddress[8].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[33].startAddress[8].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[33].startAddress[8].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[33].startAddress[8].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3401 */
strcpy(grpStructArray[34].startAddress[0].name, "DCMotPolyX0");       /*Name of Parameter*/
       grpStructArray[34].startAddress[0].numb = 3401;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[0].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[34].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[34].startAddress[0].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[0].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[34].startAddress[0].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[34].startAddress[0].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[34].startAddress[0].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[34].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3402 */
strcpy(grpStructArray[34].startAddress[1].name, "DCMotPolyX1");       /*Name of Parameter*/
       grpStructArray[34].startAddress[1].numb = 3402;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[1].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[34].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[34].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[1].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[34].startAddress[1].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[34].startAddress[1].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[34].startAddress[1].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[34].startAddress[1].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3403 */
strcpy(grpStructArray[34].startAddress[2].name, "DCMotPolyX2");       /*Name of Parameter*/
       grpStructArray[34].startAddress[2].numb = 3403;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[2].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[34].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[34].startAddress[2].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[2].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[34].startAddress[2].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[34].startAddress[2].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[34].startAddress[2].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[34].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3404 */
strcpy(grpStructArray[34].startAddress[3].name, "DCMotPolyX3");       /*Name of Parameter*/
       grpStructArray[34].startAddress[3].numb = 3404;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[3].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[34].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[34].startAddress[3].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[3].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[34].startAddress[3].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[34].startAddress[3].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[34].startAddress[3].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[34].startAddress[3].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3405 */
strcpy(grpStructArray[34].startAddress[4].name, "DCMotPolyX4");       /*Name of Parameter*/
       grpStructArray[34].startAddress[4].numb = 3405;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[4].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[34].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[34].startAddress[4].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[4].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[4].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[34].startAddress[4].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[34].startAddress[4].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[34].startAddress[4].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[34].startAddress[4].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[4].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3406 */
strcpy(grpStructArray[34].startAddress[5].name, "DCMotPolyX5");       /*Name of Parameter*/
       grpStructArray[34].startAddress[5].numb = 3406;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[5].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[34].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[34].startAddress[5].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[5].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[5].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[5].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[34].startAddress[5].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[34].startAddress[5].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[34].startAddress[5].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[34].startAddress[5].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[5].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3407 */
strcpy(grpStructArray[34].startAddress[6].name, "DCMotPolyX6");       /*Name of Parameter*/
       grpStructArray[34].startAddress[6].numb = 3407;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[6].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[34].startAddress[6].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[34].startAddress[6].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[6].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[6].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[6].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[34].startAddress[6].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[34].startAddress[6].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[34].startAddress[6].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[34].startAddress[6].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[6].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3408 */
strcpy(grpStructArray[34].startAddress[7].name, "DCMotGain");         /*Name of Parameter*/
       grpStructArray[34].startAddress[7].numb = 3408;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[7].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[34].startAddress[7].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[34].startAddress[7].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[7].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[7].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[7].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[34].startAddress[7].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[34].startAddress[7].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[34].startAddress[7].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[34].startAddress[7].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[7].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3409 */
strcpy(grpStructArray[34].startAddress[8].name, "CurTgt");            /*Name of Parameter*/
       grpStructArray[34].startAddress[8].numb = 3409;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[8].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[34].startAddress[8].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[34].startAddress[8].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[8].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[8].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[8].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[34].startAddress[8].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[34].startAddress[8].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[34].startAddress[8].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[34].startAddress[8].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[8].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3410 */
strcpy(grpStructArray[34].startAddress[9].name, "VolTgt");            /*Name of Parameter*/
       grpStructArray[34].startAddress[9].numb = 3410;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[9].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[34].startAddress[9].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[34].startAddress[9].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[9].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[9].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[9].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[34].startAddress[9].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[34].startAddress[9].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[34].startAddress[9].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[34].startAddress[9].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[9].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3411 */
strcpy(grpStructArray[34].startAddress[10].name, "TrqTgt");           /*Name of Parameter*/
       grpStructArray[34].startAddress[10].numb = 3411; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[10].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[34].startAddress[10].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[34].startAddress[10].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[10].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[10].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[10].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[34].startAddress[10].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[34].startAddress[10].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[34].startAddress[10].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[34].startAddress[10].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[10].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3412 */
strcpy(grpStructArray[34].startAddress[11].name, "PIDkp");            /*Name of Parameter*/
       grpStructArray[34].startAddress[11].numb = 3412; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[11].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[34].startAddress[11].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[34].startAddress[11].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[11].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[11].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[11].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[34].startAddress[11].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[34].startAddress[11].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[34].startAddress[11].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[34].startAddress[11].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[11].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3413 */
strcpy(grpStructArray[34].startAddress[12].name, "PIDki");            /*Name of Parameter*/
       grpStructArray[34].startAddress[12].numb = 3413; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[12].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[34].startAddress[12].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[34].startAddress[12].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[12].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[12].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[12].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[34].startAddress[12].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[34].startAddress[12].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[34].startAddress[12].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[34].startAddress[12].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[12].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3414 */
strcpy(grpStructArray[34].startAddress[13].name, "PIDkd");            /*Name of Parameter*/
       grpStructArray[34].startAddress[13].numb = 3414; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[13].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[34].startAddress[13].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[34].startAddress[13].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[13].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[13].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[13].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[34].startAddress[13].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[34].startAddress[13].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[34].startAddress[13].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[34].startAddress[13].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[13].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3415 */
strcpy(grpStructArray[34].startAddress[14].name, "PIDmin");           /*Name of Parameter*/
       grpStructArray[34].startAddress[14].numb = 3415; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[14].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[34].startAddress[14].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[34].startAddress[14].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[14].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[14].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[14].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[34].startAddress[14].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[34].startAddress[14].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[34].startAddress[14].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[34].startAddress[14].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[14].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3416 */
strcpy(grpStructArray[34].startAddress[15].name, "PIDmax");           /*Name of Parameter*/
       grpStructArray[34].startAddress[15].numb = 3416; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[15].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[34].startAddress[15].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[34].startAddress[15].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[15].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[15].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[15].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[34].startAddress[15].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[34].startAddress[15].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[34].startAddress[15].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[34].startAddress[15].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[15].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3417 */
strcpy(grpStructArray[34].startAddress[16].name, "PIDta");            /*Name of Parameter*/
       grpStructArray[34].startAddress[16].numb = 3417; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[16].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[34].startAddress[16].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[34].startAddress[16].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[16].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[16].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[16].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[34].startAddress[16].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[34].startAddress[16].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[34].startAddress[16].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[34].startAddress[16].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[16].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3418 */
strcpy(grpStructArray[34].startAddress[17].name, "PIDgain");          /*Name of Parameter*/
       grpStructArray[34].startAddress[17].numb = 3418; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[34].startAddress[17].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[34].startAddress[17].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[34].startAddress[17].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[34].startAddress[17].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[34].startAddress[17].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[34].startAddress[17].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[34].startAddress[17].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[34].startAddress[17].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[34].startAddress[17].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[34].startAddress[17].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[34].startAddress[17].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3501 */
strcpy(grpStructArray[35].startAddress[0].name, "SpdPolyX0");         /*Name of Parameter*/
       grpStructArray[35].startAddress[0].numb = 3501;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[0].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[35].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[35].startAddress[0].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[0].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[0].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[35].startAddress[0].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[35].startAddress[0].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[35].startAddress[0].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[35].startAddress[0].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3502 */
strcpy(grpStructArray[35].startAddress[1].name, "SpdPolyX1");         /*Name of Parameter*/
       grpStructArray[35].startAddress[1].numb = 3502;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[1].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[35].startAddress[1].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[35].startAddress[1].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[1].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[1].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[1].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[35].startAddress[1].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[35].startAddress[1].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[35].startAddress[1].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[35].startAddress[1].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[1].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3503 */
strcpy(grpStructArray[35].startAddress[2].name, "SpdPolyX2");         /*Name of Parameter*/
       grpStructArray[35].startAddress[2].numb = 3503;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[2].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[35].startAddress[2].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[35].startAddress[2].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[2].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[2].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[2].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[35].startAddress[2].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[35].startAddress[2].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[35].startAddress[2].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[35].startAddress[2].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[2].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3504 */
strcpy(grpStructArray[35].startAddress[3].name, "SpdPolyX3");         /*Name of Parameter*/
       grpStructArray[35].startAddress[3].numb = 3504;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[3].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[35].startAddress[3].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[35].startAddress[3].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[3].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[3].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[3].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[35].startAddress[3].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[35].startAddress[3].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[35].startAddress[3].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[35].startAddress[3].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[3].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3505 */
strcpy(grpStructArray[35].startAddress[4].name, "SpdPolyX4");         /*Name of Parameter*/
       grpStructArray[35].startAddress[4].numb = 3505;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[4].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[35].startAddress[4].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[35].startAddress[4].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[4].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[4].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[4].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[35].startAddress[4].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[35].startAddress[4].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[35].startAddress[4].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[35].startAddress[4].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[4].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3506 */
strcpy(grpStructArray[35].startAddress[5].name, "SpdPolyX5");         /*Name of Parameter*/
       grpStructArray[35].startAddress[5].numb = 3506;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[5].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[35].startAddress[5].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[35].startAddress[5].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[5].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[5].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[5].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[35].startAddress[5].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[35].startAddress[5].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[35].startAddress[5].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[35].startAddress[5].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[5].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3507 */
strcpy(grpStructArray[35].startAddress[6].name, "SpdPolyX6");         /*Name of Parameter*/
       grpStructArray[35].startAddress[6].numb = 3507;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[6].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[35].startAddress[6].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[35].startAddress[6].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[6].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[6].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[6].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[35].startAddress[6].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[35].startAddress[6].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[35].startAddress[6].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[35].startAddress[6].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[6].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3508 */
strcpy(grpStructArray[35].startAddress[7].name, "SpdGain");           /*Name of Parameter*/
       grpStructArray[35].startAddress[7].numb = 3508;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[7].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[35].startAddress[7].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[35].startAddress[7].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[7].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[7].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[7].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[35].startAddress[7].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[35].startAddress[7].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[35].startAddress[7].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[35].startAddress[7].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[7].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3509 */
strcpy(grpStructArray[35].startAddress[8].name, "PIDki");             /*Name of Parameter*/
       grpStructArray[35].startAddress[8].numb = 3509;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[8].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[35].startAddress[8].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[35].startAddress[8].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[8].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[8].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[8].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[35].startAddress[8].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[35].startAddress[8].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[35].startAddress[8].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[35].startAddress[8].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[8].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3510 */
strcpy(grpStructArray[35].startAddress[9].name, "PIDkp");             /*Name of Parameter*/
       grpStructArray[35].startAddress[9].numb = 3510;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[9].variType = PARAM_INT;       /*Variable type of Parameter*/
       grpStructArray[35].startAddress[9].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[35].startAddress[9].sign = PARAM_SIGNED;        /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[9].change = PARAM_NOTCHANGEABLE;             /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[9].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[9].paramDefValue.valueInt = 0; /*Default value of Parameter*/
       grpStructArray[35].startAddress[9].paramMinValue.valueInt = 0; /*Min Value of Parameter*/
       grpStructArray[35].startAddress[9].paramActValue.valueInt = 0; /*Active Value of Parameter*/
       grpStructArray[35].startAddress[9].paramMaxValue.valueInt = 0; /*Max Value of Parameter*/
       grpStructArray[35].startAddress[9].digits = 1;   /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[9].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

/*3511 */
strcpy(grpStructArray[35].startAddress[10].name, "PIDkd");            /*Name of Parameter*/
       grpStructArray[35].startAddress[10].numb = 3511; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[10].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[35].startAddress[10].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[35].startAddress[10].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[10].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[10].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[10].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[35].startAddress[10].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[35].startAddress[10].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[35].startAddress[10].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[35].startAddress[10].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[10].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3512 */
strcpy(grpStructArray[35].startAddress[11].name, "PIDmin");           /*Name of Parameter*/
       grpStructArray[35].startAddress[11].numb = 3512; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[11].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[35].startAddress[11].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[35].startAddress[11].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[11].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[11].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[11].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[35].startAddress[11].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[35].startAddress[11].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[35].startAddress[11].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[35].startAddress[11].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[11].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3513 */
strcpy(grpStructArray[35].startAddress[12].name, "PIDmax");           /*Name of Parameter*/
       grpStructArray[35].startAddress[12].numb = 3513; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[12].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[35].startAddress[12].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[35].startAddress[12].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[12].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[12].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[12].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[35].startAddress[12].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[35].startAddress[12].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[35].startAddress[12].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[35].startAddress[12].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[12].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3514 */
strcpy(grpStructArray[35].startAddress[13].name, "PIDta");            /*Name of Parameter*/
       grpStructArray[35].startAddress[13].numb = 3514; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[13].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[35].startAddress[13].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[35].startAddress[13].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[13].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[13].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[13].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[35].startAddress[13].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[35].startAddress[13].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[35].startAddress[13].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[35].startAddress[13].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[13].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3515 */
strcpy(grpStructArray[35].startAddress[14].name, "PIDgain");          /*Name of Parameter*/
       grpStructArray[35].startAddress[14].numb = 3515; /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[35].startAddress[14].variType = PARAM_INT;      /*Variable type of Parameter*/
       grpStructArray[35].startAddress[14].unit = PARAM_VOLT;         /*Unit of Parameter*/
       grpStructArray[35].startAddress[14].sign = PARAM_SIGNED;       /*is the Parameter signed or unsigned?*/
       grpStructArray[35].startAddress[14].change = PARAM_NOTCHANGEABLE;            /*is the Parameter changeable during operation?*/
       grpStructArray[35].startAddress[14].varianz = PARAM_NOTVARIABLE;             /*is the Parameter variable or fix?*/
       grpStructArray[35].startAddress[14].paramDefValue.valueInt = 0;              /*Default value of Parameter*/
       grpStructArray[35].startAddress[14].paramMinValue.valueInt = 0;              /*Min Value of Parameter*/
       grpStructArray[35].startAddress[14].paramActValue.valueInt = 0;              /*Active Value of Parameter*/
       grpStructArray[35].startAddress[14].paramMaxValue.valueInt = 0;              /*Max Value of Parameter*/
       grpStructArray[35].startAddress[14].digits = 1;  /*Number of digits for decimal Parameter*/
       grpStructArray[35].startAddress[14].decPlace = 0;              /*Number of digits behind the comma for float Parameter*/

/*3601 */
strcpy(grpStructArray[36].startAddress[0].name, "PotiLedGain");       /*Name of Parameter*/
       grpStructArray[36].startAddress[0].numb = 3601;  /*Number of Parameter [xxyy] xx = group, yy = index*/
       grpStructArray[36].startAddress[0].variType = PARAM_UINT;      /*Variable type of Parameter*/
       grpStructArray[36].startAddress[0].unit = PARAM_VOLT;          /*Unit of Parameter*/
       grpStructArray[36].startAddress[0].sign = PARAM_UNSIGNED;      /*is the Parameter signed or unsigned?*/
       grpStructArray[36].startAddress[0].change = PARAM_CHANGEABLE;  /*is the Parameter changeable during operation?*/
       grpStructArray[36].startAddress[0].varianz = PARAM_NOTVARIABLE;              /*is the Parameter variable or fix?*/
       grpStructArray[36].startAddress[0].paramDefValue.valueUint = 90;             /*Default value of Parameter*/
       grpStructArray[36].startAddress[0].paramMinValue.valueUint = 1;              /*Min Value of Parameter*/
       grpStructArray[36].startAddress[0].paramActValue.valueUint = 90;             /*Active Value of Parameter*/
       grpStructArray[36].startAddress[0].paramMaxValue.valueUint = 100;            /*Max Value of Parameter*/
       grpStructArray[36].startAddress[0].digits = 3;   /*Number of digits for decimal Parameter*/
       grpStructArray[36].startAddress[0].decPlace = 0; /*Number of digits behind the comma for float Parameter*/

 return err;

}
