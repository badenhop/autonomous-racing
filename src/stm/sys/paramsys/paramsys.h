/*******************************************************************************************
 * Unit in charge:
 * @file	paramSys.h
 * @author	Ren� Freyer
 * $Date:: 2015-02-19 14:03:20 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1312                  $
 * $Author:: neumerkel          $
 *
 * ----------------------------------------------------------
 *
 * @brief TODO What does the unit?
 *
 ******************************************************************************************/

#ifndef PARAMSYS_H_
#define PARAMSYS_H_

/************* Includes *******************************************************************/

#include "stdint.h"

#include "brd/types.h"

/************* Public typedefs ************************************************************/

#define MAX_LEN_PARAM 16		/* Max. length for the name of a parameter */

/**
 * @enum param_type_t
 *
 * Structure for type of the parameter.
 */
typedef enum {
			PARAM_INT,
			PARAM_UINT,
			PARAM_FLOAT,
} param_type_t;

/**
 * @enum param_unit_t
 *
 * Structure for the unit of the parameter.
 */
typedef enum {
			PARAM_AMPERE,
			PARAM_VOLT,
			PARAM_POWER,
			PARAM_DEGREES,
			PARAM_TEMP,
			PARAM_RPM,
			PARAM_KMH
} param_unit_t;

/**
 * @enum param_sign_t
 *
 * Is the parameter signed or unsigned?
 */
typedef enum {
			PARAM_SIGNED,
			PARAM_UNSIGNED
} param_sign_t;

/**
 * @enum param_changeable_t
 *
 * Is the parameter changeable? -->  SW Version = not changeable
 */
typedef enum {
			PARAM_CHANGEABLE,
			PARAM_NOTCHANGEABLE
} param_changeable_t;

/**
 * @enum param_variable_t
 *
 * Is the Parameter variable?
 * That means, is the parameter during the operation changeable?
 */
typedef enum {
			PARAM_VARIABLE,
			PARAM_NOTVARIABLE
} param_variable_t;


/**
 * @union param_dataType_t
 *
 * A union to handle the different types of parameter
 */
typedef union {

		int32_t   valueInt;

		uint32_t  valueUint;

		float32_t valueFloat;

} param_dataType_t;


/**
 * @struct param_value_t
 *
 * A structure for passing only the actual value of a parameter.
 * Include a union for the different types and enum to mark the type.
 */
typedef struct {

			param_dataType_t value;

			param_type_t type;

} param_value_t;




/**
 * @struct paramSysParam_t
 *
 * This structure includes all the attributes of an parameter. It can also handle the different types of
 * parameters (uint32_t, int32_t and float32_t).
 */
typedef struct {

	uint16_t numb;					/* Number of Parameter [xxyy] xx = group, yy = index */

	char_t name[MAX_LEN_PARAM];		/* Name of Parameter */

	param_type_t variType;			/* Variable type of Parameter */

	param_unit_t unit;				/* Unit of Parameter */

	param_sign_t sign;				/* is the Parameter signed or unsigned? */

	param_changeable_t change;		/* is the Parameter changeable during operation? */

	param_variable_t varianz;		/* is the Parameter variable or fix? */

	param_dataType_t paramDefValue; /* Default value of Parameter */

	param_dataType_t paramMinValue;	/* Max Value of Parameter */

	param_dataType_t paramActValue;	/* Active Value of Parameter */

	param_dataType_t paramMaxValue;	/* Min Value of Parameter */

	uint16_t digits;				/* Number of digits for decimal Parameter */

	uint16_t decPlace;				/* Number of digits behind the comma for float Parameter */

}  paramSysParam_t;


/**
 * @struct paramSysGrp_t
 *
 * This structure is used for the management of the arrays (group arrays of the parameter).
 */
typedef struct {

	uint8_t numbGrp;				/* Number of parametergroup */

	uint8_t numbElem;				/* Number of elements of the group */

	char_t nameGrp[16];				/* Name of parametergroup */

	paramSysParam_t* startAddress;	/* Pointer to the startaddress of the group-array */

}  paramSysGrp_t;


/**
 * @enum param_variable_t
 *
 * To ident the Parameter auf the array.
 * Typedefs, Constants and Functions that will later be provided by SW Component PARAM (Parameter System)
 */
typedef struct {

	uint8_t grp;			// group number

	uint8_t index;			// index within group

} param_id_t;


/* not realy in use jet. Derive from stud.c and  may needed for the terminal monitor. */
typedef uint16_t param_display_attr_t;

/* change the id of the parameter in a group number and a index number (placed in a structure) */
static inline param_id_t decToParID (uint16_t in) {param_id_t out; out.grp = in/100; out.index = in%100; return out;}


/************* Macros and constants *******************************************************/



/************* Public function prototypes *************************************************/


/**
* @brief Call the Parameterlist.
*
* @param [in,out] nothing.
*
* @return 0 = OK, anything else = erro
*/
int16_t paramsys_list(void);

/**
* @brief Thie function check the group and index of the given ID.
*
* @param par_id_t param, from terminal monitor to deal with the index an element values.
*
* @return 0 = OK,   1 = wrong ID
*/
int16_t paramsys_checkID(param_id_t param);

/**
* @brief Deliver the whole structure of the parameter (as a copy).
*
* @param par_id_t param, from terminal monitor to deal with the index an element values.
*
* @return a copy of the parameter structure (type: paramSysParam_t)
*/
paramSysParam_t paramsys_paramRead(param_id_t param);

/**
* @brief Deliver only the act. value of the parameter (as a copy).
*
* @param par_id_t param, from terminal monitor to deal with the index an element values.
*
* @return a structure which includes a union for value of the parameter and a enum for the type.
*/
param_value_t paramsys_paramValue(param_id_t param);

/**
* @brief Initialization of the parametersystem.
*
* @param [in,out] nothing.
*
* @return nothing.
*/
void paramsys_init(void);

/**
* @brief Write a Parameter.
*
* @param par_id_t param, from terminal monitor to deal with the index an element values.
* @param param_value_t newValue, is the structure of the new value which includes a union for value of the parameter and a enum for the type.
*
* @return 0 = OK, anything else = error ( 1 = wrong ID, 2 = value to small, 3 = value to big, 4 = other fault)
*/
//int16_t paramsys_paramWrite(param_id_t param, int32_t newValue);
int16_t paramsys_paramWrite(param_id_t param, param_value_t newValue);

/**
* @brief Set on Parameter to default.
*
* @param par_id_t param, from terminal monitor to deal with the index an element values.
*
* @return 0 = OK, anything else = error
*/
int16_t paramsys_default(param_id_t param);




#endif /* PARAMSYS_H_ */
