/******************************************************************************************
 * Unit in charge:
 * @file    paramlist.h
 * @author  Automatisch via VBA erzeugt
 * $Date: 2015-09-14 15:55:38 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1741                  $
 * $Author:: neumerkel          $
 *
 * ----------------------------------------------------------
 *
 * @brief Application specific initializations (e.g. of periphery such as ADC, PWM for motor control)
 *
 *****************************************************************************************/

#ifndef PARAMLIST_H_
#define PARAMLIST_H_

/************* Includes ******************************************************************/



/************* Private typedefs ***********************************************************/


/* defines of the used groups */
#define paraGrp_01_EN
#define paraGrp_02_EN
#define paraGrp_03_EN
#define paraGrp_04_EN
#define paraGrp_05_EN
//#define paraGrp_06_EN
//#define paraGrp_07_EN
//#define paraGrp_08_EN
#define paraGrp_09_EN
#define paraGrp_10_EN
#define paraGrp_11_EN
#define paraGrp_12_EN
#define paraGrp_13_EN
#define paraGrp_14_EN
#define paraGrp_15_EN
#define paraGrp_16_EN
#define paraGrp_17_EN
#define paraGrp_18_EN
#define paraGrp_19_EN
#define paraGrp_20_EN
#define paraGrp_21_EN
#define paraGrp_22_EN
#define paraGrp_23_EN
#define paraGrp_24_EN
#define paraGrp_25_EN
#define paraGrp_26_EN
//#define paraGrp_27_EN
//#define paraGrp_28_EN
//#define paraGrp_29_EN
#define paraGrp_30_EN
//#define paraGrp_31_EN
#define paraGrp_32_EN
#define paraGrp_33_EN
#define paraGrp_34_EN
#define paraGrp_35_EN
#define paraGrp_36_EN
//#define paraGrp_37_EN
//#define paraGrp_38_EN
//#define paraGrp_39_EN
//#define paraGrp_40_EN
//#define paraGrp_41_EN
//#define paraGrp_42_EN
//#define paraGrp_43_EN
//#define paraGrp_44_EN
//#define paraGrp_45_EN
//#define paraGrp_46_EN
//#define paraGrp_47_EN
//#define paraGrp_48_EN
//#define paraGrp_49_EN
//#define paraGrp_50_EN
//#define paraGrp_51_EN
//#define paraGrp_52_EN
//#define paraGrp_53_EN
//#define paraGrp_54_EN
//#define paraGrp_55_EN
//#define paraGrp_56_EN
//#define paraGrp_57_EN
//#define paraGrp_58_EN
//#define paraGrp_59_EN
//#define paraGrp_60_EN
//#define paraGrp_61_EN
//#define paraGrp_62_EN
//#define paraGrp_63_EN
//#define paraGrp_64_EN
//#define paraGrp_65_EN
//#define paraGrp_66_EN
//#define paraGrp_67_EN
//#define paraGrp_68_EN
//#define paraGrp_69_EN
//#define paraGrp_70_EN
//#define paraGrp_71_EN
//#define paraGrp_72_EN
//#define paraGrp_73_EN
//#define paraGrp_74_EN
//#define paraGrp_75_EN
//#define paraGrp_76_EN
//#define paraGrp_77_EN
//#define paraGrp_78_EN
//#define paraGrp_79_EN
//#define paraGrp_80_EN
//#define paraGrp_81_EN
//#define paraGrp_82_EN
//#define paraGrp_83_EN
//#define paraGrp_84_EN
//#define paraGrp_85_EN
//#define paraGrp_86_EN
//#define paraGrp_87_EN
//#define paraGrp_88_EN
//#define paraGrp_89_EN
//#define paraGrp_90_EN
//#define paraGrp_91_EN
//#define paraGrp_92_EN
//#define paraGrp_93_EN
//#define paraGrp_94_EN
//#define paraGrp_95_EN
//#define paraGrp_96_EN
//#define paraGrp_97_EN
//#define paraGrp_98_EN
//#define paraGrp_99_EN


/* defines for the size (number of elements) of each group */
#define sizeGrp01 17
#define sizeGrp02 7
#define sizeGrp03 5
#define sizeGrp04 1
#define sizeGrp05 4
//#define sizeGrp06 0
//#define sizeGrp07 0
//#define sizeGrp08 0
#define sizeGrp09 10
#define sizeGrp10 9
#define sizeGrp11 36
#define sizeGrp12 5
#define sizeGrp13 3
#define sizeGrp14 6
#define sizeGrp15 8
#define sizeGrp16 4
#define sizeGrp17 2
#define sizeGrp18 2
#define sizeGrp19 10
#define sizeGrp20 9
#define sizeGrp21 24
#define sizeGrp22 4
#define sizeGrp23 6
#define sizeGrp24 3
#define sizeGrp25 5
#define sizeGrp26 6
//#define sizeGrp27 0
//#define sizeGrp28 0
//#define sizeGrp29 0
#define sizeGrp30 7
//#define sizeGrp31 0
#define sizeGrp32 4
#define sizeGrp33 9
#define sizeGrp34 18
#define sizeGrp35 15
#define sizeGrp36 1
//#define sizeGrp37 0
//#define sizeGrp38 0
//#define sizeGrp39 0
//#define sizeGrp40 0
//#define sizeGrp41 0
//#define sizeGrp42 0
//#define sizeGrp43 0
//#define sizeGrp44 0
//#define sizeGrp45 0
//#define sizeGrp46 0
//#define sizeGrp47 0
//#define sizeGrp48 0
//#define sizeGrp49 0
//#define sizeGrp50 0
//#define sizeGrp51 0
//#define sizeGrp52 0
//#define sizeGrp53 0
//#define sizeGrp54 0
//#define sizeGrp55 0
//#define sizeGrp56 0
//#define sizeGrp57 0
//#define sizeGrp58 0
//#define sizeGrp59 0
//#define sizeGrp60 0
//#define sizeGrp61 0
//#define sizeGrp62 0
//#define sizeGrp63 0
//#define sizeGrp64 0
//#define sizeGrp65 0
//#define sizeGrp66 0
//#define sizeGrp67 0
//#define sizeGrp68 0
//#define sizeGrp69 0
//#define sizeGrp70 0
//#define sizeGrp71 0
//#define sizeGrp72 0
//#define sizeGrp73 0
//#define sizeGrp74 0
//#define sizeGrp75 0
//#define sizeGrp76 0
//#define sizeGrp77 0
//#define sizeGrp78 0
//#define sizeGrp79 0
//#define sizeGrp80 0
//#define sizeGrp81 0
//#define sizeGrp82 0
//#define sizeGrp83 0
//#define sizeGrp84 0
//#define sizeGrp85 0
//#define sizeGrp86 0
//#define sizeGrp87 0
//#define sizeGrp88 0
//#define sizeGrp89 0
//#define sizeGrp90 0
//#define sizeGrp91 0
//#define sizeGrp92 0
//#define sizeGrp93 0
//#define sizeGrp94 0
//#define sizeGrp95 0
//#define sizeGrp96 0
//#define sizeGrp97 0
//#define sizeGrp98 0
//#define sizeGrp99 0


/* define the highest group-number (max number of Grp)*/
#define numbOfGrps 36

/************* Macros and constants *******************************************************/



/************* Public function prototypes *************************************************/



#endif /* PARAMLIST_H_ */
