/**
 * @file	fir.h
 * @author	Robert Ledwig
 * @date	25.09.2013
 *
 *
 * @brief FIR filter
 *
 * In signal processing, a finite impulse response (FIR) filter is a filter whose impulse response (or response to any finite length input)
 * is of finite duration, because it settles to zero in finite time. This is in contrast to infinite impulse response (IIR) filters, which may
 * have internal feedback and may continue to respond indefinitely (usually decaying).
 * The impulse response of an Nth-order discrete-time FIR filter (i.e., with a Kronecker delta impulse input) lasts for N + 1 samples,
 * and then settles to zero. FIR filters can be discrete-time or continuous-time, and digital or analog.
 *
 * properties:
 *
 * An FIR filter has a number of useful properties which sometimes make it preferable to an infinite impulse response (IIR) filter.
 * FIR filters:
 *
 * - Require no feedback. This means that any rounding errors are not compounded by summed iterations.
 *   The same relative error occurs in each calculation. This also makes implementation simpler.
 *
 * - Are inherently stable. This is due to the fact that, because there is no required feedback,
 *   all the poles are located at the origin and thus are located within the unit circle (the required condition for stability in a discrete,
 *   linear-time invariant system).
 *
 * - They can easily be designed to be linear phase by making the coefficient sequence symmetric.
 *   This property is sometimes desired for phase-sensitive applications, for example data communications, crossover filters, and mastering.
 *
 * The main disadvantage of FIR filters is that considerably more computation power in a general purpose processor is
 * required compared to an IIR filter with similar sharpness or selectivity, especially when low frequency (relative to the sample rate) cutoffs are needed.
 * However many digital signal processors provide specialized hardware features to make FIR filters approximately as efficient as IIR for many applications.
 *
 */

#ifndef FIR_H_
#define FIR_H_


/************* Includes *******************************************************************/
#include <stdlib.h>
#include "brd/startup/stm32f4xx.h"
#include "math.h"

/************* Marcros ********************************************************************/
#define FIR_FALSE                       1
#define FIR_OK                        	0
#define M_PI 							3.141592654

/************* Structs **********************************************************************/

/**
 * @struct the structure of the circular buffer with pointer, size and data storage
 *
 */
typedef struct {
	uint8_t pointer;
	uint8_t size;
	float_t* array;
} circbuf_t;

/**
 * @struct the structure of FIR type with the circular buffer, coefficient buffer, sample and limit frequency
 *
 */
typedef struct {
	float_t sampleFreq;
	float_t limitFreq;
	float_t* coeffbuf;
	circbuf_t circbuf;
} fir_t;


/************* Public function prototypes *************************************************/

/**
 * @brief Init function for initialization of fir object structure and initialization of array values (coefficient array and circular array).
 *
 * @param [in,out] *channel - pointer to fir structure for adding properties to channel
 * @param [in,out] *circbuf - circular buffer for storing values which needed to be filtered
 * @param [in,out] *coeff_buf - coefficient buffer, wherein the calculated coefficients will be stored
 * @param [in] size - size of both arrays
 * @param [in] sampleFreq - sample frequency as a parameter for calculating filter coefficients
 * @param [in] limit_freq - limit frequency as a parameter for calculating filter coefficients
 *
 * @return 0
 */
int8_t fir_init(fir_t* channel, float_t *circbuf, float_t* coeffbuf, uint16_t size, float_t sampleFreq, float_t limitFreq);

/**
 * @brief Function for calculating filtered values depending on coefficients (stored in array) and input values (stored in circular buffer).
 *
 * @param [in,out] *channel - pointer to fir structure (containing frequency and buffers)
 * @param [in] input_value - value to use for filter calculation
 *
 * @param [in,out] *output_value - calculated filtered value
 *
 * @return 0
 */
int8_t fir_calculate(fir_t* channel, float_t inputValue, float_t* outputValue);


#endif	/* FIR_H_ */
