/**
 * @file	pt1.h
 * @author	Nicolai Adelhoefer
 * @date	12.04.2016
 *
 */
#ifndef PT1_H_
#define PT1_H_

#include "brd/startup/stm32f4xx.h"



/************* Public typedefs ************************************************************/

/*
 * T_opt = 1/(T/t_delta + 1) this modul uses this precalculation for perfromance issues
 */
typedef struct {
	float32_t T_opt;
	float32_t K;
	float32_t y_old;
}pt1_t;


/************* Public function prototypes *************************************************/

/* 
* call before calculations
*/
void pt1_init(pt1_t* pPT1);

/*
* call in loop for calculating each new element
*/
float32_t pt1_calculate(pt1_t* pPT1, float32_t x);

#endif
