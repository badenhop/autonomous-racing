/**
 * @file	fir.c
 * @author	Robert Ledwig
 * @date	25.09.2013
 *
 */

#include "sys/algo/fir.h"
#include "math.h"
#include "per/hwallocation.h"
#include "brd/types.h"
#include "sys/util/io.h"



int8_t fir_init(fir_t* channel, float_t* circbuf, float_t* coeffbuf, uint16_t size, float_t sampleFreq, float_t limitFreq)
{

	for(uint16_t i = 0; i < size; i++) {
		if(i == (size-1)/2) {
			coeffbuf[i] = (2.0*M_PI*limitFreq/sampleFreq)/M_PI;
		}
		else {
			coeffbuf[i] = (sin((i-((size-1.0)/2.0))*(2.0*M_PI*limitFreq/sampleFreq)))/((i-((size-1)/2.0))*M_PI);
		}
}

	for(uint16_t i = 0; i < size; i++) {
		circbuf[i] = 0;
	}

	channel->sampleFreq = sampleFreq;
	channel->limitFreq = limitFreq;
	channel->coeffbuf = coeffbuf;
	channel->circbuf.array = circbuf;
	channel->circbuf.pointer = 0;
	channel->circbuf.size = size;

	return FIR_OK;
}


int8_t fir_calculate(fir_t* channel, float_t inputValue, float_t* outputValue)
{

	channel->circbuf.array[channel->circbuf.pointer] = inputValue;
	channel->circbuf.pointer = (channel->circbuf.pointer + 1) % channel->circbuf.size;

	*outputValue = 0;

	for(uint16_t i = 0; i < channel->circbuf.size; i++) {
		*outputValue += (channel->coeffbuf[i])*(channel->circbuf.array[(channel->circbuf.pointer + i) % channel->circbuf.size]);
	}

	return FIR_OK;
}
