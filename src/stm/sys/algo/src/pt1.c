/**
 * @file	pt1.c
 * @author	Nicolai Adelhoefer
 * @date	12.04.2016
 *
 */

/************* Includes *******************************************************************/
#include "sys/algo/pt1.h"

/************* Public functions ***********************************************************/


void pt1_init(pt1_t* pPT1){
	pPT1->y_old = 0;
}

/*
* Efficient calculation of an PT1 lag element with just 2 multiplications
* T_opt = 1/(T/t_delta + 1) this modul uses this precalculation for perfromance issues
* x = Input
* y = Output
*/

float32_t pt1_calculate(pt1_t* pPT1, float32_t x){
	float32_t y = pPT1->T_opt * (pPT1->K * x - pPT1->y_old) + pPT1->y_old;
	pPT1->y_old = y;
	return y;
}
