/** *****************************************************************************************
 * Unit in charge:
 * @file	pid.c
 * @author	ledwig
 * $Date:: 2015-02-19 14:03:20 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1312                  $
 * $Author:: neumerkel          $
 *
 * ----------------------------------------------------------
 *
 * @brief PID regulation driver
 *
 ******************************************************************************************/



/************* Includes *******************************************************************/
#include "per/hwallocation.h"
#include "sys/algo/pid.h"

/************* Private typedefs ***********************************************************/


/************* Macros and constants *******************************************************/


/************* Private static variables ***************************************************/


/************* Private function prototypes ************************************************/


/************* Public functions ***********************************************************/
void pid_initialize(pidCtrl_t* pController)
{

	pController->eSum = 0;
	pController->eOld = 0;
	
	return;
}

void pid_controller(pidCtrl_t* pController, float32_t x, float32_t w, float32_t* pY, float32_t minDyn, float32_t maxDyn)
{
	float32_t min = 0;	/* absolute minimum value */
	float32_t max = 0;	/* absolute maximum value */
	float32_t e = 0;	/* control deviation */
	float32_t ret = 0;	/* return value to be written to the actuating variable pY in the end */
	float32_t eSumOld = pController->eSum; /*  storing the eSum for anti-windup */

	/* determine  min and max values for pY from static and dynamic min/max  */
	if (minDyn > pController->min) {
		min = minDyn;
	}
	else {
		min = pController->min;
	}

	if (maxDyn < pController->max) {
		max = maxDyn;
	}
	else {
		max = pController->max;
	}

	/* control deviation */
	e = w - x;
	/* integration sum */
	pController->eSum += e * pController->ta;

	/* adding up the P, I, D part of the actuating variable */
	ret = pController->kp * e;
	ret += pController->ki * pController->eSum;
	ret += pController->kd * (e - pController->eOld) / pController->ta;
	/* multiply actuation variable with gain factor for scaling */
	ret *= pController->gain;

	if (ret < min) {								/* limits have higher priority than gain */
		ret = min;									/* limitation of y */
		pController->eSum = eSumOld;				/* anti wind-up for I component (no summing up in case of limit reached) */
	}
	else if (ret > max) {
		ret = max;									/* limitation of y */
		pController->eSum = eSumOld;				/* anti wind-up for I component */
	}

	/* save old control deviation for next step derivative */
	pController->eOld = e;

	*pY = ret;

	return;
}

/************* Private functions **********************************************************/
