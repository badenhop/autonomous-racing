/** *****************************************************************************************
 * Unit in charge:
 * @file	pid.h
 * @author	ledwig
 * $Date:: 2015-02-26 14:28:45 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1344                  $
 * $Author:: hepner             $
 *
 * ----------------------------------------------------------
 *
 * @brief PID regulation driver
 *
 ******************************************************************************************/


#ifndef PID_H_
#define PID_H_

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"

/************* Public typedefs ************************************************************/
typedef struct
{
	float32_t min;
	float32_t max;
	float32_t kp;
	float32_t ki;
	float32_t kd;
	float32_t gain;
	float32_t ta;
	float32_t eSum;
	float32_t eOld;	/* save old difference variable for D component */
} pidCtrl_t;

/************* Macros and constants *******************************************************/


/************* Public function prototypes *************************************************/

/**
 * @brief	Initializes the integral proportion (*controller).eSum to zero.
 *
 * @param[out] pidCtrl_t* pController is the pointer to the pid_t struct containing the min and max limit of manipulated variable (pY), the proportional component (kp),
 * 				the integral component (ki), the gain for pY, the sample time (ta) and area sum (eSum) for the integral component
 *
 * @return void
 */
void pid_initialize(pidCtrl_t* pController);

/**
 * @brief	Function for calculation of manipulated variable (pY).
 *
 * @param[in,out] pidCtrl_t* pController is a pointer to the pidCtrl_t struct contains the min and max limit of manipulated variable (pY), the proportional component (kp),
 * 				the integral component (ki), the gain for pY, the sample time (ta) and area sum (eSum) for the integral component
 * @param[out] double* pY is the actuating variable
 * @param[in] float64_t x is the measured process variable
 * @param[in] float64_t w is the setpoint
 * @param[in] float64_t minDyn dynamic minimum limit of the actuating variable
 * @param[in] float64_t maxDyn dynamic maximum limit of the actuating variable
 *
 * @return void
 */
void pid_controller(pidCtrl_t* pController, float32_t x, float32_t w, float32_t* pY, float32_t minDyn, float32_t maxDyn);

#endif /* PID_H_ */
