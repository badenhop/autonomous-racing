/**
 * @file 		terminal.c
 * @brief		Terminal monitor that allows accessing memroy data, parameters,
 * 				tasks etc. while program is running
 *
 *
 * @author		Bernhard Kaiser
 * @date 		date
 */

/* Includes */
// #include "stm32f4xx.h"
// #include "stm32f4_discovery.h"

//#include "brd/types.h"
#include "sys/util/io.h"
#include "sys/util/stubs.h"
#include "sys/sched/scheduler.h"
#include "sys/terminal/terminal.h"
#include "sys/paramsys/paramsys.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
/* Private Constants and Macros */

// #define I_WITH_VERIFY					/* uncomment if Ix command is */
/*   desired to check written data */

#define MAX_CMD_STRING_LEN 128				/* max 128 characters in one monitor command line */
#define MAXARGS 4

static const char strTerminalPrompt[] = "STM32F4>";
static const char strMonitorBanner[] = "Debug Monitor                    Press ? for Help\n";
static const char strMonitorHelpscreen1[] = "\n                ***** Debug Monitor *****\n\
                                                                                        \n\
Available Commands are:																	\n\
                                                                                        \n\
TA <tasknr>                                         - Activate Task						\n\
TS <tasknr>                                         - Suspend Task						\n\
TL                                                  - Tasklist							\n\
PA <dec parnr>                                      - Set ONE Parameter  to default		\n\
PD                                                  - Set ALL Parameters to default		\n\
PI                                                  - Parameterinfo						\n\
PL                                                  - List of all Parameters			\n\
PR <dec parnr>                                      - Read Parameter					\n\
PV <dec parnr>                                      - Read Parameter value				\n\
PW<type> <dec parnr>  <value>                       - Write to Parameter				\n\
SW <ID> <value>                                     - Write Process Signal				\n\
SR <ID>                                             - Read Process Signal				\n\
MW<type>           <addr> <data>                    - Write data to memory address		\n\
MR<type>[<format>] <addr>                           - Read data at memory address		\n\
MO<type>[<format>] <addr>                           - Observe data until q is pressed	\n\
N                                                   - Repeat for next address			\n\
Q                                                   - Quit Terminal Monitor				\n\
FR                                                  - Reset Faults						\n\
DB                                                  - Debug Function					\n\
?                                                   - Print Help						\n\
";
static const char strMonitorHelpscreen2[] = "\n\
<TAB> repeats last command line for edit.												\n\
                                                                                        \n\
Choices for <type>:																		\n\
B - Byte        W - Word (16 bits)       L - Long (32 bits) 	F - Float (32 bits) 	\n\
                                                                                        \n\
Choices for <format> (for displaying integer memory cell contents):						\n\
D - signed dec  U - unsigned dec  X - unsigned hex										\n\
                                                                                        \n\
Upper case and lower case letters are equivalent. 										\n\
Use 0x prefix for hex input (also for entering mem addresses!)							\n\
\n";

#define FAILED				1
#define NO_VALID_NUMBER 	2
#define NO_VALID_READ_ADDR 	3
#define NO_VALID_WRITE_ADDR 4
#define NO_VALID_WRITE_DATA 5
#define ARGS_MISSING 		6
#define ALIGNMENT_ERROR 	7
#define NO_REPETITION  		8
#define NO_SUCH_TASK 		9

static const char *strErrorText[] = { "",	/* error == 0 -> no error, no message */
"Command failed",							/* error == 1 */
"Argument(s) not a valid number",			/* error == 2 */
"No valid hex address to read",				/* error == 3 */
"No valid hex address to write",			/* error == 4 */
"No valid data to write",					/* error == 5 */
"Arguments missing",						/* error == 6 */
"Wrong alignment for word/long",			/* error == 7 */
"Repetition not possible here",				/* error == 8 */
"No such task",								/* error == 9 */

};

/* Private Variables */

static uint8_t strCommandLine[MAX_CMD_STRING_LEN];	/* space to save command line */

static uint8_t *pchP;						/* static pointer for actual uint8_t */
static uint8_t chDataLength;				/* 1 when dealing with uint8_t, 2 word ... */
static uint8_t chDataType;					/* 1 when dealing with uint8_t, 2 word ... */
static uint8_t chOutputFormat;				/* A = ASCII, B binary, D decimal, U uns dec, X hex */
static uint8_t chNumberOfArguments;	 		/* depends on command to be executed */
static uint8_t chContinuous;				/* 1 if a display command has to be repeated */
/* continuously ( = Axx command) */

static int32_t aryArgs[MAXARGS];			/* space to store parsed arguments */
static float32_t argsFloat;					/* space to store parsed arguments */
//static uint32_t argsUint;					/* space to store parsed arguments */

static uint8_t chQuit;						/* !0 if quit of monitor and release of */
/* PC port is requested */

/* Private Function Declarations */

static int16_t parseCmd(void);				/* reads and interpretes monitor command */
static int16_t parseArgument(void);			/* reads and interpretes numerical argument */

static int16_t increment(void);				/* increments memory address, used for command NEXT */

static int16_t (*pCmd)(void);				/* monitor command to be executed */
static int16_t (*pOldCmd)(void);			/* for repeated execution */


/* Handler Functions for teminal monitor commands */
static int16_t cmd_next(void);				/* NEXT command */
static int16_t cmd_help(void);				/* Help command */
static int16_t cmd_MW(void);				/* Memory Write command */
static int16_t cmd_MR(void);				/* Memory read command */
static int16_t cmd_TA(void);				/* Task Activate command */
static int16_t cmd_TS(void);				/* Task Suspend command */
static int16_t cmd_TL(void);				/* Tasklist command */
static int16_t cmd_PA(void);				/* Parameter Reset command */
static int16_t cmd_PD(void);				/* Parameter Default command */
static int16_t cmd_PW(void);				/* Parameter write command */
static int16_t cmd_PR(void);				/* Parameter read command */
static int16_t cmd_PI(void);				/* Parameterinfo command */
static int16_t cmd_PL(void);				/* Parameterlist command */
static int16_t cmd_PV(void);				/* Parameter value command */
static int16_t cmd_quit(void);				/* Quit and Release PC Port */
static int16_t cmd_FR(void);				/* Fault Reset */
static int16_t cmd_DB(void);				/* Debug function */

/* Private Inline Functions */

static int16_t emptyFunction(void)
{
	return 0;
}    /* not really inline but fits in 1 line */

/* Public Functions */

/**
 * @brief Function does?
 * Computation of the Pythagorean theorem.
 *
 * @param a;
 * @param b;
 * @return 	hypotenuse
 */

/****************************************************************************

 void terminalMonitor(void)

 Main function of the terminal monitor. This function is an endless loop
 that must be executed from main after all inits and after having started
 all int16_terrupt routines and cyclic tasks. It is inserted instead of
 the empty backup loop.

 Arguments:			-
 Returns:			-
 Reads Globals:		-
 Changes Globals:	-
 Called by:			main
 Calls:				io_printf, getline, parseCmd, parseArgument,
 *pCmd, assignPort, freePort
 Uses result from:	-
 Provides data to:	-

 ****************************************************************************/

void terminalMonitor(void)
{

	uint8_t chError;					/* error code that a function may return */
										/* see defs in monitor.h */

	/* Reserve and init RS232 port */

//if(SCI_Init(RS232_PORT, 9600, 8, 1, NO_PARITY, POLLED, MONITOR))
//	return;									/* quit if failed */

	/* Init command pointer */

	pCmd = pOldCmd = emptyFunction;

	/* Show Banner */

	io_printf("\n%s", strMonitorBanner);

	/* Repeat command line interpreting until quit request */

	chQuit = 0;

	while (!chQuit) {

		chError = 0;								/* reset error */

		/* Prompt and get one line of input */

		io_printf("\n%s ", strTerminalPrompt);

// getline(strCommandLine,MAX_CMD_STRING_LEN);
//		io_scanf("%s ", &strCommandLine);
		io_getlineTerminal(((int8_t*)strCommandLine));
		io_printf("\n: %s\n", strCommandLine);
		pchP = strCommandLine;



		/* Parse command string */

		chNumberOfArguments = 0;	/* unless otherwise specified by parseCmd() */
		chDataType = 0;				/* unless otherwise specified by parseCmd() */
		aryArgs[MAXARGS] =  0;
		argsFloat = 0.0;

		/* Find out what to do or break if error ... */

		if (parseCmd() != 0)						/* if problems with parsing */
		{
			io_printf("\a   ERROR: Command not understood");
			pCmd = emptyFunction;				/* clear out function pointer */
		}

		/* ... but if command was ok, see what it was: nothing or ... */

		else if (pCmd == emptyFunction)
			;			/* if just <RETURN> do nothing */

		/* ... something to execute: a repetition of the last command or ... */

		else if (pCmd == cmd_next)
			;				/* if NEXT, keep old args in buffer */


		/* ... a valid command: Go on with parsing until all arguments are read ... */

		else								/* if no NEXT: new command, new args */
		{
			while (chNumberOfArguments-- > 0)	/* if there are still some arguments to get */
			{

				if ((chError = parseArgument()) != 0)    /* get them (or complain) */
				{
					io_printf("\a   ERROR: %s", strErrorText[chError]);
					pCmd = emptyFunction;		/* clear out function pointer */
					goto LoopEnd;


				}    /* of if (chError = parseArguments()) */

			}    /* of while(chNumberOfArguments ...) */
		}    /* of else */

		/* ... and execute command (argument passing by static variables) */

		if ((chError = pCmd()) != 0) 				/* Do it! (prompt if error) */
		{
			io_printf("\a   ERROR: %s", strErrorText[chError]);
			pCmd = emptyFunction;	/* clear out function pointer => no NEXT! */
		}    /* of if(chError...) */

		/* Label LoopEnd: */

		LoopEnd : ;						/* Label must be followed by a statement */

	}    /* of while(!chQuit) */

	/* Free PC port */

// TODO: put close() command for port here, if provided!
	return;

}    /* of terminalMonitor */

/* Private Functions */

/****************************************************************************

 static int16_t parseCmd(void)

 Parser function for the debug monitor.

 Arguments:			-
 Returns:			0 if successful, 1 if syntax error
 Reads Globals:		-
 Changes Globals:	-
 Called by:			monitor
 Calls:				toUpper
 Uses result from:	-
 Provides data to:	-

 ****************************************************************************/

static int16_t parseCmd(void)
{

	int16_t error = 0;    /* error return code */

	/* Skip Whitespace */

	while (*pchP == ' ')
		pchP++;

	/* Examine Command String char by char */

	while (*pchP != 0)    /* NULL character terminates string */
	{

		switch (toUpper(*(pchP++))) {

			default:
				error = FAILED;				/* no valid command */
				break;

				/* Input == <RETURN> */

			case '\n':						/* <RETURN> : leave (but no error) */
				pCmd = emptyFunction;
				break;

				/* Input == '?' */

			case '?':
				pCmd = cmd_help;			/* '?' means that help is desired */
				break;

				/* If input == N (NEXT), check if possible */

			case 'D':

				switch (toUpper(*(pchP++))) {

					default:
						return FAILED;

					case 'B':
						chNumberOfArguments = 1;
						pCmd = cmd_DB;					/* Fault Reset */
						break;
				}
				break;    /* of case 'D' in exterior switch() */

			case 'N':

				if (pCmd == cmd_next)		/* if NEXT was used before */
					;						/* leave things as they are */

				else if (pCmd == cmd_MR)	/* MR can be used with NEXT */
					pOldCmd = cmd_MR;

				else
					/* if no old command or no NEXT possible */
					pOldCmd = emptyFunction;

				pCmd = cmd_next;
				break;    /* of case 'N' */

				/* If cmd is A do the same as if D, but continuously */

			case 'M':						/* memory write / read / observe command  */


				switch (toUpper(*(pchP++))) /* parse next letter */
				{

					default:
						return FAILED;

					case 'W':
						chNumberOfArguments = 2;	/* for write two args: address and value */
						pCmd = cmd_MW;				/* Memory write */
						break;

					case 'R':
						chNumberOfArguments = 1;	/* for read one arg: address */
						pCmd = cmd_MR;				/* Memory read */
						break;

					case 'O':
						chNumberOfArguments = 1;	/* for read one arg: address */
						chContinuous = 1;			/* switch on continuous operation until q is pressed */
						pCmd = cmd_MR;				/* Memory Observe = continuous Memory Read */
						break;
				}    /* of interior switch() */

				/* Now check for further letters qualifying the type and optionally the format */

				switch (toUpper(*(pchP++))) {

					default:
						return FAILED;

					case 'B':					/* 2nd uint8_t B = uint8_t data */
						chDataLength = 1;
						break;

					case 'W':					/* 2nd uint8_t W = word data */
						chDataLength = 2;
						break;

					case 'L':					/* 2nd uint8_t L = int32_t data */
						chDataLength = 4;
						break;

				}    /* of switch(2nd char of Mxx command) */

				/* Now the third letter of the Mxx command: */

				if(pCmd == cmd_MR){

					switch (toUpper(*(pchP++))) {

						default:
							return FAILED;

						case 'A':
							chOutputFormat = 'A';	/* DxA : display ASCII format */
							break;

						case 'B':
							chOutputFormat = 'B';	/* DxB : display binary format */
							break;

						case 'D':
							chOutputFormat = 'D';	/* DxD : display signed decimal format */
							break;

						case 'U':
							chOutputFormat = 'U';	/* DxU : display unsigned decimal format */
							break;

						case 'X':
							chOutputFormat = 'X';	/* DxX : display hex format */
							break;

					}    /* of switch(3rd uint8_t of Mxx command) */
				}
				break;    /* of case 'M' in exterior switch() */


			/* If cmd is T more letters are required: */

			case 'T':

				switch (toUpper(*(pchP++))) {

					default:
						return FAILED;

					case 'A':
						chNumberOfArguments = 1;
						pCmd = cmd_TA;					/* Task Activate */
						break;

					case 'L':
						chNumberOfArguments = 0;
						pCmd = cmd_TL;					/* Tasklist */
						break;

					case 'S':
						chNumberOfArguments = 1;
						pCmd = cmd_TS;					/* Task Suspend */
						break;

				}    /* of interior switch() */

				break;    /* of case 'T' in exterior switch() */

			/* If cmd is P more letters are required: */

			case 'P':

				switch (toUpper(*(pchP++))) {

					default:
						return FAILED;

					case 'A':
						chNumberOfArguments = 1;		/* no arguments */
						pCmd = cmd_PA;					/* Parameter Default One */
						break;

					case 'D':
						chNumberOfArguments = 0;		/* no arguments */
						pCmd = cmd_PD;					/* Parameter Default All */
						break;

					case 'I':
						chNumberOfArguments = 1;		/* no arguments */
						pCmd = cmd_PI;					/* Parameterinfo */
						break;

					case 'L':
						chNumberOfArguments = 0;		/* parnr only */
						pCmd = cmd_PL;					/* Parameterlist */
						break;

					case 'R':
						chNumberOfArguments = 1;		/* parnr only */
						pCmd = cmd_PR;					/* Parameter Read */
						break;

					case 'V':
						chNumberOfArguments = 1;		/* parnr and value */
						pCmd = cmd_PV;					/* Parameter value */
						break;

					case 'W':
						chNumberOfArguments = 2;		/* parnr and value */
						pCmd = cmd_PW;					/* Parameter Write */

						/* Now check for further letters qualifying the type and optionally the format */

						switch (toUpper(*(pchP++))) {

							default:
								return FAILED;

							case 'W':					/* 2nd uint8_t W = uint32_t data */
								chDataType = 1;
								break;

							case 'L':					/* 2nd uint8_t L = int32_t data */
								chDataType = 2;
								break;

							case 'F':					/* 2nd uint8_t F = float32_t data */
								chDataType = 3;
								break;

						}    /* of switch(2nd char of Pxx command) */

						break;

				}    /* of int16_terior switch() */

				break;    /* of case 'P' in exterior switch() */

			/* If input == Q (Quit), set pCmd */

			case 'Q':
				pCmd = cmd_quit;
				break;

				/* Input == 'F' */

			case 'F':

				switch (toUpper(*(pchP++))) {

					default:
						return FAILED;

					case 'R':
						chNumberOfArguments = 0;
						pCmd = cmd_FR;					/* Fault Reset */
						break;
				}


		}    /* of exterior switch() */

		/* Ready with valid command, error if anything but white space follows */

		if (!(*(pchP) == ' ' || *(pchP) == '\n'))
			error = FAILED;

		return error;

	}    /* of while(*(pchP++)) */

	/* If we get here, the string ended unexpectedly */

	return 1;
}    /* of parseCmd */
/****************************************************************************

 static int16_t parseArgument(void)

 Parses numerical arguments for the monitor and stores them to an array as
 signed int32_t int16_teger. Does not check if in range!

 Arguments:			-
 Returns:			0 if successful, errorcode if syntax error
 Reads Globals:		-
 Changes Globals:	-
 Called by:			monitor
 Calls:				toUpper
 Uses result from:	-
 Provides data to:	-

 ****************************************************************************/

static int16_t parseArgument(void)
{
	int8_t ch = 0;						/* temp store of uint8_t to transform */
	uint8_t chBase;						/* 10 if decimal, 16 if hex */
	uint8_t chNegative;					/* flag if '-' sign precedes */
	int32_t chValue;					/* space for destination value */

	uint8_t indexA = 0;					/* index for position of the comma */
	int32_t tempInt = 0;				/* temp to store the integer value (argument) */
	char_t tempStr[MAX_CMD_STRING_LEN] = "0"; 			/* space to store parsed arguments */
	char_t *argsStr; 					/* space to store parsed arguments */
	float32_t tempFloat = 0.0;			/* temp store for float value */

	switch (chDataType) {

		case 3:	/* Float Value */

			/* Skip Whitespace befor parameternr. */
			while (*pchP == ' ') {
				pchP++;
			}

			/* Copy commend into temp string */
			strcpy(tempStr, (char_t*)pchP);

			/* Replace ',' with '.' for correct syntax */
			indexA = strcspn(tempStr, ",");
			if(indexA != 0) {
				tempStr[indexA] = '.';
			}
			indexA = 0;

			/* Separat the first argument */
			argsStr = strtok(tempStr, " ");
			/* Store the first argument into aryArgs-Array */
			tempInt = atoi(argsStr);
			aryArgs[chNumberOfArguments] = tempInt;

			/* Separat the second argument (the value) */
			argsStr = strtok(NULL, " ");
			strcpy(tempStr, argsStr);

			/* Transform the second argument */
			tempFloat = strtof(tempStr, (char_t**)NULL);

			/* Print the result and parse the float value */
			io_printf("\n Float Value >> %f", tempFloat);
			argsFloat = tempFloat;

			/* Fix for the change of arguments */
			chNumberOfArguments--;

			break;

		default: 	/* Word (16bit) or Long (32bit) Value */

			/* Skip Whitespace */

			while (*pchP == ' ')
				pchP++;

			/* Input == <RETURN> */

			if (*pchP == '\n')
				return ARGS_MISSING;

			/* As first charatacter a minus sign is allowed */

			if (*pchP == '-') {
				chNegative = 1;
				pchP++;
			} else
				chNegative = 0;

			/* Check for base */

			if (pchP[0] == '0' && toUpper(pchP[1]) == 'X') {
				chBase = 16;
				pchP += 2;
			} else
				chBase = 10;

			/* Check for first valid digit */

			ch = *pchP;

			if (ch >= '0' && ch <= '9')
				ch = ch - '0';

			else if (ch >= 'A' && ch <= 'F')
				ch = ch - 'A' + 10;

			else if (ch >= 'a' && ch <= 'f')
				ch = ch - 'a' + 10;

			else
				return NO_VALID_NUMBER;

			if (ch >= chBase)
				return NO_VALID_NUMBER;

			chValue = ch;							/* init chValue with first digit */

			/* Get more digits if any */

			while (*(++pchP)) {

				ch = *pchP;

				if (ch >= '0' && ch <= '9')
					ch = ch - '0';

				else if (ch >= 'A' && ch <= 'F')
					ch = ch - 'A' + 10;

				else if (ch >= 'a' && ch <= 'f')
					ch = ch - 'a' + 10;

				else if (ch == ' ' || ch == '\n') {
					aryArgs[chNumberOfArguments] = (chNegative ? -chValue : chValue);
					/* put result int16_to array */
					return 0;							/* normal end of parseArgument */
				}

				else
					return NO_VALID_NUMBER;				/* any other uint8_t -> error */

				/* Check if digit in range */

				if (ch >= chBase)
					return NO_VALID_NUMBER;

				chValue = chValue * chBase + ch;

			}    /* of while (*(++pchP)) */

			return NO_VALID_NUMBER;						/* string ended unexpectedly */
			break;

	}

return 0;
}    /* of parseArgument */

/****************************************************************************

 static int16_t increment(void)

 Increments the memory address, parameter or task number etc. that was
 used by the last command by one unit. This is necessary for the execution
 of the NEXT command.

 Arguments:			-
 Returns:			0 if successful, 1 if no appropriate pOldCmd or if
 end of range
 Reads Globals:		pOldCmd, chDataLength
 Changes Globals:	aryArgs[...]
 Called by:			cmd_next
 Calls:				-
 Uses result from:	-
 Provides data to:	-

 ****************************************************************************/

static int16_t increment(void)
{

	if (pOldCmd == cmd_MR)					/* for Dxx increment memory address */
	{
		aryArgs[0] += chDataLength;			/* one 8bit/16bit/32bit address further */
		return 0;							/* return success */
	}    /* of if(cmd_MR) */

	else
		return 1;
}    /* of increment */

/****************************************************************************

 Monitor Commands

 The following small functions execute the parsed monitor commands.

 They all take <chNumberOfArguments> argumets from aryArgs as signed int32_t
 int16_tegers. It is up to them to check the validity of the argumets and to
 set the corresponding errorcode.

 The structure of aryArgs is:

 [0] = last argument
 [1] = 2nd last argument
 :
 [n] = first argument

 Max depth of argument buffer is specified in monitor.h by MAXARGS.

 The functions return a unique (among all of them) error code which is 0 for
 ok and other for any errors that may occur. The calling function monitor()
 uses this code to generate the appropriate error message.

 ****************************************************************************/

/* Command '?' - help */

int16_t cmd_help(void)
{
	io_printf("\n%s", strMonitorHelpscreen1);
	io_printf("%s", strMonitorHelpscreen2);
	return 0;
}    // of cmd_help

/* Command NEXT - repeat last command with incremented address  */

int16_t cmd_next(void)
{
	if (pOldCmd == emptyFunction)				/* if not possible */
		return NO_REPETITION;

	else										/* NEXT possible */
	if (increment() != 0)						/* next address (returns 0 if ok) */
		return NO_REPETITION;					/* fault if returned !0 */
	else
		/* if increment was ok */
		return pOldCmd();						/* do it again and propagate ret value */

}    /* of cmd_next */

/* Command - display data */

int16_t cmd_MR(void)
{
	uint8_t uint8_tBuf[4 + 1];						/* for ASCII output */
	uint8_t i;										/* Index */
	uint8_t *tmpPtr;								/* for string copying */
	uint8_t chNumberOfchars = 0;					/* notes how many data characterss have been */
													/* printed (for cont. operation) */

	/* Check if address in range for reading */

	/* if not return NO_VALID_READ_ADDR */
	/* actually not implemented */

	/* print common part of output */

	io_printf(" >>> At 0x%x ", (unsigned long)aryArgs[0]);

	/* Repeat the following if and as long as <continuous> is true (cmd A)
	 If <continuous> is not true, execute one time (cmd D) */

	do {

		/* Now distinguish output format ... */

		/* Case ASCII */

		if (chOutputFormat == 'A') {
			tmpPtr = (uint8_t *)aryArgs[0];

			/* Copy uint8_t(s) to buffer */

			for (i = 0; i < chDataLength; i++) {
				if (*tmpPtr < 0x20) {
					uint8_tBuf[i] = '.';	/* don't try to print what you cannot print */
					tmpPtr++;				/* increment source pointer */
				}

				else
					/* if positive and greater than 0x20 = printable char */
					uint8_tBuf[i] = *(tmpPtr++);
			}    /* of for(i<chDataLength) */

			/* Close string */

			uint8_tBuf[i] = '\0';

			/* print out and note number of data uint8_ts */

			chNumberOfchars = io_printf("(as ASCII): %s", uint8_tBuf);

		}    /* of if('A') */

		else {

			/* Check if alignment is ok (e.g. 2 for Word) */

			if (aryArgs[0] % chDataLength)
				return ALIGNMENT_ERROR;

			switch (chOutputFormat) {

				/* Case Binary */

				case 'B':

					switch (chDataLength) {

						case 1:

							chNumberOfchars =
//							io_printf(": %b", (uint16_t) * (uint8_t *) aryArgs[0]);
							io_printf("Sorry, binary is not implemented!\n");
							break;    // end of chDataLength 1

						case 2:

							chNumberOfchars =
//							io_printf(": %B", * (uint16_t *) aryArgs[0]);
							io_printf("Sorry, binary is not implemented!\n");
							break;    // end of chDataLength 2

						case 4:

							chNumberOfchars =
//							io_printf(": %lB", * (uint32_t *) aryArgs[0]);
							io_printf("Sorry, binary is not implemented!\n");
							break;    // end of chDataLength 4

					}    /* switch(chDataLength) */

					break;    /* of case 'B' */

					/* Case Signed Decimal */

				case 'D':

					switch (chDataLength) {

						case 1:

							chNumberOfchars = io_printf("(dec): %4d", *(int8_t *)aryArgs[0]);
							break;    /* end of chDataLength 1 */

						case 2:

							chNumberOfchars = io_printf("(dec): %6d", *(int16_t *)aryArgs[0]);
							break;    /* end of chDataLength 2 */

						case 4:

							chNumberOfchars = io_printf("(dec): %11d", *(long *)aryArgs[0]);
							break;    /* end of chDataLength 4 */

					}    /* switch(chDataLength) */

					break;    /* of case 'D' */

					/* Case Unsigned Decimal */

				case 'U':
					switch (chDataLength) {

						case 1:

							chNumberOfchars = io_printf("(unsigned): %3u", *(uint8_t *)aryArgs[0]);
							break;    /* end of chDataLength 1 */

						case 2:

							chNumberOfchars = io_printf("(unsigned): %5u", *(uint16_t *)aryArgs[0]);
							break;    /* end of chDataLength 2 */

						case 4:

							chNumberOfchars = io_printf("(unsigned): %10u", *(unsigned long *)aryArgs[0]);
							break;    /* end of chDataLength 4 */

					}    /* of switch(chDataLength) */

					break;    /* of case 'U'  */

					/* Case Unsigned Hexadecimal */

				case 'X':
					switch (chDataLength) {

						case 1:

							chNumberOfchars = io_printf(": 0x%02X", *(uint8_t *)aryArgs[0]);
							break;    /* end of chDataLength 1 */

						case 2:

							chNumberOfchars = io_printf(": 0x%04X", *(uint16_t *)aryArgs[0]);
							break;    /* end of chDataLength 2 */

						case 4:

							chNumberOfchars = io_printf(": 0x%08X", *(unsigned long *)aryArgs[0]);
							break;    /* end of chDataLength 4 */

					}    /* of switch(chDataLength) */

					break;    /* of case 'X' */

			}    /* of switch(chOutputFormat) */

		}    /* of else */

		/*******************************************************************************

		 Check if chContinuous is true ( = command A selected). If so, wait a little
		 bit for reading, then check if any key is pressed, if so abort and clear
		 chContinuous, if not delete chNumberOfuint8_t uint8_ts by sending backspaces and
		 repeat the output procedure beginning from the "do" statement.
		 Repeat this until any key is presseed. If only command D is selected,
		 chContinuous is false from the beginning. In this case the do - while clause
		 is executed once.

		 *******************************************************************************/

		if (io_getchar() <= 0 && chContinuous) {

			/*FRY...Mod ... Original Value = 100  */
			delay(800);						/* wait 0.1 s for user pressing key */

			if (io_getchar() > 0)
				chContinuous = 0;

			while (chNumberOfchars--) {
//	   		DCS_putuint8_t('\b');			/* print backspaces  */
				io_printf("\b");			/* print backspaces to erase display */
			}

		}    /* of if(polluint8_t() == -1 && chContinuous) */

		else
			chContinuous = 0;

	} while (chContinuous);

	return 0;
}    /* of cmd_MR  */

/* Command MW - modify memory data */

static int16_t cmd_MW(void)
{

	/* Check if address in range for writing */
	/* if not return NO_VALID_WRITE_ADDR  */
	/* Check if alignment is ok (e.g. 2 for Word) */

	if (aryArgs[1] % chDataLength)
		return ALIGNMENT_ERROR;

	/* Check if data fits in chDataLength */

	if ((chDataLength == 1) && (ABS(aryArgs[0]) > (1 << 8)))
		return NO_VALID_WRITE_DATA;				/* entered argument too int32_t */

	if ((chDataLength == 2) && (ABS(aryArgs[0]) > (1 << 16)))
		return NO_VALID_WRITE_DATA;				/* entered argument too int32_t */

	/* Modify Data according to chDataLength and check if successful */


	if (chDataLength == 1)
	{
		*(uint8_t *)aryArgs[1] = (uint8_t)aryArgs[0];


	}    /* of if(chDataLength == 1) */

	else if (chDataLength == 2)
	{
		*(uint16_t *)aryArgs[1] = (uint16_t)aryArgs[0];

	}    /* of if(chDataLength == 2) */

	else if (chDataLength == 4)
	{
		*(uint32_t *)aryArgs[1] = (uint32_t)aryArgs[0];


	}    /* of if(chDataLength == 4) */



	io_printf(" >>> Data at 0x%06X written  (read back to verify)", (unsigned long)aryArgs[1]);


	return 0;
}    /* of cmd_MW */

/* Command TA - Activate Task  */

int16_t cmd_TA(void)
{
	if (sched_activateTask((uint8_t)aryArgs[0]))	/* call taskExec function and return error code */
		return NO_SUCH_TASK;
	else
		return 0;
}    /* of cmd_TA */

/* Command TS - Suspend Task  */

int16_t cmd_TS(void)
{
	if (sched_suspendTask((uint8_t)aryArgs[0]))	/* call taskKill function and return error code */
		return NO_SUCH_TASK;
	else
		return 0;
}    /* of cmd_TS */

/* Command TL - Tasklist  */

int16_t cmd_TL(void)
{
	taskList();
	return 0;
}    /* of cmd_TL */

/* Command PA - Parameter Default One */

int16_t cmd_PA(void)
{
	int16_t err;

	if (paramsys_default(decToParID(aryArgs[0])) != 0){ /* call paramsys_default and check return error code */
		err = 1;
		io_printf(" >>> Failure! Maybe something with the ID\n");
	}
	else {
		io_printf("\n >>> Parameter set to default\n");
		io_printf(" >>> Please check with CMD PR <dec parnr> !\n");

		err = 0;
	}

	return err;
}    /* of cmd_PD */

/* Command PD - Parameter Default All  */

int16_t cmd_PD(void)
{

	paramsys_init();
	io_printf("\n >>> Pararameterlist set to default\n");
	return 0;
}    /* of cmd_PD */


/* Command PW - Parameter Write  */

int16_t cmd_PW(void)
{

	int16_t err;
	int16_t retVal;
	param_value_t parsVal;

	switch (chDataType) {

		case 1:
			parsVal.value.valueUint = aryArgs[0];
			parsVal.type = PARAM_UINT;
			break;

		case 2:
			parsVal.value.valueInt = aryArgs[0];
			parsVal.type = PARAM_INT;
			break;

		case 3:
			parsVal.value.valueFloat = argsFloat;
			parsVal.type = PARAM_FLOAT;
			break;

		default:
			break;
	}

	retVal = paramsys_paramWrite(decToParID((uint16_t)aryArgs[1]), parsVal);

    switch (retVal) {
		case 0:
			io_printf("\n >>> Done!\n");
			io_printf(" >>> Please check with CMD PR <dec parnr> !\n");
			err = 0;
			break;

		case 1:
			io_printf("\n>>> Wrong parameter ID\n");
			err = FAILED;
			break;

		case 2:
			io_printf("\n>>> New Parameter is out of range (value to small)\n");
			err = FAILED;
			break;

		case 3:
			io_printf("\n>>> New Parameter is out of range (value to big)\n");
			err = FAILED;
			break;

		case 4:
			io_printf("\n>>> Failure! Maybe something with the type\n");
			err = FAILED;
			break;

		default:
			err = FAILED;
			break;
	}

	return err;

}    /* of cmd_PW */

/* Command PR - Parameter Read  */

int16_t cmd_PR(void)
{
	int16_t err;
	paramSysParam_t tempStruct;

	if (paramsys_checkID(decToParID((uint16_t)aryArgs[0])) != 0){ /* call checkID and check error code */
		err = FAILED;
	}

	else{
		tempStruct = paramsys_paramRead(decToParID((uint16_t)aryArgs[0]));

		io_printf("\n>>> Parameter [%04d]: ", tempStruct.numb);
		io_printf("%s", tempStruct.name);
		/* switch type of parameter */
		switch (tempStruct.variType) {
			default:
				err = FAILED;
				break;

			case PARAM_INT:
				io_printf(" = %d", tempStruct.paramActValue.valueInt);
				break;

			case PARAM_UINT:
				io_printf(" = %d", tempStruct.paramActValue.valueUint);
				break;

			case PARAM_FLOAT:
				io_printf(" = %f", tempStruct.paramActValue.valueFloat);
				break;

		}
		/* switch unit of parameter */
		switch (tempStruct.unit) {

			default:
				io_printf("\n");
				break;

			case PARAM_AMPERE:
				io_printf(" A\n");
				break;

			case PARAM_VOLT:
				io_printf(" V\n");
				break;

			case PARAM_POWER:
				io_printf(" W\n");
				break;

			case PARAM_DEGREES:
				io_printf("�\n");
				break;

			case PARAM_TEMP:
				io_printf(" �C\n");
				break;

			case PARAM_RPM:
				io_printf(" RPM\n");
				break;

			case PARAM_KMH:
				io_printf(" Km/h\n");
				break;
		}
		err = 0;
	}

	return err;
}    /* of cmd_PR */



/* Command PI - Parameterinfo  */

int16_t cmd_PI(void)
{

	int16_t err;
	paramSysParam_t tempStruct;

	if (paramsys_checkID(decToParID((uint16_t)aryArgs[0])) != 0){ /* call checkID and check error code */
		err = FAILED;
	}
	else{
		tempStruct = paramsys_paramRead(decToParID((uint16_t)aryArgs[0]));

		io_printf("\n*** Parameter [%04d]              *** \n*", tempStruct.numb );
		io_printf("\n* Name        = %s \n*", tempStruct.name);

		/* switch variType of parameter */
		switch (tempStruct.variType) {
			default:
				err = 1;
				break;

			case PARAM_INT:
				io_printf("\n* Type        = int\n*");
				break;

			case PARAM_UINT:
				io_printf("\n* Type        = uint\n*");
				break;
			case PARAM_FLOAT:
				io_printf("\n* Type        = float\n*");
				break;
		}
		/* switch change of parameter */
		switch (tempStruct.change) {
			default:
				err = 1;
				break;

			case PARAM_CHANGEABLE:
				io_printf("\n* Changeable? = changeable\n*");
				break;

			case PARAM_NOTCHANGEABLE:
				io_printf("\n* Changeable? = not changeable)\n*");
				break;
		}

		/* switch signed of parameter */
		switch (tempStruct.sign) {
			default:
				err = 1;
				break;

			case PARAM_SIGNED:
				io_printf("\n* Signed?     = signed\n*");
				break;

			case PARAM_UNSIGNED:
				io_printf("\n* Signed?     = unsigned\n*");
				break;
		}

		/* switch varianz of parameter */
		switch (tempStruct.varianz) {
			default:
				err = 1;
				break;

			case PARAM_VARIABLE:
				io_printf("\n* Variable?   = variable\n*");
				break;

			case PARAM_NOTVARIABLE:
				io_printf("\n* Variable?   = fix\n*");
				break;
		}

		/* switch unit of parameter */
		switch (tempStruct.unit) {

			default:
				err = 1;
				break;

			case PARAM_AMPERE:
				io_printf("\n* Unit        = A (Ampere)\n");
				break;

			case PARAM_VOLT:
				io_printf("\n* Unit        = V (Volt)\n");
				break;

			case PARAM_POWER:
				io_printf("\n* Unit        = W (Watt)\n");
				break;

			case PARAM_DEGREES:
				io_printf("�\n");
				break;

			case PARAM_TEMP:
				io_printf("\n* Unit        = �C (Celsius)\n");
				break;

			case PARAM_RPM:
				io_printf("\n* Unit        = RPM (Rounds per minute)\n");
				break;

			case PARAM_KMH:
				io_printf("\n* Unit        = Km/h\n");
				break;
		}

		/* switch values (min, max, act, def) of parameter */
		switch (tempStruct.variType) {
			default:
				err = 1;
				break;

			case PARAM_INT:
				io_printf("\n* Def. Value  = %d \n*", tempStruct.paramDefValue.valueInt);
				io_printf("\n* Min. Value  = %d \n*", tempStruct.paramMinValue.valueInt);
				io_printf("\n* Act. Value  = %d \n*", tempStruct.paramActValue.valueInt);
				io_printf("\n* Max. Value  = %d \n*", tempStruct.paramMaxValue.valueInt);
				break;

			case PARAM_UINT:
				io_printf("\n* Def. Value  = %d \n*", tempStruct.paramDefValue.valueUint);
				io_printf("\n* Min. Value  = %d \n*", tempStruct.paramMinValue.valueUint);
				io_printf("\n* Act. Value  = %d \n*", tempStruct.paramActValue.valueUint);
				io_printf("\n* Max. Value  = %d \n*", tempStruct.paramMaxValue.valueUint);
				break;

			case PARAM_FLOAT:
				io_printf("\n* Def. Value  = %f \n*", tempStruct.paramDefValue.valueFloat);
				io_printf("\n* Min. Value  = %f \n*", tempStruct.paramMinValue.valueFloat);
				io_printf("\n* Act. Value  = %f \n*", tempStruct.paramActValue.valueFloat);
				io_printf("\n* Max. Value  = %f \n*", tempStruct.paramMaxValue.valueFloat);
				break;

		}
		io_printf("\n* Digits      = %i \n*", tempStruct.digits);
		io_printf("\n* Dec. Place  = %d \n*", tempStruct.decPlace);
		io_printf("\n***\n");

		err = 0;
	}

	return err;

}    /* of cmd_PI */


/* Command PL - Parameterlist  */

int16_t cmd_PL(void)
{
	int16_t err;
	if (paramsys_list() != 0) { /* call parameterlist and check error code */
		err = FAILED;
	}
	else{
		err = 0;
	}
	return err;
}    /* of cmd_PL */

/* Command PV - Read only the value of a parameter  */

int16_t cmd_PV(void)
{
	int16_t err;
	param_value_t tempVal;
	if (paramsys_checkID(decToParID((uint16_t)aryArgs[0])) != 0){ /* call checkID and check error code */
			err = FAILED;
	}
	else{
		tempVal = paramsys_paramValue(decToParID((uint16_t)aryArgs[0]));

		/* switch values (min, max, act, def) of parameter */
		switch (tempVal.type) {
			default:
				err = FAILED;
				break;

			case PARAM_INT:
				io_printf("\n Act. Value  = %d \n", tempVal.value.valueInt);
				err = 0;
				break;

			case PARAM_UINT:
				io_printf("\n Act. Value  = %d \n", tempVal.value.valueUint);
				err = 0;
				break;

			case PARAM_FLOAT:
				io_printf("\n Act. Value  = %f \n", tempVal.value.valueFloat);
				err = 0;
				break;
		}
	}
	return err;
}    /* of cmd_PL */

/* Command DB - Debug Function  */

int16_t cmd_DB(void)
{

	io_printf("\n*** DEBUG DONE ***\n");
	return 0;
}    /* of cmd_DB */


/* Command Q - Quit Monitor and release PC port  */

int16_t cmd_quit(void)
{

	chQuit = 1;								/* this command is for test purposes */
											/* later there will be other methods to quit */
	io_printf(" >>> Quit");
	return 0;

}    /* of cmd_quit */

/* Command R - Fault Reset  */

int16_t cmd_FR(void)
{

	EVENTMGR_resetAllFaults();
	return 0;

}    /* of cmd_reset */
