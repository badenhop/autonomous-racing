/*
 * appinit.h
 *
 *  Created on: Sep 1, 2013
 *      Author: KAISER
 */

#ifndef APP_H_
#define APP_H_

#include "brd/startup/stm32f4xx.h"
#include "stm/stm.h"
#include "vehspdctrl/vehspdctrl.h"
#include "stangsel/stangsel.h"
#include "stangproc/stangproc.h"
#include "dev/rcrec/rcrec.h"
#include "dev/wenc/wenc.h"
#include "app/odom/odom.h"
#include "app/mavlink/mavlink.h"
#include "dev/rx24f/rx24f.h"
#include "app/rx24fctrl/rx24fctrl.h"
#include "app/latctrl/latctrl.h"

/****************************************************************************
*
*	Typedefs
*
****************************************************************************/




/****************************************************************************
*
*	Constants and Macros
*
****************************************************************************/



/****************************************************************************
*
*	Declare Public Functions
*
****************************************************************************/

void app_init(void);

void app_pwrDown(void);




#endif /* APP_H_ */
