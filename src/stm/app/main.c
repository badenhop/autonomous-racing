/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "sys/sigp/sigp.h"
#include "sys/paramsys/paramsys.h"
#include "sys/sched/scheduler.h"
#include "sys/systime/systime.h"
#include "sys/terminal/terminal.h"
#include "app.h"
#include "brd/startup/RCC_Configuration.h"
#include "per/gpio.h"
#include "per/clocksync.h"
#include "sys/util/io.h"
#include "leddebug.h"


/************* Private typedefs ***********************************************************/
/************* Macros and constants *******************************************************/
/************* Private static variables ***************************************************/
/************* Private function prototypes ************************************************/
/************* Public functions ***********************************************************/

/**
 * main program
 */


/* Diagnosis of the communication protocol */
int8_t recBuf[750], sendBuf[750];
uint32_t recBufSize, sendBufSize;
uint32_t sendBufTimestamp[750], recBufTimestamp[750];

static gpio_t debugLed;

void debugLedInit()
{
	debugLed.channel = GPIO_CHAN_13;
	debugLed.mode = GPIO_MODE_OUT;
	debugLed.state = GPIO_STATE_OFF;
	gpio_init(&debugLed);
}

void debugLedOn(void)
{
	gpio_setBit(&debugLed);
}

void debugLedOff(void)
{
	gpio_clearBit(&debugLed);
}

int main (void)
{
//	int8_t varMode=8;
 	RCC_Configuration();

	debugLedInit();

	/* Diagnosis of the communication protocol */
	recBufSize = 750; sendBufSize = 750;
	uart_t dummyUart;
	io_init(&dummyUart, recBuf, recBufSize, sendBuf, sendBufSize, sendBufTimestamp, recBufTimestamp);

	/* System init */
	sigp_init();
	paramsys_init();
	sched_init();
	systime_init();
	clocksync_initDevice();

	/* Application init */

	app_init();

	sched_start();
	irq_enable();
	//terminalMonitor();

	while (1)
	{
	}

	return 0;
}



