/********************************************************************************************
 * Unit in charge:
 * @file    stangproc.c
 * @author  hinze
 * $Date:: 2017-09-06 12:43:27 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2158                  $
 * $Author:: neumerkel			$
 *
 * ----------------------------------------------------------
 *
 * @brief The steering processing consist in the linear transformation of the steering angle
 * in an angle which can be read by the servo device.
 *
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "../stangproc.h"
#include "sys/util/io.h"

/************* Private typedefs ***********************************************************/

/************* Macros and constants *******************************************************/

/************* Global variables ***********************************************************/
/* Signal pointers for input signals */
float32_t* pStAngTgtSel_stangproc;

/* Signal pointers for output signals */
uint32_t* pStAngTimestamp_stangproc;
float32_t* pStAngTgt_stangproc;
float32_t* pStAngAct_stangproc;
float32_t* pSrvAngAct_stangproc;      /**< signal from servo  */

/* Parameter pointers */
float32_t* pStAngMax_stangproc;
float32_t* pStAngMin_stangproc;
float32_t* pStAngTgtFailSafe_stangproc;
float32_t* pMinAbsAngSrv1_stangproc;
float32_t* pMaxAbsAngSrv1_stangproc;

/************* Private static variables ***************************************************/
static float32_t stAngMax = 0;
static float32_t stAngMin = 0;
static float32_t stAngTgtFailSafe = 0;
static float32_t stAngTgt = 0;
static float32_t srvAngMax = 0;
static float32_t srvAngMin = 0;

/************* Private function prototypes ************************************************/
static void updateParameters(void);
static float32_t transformAng(float32_t inAng);
static float32_t transformSvrAngToStAng(float32_t servoAngAct);

/************* Public functions ***********************************************************/
int8_t stangproc_init(void)
{
	stAngMax = 0;
	stAngMin = 0;
	stAngTgtFailSafe = 0;
	stAngTgt = 0;
	srvAngMax = 0;
	srvAngMin = 0;

	/* fetch updated parameters */
	updateParameters();
	/* set output signals to init values */
	*pStAngTimestamp_stangproc = 0;
	*pStAngTgt_stangproc = stAngTgtFailSafe;

	return 0;
}

void stangproc_step(void)
{
	float32_t sigSrvAngAct = 0.0;
	/* fetch updated parameters */
	irq_disable();
	updateParameters();
	sigSrvAngAct = *pSrvAngAct_stangproc;
	/* store target value as intermediate taret value */
	stAngTgt = *pStAngTgtSel_stangproc;
	/* transform steer angle target value into servo steer angle target value */
	*pStAngTgt_stangproc = transformAng(stAngTgt);
	/* transform the actual servo angle value into the actual steer angle  */
	*pStAngAct_stangproc = transformSvrAngToStAng(sigSrvAngAct);
	/* publish timestamp for steering angle */
	*pStAngTimestamp_stangproc = systime_getSysTime();
	irq_enable();

	/* call servo device driver to update to the new target value */
	#ifndef USE_RX24F
	servo_updateAngleSrv1();
	#endif
}

/************* Private functions **********************************************************/
static void updateParameters(void)
{
	/* update all parameters to current values from parameter system */
	stAngMax = *pStAngMax_stangproc;
	stAngMin = *pStAngMin_stangproc;
	srvAngMax = *pMaxAbsAngSrv1_stangproc;
	srvAngMin = *pMinAbsAngSrv1_stangproc;
	stAngTgtFailSafe = transformAng(*pStAngTgtFailSafe_stangproc);
}

static float32_t transformAng(float32_t inAng)
{
	float32_t ret = 0;
	float32_t m = 0;

	/* linear transformation of steer angle input into servo steer angle */
	m = (srvAngMax - srvAngMin) / (stAngMax - stAngMin);
    ret = m * (inAng - stAngMin) + srvAngMin;

	return ret;
}

/* transform the actual servo angle value into the actual steer angle  */
static float32_t transformSvrAngToStAng(float32_t servoAngAct)
{
	float32_t retStAngAct = 0.0;
	float32_t res = 0.0;

	/* linear transformation of servo steer angle input into steer angle */
	res = (servoAngAct - srvAngMin) / (srvAngMax - srvAngMin);
	retStAngAct = res * (stAngMax - stAngMin) + stAngMin;

	return retStAngAct;
}
