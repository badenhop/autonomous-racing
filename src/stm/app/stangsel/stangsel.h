/** *****************************************************************************************
 * Unit in charge:
 * @file	stangsel.h
 * @author	hinze
 * $Date:: 2017-09-06 12:43:27 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2158                  $
 * $Author:: mbrieske           $
 * 
 * ----------------------------------------------------------
 *
 * @brief TODO What does the unit?
 *
 ******************************************************************************************/


#ifndef STANGSEL_H_
#define STANGSEL_H_
/************* Includes *******************************************************************/
#include "app/app.h"
#include "sys/sigp/sigp.h"

/************* Public typedefs ************************************************************/


/************* Macros and constants ******************************************************/

/************* Public function prototypes *************************************************/
/**
 * @brief Initializes the steering angle selector
 *
 * @param [in,out] void
 *
 * @return  0
 */
int8_t stangsel_init(void);

/**
 * @brief Select the appropriate steering angle according to the operation mode
 *
 * @param [in,out] void
 *
 * @return void
 */
void stangsel_step(void);

#endif /* STANGSEL_H_ */
