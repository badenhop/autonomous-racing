/********************************************************************************************
 * Unit in charge:
 * @file    stm.c
 * @author  Brieske
 * $Date:: 2017-06-16 13:20:56 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2022                  $
 * $Author:: mbrieske			$
 *
 * ----------------------------------------------------------
 *
 ******************************************************************************************/


/************* Includes *******************************************************************/

#include "../stm.h"
#include "sys/errorhandler/errorHandler.h"
#include "dev/rcrec/rcrec.h"
#include "app/leddebug.h"

/************* Private typedefs ***********************************************************/

/************* Macros and constants *******************************************************/

/************* Global variables ***********************************************************/

uint8_t *pState_stm;
uint8_t *pRequestedState_stm;
uint32_t *pSyncOffset_stm;
uint64_t *pErrorRegister_stm;
uint8_t *pRcState_stm;
uint16_t *pRcChannel5_stm;

/************* Private static variables ***************************************************/

static uint8_t rcState;
static uint16_t rcOverrideChannelValue;

/************* Private function prototypes ************************************************/

/************* Public functions ***********************************************************/

int8_t stm_init(void)
{
	io_printf("STM: initializing\n");
	errorHandler_init();
	*pState_stm = SYSTEM_STATE_INITIALIZING;
	*pRequestedState_stm = SYSTEM_STATE_NO_REQUEST;
	mavlink_activateStateMessages(SYSTEM_STATE_INITIALIZING);
	return 0;
}

void stm_step(void) {
	uint8_t prevState, state;
	uint64_t error;

	irq_disable();
	prevState = *pState_stm;
	state = *pState_stm;
	error = *pErrorRegister_stm;
	rcState = *pRcState_stm;
	rcOverrideChannelValue = *pRcChannel5_stm;

	irq_enable();

	/* main routine: check actual state and see if it needs to change */

	if(rcState == RCREC_STATE_REC && rcOverrideChannelValue >= RC_CHANNEL_HIGH)
	{
		state = SYSTEM_STATE_RUNNING_RC;
	}

	else if (error != 0)
	{
		state = SYSTEM_STATE_EMERGENCY;
	}

	else {
		switch (prevState)
		{
			case SYSTEM_STATE_INITIALIZING:

				// ATTENTION
				// ---------
				// The following code segment was commented out due to the following reason:
				// It only changes state when the syncOffset variable is set which will
				// occur on an external interrupt from the Odroid.
				// However, this interrupt will be only signaled once when the odroid boots.
				// Since we don't use the syncOffset, we will just ignore this value and directly go to
				// the running_ext state.

				// irq_disable();
				// /*  syncOffset is initialized with 0, so if it is != 0 here, clocksync has been successful.
				//  *  if clocksync successful: goto running_ext
				//  */
				// if (*pSyncOffset_stm != 0)
				// {
				// 	state = SYSTEM_STATE_RUNNING_EXT;
				// }

				// irq_enable();

				state = SYSTEM_STATE_RUNNING_EXT;

				break;
			case SYSTEM_STATE_IDLE:
			case SYSTEM_STATE_RUNNING_EXT:
				/* if got request for state change: goto different state */
				irq_disable();
				uint8_t requestedState = *pRequestedState_stm;
				*pRequestedState_stm = SYSTEM_STATE_NO_REQUEST;
				irq_enable();
				if (requestedState != SYSTEM_STATE_NO_REQUEST)
				{
					state = requestedState;
				}
				break;
			case SYSTEM_STATE_RUNNING_RC:
				state = SYSTEM_STATE_IDLE;
			case SYSTEM_STATE_EMERGENCY:
				state = SYSTEM_STATE_INITIALIZING;
				irq_disable();
				*pSyncOffset_stm = 0;
				irq_enable();
				break;
			default:
				io_printf("STM: Illegal prevState\n");
		}
	}

	/* end of main routine */

	if (state != prevState)
	{
		io_printf("STM: switch state %u -> %u\n", prevState, state);

		switch (state)
		{
		case SYSTEM_STATE_IDLE:
		case SYSTEM_STATE_RUNNING_RC:
		case SYSTEM_STATE_RUNNING_EXT:
			servo_enablePwmSrv1();
			servo_enablePwmSrv2();
			break;
		case SYSTEM_STATE_EMERGENCY:
			//servo_disablePwmSrv1();
			//servo_disablePwmSrv2();
			errorHandler_printErrors();
		default:
			break;
		}

		/* state changed, configure mavlink messages for new state */
		mavlink_deactivateStateMessages(prevState);
		mavlink_activateStateMessages(state);

		irq_disable();
		*pState_stm = state;
		irq_enable();
	}
}

/************* Private functions **********************************************************/

