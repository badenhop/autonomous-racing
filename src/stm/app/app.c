/** *****************************************************************************************
 * Unit in charge:
 * @file	app.c
 * @author	kaiser
 * $Date:: 2017-12-05 12:29:39 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2213                  $
 * $Author:: mbrieske           $
 *
 * ----------------------------------------------------------
 *
 * @brief Application specific initializations (e.g. of periphery such as ADC, PWM for motor control)
 * including registration of all scheduler tasks.
 *
 ******************************************************************************************/



/************* Includes *******************************************************************/
#include "app.h"
#include "sys/util/stubs.h"
#include "sys/sched/scheduler.h"
#include "app/leddebug.h"


/************* Private typedefs ***********************************************************/

/************* Macros and constants *******************************************************/

/*************Global variables ************************************************************/

/************* Private static variables ***************************************************/

/************* Private function prototypes ************************************************/
int8_t callAppInits(void);
void terminalPortInit(void);		// Initialization of UART as ASCII Terminal Port

/************* Public functions ***********************************************************/
/** Header of the public functions come in the header file. **/

void app_init(void)
{
	int8_t err = callAppInits();
}

void app_pwrDown(void) {
	// TODO
}


/************* Private functions **********************************************************/

int8_t callAppInits(void)
{
	/* the error variable indicates an error if err!=0 and no error if err==0*/
	int8_t err = 0;

	/* call all init functions of the apps and devices to run here */
	/* also register the respective scheduler task for the app here */
	/* e.g. err += rcmgr_init();	<- for evaluation of errors during application init	*/
	/*      uint8_t rcmgrTask = sched_registerTask(rcmgr_step, "Run RC Manager", 3); 	*/
	/*      sched_activateTask(rcmgrTask);												*/

	/* init the servo for control of Steer-Angle and Modell Motor */
	err |= servo_init();

	/* MAVLink */
	err |= mavlink_init();

	err |= rx24f_init();

	uint8_t mavlinkGetMessagesTask = sched_registerTask(mavlink_getMessages, "process messages from serial port", 2);
	sched_activateTask(mavlinkGetMessagesTask);

	uint8_t mavlinkSendMessagesTask = sched_registerTask(mavlink_sendMessages, "send mavlink messages", 2);
	sched_activateTask(mavlinkSendMessagesTask);

	/* stm */
	err |= stm_init();
	uint8_t stmTask = sched_registerTask(stm_step, "statemachine step function", 4);
	sched_activateTask(stmTask);

	/* wenc */
    wenc_init();
	uint8_t wencTask = sched_registerTask(wenc_step, "run the wheel encoder", 2);
	sched_activateTask(wencTask);

	/* odom */
	err |= odom_init();
	uint8_t odomTask = sched_registerTask(odom_step, "run odometry module", 2);
	sched_activateTask(odomTask);

	/* rcrec */
	err |= rcrec_init();

	/* vehspdctrl*/
	err |= vehspdctrl_init();
	uint8_t vehspdctrlTask = sched_registerTask(vehspdctrl_step, "run vehicle speed control", 3);
	sched_activateTask(vehspdctrlTask);

	/* servo */
	uint8_t serovangTask = sched_registerTask(servo_srvAngCalc, "run the servo angle calculation", 3);
	sched_activateTask(serovangTask);

	/* stangsel */
	err |= stangsel_init();
	uint8_t stangselTask = sched_registerTask(stangsel_step, "run steering angle selector", 3);
	sched_activateTask(stangselTask);

	/* latctrl */
	err |= latctrl_init();
	uint8_t latctrlTask = sched_registerTask(latctrl_step, "run lateral controller", 3);
	sched_activateTask(latctrlTask);

#ifndef USE_RX24F
	/* stangproc */
	err |= stangproc_init();
	uint8_t stangprocTask = sched_registerTask(stangproc_step, "run steering angle processing", 3);
	sched_activateTask(stangprocTask);
#else
	/* smart servo */
	uint8_t rx24fTask = sched_registerTask(rx24f_step, "run smart servo device", 1);
	sched_activateTask(rx24fTask);

	/* servo pi controller */
	rx24fctrl_init();
	uint8_t rx24fCtrlTask = sched_registerTask(rx24fctrl_step, "run servo pi", 1);
	sched_activateTask(rx24fCtrlTask);

#endif /* #ifdef USE_RX24F */

	/* if one or multiple errors have occured, then err>0. So check for err!=0.
	 * Error -> return 1.
	 * No error -> return 0. */

	return (err!=0);
}

/**
 * terminalPortInit
 * @brief Initialization of UART as ASCII Terminal Port
 *
 * @param [in, out]
 * @param [in]
 * @param [out]
 *
 * @return
 *
 */
void terminalPortInit(void)
{

/*
	uart_t uart;
	int8_t recBuf[12], sendBuf[12];
	uint32_t recBufSize = 12, sendBufSize = 12;
	uint32_t sendBufTimestamp[12], recBufTimestamp[12];
	io_init(&uart, recBuf, recBufSize, sendBuf, sendBufSize, sendBufTimestamp, recBufTimestamp);

 */

	return;
}
