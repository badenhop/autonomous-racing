/** *****************************************************************************************
 * Unit in charge:
 * @file	rx24fctrl.h
 * @author	fklemm
 * $Date:: 2017-05-18 14:51:13 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2110                  $
 * $Author:: fklemm             $
 *
 * ----------------------------------------------------------
 *
 * @brief PI steering angle controller for RX24F (smart servo)
 *
 ******************************************************************************************/

#ifndef RX24FCTRL_H_
#define RX24FCTRL_H_
/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
/************* Public typedefs ************************************************************/
/************* Macros and constants ******************************************************/
/************* Public function prototypes *************************************************/
/**
 * @brief initializes the the pi steering angle controller
 *
 * @param [in,out] void
 *
 * @return 0
 */
int8_t rx24fctrl_init(void);

/**
 * @brief caculates the corrected steering angle
 *
 * @param [in,out] void
 *
 * @return void
 */
void rx24fctrl_step(void);

#endif /* RX24FCTRL_H_ */
