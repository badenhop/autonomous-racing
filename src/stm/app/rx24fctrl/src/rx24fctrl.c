/** *****************************************************************************************
 * Unit in charge:
 * @file	rx24fctrl.c
 * @author	fklemm
 * $Date:: 2017-06-29 19:22:45 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2146                  $
 * $Author:: fklemm             $
 *
 * ----------------------------------------------------------
 *
 * @brief PI steering angle controller for RX24F (smart servo)
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "app/rx24fctrl/rx24fctrl.h"

/************* Global variables ***********************************************************/
/* Input */
float32_t *pStAngTgtSel_rx24fctrl;
float32_t *pSrvAngAct_rx24fctrl;
float32_t *pStAngTgtStress_rx24fctrl;

/* Output */
float32_t *pStAngTgtPiCtrl_rx24fctrl;

/************* Private typedefs ***********************************************************/
/************* Macros and constants *******************************************************/
//#define STRESS_TEST
#define P_GAIN 					0.3F
#define LAMBDA					1
#define I_GAIN					P_GAIN * LAMBDA
#define MAX_PHY_STEERING_ANGLE 	22.0F
#define MIN_PHY_STEERING_ANGLE 	-22.0F
/************* Private static variables ***************************************************/
static float32_t eSum = 0.00;

/************* Private function prototypes ************************************************/
/************* Public functions ***********************************************************/
int8_t rx24fctrl_init(void)
{
	return 0;
}

void rx24fctrl_step(void)
{
	float32_t error = 0.00;
	float32_t pi = 0.00;
	float32_t test = 0.00;
	float32_t eSumOld = eSum;

	irq_disable();
#ifndef STRESS_TEST
	float32_t target = *pStAngTgtSel_rx24fctrl;
#else
	float32_t target = *pStAngTgtStress_rx24fctrl;
#endif
	float32_t actual = *pSrvAngAct_rx24fctrl;
	irq_enable();

	// pi controller
	error = target - actual;
	eSum += I_GAIN * error; // prevent overflow

	pi = P_GAIN * error + I_GAIN * eSum;
	test = P_GAIN * error + I_GAIN * eSum;
	;
	if (pi > MAX_PHY_STEERING_ANGLE) {
		eSum = eSumOld;
		pi = MAX_PHY_STEERING_ANGLE;
	}
	if (pi < MIN_PHY_STEERING_ANGLE) {
		eSum = eSumOld;
		pi = MIN_PHY_STEERING_ANGLE;
	}

	irq_disable();
	*pStAngTgtPiCtrl_rx24fctrl = pi;
	irq_enable();
}

/************* Private functions **********************************************************/
