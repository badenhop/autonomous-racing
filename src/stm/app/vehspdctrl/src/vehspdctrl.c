/** *****************************************************************************************
 * Unit in charge:
 * @file	vehspdctrl.c
 * @author	Brieske
 * $Date:: 2017-09-12 08:12:11 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2022                  $
 * $Author:: Brieske            $
 *
 * ----------------------------------------------------------
 *
 * @brief TODO
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "../vehspdctrl.h"
#include "app/stm/stm.h"
#include "dev/rcrec/rcrec.h"
#include "dev/servo/servo.h"
#include "app/leddebug.h"

/************* Global variables *******************************************************************/
/* input signals */
float *pVehSpdOdom_vehspdctrl;
uint8_t *pState_vehspdctrl;
carcontrol_t *pCarControl_vehspdctrl;
//float *pVehSpdTgtTraj_vehspdctrl;
uint16_t *pRcChannel1_vehspdctrl;

/* output signals */

/* parameters */

/************* Private typedefs ***********************************************************/

/************* Macros and constants *******************************************************/
#define TDELTA 0.02

#define PID_K_P 0.005
// No I value required since we do not need to correct a constant error.
#define PID_K_I 0
// D value to avoid overshoot.
#define PID_K_D 0.001

#define PID_GAIN 1

#define PT1_T 0.08
#define PT1_T_OPT (1. / (PT1_T / TDELTA) + 1)

#define MOT_MAXVAL 10.
// #define U_DEADBAND_POSITIVE 0.0583
// #define U_DEADBAND_NEGATIVE -0.075
#define U_DEADBAND_POSITIVE 0.02
#define U_DEADBAND_NEGATIVE -U_DEADBAND_POSITIVE * 4

#define MOTOR_LIMIT	0.2f
#define MOTOR_FULLSTOP -10
#define MOTOR_AFTER_FULLSTOP 0

#define MIN(a, b)	(a < b ? a : b)
#define MAX(a, b)	(a > b ? a : b)
#define ABS(x) 		(x < 0 ? -x : x)

/************* Private static variables ***************************************************/
static pidCtrl_t pid;
static pt1_t pt1;

static float vehSpdOdom;
static uint8_t state;

static float motTrqTgtSrv;

// tells whether there was a fullstop on the last step
static bool lastFullstop;

/************* Private function prototypes ************************************************/

static float spdCtrlAssystem(float w);
static void spdCtrl(float destSpd);
static void setThrottle(float throttle);

/************* Public functions ***********************************************************/
int8_t vehspdctrl_init(void)
{
	pCarControl_vehspdctrl->vehSpdTgtExt = 0.0f;

	pt1.K = 1;
	pt1.T_opt = PT1_T_OPT;
	pt1.y_old = 0;

	pid.kp = PID_K_P;
	pid.ki = PID_K_I;
	pid.kd = PID_K_D;

//	pid.ki = 0.F;
//	pid.kd = 0.F;

	pid.gain = PID_GAIN;
	pid.ta = TDELTA;
	pid.min = -MOT_MAXVAL;
	pid.max = MOT_MAXVAL;

	pid_initialize(&pid);

	return 0;
}

void vehspdctrl_step(void)
{
	irq_disable();
	state = *pState_vehspdctrl;
	irq_enable();

	switch (state) {
	case SYSTEM_STATE_INITIALIZING:
		// motor is disabled, nothing to do here
		return;
	case SYSTEM_STATE_IDLE:
		irq_disable();
		motTrqTgtSrv = 0;
		lastFullstop = false;
	case SYSTEM_STATE_EMERGENCY:
		spdCtrl(0.F);
		break;
	case SYSTEM_STATE_RUNNING_RC:
		irq_disable();
		float rcvalue = (float) *pRcChannel1_vehspdctrl;
		irq_enable();
//		if (rcvalue < 1400) {
//			motTrqTgtSrv = 0;
//		}
//		else {
			motTrqTgtSrv = (rcvalue - 1500.0F) / (float) (MAX_PERIOD_CHANNEL - MIN_PERIOD_CHANNEL);
			motTrqTgtSrv *= 0.8;
//			motTrqTgtSrv *= 3;
//			motTrqTgtSrv = spdCtrl(motTrqTgtSrv);
//		}
		break;
	case SYSTEM_STATE_RUNNING_EXT:
		irq_disable();
		float spdTgt = pCarControl_vehspdctrl->vehSpdTgtExt;
		irq_enable();

		// debugging
		if (spdTgt != 0.0f)
			debugLedOn();
		else
			debugLedOff();
		
		setThrottle(spdTgt);
		break;
//	case SYSTEM_STATE_RUNNING_TRAJ:
//		TODO system state not implemented yet
//		irq_disable();
//		float spdTgt = *pVehSpdTgtTraj_vehspdctrl;
//		irq_enable();
//		motTrqTgtSrv = spdCtrl(spdTgt);
//		break
	default:
		// cannot go here
		break;
	}

	servo_updateMotorTrq(motTrqTgtSrv);
}

/************* Private functions **********************************************************/

static float spdCtrlAssystem(float w)
{
	float u = 0;	// manipulated value
	float u_f = 0;	// feed forward value
	float y = 0;	// controlled value

	irq_disable();
	y = *pVehSpdOdom_vehspdctrl;
	irq_enable();

	y = pt1_calculate(&pt1, y);
	// 'u' is the new motTrqTgtSrv value which is not what the PID should calculate.
	// A PID controller should actually output a change on the motTrqTgtSrv value.
	// This obviously leads to a very unstable driving experience.
	pid_controller(&pid, y, w, &u, -MOT_MAXVAL, MOT_MAXVAL);

	if (u > 0) {
		u_f = U_DEADBAND_POSITIVE;
	}
	else if (u < 0) {
		u_f = U_DEADBAND_NEGATIVE;
	}

	u += u_f;

	return u;
}

static void spdCtrl(float destSpeed)
{
	// Here we first check if speed 0 was received.
	// In this case we perform a fullstop with maximum brake power.
	// This might have been implemented differently but there wasn't enough time at this point.
	// However this functionality is definitely required since when left out the PID tries to smoothly reduce speed to 0
	// which is not what we desire when trying to avoid a collision.
	bool fullstop;
	if (destSpeed <= 0)
		fullstop = true;
	else
		fullstop = false;

	if (!fullstop && lastFullstop)
	{
		motTrqTgtSrv = MOTOR_AFTER_FULLSTOP;
		pid_initialize(&pid);
	}

	lastFullstop = fullstop;

	if (fullstop)
	{
		motTrqTgtSrv = MOTOR_FULLSTOP;
		return;
	}

	float change = 0;		// value calculated by PID
	float currSpeed = 0;	// controlled value

	irq_disable();
	currSpeed = ABS(*pVehSpdOdom_vehspdctrl);
	irq_enable();

	currSpeed = pt1_calculate(&pt1, currSpeed);
	pid_controller(&pid, currSpeed, destSpeed, &change, -MOT_MAXVAL, MOT_MAXVAL);

	motTrqTgtSrv += change;

	if (motTrqTgtSrv > 0 && motTrqTgtSrv < U_DEADBAND_POSITIVE)
		motTrqTgtSrv = U_DEADBAND_POSITIVE;

	motTrqTgtSrv = MIN(motTrqTgtSrv, MOTOR_LIMIT);
}

static void setThrottle(float throttle)
{
	throttle = MIN(throttle, 1.0f);
	throttle = MAX(throttle, 0.0f);

	if (throttle == 0.0f)
	{
		motTrqTgtSrv = MOTOR_FULLSTOP;
		return;
	}
	
	motTrqTgtSrv = throttle;
}