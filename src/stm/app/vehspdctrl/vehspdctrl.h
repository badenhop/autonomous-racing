
/** *****************************************************************************************
 * Unit in charge:
 * @file	vehspdctrl.h
 * @author	Brieske
 * $Date:: 2017-09-12 08:12:11 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2022                  $
 * $Author:: Brieske            $
 *
 * ----------------------------------------------------------
 *
 * @brief TODO
 *
 ******************************************************************************************/

#ifndef VEHSPDCTRL_H_
#define VEHSPDCTRL_H_

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "per/irq.h"
#include "sys/algo/pid.h"
#include "sys/algo/pt1.h"
/************* Public typedefs ************************************************************/

/************* Macros and constants *******************************************************/

/************* Public function prototypes *************************************************/
int8_t vehspdctrl_init(void);
/**
 * @brief Initialize the parameters and signals used by the application vehspdctrl
 *
 * @return int8_t success
 **/
void vehspdctrl_step(void);
/**
 * @brief TODO
 */

#endif /* VEHSPDPROC_H_ */
