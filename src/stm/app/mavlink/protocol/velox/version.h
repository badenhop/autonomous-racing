/** @file
 *  @brief MAVLink comm protocol built from velox.xml
 *  @see http://mavlink.org
 */
#pragma once
 
#ifndef MAVLINK_VERSION_H
#define MAVLINK_VERSION_H

#define MAVLINK_BUILD_DATE "Mon Aug 07 2017"
#define MAVLINK_WIRE_PROTOCOL_VERSION "2.0"
#define MAVLINK_MAX_DIALECT_PAYLOAD_SIZE 94
 
#endif // MAVLINK_VERSION_H
