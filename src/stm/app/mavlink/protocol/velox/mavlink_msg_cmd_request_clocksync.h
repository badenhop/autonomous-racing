#pragma once
// MESSAGE CMD_REQUEST_CLOCKSYNC PACKING

#define MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC 102

MAVPACKED(
typedef struct __mavlink_cmd_request_clocksync_t {
 uint8_t dummy; /*< message cannot be empty*/
}) mavlink_cmd_request_clocksync_t;

#define MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN 1
#define MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_MIN_LEN 1
#define MAVLINK_MSG_ID_102_LEN 1
#define MAVLINK_MSG_ID_102_MIN_LEN 1

#define MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_CRC 119
#define MAVLINK_MSG_ID_102_CRC 119



#if MAVLINK_COMMAND_24BIT
#define MAVLINK_MESSAGE_INFO_CMD_REQUEST_CLOCKSYNC { \
    102, \
    "CMD_REQUEST_CLOCKSYNC", \
    1, \
    {  { "dummy", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_cmd_request_clocksync_t, dummy) }, \
         } \
}
#else
#define MAVLINK_MESSAGE_INFO_CMD_REQUEST_CLOCKSYNC { \
    "CMD_REQUEST_CLOCKSYNC", \
    1, \
    {  { "dummy", NULL, MAVLINK_TYPE_UINT8_T, 0, 0, offsetof(mavlink_cmd_request_clocksync_t, dummy) }, \
         } \
}
#endif

/**
 * @brief Pack a cmd_request_clocksync message
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 *
 * @param dummy message cannot be empty
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cmd_request_clocksync_pack(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg,
                               uint8_t dummy)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN];
    _mav_put_uint8_t(buf, 0, dummy);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN);
#else
    mavlink_cmd_request_clocksync_t packet;
    packet.dummy = dummy;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC;
    return mavlink_finalize_message(msg, system_id, component_id, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_CRC);
}

/**
 * @brief Pack a cmd_request_clocksync message on a channel
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param dummy message cannot be empty
 * @return length of the message in bytes (excluding serial stream start sign)
 */
static inline uint16_t mavlink_msg_cmd_request_clocksync_pack_chan(uint8_t system_id, uint8_t component_id, uint8_t chan,
                               mavlink_message_t* msg,
                                   uint8_t dummy)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN];
    _mav_put_uint8_t(buf, 0, dummy);

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), buf, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN);
#else
    mavlink_cmd_request_clocksync_t packet;
    packet.dummy = dummy;

        memcpy(_MAV_PAYLOAD_NON_CONST(msg), &packet, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN);
#endif

    msg->msgid = MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC;
    return mavlink_finalize_message_chan(msg, system_id, component_id, chan, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_CRC);
}

/**
 * @brief Encode a cmd_request_clocksync struct
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param msg The MAVLink message to compress the data into
 * @param cmd_request_clocksync C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cmd_request_clocksync_encode(uint8_t system_id, uint8_t component_id, mavlink_message_t* msg, const mavlink_cmd_request_clocksync_t* cmd_request_clocksync)
{
    return mavlink_msg_cmd_request_clocksync_pack(system_id, component_id, msg, cmd_request_clocksync->dummy);
}

/**
 * @brief Encode a cmd_request_clocksync struct on a channel
 *
 * @param system_id ID of this system
 * @param component_id ID of this component (e.g. 200 for IMU)
 * @param chan The MAVLink channel this message will be sent over
 * @param msg The MAVLink message to compress the data into
 * @param cmd_request_clocksync C-struct to read the message contents from
 */
static inline uint16_t mavlink_msg_cmd_request_clocksync_encode_chan(uint8_t system_id, uint8_t component_id, uint8_t chan, mavlink_message_t* msg, const mavlink_cmd_request_clocksync_t* cmd_request_clocksync)
{
    return mavlink_msg_cmd_request_clocksync_pack_chan(system_id, component_id, chan, msg, cmd_request_clocksync->dummy);
}

/**
 * @brief Send a cmd_request_clocksync message
 * @param chan MAVLink channel to send the message
 *
 * @param dummy message cannot be empty
 */
#ifdef MAVLINK_USE_CONVENIENCE_FUNCTIONS

static inline void mavlink_msg_cmd_request_clocksync_send(mavlink_channel_t chan, uint8_t dummy)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char buf[MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN];
    _mav_put_uint8_t(buf, 0, dummy);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC, buf, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_CRC);
#else
    mavlink_cmd_request_clocksync_t packet;
    packet.dummy = dummy;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC, (const char *)&packet, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_CRC);
#endif
}

/**
 * @brief Send a cmd_request_clocksync message
 * @param chan MAVLink channel to send the message
 * @param struct The MAVLink struct to serialize
 */
static inline void mavlink_msg_cmd_request_clocksync_send_struct(mavlink_channel_t chan, const mavlink_cmd_request_clocksync_t* cmd_request_clocksync)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    mavlink_msg_cmd_request_clocksync_send(chan, cmd_request_clocksync->dummy);
#else
    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC, (const char *)cmd_request_clocksync, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_CRC);
#endif
}

#if MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN <= MAVLINK_MAX_PAYLOAD_LEN
/*
  This varient of _send() can be used to save stack space by re-using
  memory from the receive buffer.  The caller provides a
  mavlink_message_t which is the size of a full mavlink message. This
  is usually the receive buffer for the channel, and allows a reply to an
  incoming message with minimum stack space usage.
 */
static inline void mavlink_msg_cmd_request_clocksync_send_buf(mavlink_message_t *msgbuf, mavlink_channel_t chan,  uint8_t dummy)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    char *buf = (char *)msgbuf;
    _mav_put_uint8_t(buf, 0, dummy);

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC, buf, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_CRC);
#else
    mavlink_cmd_request_clocksync_t *packet = (mavlink_cmd_request_clocksync_t *)msgbuf;
    packet->dummy = dummy;

    _mav_finalize_message_chan_send(chan, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC, (const char *)packet, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_MIN_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_CRC);
#endif
}
#endif

#endif

// MESSAGE CMD_REQUEST_CLOCKSYNC UNPACKING


/**
 * @brief Get field dummy from cmd_request_clocksync message
 *
 * @return message cannot be empty
 */
static inline uint8_t mavlink_msg_cmd_request_clocksync_get_dummy(const mavlink_message_t* msg)
{
    return _MAV_RETURN_uint8_t(msg,  0);
}

/**
 * @brief Decode a cmd_request_clocksync message into a struct
 *
 * @param msg The message to decode
 * @param cmd_request_clocksync C-struct to decode the message contents into
 */
static inline void mavlink_msg_cmd_request_clocksync_decode(const mavlink_message_t* msg, mavlink_cmd_request_clocksync_t* cmd_request_clocksync)
{
#if MAVLINK_NEED_BYTE_SWAP || !MAVLINK_ALIGNED_FIELDS
    cmd_request_clocksync->dummy = mavlink_msg_cmd_request_clocksync_get_dummy(msg);
#else
        uint8_t len = msg->len < MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN? msg->len : MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN;
        memset(cmd_request_clocksync, 0, MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC_LEN);
    memcpy(cmd_request_clocksync, _MAV_PAYLOAD(msg), len);
#endif
}
