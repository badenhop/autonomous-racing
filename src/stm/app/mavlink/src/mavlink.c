/********************************************************************************************
 * Unit in charge:
 * @file    odom.c
 * @author  mbrieske
 * $Date:: 2017-05-22 17:17:17 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1538                  $
 * $Author:: mbrieske			$
 *
 * ----------------------------------------------------------
 *
 * @brief	MAVLink see mavlink.h
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "../mavlink.h"
#include "app/leddebug.h"

#define ABS(x) 		(x < 0 ? -x : x)

/************* Private typedefs ***********************************************************/

#define NUM_MSGS (sizeof mavlinkMessageInfo / sizeof mavlinkMessageInfo[0])

typedef struct msgmeta {
		uint8_t				msgid;			/* mavlink message id */
		bool				active;			/* will send message if set to TRUE */
		uint8_t				period;			/* signal will be send every [period] * 10 ms */
		bool				customRequest;	/* true if message was requested via mavlink message */
} msgmeta_t;

/************* Private signals and variables **********************************************************/

static const mavlink_message_info_t mavlinkMessageInfo[] = MAVLINK_MESSAGE_INFO;
mavlink_system_t mavlinkSystem;
msgmeta_t msgMeta[NUM_MSGS];
static bool prevTimeout;

/* UART */
uart_t uart;
static uint8_t byteBuffer;
static int8_t receiveBuffer[MAX_BUFFER_LEN], transmitBuffer[MAX_BUFFER_LEN];
static uint32_t receiveBufferSize = MAX_BUFFER_LEN, transmitBufferSize = MAX_BUFFER_LEN;
static uint32_t receiveBufferTimestamp[MAX_BUFFER_LEN], transmitBufferTimestamp[MAX_BUFFER_LEN];

static uint16_t transmitcounter;

/* Signals */
uint32_t *pSyncOffset_mavlink;
heartbeat_t *pHeartbeat_mavlink;
carcontrol_t *pCarControl_mavlink;
trajectory_t *pTrajectory_mavlink;
uint8_t *pState_mavlink;
uint8_t *pRequestedState_mavlink;
uint64_t *pErrorRegister_mavlink;

uint32_t *pOdomTimestamp_mavlink;
uint32_t *pStAngTimestamp_mavlink;
float32_t *pVehSpdOdom_mavlink;
float32_t *pVehXDistOdom_mavlink;
float32_t *pVehYDistOdom_mavlink;
float32_t *pVehYawAngOdom_mavlink;
float32_t *pStAngAct_mavlink;

float *pLeftWheelSpeedOdom_mavlink;
float *pRightWheelSpeedOdom_mavlink;

/************* Private functions **********************************************************/

bool configureOutgoingMessage(uint8_t msgid, bool active, uint8_t period, bool customRequest)
{
	if (msgid == MAVLINK_MSG_ID_HEARTBEAT && customRequest) return false; // do not allow heartbeat to be reconfigured by custom requests
	for (uint8_t i = 0; i < NUM_MSGS; i++)
		{
			if (msgMeta[i].msgid == msgid)
			{
				if (msgMeta[i].customRequest == false || customRequest == true)
				{
					msgMeta[i].active = active;
					msgMeta[i].period = period;
					msgMeta[i].customRequest = customRequest;
				}
				return true;
			}
		}
		return false;
}

void configureStateMessages(uint8_t state, bool active)
{
	switch (state)
	{
	case SYSTEM_STATE_INITIALIZING:
		configureOutgoingMessage(MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC, active, 50, false);
		break;
	case SYSTEM_STATE_IDLE:
		configureOutgoingMessage(MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC, active, 10, false);
		break;
	case SYSTEM_STATE_RUNNING_EXT:
		configureOutgoingMessage(MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC, active, 10, false);
		configureOutgoingMessage(MAVLINK_MSG_ID_ODOMETRY, active, 2, false);
		break;
	case SYSTEM_STATE_RUNNING_RC:
		configureOutgoingMessage(MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC, active, 10, false);
		break;
	case SYSTEM_STATE_EMERGENCY:
		configureOutgoingMessage(MAVLINK_MSG_ID_ERROR, active, 10, false);
		break;
	default:
		io_printf("MAVLINK: switched to unknown state");
	}
}

bool sendMessage(uint8_t msgid)
{
	mavlink_message_t msg;
	uint8_t buf[MAVLINK_MAX_PACKET_LEN];

	irq_disable();
	uint32_t syncOffset = *pSyncOffset_mavlink;
	switch (msgid)
	{
	case MAVLINK_MSG_ID_HEARTBEAT:
		mavlink_msg_heartbeat_pack(mavlinkSystem.sysid, mavlinkSystem.compid, &msg, *pState_mavlink);
		break;
	case MAVLINK_MSG_ID_ERROR:
		mavlink_msg_error_pack(mavlinkSystem.sysid, mavlinkSystem.compid, &msg, *pErrorRegister_mavlink);
		break;
	case MAVLINK_MSG_ID_ODOMETRY:
		//mavlink_msg_odometry_pack(mavlinkSystem.sysid, mavlinkSystem.compid, &msg, *pOdomTimestamp_mavlink - syncOffset, *pStAngTimestamp_mavlink - syncOffset, ABS(*pVehSpdOdom_mavlink), *pVehXDistOdom_mavlink, *pVehYDistOdom_mavlink, *pVehYawAngOdom_mavlink, *pStAngAct_mavlink);
		mavlink_msg_odometry_pack(mavlinkSystem.sysid, mavlinkSystem.compid, &msg, *pOdomTimestamp_mavlink - syncOffset, *pStAngTimestamp_mavlink - syncOffset, ABS(*pVehSpdOdom_mavlink), *pLeftWheelSpeedOdom_mavlink, *pRightWheelSpeedOdom_mavlink, *pVehYawAngOdom_mavlink, *pStAngAct_mavlink);
		break;
	case MAVLINK_MSG_ID_CMD_REQUEST_CLOCKSYNC:
		mavlink_msg_cmd_request_clocksync_pack(mavlinkSystem.sysid, mavlinkSystem.compid, &msg, 0);
		break;
	default:
		io_printf("MAVLink: not implemented or unknown msgid\n");
		irq_enable();
		return false;
	}
	irq_enable();

	uint16_t len = mavlink_msg_to_send_buffer(buf, &msg);
	if (uart.sendring.size - uart.sendring.count > len) {
		uart_writeString(&uart, (int8_t*) buf, len);
	}
	else {
		io_printf("MAVLink: UART TX buffer full\n");
		return false;
	}

	return true;
}

void handleMessage(mavlink_message_t *msg)
{
	uint32_t recTimestamp = systime_getSysTime();
	irq_disable();
	uint32_t syncOffset = *pSyncOffset_mavlink;
	switch(msg->msgid)
	{
	case MAVLINK_MSG_ID_HEARTBEAT:
		pHeartbeat_mavlink->lastReceiveTimestamp = recTimestamp;
		pHeartbeat_mavlink->mavlink_version = mavlink_msg_heartbeat_get_mavlink_version(msg);
		pHeartbeat_mavlink->state = mavlink_msg_heartbeat_get_state(msg);
		break;
	case MAVLINK_MSG_ID_CARCONTROL:
		pCarControl_mavlink->lastReceiveTimestamp = recTimestamp;
		pCarControl_mavlink->vehSpdTgtExt = mavlink_msg_carcontrol_get_vehspd(msg);
		pCarControl_mavlink->stAngTgtExt = mavlink_msg_carcontrol_get_stang(msg) - 4;
		break;
	case MAVLINK_MSG_ID_TRAJECTORY:
		pTrajectory_mavlink->lastReceiveTimestamp = recTimestamp;
		pTrajectory_mavlink->timestamp = mavlink_msg_trajectory_get_timestamp(msg) + syncOffset;
		mavlink_msg_trajectory_get_x(msg, pTrajectory_mavlink->x);
		mavlink_msg_trajectory_get_y(msg, pTrajectory_mavlink->y);
		mavlink_msg_trajectory_get_theta(msg, pTrajectory_mavlink->theta);
		mavlink_msg_trajectory_get_kappa(msg, pTrajectory_mavlink->kappa);
		mavlink_msg_trajectory_get_v(msg, pTrajectory_mavlink->v);
		break;
	case MAVLINK_MSG_ID_CMD_REQUEST_MSG: ; /* semicolon is needed here ... */
		uint8_t msgid = mavlink_msg_cmd_request_msg_get_msgid(msg);
		bool_t active = (bool_t) mavlink_msg_cmd_request_msg_get_active(msg);
		uint8_t period = mavlink_msg_cmd_request_msg_get_period(msg);
		configureOutgoingMessage(msgid, active, period, true);
		break;
	case MAVLINK_MSG_ID_CMD_REQUEST_STATECHANGE:
		*pRequestedState_mavlink = mavlink_msg_cmd_request_statechange_get_state(msg);
		break;
	default:
		io_printf("MAVLINK: Don't know how to handle message id %u\n", msg->msgid);
	}
	irq_enable();
}

/************* Public functions ***********************************************************/

int8_t mavlink_init(void)
{
	io_printf("MAVLINK: initializing\n");
	uart.baudrate = MAVLINK_BAUDRATE;
	uart.channel = UART_CH3;
	if (uart_init(&uart, receiveBuffer, receiveBufferSize, transmitBuffer, transmitBufferSize, transmitBufferTimestamp, receiveBufferTimestamp)) {
		return -1;
	}

	mavlinkSystem.sysid = 0;
	mavlinkSystem.compid = MAV_COMP_ID_STM;

	for (uint8_t i = 0; i < NUM_MSGS; i++)
	{
		msgMeta[i].msgid = mavlinkMessageInfo[i].msgid;
		msgMeta[i].active = false;
		msgMeta[i].customRequest = false;
	}

	/* heartbeat is always active, might as well activate it here (and never deactivate) */
	configureOutgoingMessage(MAVLINK_MSG_ID_HEARTBEAT, true, 10, false);

	prevTimeout = true;

	return 0;
}

void mavlink_getMessages(void)
{
	mavlink_message_t msg;
	mavlink_status_t status;
	uint8_t chan = 0;

	while(uart_readByte(&uart, (int8_t*) &byteBuffer))
	{
		if (mavlink_parse_char(chan, byteBuffer, &msg, &status))
		{
			handleMessage(&msg);
		}
	}

	bool timeout = (systime_getSysTime() - pHeartbeat_mavlink->lastReceiveTimestamp) > 2.5e5; // heartbeat timeout after 250ms
	if (timeout && !prevTimeout)
	{
		errorHandler_setError(ERROR_MAVLINK_Timeout);
		prevTimeout = true;
	}
	else if (!timeout && prevTimeout) {
		errorHandler_clearError(ERROR_MAVLINK_Timeout);
		prevTimeout = false;
	}
}

void mavlink_sendMessages(void)
{
	transmitcounter++;
	for (uint8_t i = 0; i < NUM_MSGS; i++)
	{
		if (msgMeta[i].active && (transmitcounter % msgMeta[i].period) == 0)
		{
			sendMessage(msgMeta[i].msgid);
		}
	}
}

void mavlink_activateStateMessages(uint8_t state)
{
	configureStateMessages(state, true);
}

void mavlink_deactivateStateMessages(uint8_t state)
{
	configureStateMessages(state, false);
}
