
#ifndef MAVLINK_H_
#define MAVLINK_H_

/************* Includes *******************************************************************/

#include <stdlib.h>
#include <stdbool.h>
#include "per/irq.h"
#include "sys/util/io.h"
#include "per/uart.h"
#include "sys/sigp/sigp.h"
#include "sys/systime/systime.h"
#include "protocol/velox/mavlink.h"
#include "sys/errorhandler/errorHandler.h"

/************* Public typedefs ************************************************************/

/************* Macros and constants *******************************************************/

#define MAVLINK_BAUDRATE 115200
#define MAX_BUFFER_LEN 1000
/************* Public function prototypes *************************************************/
/**
 *	@brief The init function needs to be called during the initialization of the system.
 *	It sets all local variables to their initial values.
 */
int8_t mavlink_init(void);

/**
 *	@brief read messages from uart buffer and interpret them
 */
void mavlink_getMessages(void);

/**
 *	@brief pack and send all messages that are marked as active
 */
void mavlink_sendMessages(void);

/**
 *	@brief activate outgoing messages that are linked to the specified state
 */
void mavlink_activateStateMessages(uint8_t state);

/**
 *	@brief deactivate outgoing messages that are linked to the specified state
 */
void mavlink_deactivateStateMessages(uint8_t state);

#endif /* MAVLINK_H_ */
