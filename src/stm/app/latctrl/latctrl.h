/** *****************************************************************************************
 * Unit in charge:
 * @file	latctrl.h
 * @author	fklemm
 * $Date:: 2017-05-18 14:51:13 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2110                  $
 * $Author:: fklemm             $
 *
 * ----------------------------------------------------------
 *
 * @brief Robust PID based lateral controller
 *
 ******************************************************************************************/

#ifndef LATCTRL_H_
#define LATCTRL_H_
/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
/************* Public typedefs ************************************************************/
/************* Macros and constants ******************************************************/
/************* Public function prototypes *************************************************/
/**
 * @brief initializes the the pi steering angle controller
 *
 * @param [in,out] void
 *
 * @return 0
 */
int8_t latctrl_init(void);

/**
 * @brief caculates the corrected steering angle
 *
 * @param [in,out] void
 *
 * @return void
 */
void latctrl_step(void);

#endif /* LATCTRL_H_ */
