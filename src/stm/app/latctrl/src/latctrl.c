/** *****************************************************************************************
 * Unit in charge:
 * @file	latctrl.c
 * @author	fklemm
 * $Date:: 2017-06-29 19:22:45 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2146                  $
 * $Author:: fklemm             $
 *
 * ----------------------------------------------------------
 *
 * @brief Robust PID based lateral controller
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "../latctrl.h"
#include "sys/sigp/sigp.h"
#include "sys/algo/pid.h"
#include "per/irq.h"

/************* Global variables ***********************************************************/
/* output signals */
float32_t *pLateralOffsets_latctrl[10];
float32_t *pAngOffset_latctrl;
float32_t *pVehSpdAct_latctrl;

/* input signals */
trajectory_t *pTrajectory_latctrl;
float32_t *pStAngTgtLatCtrl_latctrl;

/************* Private typedefs ***********************************************************/
pidCtrl_t pid;
/************* Macros and constants *******************************************************/
/************* Private static variables ***************************************************/
/************* Private function prototypes ************************************************/

/************* Public functions ***********************************************************/
int8_t latctrl_init(void)
{
	pid.gain = 1;
	pid.kp = 1;
	pid.ki = 0;
	pid.kd = 0;
	pid.max = 100;
	pid.min = -100;
	pid.ta = 1;

	pid_initialize(&pid);

	return 0;
}

void latctrl_step(void)
{
	int16_t lateralOffset = 2;
	int16_t pi = 0;

	irq_disable();
	lateralOffset = *pLateralOffsets_latctrl[0];
	irq_enable();
	pid_controller(&pid, lateralOffset, 0, &pi, 10, 10);


	irq_disable();
	*pStAngTgtLatCtrl_latctrl = pi;
	irq_enable();
}

/************* Private functions **********************************************************/
