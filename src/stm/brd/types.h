/** *****************************************************************************************
 * Unit in charge:
 * @file	types.h
 * @author	hepner
 * $Date:: 2015-02-19 14:03:20 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1312                  $
 * $Author:: neumerkel          $
 * 
 * ----------------------------------------------------------
 *
 * @brief TODO What does the unit?
 *
 ******************************************************************************************/

#ifndef TYPES_H_
#define TYPES_H_

/************* Includes *******************************************************************/


/************* Public typedefs ************************************************************/


/************* Macros and constants *******************************************************/

#if !__int8_t_defined
typedef signed 		char 	int8_t;
#define __int8_t_defined 1
#endif

#if !__int16_t_defined
typedef signed 		short 	int16_t;
#define __int16_t_defined 1
#endif

#if !__int32_t_defined
typedef signed 		int 	int32_t;
typedef unsigned 	int 	uint32_t;
#define __int32_t_defined 1
#endif

#if !__int64_t_defined
typedef signed 		long 	int64_t;
typedef unsigned 	long 	uint64_t;
#define __int64_t_defined 1
#endif

#if !__uint8_t_defined
typedef unsigned 	char 	uint8_t;
#define __uint8_t_defined 1
#endif

#if !__uint16_t_defined
typedef unsigned 	short 	uint16_t;
#define __uint16_t_defined 1
#endif

#if !__float32_t_defined
typedef 			float 	float32_t;
#define __float32_t_defined 1
#endif

#if !__float64_t_defined
typedef 			double 	float64_t;
#define __float64_t_defined 1
#endif

#if !__char_t_defined
typedef 			char 	char_t;
#define __char_t_defined 1
#endif

#if !__bool_t_defined
typedef		 		char	bool_t;
#define __bool_t_defined 1
#endif

#ifndef FALSE
#define FALSE 0
#endif

#ifndef TRUE
#define TRUE 1
#endif

/************* Public function prototypes *************************************************/

#endif /* TYPES_H_ */
