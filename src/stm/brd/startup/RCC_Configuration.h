/** *****************************************************************************************
 * Unit in charge:
 * @file	RCC_Configuration.h
 * @author	wandji-kwuntchou
 * $Date:: 2014-07-04 09:32:32 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 585                   $
 * $Author:: wandji-kwuntchou   $
 * 
 * ----------------------------------------------------------
 *
 * @brief TODO What does the unit?
 *
 ******************************************************************************************/


#ifndef RCC_CONFIGURATION_H_
#define RCC_CONFIGURATION_H_

#include "stm32f4xx.h"

/* Function Declaration for RCC Configuration */
void RCC_Configuration(void);


/************* Includes *******************************************************************/


/************* Public typedefs ************************************************************/


/************* Macros and constants ******************************************************/


/************* Public function prototypes *************************************************/


#endif /* RCC_CONFIGURATION_H_ */
