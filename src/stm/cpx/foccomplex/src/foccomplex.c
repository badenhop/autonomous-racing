/** *****************************************************************************************
 * Unit in charge:
 * @file	foccomplex.c
 * @author	Robert Ledwig
 * $Date:: 2015-10-19 16:22:22 #$
 *
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1741                  $
 * $Author:: neumerkel          $
 *
 * ----------------------------------------------------------
 *
 ******************************************************************************************/


/************* Includes *******************************************************************/
#include <stdint.h>
#include "cpx/foccomplex/foccomplex.h"
/*#include "app/foc/foc.h"*/
#include "per/gpio.h"
#include "per/pwm.h"

/************* Private typedefs ***********************************************************/
/**
 * @enum pwm_channel_t
 *
 * The different PWM Channels
 *
 */
typedef enum {
	FOCCOMPLEX_PWM_CH1 = 0,
	FOCCOMPLEX_PWM_CH2,
	FOCCOMPLEX_PWM_CH3,
	FOCCOMPLEX_PWM_NUM_CHANNELS                   			  /**< Used to calculate the amount of PWM channels */
} foccomplex_pwmCh_t;

/************* Macros and constants *****************************************************************/
#define PWMCONFIG_INIT_FAILED 		-1     			 /**< Initialization of the PWM failed.     */
#define ADCCONFIG_INIT_FAILED 	    -1     			 /**< Initialization of the ADC failed.     */
#define BUFFERSIZE 8 						 	     /**< 8 correspond to the DMA Buffer size */
//#define PWM_SETDUTYCYCLE_FAILED     -2               /**< Set the duty cycle of the PWM failed. */
#define ADC_CDR_ADDRESS             ((uint32_t)0x40012308)           	/**< Address of the ADC 	*/														    		/**< Common Data register	*/
#define PWM_RESOLUTION              4200                    /**< Set the resolution of the PWM */
#define DUTYCYCLE_SCALE             100.0       		    /**< Factor to scale the duty cycle  */

/************* Globale  variables ***************************************************/
/* ADC signals from signal pool */
float32_t* pCurrentPhaseU_foccomplex;
float32_t* pCurrentPhaseV_foccomplex;
float32_t* pCurrentPhaseW_foccomplex;
float32_t* pRotorSin_foccomplex;
float32_t* pRotorCos_foccomplex;
uint32_t*  pBatVoltage_foccomplex;
float32_t* pSrvAngVolt_foccomplex;         /**< signal for the application stang */
/* PWM Signals from signal pool */
float32_t* pMotPhaseUDutyCycle_foccomplex;
float32_t* pMotPhaseVDutyCycle_foccomplex;
float32_t* pMotPhaseWDutyCycle_foccomplex;

/* Parameters from parameter system */
float32_t* pAmpFactorU_foccomplex;		     /**< Default value: -0.016619461    */
float32_t* pAmpFactorV_foccomplex;		     /**< Default value: -0.015729056    */
float32_t* pAmpFactorW_foccomplex;		     /**< Default value: -0.016353225     */
float32_t* pOffsetFactorU_foccomplex;		 /**< Default value: 31.83384    */
float32_t* pOffsetFactorV_foccomplex;		 /**< Default value: 30.1283084    */
float32_t* pOffsetFactorW_foccomplex;		 /**< Default value: 31.32387619    */
float32_t* pAmpFactorSin_foccomplex;		 /**< Default value: 5.7096E-4*1*0.9468599 */
float32_t* pAmpFactorCos_foccomplex;		 /**< Default value: 5.898E-4*0.96286732 */
float32_t* pOffsetFactorSin_foccomplex;		 /**< Default value: 2290+122.6005-92.49 */
float32_t* pOffsetFactorCos_foccomplex;		 /**< Default value: 2285+150.1605-52.83    */
float32_t* pSrvAngVoltMax_foccomplex;        /**< Default value: 1.36 */
float32_t* pSrvAngVoltMin_foccomplex;        /**< Default value: 0.61 */

/************* Private static variables ***************************************************/
static float32_t parAmpFactorU      = 0;
static float32_t parAmpFactorV      = 0;
static float32_t parAmpFactorW      = 0;
static float32_t parAmpFactorSin    = 0;
static float32_t parAmpFactorCos    = 0;
static float32_t parOffsetFactorU   = 0;
static float32_t parOffsetFactorV   = 0;
static float32_t parOffsetFactorW   = 0;
static float32_t parOffsetFactorSin = 0;
static float32_t parOffsetFactorCos = 0;
static float32_t srvAngVoltMin = 0.0;
static float32_t srvAngVoltMax = 0.0;

static uint16_t convertedValues[BUFFERSIZE];         /**< Buffer for the ADC converted values */
static uint32_t period = 0;

/************* Private function prototypes ************************************************/
/**
 * @brief Initializes the four PWM channels. During the initialization the duty-cycle of
 * the each channel are set to 1%.
 * The PWM-Timer is the master for the ADC (Start of Conversion).
 *
 * @param [in,out] void
 *
 * @return PWMCONFIG_INIT_FAILED in case of error or 0 in case of a successful initialization
 */
int8_t pwmconfig_init(void);

/**
 * @brief Initializes the ADC Devices. The Channels are configured as regular and the both ADC
 * operate in dual mode. Furthermore the DMA is configured to get the converted ADC values.
 * The ADC regular channels start the conversion by the rising edge of the trigger signal
 * Timer 1 Capture Compare on Channel 1 (T1_CC1).
 * At the end of the conversion, a interrupt is generated and ADC interrupt handler will
 * processes the FOC application.
 *
 * @param [in,out] void
 *
 * @return ADCCONFIG_INIT_FAILED in case of error or 0 in case of a successful initialization
 */
int8_t adcconfig_init(void);

/**
 * @brief Set the duty cycle of the PWM channel.
 *
 * @param [in] pwmChannel - The PWM channel on which the duty cycle is set
 * @param [in] dutyCycle  - The duty cycle to be set
  *
 * The duty cycle can be set in a range from 0.000 to 1.0
 *
 * @return PWM_SETDUTYCYCLE_FAILED in case of error or 0 if the duty cycle has been
 * successfully set
 */
static int8_t foccomplex_pwmSetDutyCycle(foccomplex_pwmCh_t pwmChannel, float32_t dutyCycle);

/**
 * @brief Convert the ADC value in the current value using the factor provided by
 * current sensor. The converted values will be saved in the signal pool.
 *
 * @param [in] void
 *
 * @return void
 *
 */
void scale_current(void);

/**
 * @brief Convert the ADC value in the angle value using the factor provided by
 * angle sensor. The converted values will be saved in the signal pool.
 *
 * @param [in] void
 *
 * @return void
 *
 */
void scale_angle(void);

/**
 * @brief update local copies of the parameters from parameter system
 *
 * @param [in] void
 *
 * @return void
 *
 */
void servo_potiVoltage(void);

static void updateParameters(void);


/************* Public functions ***********************************************************/
int8_t foccomplex_init(void)
{
	int8_t errorControl = 0;

	/* update local copies of the parameters from parameter system */
	updateParameters();

	/* Set the init values of the duty cycle to allow the first trigger of ADC_IRQHandler*/
	*pMotPhaseUDutyCycle_foccomplex = 50;
	*pMotPhaseVDutyCycle_foccomplex = 50;
	*pMotPhaseWDutyCycle_foccomplex = 50;

	/* Set the init values of the adc input pointers */
	*pCurrentPhaseU_foccomplex = 0.0;
	*pCurrentPhaseV_foccomplex = 0.0;
	*pCurrentPhaseW_foccomplex = 0.0;

	*pRotorSin_foccomplex = 	0.0;
	*pRotorCos_foccomplex = 	0.0;
	*pBatVoltage_foccomplex = 	0.0;

	/* Initialize the pwm and the adc for the foc complex*/
	pwmconfig_init();
	adcconfig_init();

	/* Error control */
	errorControl = 1;
	if (!errorControl) {
		return FOCCPLX_INIT_FAILED;
	}
	else
	return 0;
}

int8_t foccomplex_enablePwm(void)
{
	int8_t errorControl = 0;
	GPIO_SetBits(GPIOC, GPIO_Pin_13);
	/* Enable the PWM Outputs*/
	TIM_CtrlPWMOutputs(FOCCPX_PWM_TIMER, ENABLE);

	/* Error control */
	errorControl = 1;
	if (!errorControl) {
		return FOCCPLX_ENABLE_PWM_FAILED;
	}
	else
	return 0;

}

int8_t foccomplex_disablePwm(void)
{
	int8_t errorControl = 0;

	/* Enable the PWM Outputs*/
	TIM_CtrlPWMOutputs(FOCCPX_PWM_TIMER, DISABLE);

	/* Error control */
	errorControl = 1;
	if (!errorControl) {
		return FOCCPLX_DISABLE_PWM_FAILED;
	}
	else
	return 0;

}

/* TIM1 Update interrupt handler to start ADC conversion in SW */
//void TIM1_CC_IRQHandler(void)
//{
//	TIM_ClearITPendingBit(FOCCPX_PWM_TIMER, TIM_IT_CC1);
//
//	ADC_SoftwareStartConv(FOCCPX_ADC_DEV1);
//}

void TIM1_UP_TIM10_IRQHandler(void)
{
	TIM_ClearITPendingBit(FOCCPX_PWM_TIMER, TIM_IT_Update);

	ADC_SoftwareStartConv(FOCCPX_ADC_DEV1);
}

/* ADC interrupt handler to process the FOC application */
void ADC_IRQHandler(void)
{
	ADC_ClearITPendingBit(FOCCPX_ADC_DEV1, ADC_IT_EOC);

	//outOfRangeCheckDiagCurU(convertedValues[6]);	/* out-of-range check for diagnosis current U */
//	outOfRangeCheckDiagCurV(convertedValues[8]);	/* out-of-range check for diagnosis current V */
//	outOfRangeCheckDiagCurW(convertedValues[9]);	/* out-of-range check for diagnosis current W */

	outOfRangeCheckPhaseCurU(convertedValues[0]);	/* out-of-range check for phase U */
	outOfRangeCheckPhaseCurV(convertedValues[4]);	/* out-of-range check for phase V*/
	outOfRangeCheckPhaseCurW(convertedValues[2]);	/* out-of-range check for phase W */

	/* check angle values of OOR faults */
//	outOfRangeCheckSin(convertedValues[3]);			/* out-of-range check for sin */
//	outOfRangeCheckCos(convertedValues[7]);			/* out-of-range check for cos */

	/* Call the scale function */
	scale_angle();
	scale_current();
	servo_potiVoltage();

	/* current sum plausibility */
	currentSumPlaus(*pCurrentPhaseU_foccomplex, *pCurrentPhaseV_foccomplex, *pCurrentPhaseW_foccomplex);

	/* sin-cos plausibility */
	sinCosPlaus(*pRotorSin_foccomplex, *pRotorCos_foccomplex);

	/* Call the foc_step() */
	foc_step();

	foccomplex_pwmSetDutyCycle(FOCCOMPLEX_PWM_CH1, *pMotPhaseUDutyCycle_foccomplex);
	foccomplex_pwmSetDutyCycle(FOCCOMPLEX_PWM_CH2, *pMotPhaseVDutyCycle_foccomplex);
	foccomplex_pwmSetDutyCycle(FOCCOMPLEX_PWM_CH3, *pMotPhaseWDutyCycle_foccomplex);

}

/************* Private functions ***********************************************************/
int8_t pwmconfig_init(void)
{
	RCC_ClocksTypeDef rccClock;
	RCC_GetClocksFreq(&rccClock);

	int8_t errorControl = 0;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseinitStructure;
	GPIO_InitTypeDef GPIO_initStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;

	/* Activate the clock for GPIOE */
	RCC_AHB1PeriphClockCmd(FOCCPX_PWM_PIN_CLOCK, ENABLE);

	/* Activate the clock for PWM_TIMER */
	RCC_APB2PeriphClockCmd(FOCCPX_PWM_TIM_CLOCK, ENABLE);

	/* Initialize the four GPIO pins for timer 3 */
	GPIO_initStructure.GPIO_Pin     = FOCCPX_PWM_CHAN_1_PIN | FOCCPX_PWM_CHAN_2_PIN |FOCCPX_PWM_CHAN_3_PIN;
	GPIO_initStructure.GPIO_Mode 	= GPIO_Mode_AF;
	GPIO_initStructure.GPIO_OType 	= GPIO_OType_PP;
	GPIO_initStructure.GPIO_PuPd 	= GPIO_PuPd_UP;
	GPIO_initStructure.GPIO_Speed 	= GPIO_Speed_100MHz;
	GPIO_Init(FOCCPX_PWM_PORT, &GPIO_initStructure);


	/* Configure the the GPIO pin as alternate function */
	GPIO_PinAFConfig(FOCCPX_PWM_PORT, FOCCPX_PWM_CHAN_1_AF_PIN, FOCCPX_PWM_CHAN_1_AF);
	GPIO_PinAFConfig(FOCCPX_PWM_PORT, FOCCPX_PWM_CHAN_2_AF_PIN, FOCCPX_PWM_CHAN_2_AF);
	GPIO_PinAFConfig(FOCCPX_PWM_PORT, FOCCPX_PWM_CHAN_3_AF_PIN, FOCCPX_PWM_CHAN_3_AF);

	/* Time Base configuration */
	uint8_t prescaler = 2;
	period = (rccClock.PCLK2_Frequency/prescaler)/10000;
	TIM_TimeBaseinitStructure.TIM_Prescaler 	     = prescaler - 1;            		/**< The frequency of the PWM is 84MHz / 4200 * 2 = 10kHz */
	TIM_TimeBaseinitStructure.TIM_Period 		     = period - 1;               		/**< The resolution of the PWM is 4200 */
	TIM_TimeBaseinitStructure.TIM_ClockDivision      = TIM_CKD_DIV1;
	TIM_TimeBaseinitStructure.TIM_RepetitionCounter  = 1;								/**< Generate the interrupt event on counter overflow */
	TIM_TimeBaseinitStructure.TIM_CounterMode        = TIM_CounterMode_CenterAligned1;   /**< The count mode is the center aligned up counting */
	TIM_TimeBaseInit(FOCCPX_PWM_TIMER, &TIM_TimeBaseinitStructure);

	/* PWM Output configuration */
	TIM_OCInitStructure.TIM_OCMode 		  = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState   = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity 	  = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_Pulse         = 0.5 * period; 								/**< default duty cycle is 50% */
	TIM_OCInitStructure.TIM_OutputNState  = TIM_OutputNState_Disable;
	TIM_OCInitStructure.TIM_OCNPolarity   = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OCIdleState   = TIM_OCIdleState_Reset;
	TIM_OCInitStructure.TIM_OCNIdleState  = TIM_OCIdleState_Reset;
	TIM_OC1Init(FOCCPX_PWM_TIMER, &TIM_OCInitStructure);
	TIM_OC2Init(FOCCPX_PWM_TIMER, &TIM_OCInitStructure);
	TIM_OC3Init(FOCCPX_PWM_TIMER, &TIM_OCInitStructure);

	/* Enable the Output Compare Pre-load */
	TIM_OC1PreloadConfig(FOCCPX_PWM_TIMER,TIM_OCPreload_Enable);
	TIM_OC2PreloadConfig(FOCCPX_PWM_TIMER,TIM_OCPreload_Enable);
	TIM_OC3PreloadConfig(FOCCPX_PWM_TIMER,TIM_OCPreload_Enable);

	/* timer interrupt */
	NVIC_InitTypeDef NVIC_InitStructure;
	NVIC_InitStructure.NVIC_IRQChannel = TIM1_UP_TIM10_IRQn;    /* timer 1 global interrupt */
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_Init(&NVIC_InitStructure);

	TIM_ITConfig(FOCCPX_PWM_TIMER, TIM_IT_Update, ENABLE);       /* enable Timer 1 interrupt source (update) */

	/* PWM Timer enable */
	TIM_Cmd(FOCCPX_PWM_TIMER,ENABLE);

	/* Error control */
	errorControl = 1;
	if (!errorControl) {
		return PWMCONFIG_INIT_FAILED;
	}
	else
	return 0;
}

int8_t adcconfig_init(void)
{

	int8_t errorControl = 0;
	GPIO_InitTypeDef GPIO_InitStructure;
	DMA_InitTypeDef DMA_InitStructure;
	NVIC_InitTypeDef NVIC_InitStructure;
	ADC_CommonInitTypeDef ADC_CommonInitStructure;
	ADC_InitTypeDef ADC_InitStructure;

	/* Activate the GPIO pins and ADC Clocks */
	RCC_AHB1PeriphClockCmd(FOCCPX_ADC_PIN_CLOCK1, ENABLE);
	RCC_AHB1PeriphClockCmd(FOCCPX_ADC_PIN_CLOCK3, ENABLE);
	RCC_AHB1PeriphClockCmd(FOCCPX_DMA_CLOCK, ENABLE);
	RCC_APB2PeriphClockCmd(FOCCPX_ADC_ADC1_CLOCK, ENABLE);
	RCC_APB2PeriphClockCmd(FOCCPX_ADC_ADC2_CLOCK, ENABLE);

	/* Configure the GPIO pins as analogue for the ADC */
	GPIO_InitStructure.GPIO_Pin = FOCCPX_ADC_CH1_PIN | FOCCPX_ADC_CH2_PIN | FOCCPX_ADC_CH3_PIN | FOCCPX_ADC_CH8_PIN | FOCCPX_ADC_CH9_PIN|GPIO_Pin_6;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(FOCCPX_ADC_PORT1, &GPIO_InitStructure);
	/* Configure the GPIO pins as analogue for the ADC */
	GPIO_InitStructure.GPIO_Pin = FOCCPX_ADC_CH6_PIN | FOCCPX_ADC_CH7_PIN | FOCCPX_ADC_CH4_PIN | FOCCPX_ADC_CH5_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(FOCCPX_ADC_PORT3, &GPIO_InitStructure);

	/* Configure the GPIO pins as analogue for the ADC */
	GPIO_InitStructure.GPIO_Pin = FOCCPX_ADC_CH10_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(FOCCPX_ADC_PORT1, &GPIO_InitStructure);

	/* Configure the DMA for the ADC*/
	DMA_InitStructure.DMA_Channel = FOCCPX_ADC_DMA_CHAN;
	DMA_InitStructure.DMA_Memory0BaseAddr = (uint32_t)convertedValues;
	DMA_InitStructure.DMA_PeripheralBaseAddr = (uint32_t)ADC_CDR_ADDRESS;
	DMA_InitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMA_InitStructure.DMA_BufferSize = BUFFERSIZE;
	DMA_InitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMA_InitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMA_InitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMA_InitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMA_InitStructure.DMA_Mode = DMA_Mode_Circular;
	DMA_InitStructure.DMA_Priority = DMA_Priority_High;
	DMA_InitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMA_InitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMA_InitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(FOCCPX_ADC_DMA_STREAM, &DMA_InitStructure);

	/* Configure the ADC common features */
	ADC_CommonInitStructure.ADC_Mode = ADC_DualMode_RegSimult;
	ADC_CommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div8;
	ADC_CommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_1;
	ADC_CommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADC_CommonInitStructure);

	/*ADC Device 1 configuration */
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_ExternalTrigConv = ADC_ExternalTrigConv_T1_CC1;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 4;
	ADC_Init(FOCCPX_ADC_DEV1, &ADC_InitStructure);

	/*ADC Device 2 configuration */
	ADC_InitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADC_InitStructure.ADC_ScanConvMode = ENABLE;
	ADC_InitStructure.ADC_ContinuousConvMode = DISABLE;
	ADC_InitStructure.ADC_ExternalTrigConvEdge = ADC_ExternalTrigConvEdge_None;
	ADC_InitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADC_InitStructure.ADC_NbrOfConversion = 4;
	ADC_Init(FOCCPX_ADC_DEV2, &ADC_InitStructure);

	/* Configure the ADC channels  as regular */
	ADC_RegularChannelConfig(FOCCPX_ADC_DEV1, FOCCPX_ADC_CH1_CHAN, 1, ADC_SampleTime_3Cycles );	/**< Iu  - PA1 - convertedValues[0] */
	ADC_RegularChannelConfig(FOCCPX_ADC_DEV1, FOCCPX_ADC_CH2_CHAN, 2, ADC_SampleTime_3Cycles );	/**< Iw  - PA2 - convertedValues[2] */
	ADC_RegularChannelConfig(FOCCPX_ADC_DEV1, FOCCPX_ADC_CH3_CHAN, 3, ADC_SampleTime_3Cycles );	/**< Iv  - PA3 - convertedValues[4] */
	ADC_RegularChannelConfig(FOCCPX_ADC_DEV1, FOCCPX_ADC_CH8_CHAN, 4, ADC_SampleTime_3Cycles );	/**< ISu - PA5 - convertedValues[6] */
	ADC_RegularChannelConfig(FOCCPX_ADC_DEV1, FOCCPX_ADC_CH9_CHAN, 5, ADC_SampleTime_3Cycles );	/**< ISv - PA4 - convertedValues[8] */
	ADC_RegularChannelConfig(ADC1, ADC_Channel_6, 6, ADC_SampleTime_3Cycles ); /**< ISw         - PA6 - convertedValues[10] */

	ADC_RegularChannelConfig(FOCCPX_ADC_DEV2, FOCCPX_ADC_CH6_CHAN, 1, ADC_SampleTime_3Cycles );	 /**< Bat Voltage - PC1 - convertedValues[1] */
	ADC_RegularChannelConfig(FOCCPX_ADC_DEV2, FOCCPX_ADC_CH7_CHAN, 2, ADC_SampleTime_3Cycles );	 /**< Sin		  - PC2 - convertedValues[3] */
	ADC_RegularChannelConfig(FOCCPX_ADC_DEV2, FOCCPX_ADC_CH4_CHAN, 3, ADC_SampleTime_3Cycles );	 /**< Temperature - PC4 - convertedValues[5] */
	ADC_RegularChannelConfig(FOCCPX_ADC_DEV2, FOCCPX_ADC_CH5_CHAN, 4, ADC_SampleTime_3Cycles );	 /**< Cos         - PC5 - convertedValues[7] */
	ADC_RegularChannelConfig(FOCCPX_ADC_DEV2, FOCCPX_ADC_CH10_CHAN, 5, ADC_SampleTime_3Cycles ); /**< ISw         - PA7 - convertedValues[9] */

	/* Enable DMA request after last transfer (Multi-ADC mode)  */
	ADC_MultiModeDMARequestAfterLastTransferCmd(ENABLE);

	/* ADC Interrupt Configuration*/
	NVIC_InitStructure.NVIC_IRQChannel = ADC_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x02;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	/* Enable the EOC interrupt to operate the FOC application */
	ADC_ITConfig(FOCCPX_ADC_DEV1, ADC_IT_EOC, ENABLE);
	ADC_ITConfig(FOCCPX_ADC_DEV2, ADC_IT_EOC, DISABLE);

	/* Enable ADC1 & ADC2 */
	ADC_Cmd(FOCCPX_ADC_DEV1, ENABLE);
	ADC_Cmd(FOCCPX_ADC_DEV2, ENABLE);

	/* DMA2_Stream0 enable */
	DMA_Cmd(FOCCPX_ADC_DMA_STREAM, ENABLE);

	/* Enable ADC1 & ADC2 DMA */
	ADC_DMACmd(FOCCPX_ADC_DEV1, ENABLE);

	/* Error control */
	errorControl = 1;
	if (!errorControl) {
		return ADCCONFIG_INIT_FAILED;
	}
	else return 0;

	return 0;
}

static int8_t foccomplex_pwmSetDutyCycle(foccomplex_pwmCh_t pwmChannel, float32_t dutyCycle)
{
	/* first scale the duty cycle delivered by the foc application
	 * by dividing the it over 100.0, which correspond to 100% */
	dutyCycle = dutyCycle / DUTYCYCLE_SCALE;

	switch (pwmChannel) {
		case FOCCOMPLEX_PWM_CH1:
			TIM_SetCompare1(FOCCPX_PWM_TIMER, (uint32_t)(dutyCycle*period));
		break;
		case FOCCOMPLEX_PWM_CH2:
			TIM_SetCompare2(FOCCPX_PWM_TIMER, (uint32_t)(dutyCycle*period));
			break;
		case FOCCOMPLEX_PWM_CH3:
			TIM_SetCompare3(FOCCPX_PWM_TIMER, (uint32_t)(dutyCycle*period));
			break;
		default:
		return PWM_SETDUTYCYCLE_FAILED;
	}
	return 0;
}

void scale_current(void)
{
	/* Convert the ADC values in current and save them in signal pool
	 * This sensor function must be used  current = value * amp + offset
	 */

	/* update local copies of the parameters from parameter system */
	updateParameters();
	float32_t currU   =  parAmpFactorU * (float32_t)convertedValues[0] + parOffsetFactorU;
	float32_t currV   =  parAmpFactorV * (float32_t)convertedValues[4] + parOffsetFactorV;
	float32_t currW   =  parAmpFactorW * (float32_t)convertedValues[2] + parOffsetFactorW;

	*pCurrentPhaseU_foccomplex  = currU;
	*pCurrentPhaseV_foccomplex  = currV;
	*pCurrentPhaseW_foccomplex  = currW;
}

void scale_angle(void)
{
	/* Convert the ADC values in angle and save them in signal pool
	 * This sensor function must be used  current = value - offset
	 */

	/* update local copies of the parameters from parameter system */
	updateParameters();

    *pRotorSin_foccomplex = ((float32_t)convertedValues[3] - parOffsetFactorSin) * parAmpFactorSin;
    *pRotorCos_foccomplex = ((float32_t)convertedValues[7] - parOffsetFactorCos) * parAmpFactorCos;

}

void servo_potiVoltage(void)
{
	/* Convert the ADC values in voltage and save them in signal pool */

	/* update local copies of the parameters from parameter system */
	updateParameters();

	uint32_t adcValueTemp = convertedValues[1];
	float32_t stAngPoti = ((float32_t)adcValueTemp * 3.0) / 4096.0;

	/* limit the servo poti voltage */
	if (stAngPoti > srvAngVoltMax)
	{
		stAngPoti = srvAngVoltMax;
	}

	if (stAngPoti < srvAngVoltMin)
	{
		stAngPoti = srvAngVoltMin;
	}

	/* save the poti voltage in signal pool */
	*pSrvAngVolt_foccomplex = stAngPoti;

}

/************* Private function ************************************************/
static void updateParameters(void)
{
	 parAmpFactorU      = *pAmpFactorU_foccomplex;
	 parAmpFactorV      = *pAmpFactorV_foccomplex;
	 parAmpFactorW      = *pAmpFactorW_foccomplex;
	 parAmpFactorSin    = *pAmpFactorSin_foccomplex;
	 parAmpFactorCos    = *pAmpFactorCos_foccomplex;
	 parOffsetFactorU   = *pOffsetFactorU_foccomplex;
	 parOffsetFactorV   = *pOffsetFactorV_foccomplex;
	 parOffsetFactorW   = *pOffsetFactorW_foccomplex;
	 parOffsetFactorSin = *pOffsetFactorSin_foccomplex;
	 parOffsetFactorCos = *pOffsetFactorCos_foccomplex;
	 srvAngVoltMax      = *pSrvAngVoltMax_foccomplex;
	 srvAngVoltMin      = *pSrvAngVoltMin_foccomplex;

	return;
}
