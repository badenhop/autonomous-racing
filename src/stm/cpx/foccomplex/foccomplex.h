/** *****************************************************************************************
 * Unit in charge:
 * @file	adc.h
 * @author	Robert Ledwig
 * $Date:: 2015-06-01 17:17:57 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1538                  $
 * $Author:: Yoga               $
 *
 * ----------------------------------------------------------
 *
 * @brief The module is an interface for the ADC and PWM.
 * It provides functions to initialize the ADC and PWM periphery,
 * to configure the ADC and PWM channels, to convert the analog values and to set
 * the duty cycle.
 *
 * The ADC part of the module consists in an initialization of the ADC periphery,
 * which is done by adcconfig_init(). This function contains
 * the initialization of the GPIO pins, the initialization of the interrupt controller
 * and the configuration of the DMA to store the converted
 * values. It also configure the ADC channels as regulars channels with the conversion
 * triggered external by the PWM timer 1 Capture Compare Event on Channel 1.
 * This first part also include the ADC_IRQHandler() released by the ADC
 * interrupt(ADC end of conversion, EOC) in which the FOC application will be processed.
 *
 * The ADC samples continuously. Therefore when the ADC has sampled one value, the next
 * sampling starts. The conversion occurs in scan mode to get the converted values of each
 * regular group(four value or channels per group) by the rising edge of the trigger signal.
 * The DMA is used to allow the CPU not be stressed or interrupted by getting the converted
 * values. Each converted value is automatically stored
 * in the DMA buffer and transferred or connected to the signal pool by using the sig  
 *
 * The PWM part of the module consists in the initialization of the PWM periphery,
 * which is done by pwmconfig_init(). This function contains the initialization of the GPIO
 * pins and the initialization of the PWM timer. It also configure the PWM with the desired
 * timer frequency and the timer output compare mode for the duty cycle.
 * The function pwm_setDutyCycle() is used to set manually the required duty cycle
 * at each channel. The timer 1 counter using by the PWM is the center aligned mode2.
 * That means counter count up center aligned.
 *
 ******************************************************************************************/

#ifndef FOCCOMPLEX_H_
#define FOCCOMPLEX_H_

/************* Includes *******************************************************************/
#include "app/config.h"
#include "per/hwallocation.h"
#include "sys/sigp/sigp.h"
/*#include "curmon.h"
#include "angmon.h"
#include "pemon.h"*/

/************* Public typedefs ************************************************************/
#define FOCCPLX_INIT_FAILED          1     		 /**< Initialization of the PWM failed.   */
#define FOCCPLX_ENABLE_PWM_FAILED    1     		 /**< Initialization of the PWM failed.   */
#define FOCCPLX_DISABLE_PWM_FAILED   1     		 /**< Initialization of the PWM failed.   */
/************* Public function prototypes *************************************************/
/**
 * @brief Initializes the complex driver foccomplex using the functions adcconfig_init() and
 * pwmconfig_init().
 *
 * @param [in,out] void
 *
 * @return FOCCPLX_INIT_FAILED in case of error or 0 in case of a successful initialization
 */
int8_t foccomplex_init(void);

/**
 * @brief Enable the pwm outputs of the foccomplex
 *
 * @param [in,out] void
 *
 * @return FOCCPLX_ENABLE_PWM_FAILED in case of error or 0 in case of a successful enable
 */
int8_t foccomplex_enablePwm(void);

/**
 * @brief Disable the pwm outputs of the foccomplex
 *
 * @param [in,out] void
 *
 * @return FOCCPLX_ENABLE_PWM_FAILED in case of error or 0 in case of a successful enable
 */
int8_t foccomplex_disablePwm(void);

#endif /* ADCCOMPLEX_H_ */

