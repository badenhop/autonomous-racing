/** *****************************************************************************************
 * Unit in charge:
 * @file	servo.c
 * @author	killer
 * $Date:: 2017-12-05 12:29:39 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2213                  $
 * $Author:: mbrieske           $
 * 
 * ----------------------------------------------------------
 *
 * @brief This module is for controlling of two servo devices.
 *        It supports the initialization, the activation and
 *        the deactivation of the servo channels and it can set
 *        a desired position angle of a servo from the signal pool.
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/

#include "dev/servo/servo.h"
#include "dev/rcrec/rcrec.h"
#include "sys/util/io.h"
#include "app/leddebug.h"

/************* Private typedefs ***********************************************************/

/************* Macros and constants ******************************************************/
#define SERVO_FREQUENCY_HZ 71.7		 /**< servo frequency 50Hz */
#define SERVO_PERIOD_MS (1000.0/SERVO_FREQUENCY_HZ)	/**< time of one servor period in ms depending on the servo freqzency  */
#define SERVO_PWMDUTY_SIGMAXLENGTH (1.0/SERVO_PERIOD_MS)  /**< maximum length of the signal of the PWM duty cucly in ms (e.g. for RC-servo 1ms) in relation to the period length of the entire PWM period time in ms */
#define SERVO_PWMDUTY_OFFSET (1.0/SERVO_PERIOD_MS) 		 /**< Servo signal offset in percent (e.g. for RC-servo 5 percent is 1ms of signal period 20ms) */


/************* Global variables **********************************************************/
/* parameter pointers */
float32_t* pMinAbsAngSrv1_servo; /**< Minimum value in degrees for servo channel 1 (Default value 0)*/
float32_t* pMaxAbsAngSrv1_servo; /**< Maximum value in degrees for servo channel 1 (default value 180) */
float32_t* pInitAngSrv1_servo;	/**< Initial value in degrees for servo channel 1 (default value 90) */

float32_t* pMinAbsAngSrv2_servo; /**< Minimum value in degrees for servo channel 2 (Default value 0)*/
float32_t* pMaxAbsAngSrv2_servo; /**< Maximum value in degrees for servo channel 2 (default value 180) */
float32_t* pInitAngSrv2_servo;   /**< Initial value in degrees for servo channel 2 (default value 90) */
float32_t* pSrvAngVoltMax_servo;
float32_t* pSrvAngVoltMin_servo;

/* input- output signals */
float32_t* pStAngTgt_servo;		/**< Signal for the current deflection angle for servo channel 1 */
float32_t* pMotTrqTgtSrv_servo;	/**< Signal for the current deflection angle for servo channel 2 */
float32_t* pSrvAngAct_servo;    /**< signal for the application stang */
float32_t* pSrvAngVolt_servo;   /**< signal from foccomplex */
bool_t*	pRcOverrideStangEnabled_servo;
bool_t*	pRcOverrideSpdEnabled_servo;
uint16_t* pRcChannel1_servo;
uint16_t* pRcChannel4_servo;


/************* Private static variables ***************************************************/
static  pwm_t servoPwm;
static float32_t minAbsAngSrv1 = 0.0;
static float32_t maxAbsAngSrv1 = 0.0;
static float32_t minAbsAngSrv2 = 0.0;
static float32_t maxAbsAngSrv2 = 0.0;
static float32_t srvAngVoltMin = 0.0;
static float32_t srvAngVoltMax = 0.0;

/************* Private function prototypes ************************************************/
static float32_t dutyCycleCalc(float32_t absAngSrv, float32_t minAng, float32_t maxAng);
static void updateParameters(void);

/************* Public functions ***********************************************************/

int8_t servo_init(void)
{
	/* variable for return value */
	int8_t servo_int_state = 0;

	/* set all private variables to init values */
	minAbsAngSrv1 = 0.0;
	maxAbsAngSrv1 = 0.0;
	minAbsAngSrv2 = 0.0;
	maxAbsAngSrv2 = 0.0;

	updateParameters();

	/* initialization of the PWM Ports */
	servoPwm.flexOfFrequency = FALSE; /* pwm.resolution = 10000; */
	servoPwm.frequency = SERVO_FREQUENCY_HZ; /* 50 Hz */
	servoPwm.mode = centeraligned; /* Center aligned mode */
	pwm_init(&servoPwm);

	pwm_disable(PWM_CH1);

	float32_t initAng1 = *pInitAngSrv1_servo;
	float32_t initAng2 = *pInitAngSrv1_servo;

	/* initialization of Servo1 with Out-of-Range Check for init value */
	if ((initAng1 < maxAbsAngSrv1) && (initAng1 > minAbsAngSrv1))
	{
		//Calculation of initial duty cycle for servo1 control
		float32_t initDutyCycleSrv1 = dutyCycleCalc(initAng1, minAbsAngSrv1, maxAbsAngSrv1);

		/* setting of initial duty cycle and activating of servo1 control PWM port */
		pwm_setDutyCycle(&servoPwm, PWM_CH1, initDutyCycleSrv1);
	}
	else servo_int_state = servo_int_state + 3;

	pwm_disable(PWM_CH2);

	/* initialization of Servo2 with Out-of-Range Check for init value */
	if ((initAng2 < maxAbsAngSrv2) && (initAng2 > minAbsAngSrv2))
	{
		/* calculation of initial duty cycle for servo2 control */
		float32_t initDutyCycleSrv2 = dutyCycleCalc(initAng2, minAbsAngSrv2, maxAbsAngSrv2);

		/* setting of initial duty cycle and activating of servo2 control PWM port */
		pwm_setDutyCycle(&servoPwm, PWM_CH2, initDutyCycleSrv2);
	}
	else servo_int_state = servo_int_state + 4;

	return servo_int_state;
}

int8_t servo_enablePwmSrv1(void)
{
	/* enable PWM Channel with Servo1 */
	pwm_enable(&servoPwm, PWM_CH1);
	return 0;
}

int8_t servo_disablePwmSrv1(void)
{
	/* disable PWM Channel with Servo1 */
	pwm_disable(PWM_CH1);
	return 0;
}

int8_t servo_updateAngleSrv1(void)
{
	/* variable for return value */
	int8_t servo_updateAngleSrv1_state = 0;

	/* RC Override Check */
	if (*pRcOverrideStangEnabled_servo) {
		irq_disable();
		float32_t rcValue = (float) *pRcChannel4_servo;
		irq_enable();
		float32_t dutyCycleSrv1 = dutyCycleCalc(rcValue, (float) MIN_PERIOD_CHANNEL, (float) MAX_PERIOD_CHANNEL);
		pwm_setDutyCycle(&servoPwm, PWM_CH1, dutyCycleSrv1);
	}

	else {
		irq_disable();
		float32_t stAngTgt = *pStAngTgt_servo;
		updateParameters();
		irq_enable();

		if ((stAngTgt-maxAbsAngSrv1 <= 1E-4) && (stAngTgt-minAbsAngSrv1 >= -1E-4))
		{
			/* Calculation of initial duty cycle for servo1 control */
			float32_t dutyCycleSrv1 = dutyCycleCalc(stAngTgt, minAbsAngSrv1, maxAbsAngSrv1);

			/* Setting of actual duty cycle of servo1 control to PWM port */
			pwm_setDutyCycle(&servoPwm,  PWM_CH1, dutyCycleSrv1);
		}
		else servo_updateAngleSrv1_state = 3;
	}

	return servo_updateAngleSrv1_state;
}

int8_t servo_enablePwmSrv2(void)
{
	/* Enable PWM Channel with Servo2 */
	pwm_enable(&servoPwm, PWM_CH2);
	return 0;
}

int8_t servo_disablePwmSrv2(void)
{
	/* Disable PWM Channel with Servo2 */
	pwm_disable(PWM_CH2);
	return 0;
}

void servo_updateMotorTrq(float u)
{
	float32_t dutyCycle = dutyCycleCalc(u, -1.0F, 1.0F);
	pwm_setDutyCycle(&servoPwm, PWM_CH2, dutyCycle);
}

int8_t servo_updateAngleSrv2(void)
{
	/* variable for return value */
	int8_t servo_updateAngleSrv2_state = 0;

	/* RC Override Check */
	if (*pRcOverrideSpdEnabled_servo) {
		irq_disable();
		float32_t rcValue = *pRcChannel1_servo * 1.0F;
		irq_enable();
		float32_t dutyCycleSrv2 = dutyCycleCalc(rcValue/3. + 1000., (float) MIN_PERIOD_CHANNEL, (float) MAX_PERIOD_CHANNEL);
		pwm_setDutyCycle(&servoPwm, PWM_CH2, dutyCycleSrv2);
	}

	else {
		irq_disable();
		float32_t trqTgt = *pMotTrqTgtSrv_servo;
		updateParameters();
		irq_enable();

		if ((trqTgt <= maxAbsAngSrv2) && (trqTgt >= minAbsAngSrv2))
		{
			/* Calculation of initial duty cycle for servo2 control*/
			float32_t dutyCycleSrv2 = dutyCycleCalc(trqTgt, minAbsAngSrv2, maxAbsAngSrv2);

			/* Setting of actual duty cycle of servo2 control to PWM port */
			pwm_setDutyCycle(&servoPwm,  PWM_CH2, dutyCycleSrv2);
		}
		else servo_updateAngleSrv2_state = 3;
	}

	return servo_updateAngleSrv2_state;
}

void servo_srvAngCalc(void)
{

	float32_t sigSvrAngVolt = 0.0;
	float32_t srvAngAct = 0.0;
	float32_t res = 0.0;

	/* variable for return value */
	irq_disable();
	sigSvrAngVolt = *pSrvAngVolt_servo;
	updateParameters();
	irq_enable();

	/* transform the actual servo poti voltage in actual servo angle */
	res = (sigSvrAngVolt - srvAngVoltMin) / (srvAngVoltMax - srvAngVoltMin);
	srvAngAct = res * (maxAbsAngSrv2 - minAbsAngSrv2) + minAbsAngSrv2;

	/* write a signal in signal pool */
	irq_disable();
	*pSrvAngAct_servo = srvAngAct;
	irq_enable();

}


/************* Private functions **********************************************************/

/**
 *
 *  @brief 
 *
 *  @param [in,out] *pAbsAngSrv - ?
 *
 *  @return servo_dutyCycle
 *
 */
float32_t dutyCycleCalc(float32_t absAngSrv, float32_t minAng, float32_t maxAng)
{
	/* calculates the dutycylce for the PWM */
	float32_t servo_dutyCycle = SERVO_PWMDUTY_SIGMAXLENGTH/(maxAng - minAng) * (absAngSrv - minAng) + SERVO_PWMDUTY_OFFSET;
	return servo_dutyCycle;
}

/**
 *
 *  @brief This function updates the private variables for storing the parameters
 *  with the current values from the parameter system.
 *
 *  @param
 *
 *  @return
 *
 */
static void updateParameters(void)
{
	minAbsAngSrv1 = *pMinAbsAngSrv1_servo;
	maxAbsAngSrv1 = *pMaxAbsAngSrv1_servo;

	minAbsAngSrv2 = *pMinAbsAngSrv2_servo;
	maxAbsAngSrv2 = *pMaxAbsAngSrv2_servo;
	srvAngVoltMax = *pSrvAngVoltMax_servo;
	srvAngVoltMin = *pSrvAngVoltMin_servo;
}
