/** *****************************************************************************************
 * Unit in charge:
 * @file	eeprom_25LC080A.h
 * @author	Adrian
 * $Date:: 2017-01-16 17:49:30 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2069                  $
 * $Author:: wilhelma           $
 *
 * ----------------------------------------------------------
 *
 * @brief Driver for SPI EEPROM CHIP 25LC080A
 * Datasheet: http://ww1.microchip.com/downloads/en/DeviceDoc/21808D.pdf
 *
 ******************************************************************************************/


#ifndef EEPROM_25LC080A_H_
#define EEPROM_25LC080A_H_

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
/************* Public typedefs ************************************************************/

/************* Macros and constants *******************************************************/

/************* Public function prototypes *************************************************/
int8_t eeprom_init();

uint8_t eeprom_writeByte(uint16_t address, uint8_t data);
uint8_t eeprom_writeBuffer(uint16_t address, uint8_t* dataBuffer, uint8_t bufferLength);
uint8_t eeprom_readByte(uint16_t address);
uint8_t eeprom_readBuffer(uint16_t address, uint8_t* dataBuffer, uint8_t bufferLength);

#endif /* EEPROM_25LC080A_H_ */



