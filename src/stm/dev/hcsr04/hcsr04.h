/********************************************************************************************
 * Unit in charge:
 * @file    hcsr04.h
 * @author  ledwig
 * $Date:: 2015-04-07 14:30:00 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1131                  $
 * $Author:: ledwig				$
 *
 * ----------------------------------------------------------
 *
 * @brief 	This is the device driver of the HC-SR04 ultrasonic devices. This device driver
 * 			uses the I2C bus driver for communication. This driver only works in combination
 * 			with the implemented VHDL I2C Slave design, because the HC-SR04 sensors are not
 * 			capable to use I2C bus themselves.
 *
 *
 ******************************************************************************************/

#ifndef HC_SR04_H_
#define HC_SR04_H_

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "brd/stm32f4xx_i2c.h"

#include "per/i2c2.h"	/* include the I2C2 bus driver interface */


/************* Public typedefs ************************************************************/
typedef enum
{
	START_STOP = 20, TRIGGER = 21
} command_t;

typedef enum
{ /* instruction for device address */
	DEVICE0 = 10,
	DEVICE1 = 11,
	DEVICE2 = 12,
	DEVICE3 = 13,
	DEVICE4 = 14,
	DEVICE5 = 15,
	DEVICE6 = 16,
	DEVICE7 = 17,
} device_address_t;

typedef enum
{ /* timestamp instruction of devices */
	TIMEST_DEV0 = 30,
	TIMEST_DEV1 = 31,
	TIMEST_DEV2 = 32,
	TIMEST_DEV3 = 33,
	TIMEST_DEV4 = 34,
	TIMEST_DEV5 = 35,
	TIMEST_DEV6 = 36,
	TIMEST_DEV7 = 37,
} timestamp_dev_address_t;

/************* Macros and constants *******************************************************/
/************* Public function prototypes *************************************************/

/**
 * @brief	initializes the I2C bus interface
 *
 * @return  none
 */
void hcsr04_init(void);

/**
 * @brief	performs a range check and filters the values; values are stored in signal pool
 *
 * @return  none
 */
void hcsr04_process(void);

#endif /* HC_SR04_H_ */

