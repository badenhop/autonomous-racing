/** *****************************************************************************************
 * Unit in charge:
 * @file	led.c
 * @author	
 * $Date:: 2015-04-29 12:00:56 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1493                  $
 * $Author:: hepner             $
 * 
 * ----------------------------------------------------------
 *
 * @brief TODO What does the unit?
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "dev/led/led.h" //welche LED
#include "per/gpio.h" //f�r Konfig der LEDs (channel, mode)

/************* Private typedefs ***********************************************************/


/************* Macros and constants *******************************************************/


/************* Private static variables ***************************************************/
static gpio_t* led_getGPIO(led_t);

static gpio_t led1; //structure has either channel or mode
static gpio_t led2;
static gpio_t led3;
//static gpio_t led4;

/************* Private function prototypes ************************************************/


/************* Public functions ***********************************************************/
int8_t led_init(led_t Led)
{

	gpio_t *pTemp = NULL;

	switch(Led) {
		case LED_1:

			//for configuration
			pTemp = &led1;  				//address of pointer
			pTemp->channel = GPIO_CHAN_12;  //pointer points on channel
			pTemp->state = GPIO_STATE_OFF;  //pointer points on state
		break;
		case LED_2:
			pTemp = &led2;
			pTemp->channel = GPIO_CHAN_13;
			pTemp->state = GPIO_STATE_OFF;
		break;
		case LED_3:
			pTemp = &led3;
			pTemp->channel = GPIO_CHAN_14;
			pTemp->state = GPIO_STATE_OFF;
		break;
//		case LED_4:
//			pTemp = &led4;
//			pTemp->channel = GPIO_CHAN_15;
//			pTemp->state = GPIO_STATE_OFF;
		break;
	}

	pTemp->mode = GPIO_MODE_OUT; //universally applicable

	return gpio_init(pTemp);
}

/**
  * @brief  Turns selected LED On.
  * @param  Led: Specifies the Led to be set on.
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4
  * @retval None
  */
int8_t led_on(led_t Led) {

	gpio_t *pTemp = led_getGPIO(Led);

	return gpio_setBit(pTemp);
}


/**
  * @brief  Turns selected LED Off.
  * @param  Led: Specifies the Led to be set off.
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4
  * @retval None
  */
int8_t led_off(led_t Led) {
	gpio_t *pTemp = led_getGPIO(Led);

	return gpio_clearBit(pTemp);
}


/**
  * @brief  Toggles the selected LED.
  * @param  Led: Specifies the Led to be toggled.
  *   This parameter can be one of following parameters:
  *     @arg LED1
  *     @arg LED2
  *     @arg LED3
  *     @arg LED4
  * @retval None
  */
int8_t led_toggle(led_t Led) {
	gpio_t *pTemp = led_getGPIO(Led);
	return gpio_toggleBit(pTemp);
}

gpio_t* led_getGPIO(led_t Led) {

	gpio_t* pTemp = 0;

	switch(Led) {
		case LED_1:
			pTemp = &led1;
			break;
		case LED_2:
			pTemp = &led2;
			break;
		case LED_3:
			pTemp = &led3;
			break;
//		case LED_4:
//			pTemp = &led4;
//			break;
		}
	return pTemp;
}
/************* Private functions **********************************************************/
