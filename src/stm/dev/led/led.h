/** *****************************************************************************************
 * Unit in charge:
 * @file	led.h
 * @author	
 * $Date:: 2014-07-24 17:32:59 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 839                   $
 * $Author:: wandji-kwuntchou   $
 * 
 * ----------------------------------------------------------
 *
 * @brief TODO What does the unit?
 *
 ******************************************************************************************/

#ifndef LED_H_
#define LED_H_

/************* Includes *******************************************************************/
#include <stdlib.h>
#include "brd/startup/stm32f4xx.h"

/************* Public typedefs ************************************************************/
typedef enum {
	LED_1 = 0,
	LED_2,
	LED_3,
//	LED_4
 } led_t;

 /************* Macros and constants *******************************************************/


/************* Public function prototypes *************************************************/
 int8_t led_init(led_t Led);
 int8_t led_on(led_t Led);
 int8_t led_off(led_t Led);
 int8_t led_toggle(led_t Led);

#endif /* LED_H_ */
