/** *****************************************************************************************
 * Unit in charge:
 * @file	srf02.c
 * @author	ledwig
 * $Date:: 2014-05-16 13:48:47 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 473                   $
 * $Author:: wandji-kwuntchou   $
 *
 * ----------------------------------------------------------
 *
 * @brief A supersonic device with i2c attachment
 *
 ******************************************************************************************/



/************* Includes *******************************************************************/
#include "dev/srf02/srf02.h"
#include "sys/util/stubs.h"
#include "per/i2c.h"

#ifdef SRF02_EN

/************* Private typedefs ***********************************************************/


/************* Macros and constants *******************************************************/


/************* Private static variables ***************************************************/


/************* Private function prototypes ************************************************/


/************* Public functions ***********************************************************/
uint8_t srf02_init(srf02_t* pDevice, i2c_t* pI2c)
{
	pDevice->channel = pI2c->channel;
	i2c_init(pI2c); /* init CPU Master mode */
	i2c_enable(pI2c->channel);

	return SRF02_OK;
}

uint8_t srf02_setAddress(srf02_t* pDevice, uint8_t newaddress)
{
	uint8_t pData[4];
	pData[0] = 0xA0;
	pData[1] = 0xAA;
	pData[2] = 0xA5;
	pData[3] = newaddress; /* new address */
	i2c_writeReg(pDevice->channel, pDevice->address, 0x00, &(pData[0]), 1); /* 0x00 = Instruction register = target register */
	delay(50); /* 50ms delay after each transfer */
	i2c_writeReg(pDevice->channel, pDevice->address, 0x00, &(pData[1]), 1);
	delay(50);
	i2c_writeReg(pDevice->channel, pDevice->address, 0x00, &(pData[2]), 1);
	delay(50);
	i2c_writeReg(pDevice->channel, pDevice->address, 0x00, &(pData[3]), 1);
	delay(50);
	pDevice->address = newaddress; /* set new address */
	return SRF02_OK;
}

uint8_t srf02_getRevision(srf02_t* pDevice, uint8_t* pData)
{
	i2c_readReg(pDevice->channel, pDevice->address, 0x00, pData, 1); /* read instruction register */
	return SRF02_OK;
}

uint8_t srf02_getRangeMinimum(srf02_t* pDevice, uint16_t* pData)
{
	uint8_t highbyte = 0;
	uint8_t lowbyte = 0;

	i2c_readReg(pDevice->channel, pDevice->address, 0x04, &highbyte, 1);
	i2c_readReg(pDevice->channel, pDevice->address, 0x05, &lowbyte, 1);

	*pData = (highbyte << 8) + lowbyte;
	return SRF02_OK;
}

uint8_t srf02_getMeasurement(srf02_t* pDevice, uint16_t* pData)
{
	uint8_t highbyte = 0;
	uint8_t lowbyte = 0;
	uint8_t instruct = 0x51;

	i2c_writeReg(pDevice->channel, pDevice->address, 0x00, &instruct, 1);
	delay(65);

	i2c_readReg(pDevice->channel, pDevice->address, 0x02, &highbyte, 1);
	i2c_readReg(pDevice->channel, pDevice->address, 0x03, &lowbyte, 1);

	*pData = (uint16_t)((highbyte << 8) + lowbyte);
	return SRF02_OK;
}

#endif /* SRF02_EN */

/************* Private functions **********************************************************/
