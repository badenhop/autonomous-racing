/** *****************************************************************************************
 * Unit in charge:
 * @file	mpu9150.h
 * @author	hertel
 * $Date:: 2015-08-19 08:40:16 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1644                  $
 * $Author:: hertel             $
 *
 * ----------------------------------------------------------
 *
 * @brief 	This module is for controlling the MPU9150 device. It supports the initialization,
 * 			activation and several measurement functions. You can choose different accuracies
 * 			for accelerometer and gyrometer. The device uses the I2C Bus and needs a driver
 * 			for the communication.
 * calls:
 * 			int8_t i2c_init(i2c_t* i2c);
 * 			int8_t i2c_enable(i2c_channel_t channel);
 * 			int8_t i2c_disable(i2c_channel_t channel);
 * 			int32_t i2c_readReg(i2c_channel_t channel, uint8_t address, uint8_t reg, uint8_t* pData, uint16_t numOfBytes);
 * 			int32_t i2c_writeReg(i2c_channel_t channel, uint8_t address, uint8_t reg, uint8_t* pData, uint16_t numOfBytes);
 *
 ******************************************************************************************/


#ifndef MPU9150_H_
#define MPU9150_H_

/************* Includes *******************************************************************/
#include "per/i2c.h"
#include "brd/types.h"
#include "per/hwallocation.h"
#include "sys/systime/systime.h"
#include "sys/util/stubs.h"
/************* Public typedefs ************************************************************/
/**
* Main structure of MPU9150
*
* Parameters:
* 	uint8_t address: Address of MPU9150
* 	uint8_t address_magn: Address of Compass AK8975
* 	i2c_channel_t channel: Used I2C channel
*	int16_t magnX: Magnetometer value x-axis
*	int16_t magnY: Magnetometer value y-axis
*	int16_t magnZ: Magnetometer value z-axis
*	int16_t magnAdjX: Magnetometer value x-axis ???
*	int16_t magnAdjY: Magnetometer value y-axis ???
*	int16_t magnAdjZ: Magnetometer value z-axis ???
*/
typedef struct{
	uint8_t address;
	uint8_t addressMagn;
	i2c_channel_t channel;
#ifdef COMPASS_USE
	uint32_t magnTimeStamp;
	int16_t magnX;
	int16_t magnY;
	int16_t magnZ;
	int16_t magnAdjX;
	int16_t magnAdjY;
	int16_t magnAdjZ;
#endif
} mpu9150_t;

/************* Macros and constants *******************************************************/
mpu9150_t mpu9150Dev; /* mpu9150 structure to configure the mpu9150 device */
/************* Public function prototypes *************************************************/
/**
 * @brief Initialize the MPU9150
 *
 * @return MPU9150_OK if everything is OK
 */
uint8_t mpu9150_init();

/**
 * @brief Read raw data from accelerometer
 *
 * @param[in,out] mpu9150Dev contains the MPU9150 structure type
 *
 * @return MPU9150_OK if everything is OK
 */
uint8_t mpu9150_readAccel();

/**
 * @brief Read raw data from gyrometer
 *
 * @param[in,out] mpu9150Dev contains the MPU9150 structure type
 *
 * @return MPU9150_OK if everything is OK
 */
uint8_t mpu9150_readGyro();

/**
 * @brief Read raw data from magnetometer
 *
 * @param[in,out] mpu9150Dev contains the MPU9150 structure type
 *
 * @return MPU9150_OK if everything is OK
 */
uint8_t mpu9150_readMagn();

/**
 * @brief Read temperature from MPU9150
 *
 * @param[in,out] mpu9150Dev contains the MPU9150 structure type
 *
 * @return MPU9150_OK if everything is OK
 */
uint8_t mpu9150_readTemp();

/**
 * @brief Read all data (accel, gyro) from MPU9150 in a burst
 *
 * @param[in,out] scaled measurements in float32_t
 *
 * @return MPU9150_OK if everything is OK
 */
uint8_t mpu9150_readFifo();

#endif /* MPU9150_H_ */
