/** *****************************************************************************************
 * Unit in charge:
 * @file	mpu9150.c
 * @author	hertel
 * $Date:: 2015-09-02 11:42:04 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1664                  $
 * $Author:: hertel             $
 *
 * ----------------------------------------------------------
 *
 * @brief 	This module is for controlling the MPU9150 device. It supports the initialization,
 * 			activation and several measurement functions. You can choose different accuracies
 * 			for accelerometer and gyrometer. The device uses the I2C Bus and needs a driver
 * 			for the communication.
 * calls:
 * 			int8_t i2c_init(i2c_t* i2c);
 * 			int8_t i2c_enable(i2c_channel_t channel);
 * 			int8_t i2c_disable(i2c_channel_t channel);
 * 			int32_t i2c_readReg(i2c_channel_t channel, uint8_t address, uint8_t reg, uint8_t* mpu9150Dev, uint16_t numOfBytes);
 * 			int32_t i2c_writeReg(i2c_channel_t channel, uint8_t address, uint8_t reg, uint8_t* mpu9150Dev, uint16_t numOfBytes);
 *
 ******************************************************************************************/



/************* Includes *******************************************************************/
#include "../mpu9150.h"
#include "sys/util/stubs.h"
#include "sys/util/io.h"

/************* Global variables ***********************************************************/
/* Pointers for output signals */
int32_t*	pAccelRawX_mpu9150;			/**< output: Raw acceleration X Value */
int32_t*	pAccelRawY_mpu9150;			/**< output: Raw acceleration Y Value */
int32_t*	pAccelRawZ_mpu9150;			/**< output: Raw acceleration Z Value */
float32_t*	pAccelGeeX_mpu9150;			/**< output: Acceleration X in g */
float32_t*	pAccelGeeY_mpu9150;			/**< output: Acceleration Y in g */
float32_t*	pAccelGeeZ_mpu9150;			/**< output: Acceleration Z in g */
int32_t*	pGyroRawX_mpu9150;			/**< output: Raw gyro X Value */
int32_t*	pGyroRawY_mpu9150;			/**< output: Raw gyro Y Value */
int32_t*	pGyroRawZ_mpu9150;			/**< output: Raw gyro Z Value */
float32_t*	pGyroDpsX_mpu9150;			/**< output: Gyro X in �/s */
float32_t*	pGyroDpsY_mpu9150;			/**< output: Gyro Y in �/s */
float32_t*	pGyroDpsZ_mpu9150;			/**< output: Gyro Z in �/s */
float32_t*	pOnChipTemperature;			/**< output: On chip temperature in �C */
uint32_t*	pImuMeasTimeStamp;			/**< output: Timestamp of burst read of fifo */
/************* Private typedefs ***********************************************************/



/************* Macros and constants *******************************************************/

/** I2C config ******************************************
* Default I2C used
*/
/* Default I2C clock */
#ifndef MPU9150_I2C_CLOCK
#define MPU9150_I2C_CLOCK			400000
#endif
/*Device Address I2C*/
#define MPU9150_ADDRESS_AD0_LOW     0xD0 // address pin low (GND), default for InvenSense
//#define MPU9150_ADDRESS_AD0_HIGH    0xD2 // address pin high (VCC)
#define MPU9150_DEFAULT_ADDRESS     MPU9150_ADDRESS_AD0_LOW

#ifndef MASTER_ADDRESS
#define MASTER_ADDRESS				0x00
#endif

#define MPU9150_OK			 0
#define MPU9150_ERR			-1

/* I2C config end ***************************************/

/* MPU9150 Register *************************************/
/*Accelerometer*/
#define MPU9150_ACCEL_XOUT_H		0x3B
#define MPU9150_ACCEL_XOUT_L		0x3C
#define MPU9150_ACCEL_YOUT_H		0x3D
#define MPU9150_ACCEL_YOUT_L		0x3E
#define MPU9150_ACCEL_ZOUT_H		0x3F
#define MPU9150_ACCEL_ZOUT_L		0x40
#define MPU9150_TEMP_OUT_H			0x41
#define MPU9150_TEMP_OUT_L			0x42
/*Gyro*/
#define MPU9150_GYRO_XOUT_H			0x43
#define MPU9150_GYRO_XOUT_L			0x44
#define MPU9150_GYRO_YOUT_H			0x45
#define MPU9150_GYRO_YOUT_L			0x46
#define MPU9150_GYRO_ZOUT_H			0x47
#define MPU9150_GYRO_ZOUT_L			0x48
/*Compass*/
#define MPU9150_MAG_ADDRESS			(0x0C << 1) // = 0x18
#define MPU9150_MAG_CONTROL			0x0A
#define MPU9150_MAG_XOUT_L			0x03
#define MPU9150_MAG_XOUT_H			0x04
#define MPU9150_MAG_YOUT_L			0x05
#define MPU9150_MAG_YOUT_H			0x06
#define MPU9150_MAG_ZOUT_L			0x07
#define MPU9150_MAG_ZOUT_H			0x08
#define MPU9150_MAG_ASAX			0x10
#define MPU9150_MAG_ASAY			0x11
#define MPU9150_MAG_ASAZ			0x12
#define AKM_REG_WHOAMI      		0x00
#define AKM_REG_ST1					0x02
#define AKM_REG_ST2					0x09
/*Miscellaneous*/
#define MPU9150_AUX_VDDIO			0x01
#define MPU9150_SMPLRT_DIV			0x19
#define MPU9150_CONFIG				0x1A
#define MPU9150_GYRO_CONFIG			0x1B
#define MPU9150_ACCEL_CONFIG		0x1C
#define MPU9150_FIFO_EN				0x23
#define MPU9150_I2C_MST_CTRL		0x24
#define MPU9150_I2C_SLV0_ADDR		0x25
#define MPU9150_I2C_SLV0_REG		0x26
#define MPU9150_I2C_SLV0_CTRL		0x27
#define MPU9150_I2C_SLV1_ADDR		0x28
#define MPU9150_I2C_SLV1_REG		0x29
#define MPU9150_I2C_SLV1_CTRL		0x2A
#define MPU9150_I2C_SLV4_CTRL		0x34
#define MPU9150_MOTION_THRESH		0x1F
#define MPU9150_INT_PIN_CFG			0x37
#define MPU9150_INT_ENABLE			0x38
#define MPU9150_INT_STATUS			0x3A
#define MPU9150_MOT_DETECT_STATUS	0x61
#define MPU9150_I2C_SLV0_DO			0x63
#define MPU9150_I2C_SLV1_DO			0x64
#define MPU9150_I2C_MST_DELAY_CTRL	0x67
#define MPU9150_SIGNAL_PATH_RESET	0x68
#define MPU9150_MOT_DETECT_CTRL		0x69
#define MPU9150_USER_CTRL			0x6A
#define MPU9150_PWR_MGMT_1			0x6B
#define MPU9150_PWR_MGMT_2			0x6C
#define MPU9150_FIFO_COUNTH			0x72
#define MPU9150_FIFO_COUNTL			0x73
#define MPU9150_FIFO_R_W			0x74
#define MPU9150_WHO_AM_I			0x75

/* Register values: e.g. sensitivity, calibration, config... */

/* MPU9150 Who I am value */
#define MPU9150_I_AM				0x68

/* Accelerometer sensitivity */
#define MPU9150_ACCELEROMETER_2G	0x00
#define MPU9150_ACCELEROMETER_4G	0x01
#define MPU9150_ACCELEROMETER_8G	0x02
#define MPU9150_ACCELEROMETER_16G	0x03

/* Gyro sensitivity */
#define MPU9150_GYRO_250			0x00
#define MPU9150_GYRO_500			0x01
#define MPU9150_GYRO_1000			0x02
#define MPU9150_GYRO_2000			0x03

/* Compass settings */
#define AKM_POWER_DOWN				0x00
#define AKM_SINGLE_MEASUREMENT		0x01
#define AKM_FUSE_ROM_ACCESS			0x0F
#define AKM_MODE_SELF_TEST			0x08
#define AKM_DATA_READY				0x01
#define AKM_DATA_OVERRUN			0x02
#define AKM_OVERFLOW				0x08
#define AKM_DATA_ERROR				0x04
#define AKM_BIT_SELF_TEST   		0x40
#define AKM_WHOAMI					0x48

#define BIT_BYPASS_EN       		0x02
#define BIT_AUX_IF_EN       		0x20
#define BIT_I2C_READ        		0x80
#define BIT_SLAVE_EN				0x80
#define BIT_I2C_MST_VDDIO   		0x80

#define SAMPLE_RATE					50
/**
 * MPU9150 can have 2 different addresses, it depends on the level on pin AD0. Thus, it is
 * possible to use 2 MPU9150s on the same bus.
*/

#define MPU9150_DEVICE_0 			0x00 /* low */
#define MPU9150_DEVICE_1 			0x02 /* high */


/* Gyro sensitivities for calculating gyro values in �/s */
#define MPU9150_GFS_250				((float)(0xFFFF / 250 / 2)) /* +-250 �/S */
#define MPU9150_GFS_500				((float)(0xFFFF / 500 / 2)) /* +-500 �/S */
#define MPU9150_GFS_1000			((float)(0xFFFF / 1000 / 2)) /* +-1000 �/S */
#define MPU9150_GFS_2000			((float)(0xFFFF / 2000 / 2)) /* +-2000 �/S */

/* Accel sensitivities for calculating accelerometer values in g */
#define MPU9150_AFS_2				((float)(0xFFFF / 2 / 2)) /* +-2g */
#define MPU9150_AFS_4				((float)(0xFFFF / 4 / 2)) /* +-4g */
#define MPU9150_AFS_8				((float)(0xFFFF / 8 / 2)) /* +-8g */
#define MPU9150_AFS_16				((float)(0xFFFF / 16 / 2)) /* +-16g */

/* Settings for USER_CTRL register (0x6A) */
#define MPU9150_I2C_MST_EN			0x00
#define MPU9150_BYPASSMODE_ON		0x01

#define BIT_FIFO_RST				0x07
#define BIT_DMP_RST					0x08

/* FIFO Data */
#define FIFO_GYRO_ACCEL_TEMP_EN		0xF8
#define FIFO_GYRO_ACCEL_EN			0x78
#define FIFO_GYRO					0x70
/* FIFO Enable Mask */
#define FIFO_EN						0x40
/* Sample Rate Devider */
#define SAMPLE_RATE_DEVIDER			0x13 /* leads to 50Hz fifo output by disabled DLPF */
/* lpf config mask */
#define DLPF_CFG_MASK				0x03
/* reset fifo mask */
#define RESET_FIFO					0x0C
/* Interrupt by fifo overflow and data ready */
#define INT_EN_DATA_READY_FIFO_OVER	0x11
#define FIFO_MAX_PACKET_LENGTH		(12) /* Gyro 6 + Accel 6 */

#define FIFO_MAX					1024
#define FIFO_OVERFLOW_BIT			0x10

/************* Private static variables ***************************************************/
static i2c_t mpuI2c; /* i2c structure to configure the i2c interface */

#ifdef COMPASS_USE
static uint8_t singleMeasMode = 0x01; /* Compass config for measurement */
static uint8_t fuseRomMode = 0x0F; /* For reading a special register */
static uint8_t powerDownMode = 0x00; /* Power down the compass */
static uint8_t magSensAdj[3] = {0}; /* stores the sensitivity adjustment data from compass */
volatile uint8_t bypassMode = 0xFF; /* have to change to static */
#endif
/************* Private function prototypes ************************************************/
static void getBiases(void);
static uint8_t resetFifo(void);
#ifdef affe
static uint8_t configFifo(void);
#endif
#ifdef COMPASS_USE
static int8_t setBypass(uint8_t bypassOn);
static int8_t compass_init(void);
static int8_t setCompassSampleRate(uint8_t rate);
#endif
/************* Public functions ***********************************************************/
uint8_t mpu9150_init(void)
{
	uint8_t temp = 0;

	/* Format I2C address */
	mpuI2c.address = MASTER_ADDRESS;
	mpuI2c.channel = I2C_CH1;
	mpuI2c.frequency = MPU9150_I2C_CLOCK;
	mpuI2c.mode = I2C_MASTER;
	mpu9150Dev.address = MPU9150_DEFAULT_ADDRESS | (uint8_t)MPU9150_DEVICE_0;
#ifdef COMPASS_USE
	mpu9150Dev.addressMagn = MPU9150_MAG_ADDRESS;
#endif

	/* Init I2C */
	i2c_init(&mpuI2c);
	i2c_enable(mpuI2c.channel);

	/* Wake up imu */
	temp = 1;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_PWR_MGMT_1, &temp, 1);
	delay(200000);

	i2c_readReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_CONFIG, &temp, 1);
	temp = (temp & 0x07) | DLPF_CFG_MASK;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_CONFIG, &temp, 1);

	/* Config Accelerometer */
	i2c_readReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_ACCEL_CONFIG, &temp, 1);
	temp = (temp & 0xE7) | (uint8_t)MPU9150_ACCELEROMETER_16G << 3;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_ACCEL_CONFIG, &temp, 1);

	/* Config Gyro */
	i2c_readReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_GYRO_CONFIG, &temp, 1);
	temp = (temp & 0xE7) | (uint8_t)MPU9150_GYRO_1000 << 3;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_GYRO_CONFIG, &temp, 1);

	/* reset gyro offset registers */
	temp = 0;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x13, &temp, 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x14, &temp, 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x15, &temp, 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x16, &temp, 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x17, &temp, 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x18, &temp, 1);

	/* Bias compensation */
	getBiases();

	/* Set common settings for Gyro and Accelerometer */
	/* Accelerometer */
	i2c_readReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_ACCEL_CONFIG, &temp, 1);
	temp = (temp & 0xE7) | (uint8_t)MPU9150_ACCELEROMETER_2G << 3;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_ACCEL_CONFIG, &temp, 1);
	/* Gyro */
	i2c_readReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_GYRO_CONFIG, &temp, 1);
	temp = (temp & 0xE7) | (uint8_t)MPU9150_GYRO_250 << 3;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_GYRO_CONFIG, &temp, 1);

#ifdef affe
	if(compass_init()) {
		return -1;
	}
	if(setCompassSampleRate(10)) {
		return -1;
	}
#endif
	return MPU9150_OK;
}

uint8_t mpu9150_readAccel(){
	uint8_t highbyte = 0;
	uint8_t lowbyte = 0;

	/* Read Accelerometer data */
	/* x-axis */
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_ACCEL_XOUT_H, &highbyte, 1);
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_ACCEL_XOUT_L, &lowbyte, 1);
	*pAccelRawX_mpu9150 = (uint16_t)((highbyte << 8) | lowbyte);

	/* y-axis */
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_ACCEL_YOUT_H, &highbyte, 1);
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_ACCEL_YOUT_L, &lowbyte, 1);
	*pAccelRawY_mpu9150 = (uint16_t)((highbyte << 8) | lowbyte);

	/* z-axis */
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_ACCEL_ZOUT_H, &highbyte, 1);
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_ACCEL_ZOUT_L, &lowbyte, 1);
	*pAccelRawZ_mpu9150 = (uint16_t)((highbyte << 8) | lowbyte);

	return MPU9150_OK;
}

uint8_t mpu9150_readGyro(){
	uint8_t highbyte = 0;
	uint8_t lowbyte = 0;

	/* Read Gyro data */
	/* x-axis */
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_GYRO_XOUT_H, &highbyte, 1);
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_GYRO_XOUT_L, &lowbyte, 1);
	*pGyroRawX_mpu9150 = (uint16_t)((highbyte << 8) | lowbyte);

	/* y-axis */
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_GYRO_YOUT_H, &highbyte, 1);
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_GYRO_YOUT_L, &lowbyte, 1);
	*pGyroRawY_mpu9150 = (uint16_t)((highbyte << 8) | lowbyte);

	/* z-axis */
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_GYRO_ZOUT_H, &highbyte, 1);
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_GYRO_ZOUT_L, &lowbyte, 1);
	*pGyroRawZ_mpu9150 = (uint16_t)((highbyte << 8) | lowbyte);

	return MPU9150_OK;
}

#ifdef COMPASS_USE
uint8_t mpu9150_readMagn(){
	uint8_t tmp[9];

	if(i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, 0x49, &tmp[0], 8)) {
		return -1;
	}
	if(!(tmp[0] & AKM_DATA_READY)) {
		return -2;
	}
	if((tmp[7] & AKM_OVERFLOW) || (tmp[7] & AKM_DATA_ERROR)) {
		return -3;
	}

	mpu9150Dev.magnX = (tmp[2] << 8) | tmp[1];
	mpu9150Dev.magnY = (tmp[4] << 8) | tmp[3];
	mpu9150Dev.magnZ = (tmp[6] << 8) | tmp[5];

	mpu9150Dev.magnX = ((int64_t)mpu9150Dev.magnX * magSensAdj[0]) >> 8;
	mpu9150Dev.magnY = ((int64_t)mpu9150Dev.magnY * magSensAdj[1]) >> 8;
	mpu9150Dev.magnZ = ((int64_t)mpu9150Dev.magnZ * magSensAdj[2]) >> 8;

	/*todo timestamp for compass here */

	return MPU9150_OK;
}
#endif
uint8_t mpu9150_readTemp(){
	uint8_t highbyte = 0;
	uint8_t lowbyte = 0;
	int16_t temp;

	/* Read temperature data */
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_TEMP_OUT_H, &highbyte, 1);
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_TEMP_OUT_L, &lowbyte, 1);
	/* Format */
	temp = (int16_t)((highbyte << 8) | lowbyte);
	*pOnChipTemperature = (float32_t)( temp / (float32_t)340 + (float32_t)35 );

	return MPU9150_OK;
}

uint8_t mpu9150_readFifo(){
	uint8_t data[FIFO_MAX_PACKET_LENGTH];
	uint16_t fifoCount;

	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_FIFO_COUNTH, &data[0], 1);
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_FIFO_COUNTL, &data[1], 1);
	fifoCount = (data[0] << 8) | data[1];

	if(fifoCount < FIFO_MAX_PACKET_LENGTH){
		//io_printf("%i\n\r",fifoCount);
		return 0;
	}
	if(fifoCount > FIFO_MAX >> 1){
		i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_INT_STATUS, &data[0], 1);
		if(data[0] & FIFO_OVERFLOW_BIT){
//			io_printf("FIFO reset\n\r");
			return resetFifo();
		}
	}
	*pImuMeasTimeStamp = systime_getSysTime();

	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_FIFO_R_W, &data[0], FIFO_MAX_PACKET_LENGTH);

	*pAccelRawX_mpu9150 = (int16_t)((data[0] << 8) | data[1]) < 0 ? 0xFFFF << 16 | ((data[0] << 8) | data[1]) : ((data[0] << 8) | data[1]);
	*pAccelRawY_mpu9150 = (int16_t)((data[2] << 8) | data[3]) < 0 ? 0xFFFF << 16 | ((data[2] << 8) | data[3]) : ((data[2] << 8) | data[3]);
	*pAccelRawZ_mpu9150 = (int16_t)((data[4] << 8) | data[5]) < 0 ? 0xFFFF << 16 | ((data[4] << 8) | data[5]) : ((data[4] << 8) | data[5]);
	*pGyroRawX_mpu9150  = (int16_t)((data[6] << 8) | data[7]) < 0 ? 0xFFFF << 16 | ((data[6] << 8) | data[7]) : ((data[6] << 8) | data[7]);
	*pGyroRawY_mpu9150  = (int16_t)((data[8] << 8) | data[9]) < 0 ? 0xFFFF << 16 | ((data[8] << 8) | data[9]) : ((data[8] << 8) | data[9]);
	*pGyroRawZ_mpu9150  = (int16_t)((data[10] << 8) | data[11]) < 0 ? 0xFFFF << 16 | ((data[10] << 8) | data[11]) : ((data[10] << 8) | data[11]);

	*pAccelGeeX_mpu9150 = ((float32_t)*pAccelRawX_mpu9150) / MPU9150_AFS_2;
	*pAccelGeeY_mpu9150 = ((float32_t)*pAccelRawY_mpu9150) / MPU9150_AFS_2;
	*pAccelGeeZ_mpu9150 = ((float32_t)*pAccelRawZ_mpu9150) / MPU9150_AFS_2;

	*pGyroDpsX_mpu9150 = ((float32_t)*pGyroRawX_mpu9150) / MPU9150_GFS_250;
	*pGyroDpsY_mpu9150 = ((float32_t)*pGyroRawY_mpu9150) / MPU9150_GFS_250;
	*pGyroDpsZ_mpu9150 = ((float32_t)*pGyroRawZ_mpu9150) / MPU9150_GFS_250;

	return MPU9150_OK;
}
/************* Private functions **********************************************************/
static void getBiases(){
	volatile uint16_t i = 0;
	volatile int16_t gyrCur[3];
	volatile int16_t accCur[3];
	uint8_t data[FIFO_MAX_PACKET_LENGTH];
	volatile uint16_t packetCount;
	volatile uint16_t fifoCount;
	int64_t accelRegBias[3] = {0,0,0};
	int64_t gyroBias[3] = {0,0,0};
	int64_t accelBias[3] = {0,0,0};

	/* reset FIFO */
	data[0] = 0;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_INT_ENABLE, &data[0], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_FIFO_EN, &data[0], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_PWR_MGMT_1, &data[0], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_I2C_MST_CTRL, &data[0], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &data[0], 1);
	data[0] = RESET_FIFO;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &data[0], 1);
	delay(15000);
	data[0] = DLPF_CFG_MASK;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_CONFIG, &data[0], 1); /* LP: Accel:44Hz,4.9ms delay,Fs 1kHz; Gyro:42Hz,4.8ms delay,Fs 1kHz */
	data[0] = SAMPLE_RATE_DEVIDER;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_SMPLRT_DIV, &data[0], 1); /* 50 Hz sample rate */

	/* read Accelerometer bias register first because there have a factory trim */
	i2c_readReg(mpuI2c.channel, mpu9150Dev.address, 0x06, &data[0], 1);
	i2c_readReg(mpuI2c.channel, mpu9150Dev.address, 0x07, &data[1], 1);
	accelRegBias[0] = (int64_t)((data[0] << 8) | data[1]);
	i2c_readReg(mpuI2c.channel, mpu9150Dev.address, 0x08, &data[0], 1);
	i2c_readReg(mpuI2c.channel, mpu9150Dev.address, 0x09, &data[1], 1);
	accelRegBias[1] = (int64_t)((data[0] << 8) | data[1]);
	i2c_readReg(mpuI2c.channel, mpu9150Dev.address, 0x0A, &data[0], 1);
	i2c_readReg(mpuI2c.channel, mpu9150Dev.address, 0x0B, &data[1], 1);
	accelRegBias[2] = (int64_t)((data[0] << 8) | data[1]);

	data[0] = FIFO_EN;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &data[0], 1);
	data[0] = FIFO_GYRO_ACCEL_EN;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_FIFO_EN, &data[0], 1);

	/* fill fifo with 1020 values (1024 Byte / 12 values per measurement = 85 data sets -> 85 * 20ms = 1700ms measurement delay) */
	delay(1700000);

	data[0] = 0;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_FIFO_EN, &data[0], 1);

	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_FIFO_COUNTH, &data[0], 1);
	i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_FIFO_COUNTL, &data[1], 1);
	fifoCount = (data[0] << 8) | data[1];

	if(fifoCount < FIFO_MAX_PACKET_LENGTH) {
//		io_printf("getBiasError: %i\n\r",fifoCount);
	}

	packetCount = fifoCount / FIFO_MAX_PACKET_LENGTH;

	for(i=0; i < packetCount; i++) {
		i2c_readReg(mpu9150Dev.channel, mpu9150Dev.address, MPU9150_FIFO_R_W, &data[0], FIFO_MAX_PACKET_LENGTH);

		accCur[0] = (uint16_t)((data[0] << 8) | data[1]);
		accCur[1] = (uint16_t)((data[2] << 8) | data[3]);
		accCur[2] = (uint16_t)((data[4] << 8) | data[5]);
		gyrCur[0] = (uint16_t)((data[6] << 8) | data[7]);
		gyrCur[1] = (uint16_t)((data[8] << 8) | data[9]);
		gyrCur[2] = (uint16_t)((data[10] << 8) | data[11]);

		gyroBias[0]  += (int64_t)gyrCur[0];
		gyroBias[1]  += (int64_t)gyrCur[1];
		gyroBias[2]  += (int64_t)gyrCur[2];
		accelBias[0] += (int64_t)accCur[0];
		accelBias[1] += (int64_t)accCur[1];
		accelBias[2] += (int64_t)accCur[2];
	}

	gyroBias[0]  = gyroBias[0] / packetCount;
	gyroBias[1]  = gyroBias[1] / packetCount;
	gyroBias[2]  = gyroBias[2] / packetCount;
	accelBias[0] = accelBias[0] / packetCount;
	accelBias[1] = accelBias[1] / packetCount;
	accelBias[2] = (accelBias[2] / packetCount) - (MPU9150_AFS_16+1); /* no gravity compensation */

	for(i=0;i<3;i++) {
		gyroBias[i] = (-gyroBias[i]);
		accelRegBias[i] -= (accelBias[i] & ~1);
	}

	/* write gyro bias into the gyro offset register */
	data[0] = (gyroBias[0] >> 8) & 0xff;
	data[1] = (gyroBias[0]) & 0xff;
	data[2] = (gyroBias[1] >> 8) & 0xff;
	data[3] = (gyroBias[1]) & 0xff;
	data[4] = (gyroBias[2] >> 8) & 0xff;
	data[5] = (gyroBias[2]) & 0xff;

	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x13, &data[0], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x14, &data[1], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x15, &data[2], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x16, &data[3], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x17, &data[4], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x18, &data[5], 1);

	/* write accel bias into the accel offset register */
	data[0] = (accelRegBias[0] >> 8) & 0xff;
	data[1] = (accelRegBias[0]) & 0xff;
	data[2] = (accelRegBias[1] >> 8) & 0xff;
	data[3] = (accelRegBias[1]) & 0xff;
	data[4] = (accelRegBias[2] >> 8) & 0xff;
	data[5] = (accelRegBias[2]) & 0xff;

	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x06, &data[0], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x07, &data[1], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x08, &data[2], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x09, &data[3], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x0A, &data[4], 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, 0x0B, &data[5], 1);

	resetFifo();
}
#ifdef affe
static uint8_t configFifo() {
	uint32_t ret = 0;
	uint8_t maskFifoEn = FIFO_EN;
	uint8_t maskFifoData = FIFO_GYRO_ACCEL_EN;
	uint8_t maskSampleRate = SAMPLE_RATE_DEVIDER;
	uint8_t maskResetFIFO = RESET_FIFO;
	uint8_t maskUserCtrlZero = 0x00;

	/* reset FIFO */
	ret |= i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &maskUserCtrlZero, 1);
	ret |= i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &maskResetFIFO, 1);
	delay(50);
	ret |= i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &maskFifoEn, 1);
	ret |= i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_FIFO_EN, &maskFifoData, 1);
	/* should be done in the module init */
	ret|= i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_SMPLRT_DIV, &maskSampleRate, 1);

	return ret;
}
#endif
static uint8_t resetFifo() {
	uint8_t data = 0;

	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_INT_ENABLE, &data, 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_FIFO_EN, &data, 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &data, 1);

	data = RESET_FIFO;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &data, 1);

	data = FIFO_EN;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &data, 1);

	data = INT_EN_DATA_READY_FIFO_OVER;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_INT_ENABLE, &data, 1);
	data = FIFO_GYRO_ACCEL_EN;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_FIFO_EN, &data, 1);
	return 0;
}
#ifdef COMPASS_USE
/**
 *  @brief      Set device to bypass mode.
 *  @param[in]  bypassOn   1 to enable bypass mode.
 *  @return     0 if successful.
 */
static int8_t setBypass(uint8_t bypassOn)
{
    uint8_t tmp;

    if (bypassMode == bypassOn)
        return 0;

    if (bypassOn) {
        if (i2c_readReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &tmp, 1)) {
            return -1;
        }
        tmp &= ~BIT_AUX_IF_EN;
        if (i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &tmp, 1)) {
            return -1;
        }
        delay(3);
        tmp = BIT_BYPASS_EN;
        if (i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_INT_PIN_CFG, &tmp, 1)) {
            return -1;
        }
    } else {
        /* Enable I2C master mode if compass is being used. */
        if (i2c_readReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &tmp, 1)) {
            return -1;
        }

        tmp |= BIT_AUX_IF_EN;

        if (i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_USER_CTRL, &tmp, 1)) {
            return -1;
        }
        delay(3);
        tmp = 0x00;
        if (i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_INT_PIN_CFG, &tmp, 1)) {
            return -1;
        }
    }
    bypassMode = bypassOn;
    return 0;
}

static int8_t compass_init(void) {
	volatile uint8_t data[4];
	uint8_t test[6]={0},i;
	volatile uint8_t dataReady=1;

	/* Set up MPU in bypass mode */
	setBypass(1);

	if(i2c_readReg(mpuI2c.channel, mpu9150Dev.addressMagn, 0x00, &data[0], 1)) {
			return -1;
	}
	if(data[0] != 0x48) {
		io_printf("Compass error\n\r");
		return -1;
	}

	data[0] = AKM_POWER_DOWN;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.addressMagn, MPU9150_MAG_CONTROL, &data[0], 1);
	delay(1);

	data[0] = AKM_FUSE_ROM_ACCESS;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.addressMagn, MPU9150_MAG_CONTROL, &data[0], 1);
	delay(1);

	i2c_readReg(mpuI2c.channel, mpu9150Dev.addressMagn, MPU9150_MAG_ASAX, &data[0], 1);
	i2c_readReg(mpuI2c.channel, mpu9150Dev.addressMagn, MPU9150_MAG_ASAY, &data[1], 1);
	i2c_readReg(mpuI2c.channel, mpu9150Dev.addressMagn, MPU9150_MAG_ASAZ, &data[2], 1);
	magSensAdj[0] = (int64_t)data[0] + 128;
	magSensAdj[1] = (int64_t)data[1] + 128;
	magSensAdj[2] = (int64_t)data[2] + 128;

	data[0] = AKM_POWER_DOWN;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.addressMagn, MPU9150_MAG_CONTROL, &data[0], 1);
	delay(1);

	i2c_readReg(mpuI2c.channel, mpu9150Dev.addressMagn, 0x0C, &data[0], 1);

	/* self test */
	data[0] = 0x40;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.addressMagn, 0x0C, &data[0], 1);
	delay(10);
	data[0] = 0x08;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.addressMagn, MPU9150_MAG_CONTROL, &data[0], 1);
	while(1) {
		i2c_readReg(mpuI2c.channel, mpu9150Dev.addressMagn, AKM_REG_ST1, &data[0], 1);
		if(data[0]) {
			break;
		}
		io_printf("Compass data not ready\n\r");
		delay(10);
	}

	i2c_readReg(mpuI2c.channel, mpu9150Dev.addressMagn, AKM_REG_ST2, &data[0], 1);
	if(data[0] == 0x04 || data[0] == 0x08 || data[0] == 0x0c) {
		io_printf("Compass data error, error: %i\n\r", data[0]);
	}
	i2c_readReg(mpuI2c.channel, mpu9150Dev.addressMagn, MPU9150_MAG_XOUT_L, &test[0], 6);

	data[0] = 0x00;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.addressMagn, 0x0C, &data[0], 1);
	io_printf("%i\t%i\t%i\n\r",((int16_t)test[1]<<8) | test[0],((int16_t)test[3]<<8) | test[2],((int16_t)test[5]<<8) | test[4]);
	/* end self test */

	setBypass(0);

	/* Set up master mode, master clock, and ES bit. */
	data[0] = 0x40;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_I2C_MST_CTRL, &data[0], 1);

	/* Slave 0 reads from AKM data registers. */
	data[0] = BIT_I2C_READ | (mpu9150Dev.addressMagn >> 1);
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_I2C_SLV0_ADDR, &data[0], 1);

	/* Compass reads start at this register. */
	data[0] = AKM_REG_ST1;
	i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_I2C_SLV0_REG, &data[0], 1);

	/* Enable slave 0, 8-byte reads. */
	data[0] = BIT_SLAVE_EN | 8;
	if (i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_I2C_SLV0_CTRL, &data[0], 1)) {
		return -1;
	}

	/* Slave 1 changes AKM measurement mode. */
	data[0] = mpu9150Dev.addressMagn >> 1;
	if (i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_I2C_SLV1_ADDR, &data[0], 1)) {
		return -1;
	}

	/* AKM measurement mode register. */
	data[0] = MPU9150_MAG_CONTROL;
	if (i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_I2C_SLV1_REG, &data[0], 1)) {
		return -1;
	}

	/* Enable slave 1, 1-byte writes. */
	data[0] = BIT_SLAVE_EN | 1;
	if (i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_I2C_SLV1_CTRL, &data[0], 1)) {
		return -1;
	}

	/* Set slave 1 data. */
	data[0] = AKM_SINGLE_MEASUREMENT;
	if (i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_I2C_SLV1_DO, &data[0], 1)) {
		return -1;
	}

	/* Trigger slave 0 and slave 1 actions at each sample. */
	data[0] = 0x03;
	if (i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_I2C_MST_DELAY_CTRL, &data[0], 1)) {
		return -1;
	}

	/* For the MPU9150, the auxiliary I2C bus needs to be set to VDD. */
	data[0] = BIT_I2C_MST_VDDIO;
	if (i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_AUX_VDDIO, &data[0], 1)) {
		return -1;
	}

	return 0;
}

/**
 *  @brief      Set compass sampling rate.
 *  The compass on the auxiliary I2C bus is read by the MPU hardware at a
 *  maximum of 100Hz. The actual rate can be set to a fraction of the gyro
 *  sampling rate.
 *
 *  \n WARNING: The new rate may be different than what was requested. Call
 *  mpu_get_compass_sample_rate to check the actual setting.
 *  @param[in]  rate    Desired compass sampling rate (Hz).
 *  @return     0 if successful.
 */
static int8_t setCompassSampleRate(uint8_t rate) {
    uint8_t div;

    div = SAMPLE_RATE / rate - 1;
    if (i2c_writeReg(mpuI2c.channel, mpu9150Dev.address, MPU9150_I2C_SLV4_CTRL, &div, 1)) {
        return -1;
    }
//    compass_sample_rate = SAMPLE_RATE / (div + 1);

    return 0;
}
#endif
