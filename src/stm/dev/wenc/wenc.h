/** *****************************************************************************************
 * Unit in charge:
 * @file	wenc.j
 * @author	fklemm
 * $Date:: 2017-10-23 17:27:24 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2168              $
 * $Author:: fklemm   $
 * 
 * ----------------------------------------------------------
 *
 * @brief This unit adds triggered interrupts of the tacho signal from the encoder sensor and determines the
 * actual direction of each wheel while reading logic level at predefined GPIOS connected to the direction pin of the sensor.
 *
 ******************************************************************************************/

#ifndef WENC_H_
#define WENC_H_

/************* Includes *******************************************************************/
#include <math.h>
#include "brd/startup/stm32f4xx.h"
#include "sys/sigp/sigp.h"
#include "sys/systime/systime.h"

/************* Public typedefs ************************************************************/

/************* Macros and constants *******************************************************/
/************* Public function prototypes *************************************************/
/**
 * @brief Initializes the wenc interface and timer interface.
 *
 * @return - void.
 */
void wenc_init(void);

/**
 * @brief function to be called in the scheduler from app.c.
 *
 * @return - void
 */
void wenc_step(void);

#endif /* WENC_H_ */
