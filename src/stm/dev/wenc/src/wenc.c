/** *****************************************************************************************
 * Unit in charge:
 * @file	wenc.c
 * @author	fklemm
 * $Date:: 2017-11-13 14:05:29 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2195              $
 * $Author:: fklemm   $
 * 
 * ----------------------------------------------------------
 *
 * @brief This unit adds triggered interrupts of the tacho signal from the encoder sensor and determines the
 * actual direction of each wheel while reading logic level at predefined GPIOS connected to the direction pin of the sensor.
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "dev/wenc/wenc.h"
#include "per/eict.h"
#include "per/gpio.h"
#include "sys/util/io.h"
#include "app/config.h"

/************* Macros and constants *******************************************************/
#define MIN_TICKS_FOR_VALIDATION	  	10	// minimum ticks to trigger encoder validation
#define DRIFT_TOLERANCE					16 // tolerance for upcoming encoder drift
/************* Private typedefs ***********************************************************/
typedef struct
{
	int32_t wheelTicks;
	int8_t wheelDirection;
	uint8_t errorCounter;
} WheelEncoder;

typedef struct
{
	WheelEncoder encoderFL;
	WheelEncoder encoderFR;
	WheelEncoder encoderRL;
	WheelEncoder encoderRR;
	uint32_t carTicksUnsigned;
	int8_t carDirection;
	uint8_t encoderError;
} CarEncoder;

static gpio_t directionRR;
static gpio_t directionRL;

/************* Global variables ***********************************************************/
// output signals
int32_t* pWhlTicksFL_wenc;
int32_t* pWhlTicksFR_wenc;
int32_t* pWhlTicksRL_wenc;
int32_t* pWhlTicksRR_wenc;
int8_t* pCarDirection_wenc;

/************* Private static variables ***********************************************************/
static CarEncoder carEncoder;

/************* Private function prototypes ************************************************/
static uint8_t debounceEncoderError(WheelEncoder* encoder);
static void validateEncoderSignals(CarEncoder* carEncoder);

/************* Public functions ***********************************************************/
void wenc_init(void)
{
	// initialize external interrupt counter
	eict_init();

	// setup direction gpio for rear right encoder
	directionRR.channel = GPIO_CHAN_6;
	directionRR.mode = GPIO_MODE_IN;
	directionRR.state = GPIO_STATE_OFF;
	gpio_init(&directionRR);

	// setup direction gpio for rear left encoder
	directionRL.channel = GPIO_CHAN_9;
	directionRL.mode = GPIO_MODE_IN;
	directionRL.state = GPIO_STATE_OFF;
	gpio_init(&directionRL);

	*pWhlTicksFL_wenc = 0;
	*pWhlTicksFR_wenc = 0;
	*pWhlTicksRL_wenc = 0;
	*pWhlTicksRR_wenc = 0;
}

void wenc_step(void)
{
	// local variables
	uint8_t wheelDirectionRR = 0;
	uint8_t wheelDirectionRL = 0;

	uint16_t ticksFL = 0;
	uint16_t ticksFR = 0;
	uint16_t ticksRL = 0;
	uint16_t ticksRR = 0;

	// get counter value for each encoder channel
	eict_getCounter(&ticksFL, &ticksFR, &ticksRL, &ticksRR);

#ifdef WENC_V3
	// read rear wheel direction
	wheelDirectionRR = gpio_readBit(&directionRR);
	wheelDirectionRL = gpio_readBit(&directionRL);

	carEncoder.encoderRL.wheelDirection = wheelDirectionRL ? 1 : -1;
	carEncoder.encoderRR.wheelDirection = wheelDirectionRR ? -1 : 1;
#else
	// direction is unknown with "old" hardware
	carEncoder.encoderRL.wheelDirection = 1;
	carEncoder.encoderRR.wheelDirection = 1;
#endif

	// handle wheel direction
	if (ticksRL >= MIN_TICKS_FOR_VALIDATION && ticksRR > MIN_TICKS_FOR_VALIDATION) {
		if (carEncoder.encoderRL.wheelDirection == carEncoder.encoderRR.wheelDirection) {
			carEncoder.carDirection = carEncoder.encoderRL.wheelDirection;
		} else {
			// TODO: error handling nicolai
			io_printf("Error wenc.c: inconsistent wheel direction data\n");
		}
	}
	// TODO: connect both sensors and remove this line
	carEncoder.carDirection = carEncoder.encoderRR.wheelDirection;

	// add actual wheel ticks to overall wheel ticks
#ifdef FOUR_WHEEL_ODOM
	carEncoder.encoderFL.wheelTicks += ticksFL * carEncoder.carDirection;
	carEncoder.encoderFR.wheelTicks += ticksFR * carEncoder.carDirection;
#endif
	carEncoder.encoderRL.wheelTicks += ticksRL * carEncoder.carDirection;
	carEncoder.encoderRR.wheelTicks += ticksRR * carEncoder.carDirection;
	carEncoder.carTicksUnsigned += (uint32_t)((float32_t)(ticksFL + ticksFR + ticksRL + ticksRR) / 4.00);    // needed for local error detection

	// TODO: uncomment this line
	//validateEncoderSignals(&carEncoder);

	// write distance each encoder to signal pool
#ifdef FOUR_WHEEL_ODOM
	*pWhlTicksFL_wenc = carEncoder.encoderFL.wheelTicks;
	*pWhlTicksFR_wenc = carEncoder.encoderFR.wheelTicks;
#endif
	*pWhlTicksRL_wenc = carEncoder.encoderRL.wheelTicks;
	*pWhlTicksRR_wenc = carEncoder.encoderRR.wheelTicks;

	// write direction of car to signal pool
	*pCarDirection_wenc = carEncoder.carDirection;

	//io_printf("%d\t%d\t%d\t%d\t%d\t%d\n", *pWhlTicksFL_wenc, *pWhlTicksFR_wenc, *pWhlTicksRL_wenc, *pWhlTicksRR_wenc, carEncoder.carTicksUnsigned, carEncoder.carDirection);
}

/************* Private functions **********************************************************/

static uint8_t debounceEncoderError(WheelEncoder* encoder)
{
	uint8_t error = 0;
	// wheel ticks from encoder sensor
	if (encoder->wheelTicks > 0) {
		// debounce ticks from sensor
		if (encoder->errorCounter)
			encoder->errorCounter--;
		else
			error = 0;
		// there are no ticks from sensor
	} else {
		// debounce missing ticks from sensor
		if (encoder->errorCounter < DRIFT_TOLERANCE)
			encoder->errorCounter++;
		else
			error = 1;
	}
	return error;
}

static void validateEncoderSignals(CarEncoder* encoder)
{
	// if the car is moving
	// check each encoder sensor
	if (encoder->carTicksUnsigned > MIN_TICKS_FOR_VALIDATION) {
#ifdef FOUR_WHEEL_ODOM
		encoder->encoderError |= debounceEncoderError(&encoder->encoderFL);
		encoder->encoderError |= debounceEncoderError(&encoder->encoderFR);
#endif
		encoder->encoderError |= debounceEncoderError(&encoder->encoderRL);
		encoder->encoderError |= debounceEncoderError(&encoder->encoderRR);
		// car stand still
		// reset error counter
	} else {
#ifdef FOUR_WHEEL_ODOM
		encoder->encoderFL.errorCounter = 0;
		encoder->encoderFR.errorCounter = 0;
#endif
		encoder->encoderRL.errorCounter = 0;
		encoder->encoderRR.errorCounter = 0;
	}
	if (encoder->encoderError == 1) {
		io_printf("ERROR: wenc.c no valid Encoder data\n");
		encoder->encoderError = 0;
	}
}
