/** *****************************************************************************************
 * Unit in charge:
 * @file	rcrec.c
 * @author	ledwig
 * $Date:: 2017-12-05 12:29:39 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2213                  $
 * $Author:: mbrieske           $
 * 
 * ----------------------------------------------------------
 *
 * @brief This unit decodes the rc signal into channel times and stores them via external pointers into the signalpool.
 * Decoding is managed by a statemachine handling four different states: INITIALIZED, SYNC, REC and ERROR. Init Function
 * configures a timer, which is used to determine the detected capture times for the channels as well as the sync period.
 * Each capture is detected, the timer interrupt handler is called and the current timer value is used for further pulse
 * calculation.
 * INITIALIZED state is default state and entered at first by starting the program.
 * SYNC state is used for synchronization with sync pulse and is only leaved into REC state if the sync pulse is recognized.
 * REC state is used to determine the correct pulse times und stores the channel times into signalpool if a sync pulse is detected.
 * ERROR state is to clear up the local and global (signalpool) channel values in case of an error (e.g. undefined pulse time).
 *
 ******************************************************************************************/




/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "dev/rcrec/rcrec.h"
#include "per/ict.h"
#include "sys/util/io.h"

/************* Private typedefs ***********************************************************/

/************* Macros and constants *******************************************************/

/************* Global variables **********************************************************/
/* Pointers for parameters from parameter system */

/* Pointers for signals in signal pool */
uint16_t* pRcChannel1_rcrec;
uint16_t* pRcChannel2_rcrec;
uint16_t* pRcChannel3_rcrec;
uint16_t* pRcChannel4_rcrec;
uint16_t* pRcChannel5_rcrec;
uint16_t* pRcChannel6_rcrec;
uint16_t* pRcChannel7_rcrec;
uint16_t* pRcChannel8_rcrec;

uint8_t* pRcState_rcrec;

/************* Private static variables ***************************************************/
static uint8_t state; 		/* current state */
static uint8_t currentChannel;
static uint32_t timerValue;
static uint32_t channelTimerValues[8];

/************* Private function prototypes ************************************************/

/************* Public functions ***********************************************************/
int8_t rcrec_init(void)
{
	state = RCREC_STATE_INACTIVE;
	timerValue = 0;	/* initialize timer variable */
	currentChannel = 0;

	/* initialize signal pool rc channel pointers */
	*pRcChannel1_rcrec = 1500;
	*pRcChannel2_rcrec = 1500;
	*pRcChannel3_rcrec = 1500;
	*pRcChannel4_rcrec = 1500;
	*pRcChannel5_rcrec = 1500;
	*pRcChannel6_rcrec = 1500;
	*pRcChannel7_rcrec = 1500;
	*pRcChannel8_rcrec = 1500;

	*pRcState_rcrec = state;
	ict_t ict;
	ict.ISR = (void*) &rcrec_isr;

	ict_init(&ict);	/* input capture timer init */

	return 0;
}

int8_t rcrec_isr()
{
	timerValue = TIM_GetCounter(TIM9) * 10;
	TIM_SetCounter(TIM9, 0x00000000);

	if (TIM_GetITStatus(TIM9, TIM_IT_Update)) {
		// timeout detected

		// disable timeout interrupt
		TIM_ITConfig(CPPM_TIMER, TIM_IT_Update, DISABLE);

		// if we were in recording state, go to error state. else (if we never got a signal) go to inactive state
		if (state == RCREC_STATE_REC) {
			state = RCREC_STATE_ERROR;
		} else {
			state = RCREC_STATE_INACTIVE;
		}
	}
	else if (TIM_GetITStatus(TIM9, TIM_IT_CC1)) {
		// signal detected
		if (state != RCREC_STATE_REC) {
			// enable timeout interrupt if we weren't in recording state already
			TIM_ITConfig(CPPM_TIMER, TIM_IT_Update, ENABLE);
			state = RCREC_STATE_REC;
		}
		if (timerValue > 3000) {
			// sync detected
			currentChannel = 0;
		}
		else {
			channelTimerValues[currentChannel++] = timerValue;
			if (currentChannel == 8) {
				irq_disable();
				*pRcChannel1_rcrec = channelTimerValues[0];
				*pRcChannel2_rcrec = channelTimerValues[1];
				*pRcChannel3_rcrec = channelTimerValues[2];
				*pRcChannel4_rcrec = channelTimerValues[3];
				*pRcChannel5_rcrec = channelTimerValues[4];
				*pRcChannel6_rcrec = channelTimerValues[5];
				*pRcChannel7_rcrec = channelTimerValues[6];
				*pRcChannel8_rcrec = channelTimerValues[7];
				irq_enable();
			}
		}

		irq_disable();
		*pRcState_rcrec = state;
		irq_enable();
	}
	return 0;
}


/************* Private functions **********************************************************/


