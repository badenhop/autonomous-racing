/** *****************************************************************************************
 * Unit in charge:
 * @file	rx24f.c
 * @author	fklemm
 * $Date:: 2017-11-13 14:05:29 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2195                  $
 * $Author:: fklemm             $
 *
 * ----------------------------------------------------------
 *
 * @brief Device that controls the communication between this board and the smart Servo (RX24F) via UART.
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "../rx24f.h"
#include "../rx24f_config.h"
#include "sys/util/io.h"
#include "per/gpio.h"
#include "dev/rcrec/rcrec.h"

/************* Private typedefs ***********************************************************/

static gpio_t switchDirection;
static uart_t uart;

/************* Macros and constants ******************************************************/

// see rx24f_config.h
/************* Global variables ***********************************************************/

volatile uint8_t uartSendBufferEmpty = 0;
volatile uint8_t uartFirstReceivedByte = 0;

/* input signals */
float32_t *pStAngTgtPiCtrl_rx24f;
float32_t *pStAngTgtSel_rx24f;
bool_t*	pRcOverrideStangEnabled_rx24f;
uint16_t* pRcChannel4_rx24f;

/* output signals */
float32_t *pSrvAngAct_rx24f;
float32_t *pSrvTempAct_rx24f;
float32_t *pSrvError_rx24f;

/************* Private static variables ***************************************************/

static float32_t eSum = 0.00;    // for measurement purposes

static unsigned char instructionPacket[MAX_NUM_TX_PARAMS + 10] = { 0 };
static unsigned char statusPacket[MAX_NUM_RX_PARAMS + 10] = { 0 };

static uint32_t multiplier = 0;
static uint16_t calibSteeringAngleRegMin = 1;  //105 // 55
static uint16_t calibSteeringAngleRegMax = 300; // 215 // 300
static uint32_t steps = 0;
static uint32_t notInSyncCount = 0;
static uint16_t presentSteeringAngleReg = 0;
static uint16_t presentSteeringAngleRegStartup = 0;

static float32_t targetSteeringAngle = 0.00;
static float32_t piSteeringAngle = 0.00;
static float32_t actualSteeringAngle = 0.00;
static float32_t actualTemperature = 0.00;

static int8_t rx24fRecBuf[RX24F_RINGBUF_REC_SIZE],
	rx24fSendBuf[RX24F_RINGBUF_SEND_SIZE];
static uint32_t rx24fRecBufTimestamp[RX24F_RINGBUF_REC_SIZE],
	rx24fSendBufTimestamp[RX24F_RINGBUF_SEND_SIZE];

#ifdef RX24F_EVAL
static float32_t samples[NUMBER_OF_SAMPLES] = { 0.00 };
static uint32_t timestamps[NUMBER_OF_SAMPLES] = { 0 };
static float32_t u[NUMBER_OF_SAMPLES] = { 0.00 };

static uint8_t tempature_samples[NUMBER_OF_TEMPERATURE_SAMPLES] = { 0 };
static uint8_t info_counter = 0;
static bool_t flag = 0;
static uint8_t temp_counter = 0;
#endif

/************* Private function prototypes ************************************************/

float32_t map(float32_t x, float32_t in_min, float32_t in_max, float32_t out_min, float32_t out_max);
void delayus_init(void);
void delayus(uint32_t micros);

static uint8_t rx24f_ping(void);
static uint16_t rx24f_read(uint8_t type, uint8_t numberOfBytes);
static float32_t rx24f_convert(uint8_t type, uint16_t regValue);
static void rx24f_txrx(uint8_t instruction, unsigned char* params, uint8_t numberOfParams);
static void rx24f_eepromWriteOnce(uint8_t address, uint16_t data, uint8_t numberOfBytes);
static void rx24f_write(uint8_t address, uint16_t data, uint8_t numberOfBytes);
static void rx24f_turn(float32_t angle, uint8_t speed);

#ifdef RX24F_EVAL
static void rx24f_calib(void);
static void rx24f_meas(void);
static void rx24f_meas_pi(void);
#endif

/************* Public functions **********************************************************/

int8_t rx24f_init(void)
{
	uint8_t error = 0;

	// setup USART channel
	uart.baudrate = RX24F_BAUDRATE;
	uart.channel = UART_CH1;
	uart_init(&uart, &rx24fRecBuf, RX24F_RINGBUF_REC_SIZE, &rx24fSendBuf, RX24F_RINGBUF_SEND_SIZE, &rx24fSendBufTimestamp, &rx24fRecBufTimestamp);

	// setup switchDirection gpio
	switchDirection.channel = GPIO_CHAN_0;
	switchDirection.mode = GPIO_MODE_OUT;
	switchDirection.state = GPIO_STATE_OFF;

	gpio_init(&switchDirection);

	delayus_init();

	error |= rx24f_ping();    // ping the servo
	rx24f_eepromWriteOnce(RX24F_EE_CW_ANGLE_LIMIT_L, calibSteeringAngleRegMin, 2);    // set cw angle limit
	rx24f_eepromWriteOnce(RX24F_EE_CCW_ANGLE_LIMIT_L, calibSteeringAngleRegMax, 2);    // set cw angle limit
	rx24f_eepromWriteOnce(RX24F_EE_BAUDRATE, RX24F_BAUDRATE_500K, 1);    // set baud rate = 500000 bit/s
	rx24f_eepromWriteOnce(RX24F_EE_RETURN_DELAY, RX24F_RETURN_DELAYUS(200), 1);    // set return delay = 2us
	rx24f_eepromWriteOnce(RX24F_EE_STATUS_RETURN, RX24F_STATUS_RETURN_ALL, 1);    // set status return = all
	rx24f_eepromWriteOnce(RX24F_EE_ALARM_SHUTDOWN, RX24F_ERROR_ANGLE_LIMIT | RX24F_ERROR_OVERHEATING | RX24F_ERROR_OVERLOAD, 1);    // set alarm shutdown
	rx24f_eepromWriteOnce(RX24F_EE_ALARM_LED, RX24F_ERROR_ANGLE_LIMIT | RX24F_ERROR_OVERHEATING | RX24F_ERROR_OVERLOAD, 1);    // set alarm shutdown
	rx24f_eepromWriteOnce(RX24F_EE_MAX_TORQUE_L, RX24F_MAX_TORQUE(60), 2);    // set max torque in percent
	rx24f_eepromWriteOnce(RX24F_EE_H_LIMIT_TEMP, RX24F_MAX_TEMP(80), 1);    // set temperature limit to 80�

	uint16_t cw_angle_limit = rx24f_read(RX24F_EE_CW_ANGLE_LIMIT_L, 2);
	delayus(RX24F_EEPROM_DELAY_US);
	uint16_t ccw_angle_limit = rx24f_read(RX24F_EE_CCW_ANGLE_LIMIT_L, 2);
	delayus(RX24F_EEPROM_DELAY_US);
	uint16_t torque_limit = rx24f_read(RX24F_EE_MAX_TORQUE_L, 2);
	delayus(RX24F_EEPROM_DELAY_US);
	uint16_t temp = rx24f_read(RX24F_RAM_PRESENT_TEMP, 1);
	delayus(RX24F_EEPROM_DELAY_US);
	io_printf("\n\n***Servo settings****\n\nSCW Angle Limit: %d\nCCW Angle Limit: %d\nTorque Limit: %d\nTemperature: %d\n", cw_angle_limit, ccw_angle_limit, torque_limit, temp);

#ifdef RX24F_EVAL
	rx24f_calib();
//	rx24f_meas();
//	rx24f_meas_pi();
#endif

	// set servo position to 0�
	// speed doesn't matter (0)
	rx24f_turn(0, 0);

	return (error != 0);;
}

void rx24f_step(void)
{
	bool_t rcOverrideEnabled;
	uint16_t rcvalue;
	float32_t errorRate = 0.00;
	// overflow handling
	if (steps > 10000) {
		steps = 0;
		notInSyncCount = 0;
	}

	if (notInSyncCount)
		errorRate = (notInSyncCount / (float32_t)steps) * 100;

	irq_disable();

	targetSteeringAngle = *pStAngTgtSel_rx24f;
	piSteeringAngle = *pStAngTgtPiCtrl_rx24f;
	rcOverrideEnabled = *pRcOverrideStangEnabled_rx24f;
	rcvalue = *pRcChannel4_rx24f;

	irq_enable();
	piSteeringAngle = 20;

	if (rcOverrideEnabled) {
		piSteeringAngle = map((float) rcvalue,(float) MIN_PERIOD_CHANNEL,(float) MAX_PERIOD_CHANNEL, MIN_PHY_STEERING_ANGLE, MAX_PHY_STEERING_ANGLE);
	} else {
		piSteeringAngle = 0;
	}

	rx24f_turn(piSteeringAngle, 110);
	actualSteeringAngle = rx24f_read(RX24F_RAM_PRESENT_POS_L, 2);
	actualTemperature = rx24f_read(RX24F_RAM_PRESENT_TEMP, 1);

	irq_disable();
	*pSrvTempAct_rx24f = actualTemperature;
	*pSrvAngAct_rx24f = rx24f_convert(REG_TO_DEG, actualSteeringAngle);
	irq_enable();

	if (actualTemperature > 90) {
		GPIO_ToggleBits(GPIOD, GPIO_Pin_14);
	}

#ifdef RX24F_EVAL
	// only output info @5ms * x
	if (info_counter++ >= 99) {
		io_printf("Target Steering Angle: %.3f\n", targetSteeringAngle);
		io_printf("PI Steering Angle: %.3f\n", piSteeringAngle);
		io_printf("Actual Steering Angle: %.3f\n", *pSrvAngAct_rx24f);
		io_printf("Actual Temperature: %.3f\n", actualTemperature);
		io_printf("Error rate: %.3f\n\n", errorRate);
		info_counter = 0;

//		if(NUMBER_OF_TEMPERATURE_SAMPLES <= temp_counter) {
//			io_printf("#####################################ready!\n");
//			io_printf("Temperature measurement: \n\n");
//			io_printf("time \t value\n");
//			for(uint8_t i = 0;i < NUMBER_OF_TEMPERATURE_SAMPLES;i++) {
//				io_printf("%d\t%d\r\n", i, tempature_samples[i]);
//			}
//			temp_counter = 0;
//		} else tempature_samples[temp_counter++] = actualTemperature;
	}
#endif

}

/************* Private functions **********************************************************/

void delayus_init(void)
{
	RCC_ClocksTypeDef RCC_Clocks;

	RCC_GetClocksFreq(&RCC_Clocks);
	multiplier = RCC_Clocks.HCLK_Frequency / 8000000;
}
void delayus(uint32_t micros)
{
	irq_disable();
	micros = micros * multiplier;
	irq_enable();
	while (micros--)
		;
}
float32_t map(float32_t x, float32_t in_min, float32_t in_max, float32_t out_min, float32_t out_max)
{
	if (x >= in_max)
		return out_max;
	if (x <= in_min)
		return out_min;

	return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
}
static void rx24f_txrx(uint8_t instruction, unsigned char *params, uint8_t numberOfParams)
{
	uint32_t tries = 0;
	uint8_t i = 0;
	uint8_t detectedStartCondition = 0;
	uint8_t data = 0;
	uint8_t queue_counter = 0;
	uint8_t checksumPosition = 0;
	uint8_t checksumCalculated = 0;
	uint8_t numberOfEstimatedRxBytes = 0;

	bool_t dirty = 0;
	bool_t synchronized = 0;

	unsigned char queue[MAX_LENGTH_STATUS_PACKET] = { 0 };

	// clear arrays
	memset(instructionPacket, 0, sizeof(instructionPacket));
	memset(statusPacket, 0, sizeof(statusPacket));
	// instruction Packet in form of:
	// START_CONDITION | START_CONDITION | ID | (LENGTH_OF_PARAMS+2) | INSTRUCTION | PARAMETER1 | PARAMTER2 | ... | PARAMETERN | CHECKSUM

	// calculate checksum position in instruction packet (because of variable number of parameters
	checksumPosition = ARRAY_INDEX_INSTRUCTION + numberOfParams + 1;

	// build of instruction packet
	instructionPacket[ARRAY_INDEX_START] = RX24F_START_CONDITION;
	instructionPacket[ARRAY_INDEX_START + 1] = RX24F_START_CONDITION;
	instructionPacket[ARRAY_INDEX_ID] = RX24F_ID;
	instructionPacket[ARRAY_INDEX_LENGTH] = numberOfParams + 2;
	instructionPacket[ARRAY_INDEX_INSTRUCTION] = instruction;

#ifdef RX24F_DEBUG
	// just in case
	switch(instruction) {

		case RX24F_PING :
		// No. of parameters = 0
		break;
		case RX24F_READ_DATA:
		// No. of parameters = 2
		break;
		case RX24F_WRITE_DATA:
		// No. of parameters = 2 or more
		break;
		case RX24F_RAM_WRITE:
		// No. of parameters = 2 or more
		break;
		case RX24F_ACTION:
		// No. of parameters = 0
		break;
		case RX24F_RESET:
		// No. of parameters = 0
		break;
		case RX24F_SYNC_WRITE:
		// No. of parameters = 4 or more
		break;
	}
#endif

	// if exists, add parameters to the instruction packet
	if (numberOfParams) {
		for (i = 0; i < numberOfParams; i++) {
			instructionPacket[ARRAY_INDEX_PARAM + i] = params[i];
		}
	}
	// calculate checksum for instruction packet
	// formula: checksum = ~(START_CONDITION + START_CONDITION + ID + (LENGTH_OF_PARAMS+2) + INSTRUCTION + PARAMETER1 + PARAMTER2 + ... + PARAMETERN + CHECKSUM)
	for (i = 2; i < checksumPosition; i++) {
		instructionPacket[checksumPosition] += instructionPacket[i];
	}
	instructionPacket[checksumPosition] = ~instructionPacket[checksumPosition];

	// set DI-input HIGH
	gpio_setBit(&switchDirection);

	// is there any data in DR?
	// only continue if DR is empty
	while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
		;

	// transmit instruction packet via UART
	for (i = 0; i <= checksumPosition; i++) {
		USART_SendData(USART1, instructionPacket[i]);
		//__writeByte(instructionPacket[i]);
		while (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET)
			;    // DR empty?
	}

	// read status packet
	// set DI-input LOW
	delayus(30);    // if DR for last byte is empty, data isn't transmitted yet -> wait some �s's
	gpio_clearBit(&switchDirection);

	// blocking mode
	// wait until first byte is in USART buffer
	uartFirstReceivedByte = 0;
	do {
		delayus(10);
		if (++tries > MAX_NUM_RECEIVEWAIT_10US)
			break;    // give up
	} while (!uartFirstReceivedByte);

	// from first byte to last byte ~ 200us - 350us
	delayus(400);
	tries = 0;
	// status packet is in form of:
	// START_CONDITION | START_CONDITION | ID | (LENGTH_OF_STATUS+2) | ERROR | PARAMETER1 | PARAMTER2 | ... | PARAMETERN | CHECKSUM
	do {
		__readByte;    // is there any START_CONDITION?
		queue[queue_counter++] = data;    // for debugging purposes

		if (data == RX24F_START_CONDITION) {
			detectedStartCondition = 1;    // first START_CONDITION
		} else {
			detectedStartCondition = 0;
		}
		if (detectedStartCondition == 1) {
			__readByte;    // is there another START_CONDITIOn right behind?
			queue[queue_counter++] = data;
			if (data == RX24F_START_CONDITION) {
				detectedStartCondition = 2;    // detected second START_CONDITION in sequence
				synchronized = 1;
				break;
			} else if (data == 0x01) {    // check on ID
				detectedStartCondition = 2;
				synchronized = 1;
				dirty = 1;
				break;
			} else
				detectedStartCondition = 0;
		} else
			detectedStartCondition = 0;
		// just look at chunks in size of "tries"
		// with bad a bad signal it could be something like while(1);
		if (++tries > MAX_LENGTH_STATUS_PACKET)
			break;
	} while (detectedStartCondition < 2);    // only continue, if START_CONDITION appeared 2 x in a sequence OR number of tries reached limit

	// check if there was found a valid START_CONDITION sequence
	if (synchronized) {

		// build the status packet
		statusPacket[ARRAY_INDEX_START] = RX24F_START_CONDITION;
		statusPacket[ARRAY_INDEX_START + 1] = RX24F_START_CONDITION;

		// ID
		// if something like that occurs: 255 | 1 | 2
		// try to fix signal
		if (!dirty)
			__readByte;
		// if something like that occurs: 255 | 255 | 255 | 1 | 2
		// try to fix signal
		if (data == 255)
			__readByte;

		statusPacket[ARRAY_INDEX_ID] = data;

		// LENGTH
		__readByte;
		statusPacket[ARRAY_INDEX_LENGTH] = data;

		// ERROR
		__readByte;
		statusPacket[ARRAY_INDEX_ERROR] = data;

		// error handling
		if (data & RX24F_ERROR_INPUT_VOLTAGE)
			__printError("INPUT VOLTAGE");
		if (data & RX24F_ERROR_ANGLE_LIMIT)
			__printError("ANGLE LIMIT REACHED");
		if (data & RX24F_ERROR_OVERHEATING)
			__printError("OVERHEATING");
		if (data & RX24F_ERROR_RANGE)
			__printError("RANGE");
		if (data & RX24F_ERROR_CHECKSUM)
			__printError("CHECKSUM RX24F");
		if (data & RX24F_ERROR_OVERLOAD)
			__printError("OVERLOAD");
		if (data & RX24F_ERROR_INSTRUCTION)
			__printError("INSTRUCTION");

		numberOfParams = statusPacket[ARRAY_INDEX_LENGTH] - 2;

		// quick fix for rubish data
		if (numberOfParams > MAX_NUM_RX_PARAMS) {
			// if signal is still bad, return
			notInSyncCount++;
			return;
		}
		for (i = 0; i < numberOfParams; i++) {
			__readByte;
			statusPacket[ARRAY_INDEX_PARAM + i] = data;
		}

		__readByte;
		checksumPosition = ARRAY_INDEX_ERROR + numberOfParams + 1;

		// quick fix, sorry
		if (checksumPosition >= MAX_NUM_RX_PARAMS + 10) {
			// if signal is still bad, return
			notInSyncCount++;
			return;
		}
		statusPacket[checksumPosition] = data;

		// calculate checksum and compare with received
		for (i = 2; i < checksumPosition; i++) {
			checksumCalculated += statusPacket[i];
		}
		checksumCalculated = ~checksumCalculated;

		// todo
		if (checksumCalculated != statusPacket[checksumPosition]) {

			__printError("CHECKSUM STM");
		}
	} else {

		notInSyncCount++;
		__printError("NO SYNC BYTES RECEIVED");
	}

	steps++;

#ifdef RX24F_DEBUG
	io_printf("instruction packet: ");
	for(i = 0; i <= checksumPosition; i++) {
		io_printf("%d ", instructionPacket[i]);
	}
	io_printf("\n");
	io_printf("queue: ");
	for(i = 0; i < MAX_LENGTH_STATUS_PACKET; i++) {
		io_printf("%d ", queue[i]);
	}
	io_printf("\n");
	io_printf("status packet: ");
	for(i = 0; i <= checksumPosition; i++) {
		io_printf("%d ", statusPacket[i]);
	}
	io_printf("\n\n");
#endif
}
static uint16_t rx24f_read(uint8_t reg, uint8_t numberOfBytes)
{
	if (numberOfBytes <= 2) {

		uint8_t numberOfParams = 2;
		unsigned char params[numberOfParams];

		params[0] = reg;    // address (low)
		params[1] = numberOfBytes;    // number of bytes to read
		rx24f_txrx(RX24F_READ_DATA, &params, numberOfParams);    // blocking mode

		if (numberOfBytes > 1)
			return (statusPacket[ARRAY_INDEX_PARAM + 1] << 8) + statusPacket[ARRAY_INDEX_PARAM];
		else
			return statusPacket[ARRAY_INDEX_PARAM];
	} else
		return 0;
}
static void rx24f_eepromWriteOnce(uint8_t address, uint16_t data, uint8_t numberOfBytes)
{
	// check if value already written to eeprom
	// just to reduce write-cycles
	if (data == rx24f_read(address, numberOfBytes)) {
		delayus(RX24F_EEPROM_DELAY_US);
		return;
	}
	rx24f_write(address, data, numberOfBytes);
	io_printf("EEPROM WRITE ONCE: %04X DATA: %d\n", address, data);
	delayus(RX24F_EEPROM_DELAY_US);
}
static void rx24f_write(uint8_t address, uint16_t data, uint8_t numberOfBytes)
{
	uint8_t low = data;
	uint8_t high = 0;

	uint8_t numberOfParams = 1 + numberOfBytes;
	unsigned char params[3];
	params[0] = address;    // address (low)

	// check if 8-bit or 16-bit
	if (numberOfBytes == 2) {
		high = data >> 8;
	}
	params[1] = low;
	params[2] = high;
	rx24f_txrx(RX24F_WRITE_DATA, &params, numberOfParams);
}
static uint8_t rx24f_ping(void)
{
	uint8_t numberOfParams = 0;
	unsigned char params[numberOfParams];
	rx24f_txrx(RX24F_PING, &params, numberOfParams);

	return !(statusPacket[ARRAY_INDEX_ERROR] == RX24F_OK);
}
static void rx24f_turn(float32_t angle, uint8_t speed)
{
	uint16_t positionMapped = (uint16_t)map(angle, MIN_PHY_STEERING_ANGLE, MAX_PHY_STEERING_ANGLE, calibSteeringAngleRegMin, calibSteeringAngleRegMax);
	uint16_t speedMapped = 0;
	uint8_t numberOfParams = 3;

	if (speed) {
		speedMapped = (uint16_t)map(speed, SPEED_MIN, SPEED_MAX, SPEED_REG_MIN, SPEED_REG_MAX);
		numberOfParams = 5;
	}

	unsigned char params[numberOfParams];
	params[0] = RX24F_RAM_GOAL_POS_L;    // address of GOAL POSITION (low)
	params[1] = positionMapped;    // GOAL POSITION (low)
	params[2] = positionMapped >> 8;    // GOAL POSITION (high)
	if (speed) {
		params[3] = speedMapped;    // GOAL SPEED (low)
		params[4] = speedMapped >> 8;    // GOAL SPEED (high)
	}
	rx24f_txrx(RX24F_WRITE_DATA, &params, numberOfParams);
}

static float32_t rx24f_convert(uint8_t type, uint16_t regValue)
{
	float32_t mapped = 0.00;
	switch (type) {
		case REG_TO_DEG:
			mapped = map(regValue, calibSteeringAngleRegMin, calibSteeringAngleRegMax, MIN_PHY_STEERING_ANGLE, MAX_PHY_STEERING_ANGLE);
			break;
	}
	return mapped;
}
#ifdef RX24F_EVAL
static void rx24f_calib(void)
{
	uint16_t i = 0;
	uint16_t setpoint = 0;

	// set torque limit for detection/ calib mode
	rx24f_write(RX24F_RAM_TORQUE_LIMIT_L, 900, 2);
	// get present steering angle
	presentSteeringAngleRegStartup = rx24f_read(RX24F_RAM_PRESENT_POS_L, 2);

	// STARTUP ANGLE -> -22* ^= 0x01
	for (i = 0; i < presentSteeringAngleRegStartup; i++) {

		setpoint = presentSteeringAngleRegStartup - i;
		io_printf("SETPOINT: %d ACTUAL: %d \n", setpoint, presentSteeringAngleReg);
		rx24f_write(RX24F_RAM_GOAL_POS_L, setpoint, 2);
		presentSteeringAngleReg = rx24f_read(RX24F_RAM_PRESENT_POS_L, 2);
		io_printf("SETPOINT: %d ACTUAL: %d \n", setpoint, presentSteeringAngleReg);
		// calculate error between present steering angle and setpoint to detect minimum

		if (presentSteeringAngleReg - setpoint >= MAX_CALIB_ERROR) {
			calibSteeringAngleRegMin = presentSteeringAngleReg;
			io_printf("detected minimum steering angle at: %d\n", calibSteeringAngleRegMin);
			break;
		}
		delayus(10000);
	}
	delayus(10000);
	// STARTUP ANGLE -> +22* ^= 0x3fe
	for (i = calibSteeringAngleRegMin; i < calibSteeringAngleRegMax; i++) {

		setpoint = i;
		io_printf("SETPOINT: %d ACTUAL: %d \n", setpoint, presentSteeringAngleReg);
		rx24f_write(RX24F_RAM_GOAL_POS_L, setpoint, 2);
		presentSteeringAngleReg = rx24f_read(RX24F_RAM_PRESENT_POS_L, 2);
		// calculate error between present steering angle and setpoint to detect minimum
		if (setpoint - presentSteeringAngleReg > MAX_CALIB_ERROR) {
			calibSteeringAngleRegMax = presentSteeringAngleReg;
			io_printf("detected maximum steering angle at: %d\n", calibSteeringAngleRegMax);
			break;
		}
		delayus(10000);
	}
	delayus(10000);
	// just to give the servo a bit tolerance
	calibSteeringAngleRegMin += (MAX_CALIB_ERROR);
	calibSteeringAngleRegMax -= (MAX_CALIB_ERROR);

 	rx24f_write(RX24F_RAM_TORQUE_LIMIT_L, RX24F_MAX_TORQUE(100), 2);    // set max torque in percent
	rx24f_turn(0, 110);
}
static void rx24f_meas(void)
{
	uint8_t i = 0;

	// bring servo to min. position
	rx24f_turn(0, 110);
	delayus(5000000);
	// simulate a step from 0� to 10�
	rx24f_turn(10, 110);

	// start measurement of step response to
	for (i = 0; i < NUMBER_OF_SAMPLES; i++) {
		samples[i] = rx24f_read(RX24F_RAM_PRESENT_POS_L, 2);
		timestamps[i] = systime_getSysTime();
	}

	// measurement finished
	// convert register value to degrees
	io_printf("Step measurement: \n\n");
	io_printf("time \t value\n");
	for (i = 0; i < NUMBER_OF_SAMPLES; i++) {
		samples[i] = rx24f_convert(REG_TO_DEG, samples[i]);
		io_printf("%d\t%.3f\r\n", timestamps[i], samples[i]);
	}
	delayus(5000000);
}
static void rx24f_meas_pi(void)
{
	uint16_t i = 0;

	float32_t error = 0.00;
	float32_t pi = 0.00;
	float32_t eSumOld = 0.00;
	float32_t target = 10;
	float32_t actual = 0.00;
	float32_t sample_time = 0.005;

	// bring servo to min. position
	rx24f_turn(0, 110);
	delayus(1000000);

	for (i = 0; i < NUMBER_OF_SAMPLES; i++) {

		eSumOld = eSum;
		// pi controller
		error = target - actual;
		eSum += error * sample_time;    // prevent overflow

		pi = P_GAIN * error + I_GAIN * eSum;
		if (pi > MAX_PHY_STEERING_ANGLE) {
			eSum = eSumOld;
			pi = MAX_PHY_STEERING_ANGLE;
		}
		if (pi < MIN_PHY_STEERING_ANGLE) {
			eSum = eSumOld;
			pi = MIN_PHY_STEERING_ANGLE;
		}

		rx24f_turn(pi, 110);
		samples[i] = rx24f_convert(REG_TO_DEG, rx24f_read(RX24F_RAM_PRESENT_POS_L, 2));
		u[i] = pi;
		actual = samples[i];
		timestamps[i] = systime_getSysTime();
		delayus(5000);    // sample time
	}

	// measurement finished
	// convert register value to degrees
	io_printf("PI controller measurement: \n\n");
	io_printf("time \t in \t out\n");
	for (i = 0; i < NUMBER_OF_SAMPLES; i++) {
		io_printf("%d\t%.3f\t%.3f\r\n", timestamps[i], u[i], samples[i]);
	}
	delayus(1000000);
}
#endif
