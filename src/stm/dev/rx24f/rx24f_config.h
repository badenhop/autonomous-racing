/** *****************************************************************************************
 * Unit in charge:
 * @file	rx24f_config.h
 * @author	fklemm
 * $Date:: 2017-06-29 19:22:45 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2146                  $
 * $Author:: fklemm             $
 *
 * ----------------------------------------------------------
 *
 * @brief Device that controls the communication between this board and the smart Servo (RX24F) via UART.
 *
 ******************************************************************************************/


//#define RX24F_DEBUG
#define RX24F_EVAL

// gains only for evaluation!
#define P_GAIN 							0.7F
#define LAMBDA							32
#define I_GAIN							P_GAIN * LAMBDA

#define SPEED_MIN						0
#define SPEED_MAX						114
#define SPEED_REG_MIN					0
#define SPEED_REG_MAX					1023

#define MAX_PHY_STEERING_ANGLE			22.0F
#define MIN_PHY_STEERING_ANGLE			-22.0F
#define MAX_STEERING_ANGLE				22.0F
#define MIN_STEERING_ANGLE				-22.0F

#define __writeByte(byte) 				uart_writeByte(&uart, byte)
#define __readByte						uart_readByte(&uart, &data)
#define __printError(message)			io_printf("\n\n***ERROR: %s***\n\n", message)
#define RX24F_RETURN_DELAYUS(micros)	(micros/2) // minimum return delay = 2us; steps of 2us/inc
#define RX24F_MAX_TORQUE(percentage)	(1023/(100/percentage))
#define RX24F_MAX_TEMP(degrees)			((degrees > 80) ? 80 : degrees)

#define RX24F_START_CONDITION			0xFF
#define RX24F_ID						0x01
#define RX24F_BAUDRATE					500000
#define RX24F_OK						0x00

#define MAX_NUM_RX_PARAMS				10
#define MAX_NUM_TX_PARAMS				10
#define MAX_NUM_ERRORS					6
#define MAX_LENGTH_STATUS_PACKET		20
#define MAX_NUM_RECEIVEWAIT_10US		400
#define MAX_CALIB_ERROR					10

// read commands
#define READ_POSITION					0x00
#define READ_SPEED						0x01
#define READ_LOAD						0x02
#define READ_VOLTAGE					0x03
#define READ_TEMP						0x04

// for building the instruction packet/ status packet array
#define ARRAY_INDEX_START				0
#define ARRAY_INDEX_ID					2
#define ARRAY_INDEX_LENGTH				3
#define ARRAY_INDEX_INSTRUCTION			4
#define ARRAY_INDEX_ERROR				4
#define ARRAY_INDEX_PARAM				5

#define RX24F_BAUDRATE_1M				1
#define RX24F_BAUDRATE_500K				3
#define RX24F_BAUDRATE_400K				4
#define RX24F_BAUDRATE_250K				7
#define RX24F_BAUDRATE_200K				9
#define RX24F_BAUDRATE_115200			16
#define RX24F_BAUDRATE_57600			34
#define RX24F_BAUDRATE_19200			103
#define RX24F_BAUDRATE_9600				207

#define RX24F_STATUS_RETURN_NONE		0x00
#define RX24F_STATUS_RETURN_ONLY_READ	0x01
#define RX24F_STATUS_RETURN_ALL			0x02

// error codes
#define RX24F_ERROR_INPUT_VOLTAGE		(1 << 0)
#define RX24F_ERROR_ANGLE_LIMIT			(1 << 1)
#define RX24F_ERROR_OVERHEATING			(1 << 2)
#define RX24F_ERROR_RANGE				(1 << 3)
#define RX24F_ERROR_CHECKSUM			(1 << 4)
#define RX24F_ERROR_OVERLOAD			(1 << 5)
#define RX24F_ERROR_INSTRUCTION			(1 << 6)

// instructions
#define RX24F_PING						0x01 // no execution, used when controller is ready to receive status packet
#define RX24F_READ_DATA					0x02 // command reads data from RX24F
#define RX24F_WRITE_DATA				0x03 // command writes data to RX24F
#define RX24F_RAM_WRITE					0x04 // similar to WRTE_DATA, but it remains in the standby state without being executed until the ACTION command arrives
#define RX24F_ACTION					0x05 // command initiates motions registered with REG WRIT
#define RX24F_RESET						0x06 // command restores the state of RX24F to the factory default setting
#define RX24F_SYNC_WRITE				0x07 // command is used to control several RX24Fs simultaneously at a time
// addresses in EEPROM area
#define RX24F_EE_NODEL_NUMBER_L			0x00 // lowest byte of model number
#define RX24F_EE_NODEL_NUMBER_H			0x01 // highest byte of model number
#define RX24F_EE_VERSION				0x02 // information on the version of firmware
#define RX24F_EE_ID						0x03 // ID of RX24F
#define RX24F_EE_BAUDRATE				0x04 // baud rate of RX24F
#define RX24F_EE_RETURN_DELAY			0x05 // return delay time
#define RX24F_EE_CW_ANGLE_LIMIT_L		0x06 // lowest byte of clockwise angle limit
#define RX24F_EE_CW_ANGLE_LIMIT_H		0x07 // highest byte of clockwise angle limit
#define RX24F_EE_CCW_ANGLE_LIMIT_L		0x08 // lowest byte of counterclockwise angle limit
#define RX24F_EE_CCW_ANGLE_LIMIT_H		0x09 // highest byte of counterclockwise angle limit
#define RX24F_EE_H_LIMIT_TEMP			0x0B // internal limit temperature
#define RX24F_EE_L_LIMIT_VOLTAGE		0x0C // lowest limit voltage
#define RX24F_EE_H_LIMIT_VOLTAGE		0x0D // highest limit voltage
#define RX24F_EE_MAX_TORQUE_L			0x0E // lowest byte of max. torque
#define RX24F_EE_MAX_TORQUE_H			0x0F // highest byte of max. torque
#define RX24F_EE_STATUS_RETURN			0x10 // status return level
#define RX24F_EE_ALARM_LED				0x11 // LED for alarm
#define RX24F_EE_ALARM_SHUTDOWN			0x12 // shutdown for alarm
// addresses in RAM area
#define RX24F_RAM_TOQRUE_ENABLE			0x18 // torque on/off
#define RX24F_RAM_LED					0x19 // LED on/off
#define RX24F_RAM_CW_COMP_MARGIN		0x1A // CW compliance margin
#define RX24F_RAM_CCW_COMP_MARGIN		0x1B // CCW compliance margin
#define RX24F_RAM_CW_COMP_SLOPE			0x1C // CW compliance slope
#define RX24F_RAM_CCW_COMP_SLOPE		0x1D // CCW compliance slope
#define RX24F_RAM_GOAL_POS_L			0x1E // lowest byte of goal position
#define RX24F_RAM_GOAL_POS_H			0x1F // highest byte of goal position
#define RX24F_RAM_SPEED_L				0x20 // lowest byte of moving speed (moving velocity)
#define RX24F_RAM_SPEED_H				0x21 // highest byte of moving speed (moving velocity)
#define RX24F_RAM_TORQUE_LIMIT_L		0x22 // lowest byte of torque limit (goal torque)
#define RX24F_RAM_TORQUE_LIMIT_H		0x23 // highest byte of torque limit (goal torque)
#define RX24F_RAM_PRESENT_POS_L			0x24 // lowest byte of current position (present velocity)
#define RX24F_RAM_PRESENT_POS_H			0x25 // highest byte of current position (present velocity)
#define RX24F_RAM_PRESENT_SPEED_L		0x26 // lowest byte of current speed
#define RX24F_RAM_PRESENT_SPEED_H		0x27 // highest byte of current speed
#define RX24F_RAM_PRESENT_LOAD_L		0x28 // lowest byte of current load
#define RX24F_RAM_PRESENT_LOAD_H		0x29 // highest byte of current load
#define RX24F_RAM_PRESENT_VOLTAGE		0x2A // current voltage
#define RX24F_RAM_PRESENT_TEMP			0x2B // current temperature
#define RX24F_RAM_REGISTERED			0x2C // means if instruction is registered
#define RX24F_RAM_MOVING				0x2E // means if there is any movement
#define RX24F_RAM_LOCK					0x2F // locking EEPROM
#define RX24F_RAM_PUNCH_L				0x30 // lowest byte of punch
#define RX24F_RAM_PUNCH_H				0x31 // highest byte of punch
#define REG_TO_DEG						0x00

#define RX24F_RINGBUF_REC_SIZE			100
#define RX24F_RINGBUF_SEND_SIZE			500

#define RX24F_EEPROM_DELAY_US			2000

#define NUMBER_OF_SAMPLES				200
#define NUMBER_OF_TEMPERATURE_SAMPLES	120
