/** *****************************************************************************************
 * Unit in charge:
 * @file	srf02_t.c
 * @author	wandji-kwuntchou
 * $Date:: 2015-02-20 12:12:49 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1135                  $
 * $Author:: wandji-kwuntchou   $
 *
 * ----------------------------------------------------------
 *
 * @brief A supersonic device with i2c attachment for the adaptive cruise control
 *
 ******************************************************************************************/



/************* Includes *******************************************************************/
#include "dev/sfr02_t/srf02_t.h"
#include "sys/systime/systime.h"
#include "sys/util/io.h"

//#ifdef SRF02_T_EN

/************* Private typedefs ***********************************************************/
typedef struct
{
	uint8_t address; /* address of the I2C slave */
	i2c_channel_t channel;
} srf02t_t;

/************* Macros and constants *******************************************************/
#define MASTER_ADDRESS			0x00;		/**< Master address of the stm32 */
#define I2C_FREQUENCY			100000;		/**< Frequency of the ic2c peripheral is 100kHz*/
#define SLAVEID					0xEA;		/**< srf02_t slave ID/ address */
#define SRF02_T_OK 	0
#define SRF02_T_ERR 	-1
#define TIMESTAMP_TOLERANZ      1500
#define SFR02_DELAY_US          130000

/************* Private static variables ***************************************************/
static i2c_t i2c1;				       /* i2c structure to configure the i2c interface        */
static srf02t_t slave;			       /* srf02_t structure to configure the srf02 device     */
static uint32_t timeCounter;           /* Counter to process the srf02t in 65 ms task         */
static uint32_t timeStampSum;          /* total time stamp since the start of measurement     */
static uint32_t timeStampOld;          /* time stamp for the function call with the scheduler */
static uint32_t timeStampSumLastMeas;  /* time stamp for the last measurement                 */


/************* Global variables ***************************************************/
uint32_t* pDistUs_srf02t;

/************* Private function prototypes ************************************************/
/**
 * @brief	Initializes the chosen SRF02_t device
 *
 * @param[in,out] device contains the SRF02 configuration type such as the device address and i2c channel to initialize
 *
 * @param[in,out] i2c contains a i2c configuration type to initialize
 *
 * @return SRF02_OK if everything went fine, I2C_ERR in case of error
 */
uint8_t srf02t_init(srf02t_t* pDevice, i2c_t* pI2c);

/**
 * @brief	start the measurement of SRF02 device by sending the write command
 *
 * @param[in,out] device contains the SRF02 configuration type
 * @param[in,out] pData contains the current measurement value in cm
 *
 * @return SRF02_OK if everything went fine, I2C_ERR in case of error
 */
uint8_t srf02t_start(srf02t_t* pDevice);

/**
 * @brief	returns the current measurement value of SRF02 device by reading from register 2 and 3
 *
 * @param[in,out] device contains the SRF02 configuration type
 *
 * @param[in,out] pData contains the current measurement value in cm
 *
 * @return SRF02_OK if everything went fine, I2C_ERR in case of error
 */
uint8_t srf02t_getMeasurement(srf02t_t* pDevice, uint16_t* pData);

/**
 * @brief save the actual measure distance in the signal pool
 *
 * @param[in,out] void
 *
 * @return void
 */
static void srf02t_getValue(void);

/************* Public functions ***********************************************************/
void srf02t_inititialize(void)
{
	/* Configure the i2c and the srf02 structure */
	i2c1.address 	= MASTER_ADDRESS;
    i2c1.channel 	= I2C_CH1;
    i2c1.frequency 	= I2C_FREQUENCY;
    i2c1.mode 		= I2C_MASTER;
    slave.address 	= SLAVEID;

    /* Initialize the srf02 slave device */
    srf02t_init(&slave, &i2c1);
}

void srf02t_startMeasurement(void)
{
	/* Start the task and reset the counter */
	srf02t_start(&slave);
	srf02t_getValue();
	timeCounter = 0;
}

/************* Public functions ***********************************************************/
uint8_t srf02t_init(srf02t_t* pDevice, i2c_t* pI2c)
{
	pDevice->channel = pI2c->channel;
	i2c_init(pI2c); /* init CPU Master mode */
	i2c_enable(pI2c->channel);
	return SRF02_T_OK;
}

uint8_t srf02t_start(srf02t_t* pDevice)
{
	uint8_t instruct = 0x51;
	i2c_writeReg(pDevice->channel, pDevice->address, 0x00, &instruct, 1);
	return SRF02_T_OK;
}

uint8_t srf02t_getMeasurement(srf02t_t* pDevice, uint16_t* pData)
{
	uint8_t highbyte = 0;
	uint8_t lowbyte = 0;

	i2c_readReg(pDevice->channel, pDevice->address, 0x02, &highbyte, 1);
	i2c_readReg(pDevice->channel, pDevice->address, 0x03, &lowbyte, 1);
	*pData = (uint16_t)((highbyte << 8) + lowbyte);
	return SRF02_T_OK;
}

static void srf02t_getValue(void)
{
	uint16_t tempData = 0;
	uint32_t timeStampAct = 0;
	uint32_t timeStampDiff = 0;
	uint32_t timeStampSumDiff = 0;

	/* read the actual system Time */
	timeStampAct = systime_getSysTime();

	/* Calcul the time between two step */
	timeStampDiff = timeStampAct - timeStampOld;

	/* Total TimeStamp to do a new measurement */
	timeStampSum += timeStampDiff;

	/* built the difference between the actual total time and the time for the last measurement call of measurement */
	timeStampSumDiff = timeStampSum - timeStampSumLastMeas;

	/* */
	if(((timeStampSumDiff - TIMESTAMP_TOLERANZ) >= SFR02_DELAY_US) || ((timeStampSumDiff + TIMESTAMP_TOLERANZ) >= SFR02_DELAY_US)) {

		/* do a new measurement */
		srf02t_getMeasurement(&slave, &tempData);

		/* write the measured distance in the signal pool */
	    *pDistUs_srf02t = (uint32_t)tempData;

	    /* save the actual total time to do a new measurement in the next step */
	    timeStampSumLastMeas = timeStampSum;
	}

//	io_printf("%i\r\n", *pDistUs_srf02t);

	/* save the actual Time for the next step */
	timeStampOld = timeStampAct;

}

/************* Private functions **********************************************************/
//#endif /* SRF02_T_EN */
