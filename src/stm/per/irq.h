/** *****************************************************************************************
 * Unit in charge:
 * @file	irq.h
 * @author	Hauke Petersen <mail@haukepetersen.de>
 * $Date:: 2014-05-08 18:05:37 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 449                   $
 * $Author:: wandji-kwuntchou   $
 * 
 * ----------------------------------------------------------
 *
 * @brief 	Abstraction layer for the controllers interrupt system.
 *
 ******************************************************************************************/


#ifndef IRQ_H_
#define IRQ_H_

/************* Includes *******************************************************************/


/************* Public typedefs ************************************************************/


/************* Macros and constants *******************************************************/


/************* Public function prototypes *************************************************/
/**
 * @brief 	Globally enable maskable interrupts
 */
void irq_enable(void);

/**
 * @brief	Globally disable maskable interrupts
 */
void irq_disable(void);


#endif /* IRQ_H_ */
