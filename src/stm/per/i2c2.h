/********************************************************************************************
 * Unit in charge:
 * @file    i2c2.h
 * @author  ledwig
 * $Date:: 2015-04-07 14:30:00 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1131                  $
 * $Author:: ledwig				$
 *
 * ----------------------------------------------------------
 *
 * @brief 	This is the I2C2 peripheral driver, based on interrupt communication.
 *			original source code: http://www.diller-technologies.de/stm32.html#i2c (modified)
 *
 ******************************************************************************************/

#ifndef I2C_H_
#define I2C_H_

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "hwallocation.h"

/************* Public typedefs ************************************************************/
/************* Macros and constants *******************************************************/
/************* Public function prototypes *************************************************/

/**
 * @brief	initializes the i2c bus unit. configures certain gpio pins and enables interrupt sources.
 *
 * @return  	  none
 */
void i2c2_init(void);

void i2c2_writeByte(uint8_t address, uint8_t byte0);

/**
 * @brief	starts the i2c bus communication as master transmitter and sends 2 data bytes to i2c slave.
 *
 * @param[in] 	  uint8_t address: i2c slave address (needs to be shifted left, because of 7 bit range).
 * @param[in] 	  uint8_t byte1: first byte to send
 * @param[in] 	  uint8_t byte0: second byte to send
 *
 * @return  	  none
 */
void i2c2_writeTwoBytes(uint8_t address, uint8_t byte1, uint8_t byte0);

uint8_t i2c2_readByte(uint8_t address);
uint16_t i2c2_readTwoBytes(uint8_t address);

/**
 * @brief	starts the i2c bus communication as master receiver and reads 4 bytes from i2c slave.
 *
 * @param[in] 	  uint8_t address: i2c slave address (needs to be shifted left, because of 7 bit range).
 *
 * @return  	  uint32_t data read from i2c slave
 */
uint32_t i2c2_readFourBytes(uint8_t address);

#endif /* I2C_H_ */

