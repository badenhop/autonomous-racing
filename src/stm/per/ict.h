
/** *****************************************************************************************
 * Unit in charge:
 * @file	ict.h
 * @author	ledwig
 * $Date:: 2015-02-19 14:03:20 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1312                  $
 * $Author:: neumerkel          $
 * 
 * ----------------------------------------------------------
 *
 * @brief This unit configures an input capture timer triggering on the edges of the rc signal.
 *
 ******************************************************************************************/


#ifndef ICT_H_
#define ICT_H_

/************* Includes *******************************************************************/

/************* Public typedefs ************************************************************/
typedef struct {
	void(*ISR)(void);
}ict_t;
/************* Macros and constants *******************************************************/
/************* Public function prototypes *************************************************/
void ict_init(ict_t* initIct);

#endif /* ICT_H_ */
