/** *****************************************************************************************
 * Unit in charge:
 * @file	clocksync.c
 * @author	Malte Brieske
 * $Date:: 2017-06-08 09:32:32 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 585                   $
 * $Author:: mbrieske   $
 *
 * ----------------------------------------------------------
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/

#include "per/clocksync.h"
#include "app/leddebug.h"

/************* Private typedefs ***********************************************************/


/************* Macros and constants *******************************************************/


/************* Private static variables ***************************************************/

uint32_t *pSyncOffset_clocksync;

/************* Private function prototypes ************************************************/


/************* Public functions ***********************************************************/

int8_t clocksync_initDevice(void)
{
	*pSyncOffset_clocksync = 0;

	GPIO_InitTypeDef GPIO_InitStruct;
	EXTI_InitTypeDef EXTI_InitStruct;
	NVIC_InitTypeDef NVIC_InitStruct;

	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_SYSCFG, ENABLE);

	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_IN;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_13;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_Init(GPIOC, &GPIO_InitStruct);

	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOC, EXTI_PinSource13);

	EXTI_InitStruct.EXTI_Line = EXTI_Line13;
	EXTI_InitStruct.EXTI_LineCmd = ENABLE;
	EXTI_InitStruct.EXTI_Mode = EXTI_Mode_Interrupt;
	EXTI_InitStruct.EXTI_Trigger = EXTI_Trigger_Rising;
	EXTI_Init(&EXTI_InitStruct);

	NVIC_InitStruct.NVIC_IRQChannel = EXTI15_10_IRQn;
	NVIC_InitStruct.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_InitStruct.NVIC_IRQChannelCmd = ENABLE;
	NVIC_Init(&NVIC_InitStruct);
	return 0;
}

void EXTI15_10_IRQHandler(void) {
    /* Make sure that interrupt flag is set */
    if (EXTI_GetITStatus(EXTI_Line13) != RESET) {
    	*pSyncOffset_clocksync = systime_getSysTime();

        EXTI_ClearITPendingBit(EXTI_Line13);
    }
}
