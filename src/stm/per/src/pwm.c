/** *****************************************************************************************
 * Unit in charge:
 * @file	pwm.c
 * @author	wandji-kwuntchou
 * $Date:: 2015-02-19 14:03:20 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1312                  $
 * $Author:: neumerkel          $
 * 
 * ----------------------------------------------------------
 *
 * @brief The module generates a PWM signal to the selected channel. It Provides function to initialize the PWM channels wherein the PWM-frequency is choose.
 *
 ******************************************************************************************/


/************* Includes *******************************************************************/
#include "per/pwm.h"
#include "app/config.h"


#ifdef PWM_EN

/************* Macros and constants *******************************************************/


/************* Private static variables ***************************************************/
static uint8_t pwmChanDisable[4] = {1, 1, 1, 1};

/************* Private function prototypes ************************************************/


/************* Public functions ***********************************************************/
int8_t pwm_init(pwm_t* pPwm)
{

	uint16_t prescalerTemp = 0;
	GPIO_InitTypeDef GPIO_initStructure;
	TIM_TimeBaseInitTypeDef TIM_TimeBaseinitStructure;
	TIM_OCInitTypeDef TIM_OCInitStructure;
	RCC_ClocksTypeDef RCC_Clocks;

	/* Activate the clock for GPIOB Pin 0 */
	RCC_AHB1PeriphClockCmd(PWM_PIN_CLOCK, ENABLE);

	/* Initialize the four GPIO pins for timer 3 */
	GPIO_initStructure.GPIO_Pin     = PWM_CHAN_1_PIN | PWM_CHAN_2_PIN;
//									  PWM_CHAN_3_PIN;
//									  PWM_CHAN_4_PIN;

	GPIO_initStructure.GPIO_Mode 	= GPIO_Mode_AF;
	GPIO_initStructure.GPIO_OType 	= GPIO_OType_PP;
	GPIO_initStructure.GPIO_PuPd 	= GPIO_PuPd_UP;
	GPIO_initStructure.GPIO_Speed 	= GPIO_Speed_100MHz;

	GPIO_Init(PWM_PORT, &GPIO_initStructure);

	GPIO_PinAFConfig(PWM_PORT, PWM_CHAN_1_AF_PIN, PWM_CHAN_1_AF);
	GPIO_PinAFConfig(PWM_PORT, PWM_CHAN_2_AF_PIN, PWM_CHAN_2_AF);
//	GPIO_PinAFConfig(PWM_PORT, PWM_CHAN_3_AF_PIN, PWM_CHAN_3_AF);
//	GPIO_PinAFConfig(PWM_PORT, PWM_CHAN_4_AF_PIN, PWM_CHAN_4_AF);

	/* Get the clock frequency 84 Mhz */
	RCC_GetClocksFreq(&RCC_Clocks);

	/* Activate the clock for PWM_TIMER */
	RCC_APB2PeriphClockCmd(PWM_TIM_CLOCK, ENABLE);
	if (!pPwm->flexOfFrequency) {
		pPwm->resolution = 10000;
		/* Time Base configuration */
		TIM_TimeBaseinitStructure.TIM_Period 		     = pPwm->resolution-1;
		prescalerTemp                                    = RCC_Clocks.PCLK2_Frequency/(pPwm->resolution * pPwm->frequency);
		TIM_TimeBaseinitStructure.TIM_Prescaler 	     = prescalerTemp-1;
		TIM_TimeBaseinitStructure.TIM_ClockDivision      = TIM_CKD_DIV1;
		TIM_TimeBaseinitStructure.TIM_RepetitionCounter  = 0;
	}
	else if (pPwm->flexOfFrequency) {
		if (pPwm->frequency >= 500 && pPwm->frequency < 1300 ) {
			TIM_TimeBaseinitStructure.TIM_Prescaler = 7;
		}
		else if (pPwm->frequency >= 100 && pPwm->frequency < 500 ) {
			TIM_TimeBaseinitStructure.TIM_Prescaler = 15;
		}
		else if (pPwm->frequency >= 50 && pPwm->frequency < 100 ) {
			TIM_TimeBaseinitStructure.TIM_Prescaler = 31;
		}
		else if (pPwm->frequency >= 1300 && pPwm->frequency <= 20000) {
			TIM_TimeBaseinitStructure.TIM_Prescaler = 0;
		}
		else return PWM_INIT_FAILED;
		pPwm->resolution                              = RCC_Clocks.PCLK2_Frequency/(pPwm->frequency*(TIM_TimeBaseinitStructure.TIM_Prescaler +1));
		TIM_TimeBaseinitStructure.TIM_Period 		= pPwm->resolution-1;
		TIM_TimeBaseinitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	}
	switch (pPwm->mode) {
			break;
		case leftaligned:
			TIM_TimeBaseinitStructure.TIM_CounterMode 	    = TIM_CounterMode_CenterAligned1;
			break;
		case rightaligned:
			TIM_TimeBaseinitStructure.TIM_CounterMode 	    = TIM_CounterMode_CenterAligned2;
			break;
		case centeraligned:
				TIM_TimeBaseinitStructure.TIM_CounterMode 	= TIM_CounterMode_CenterAligned3;
				break;
		default:
			return PWM_INIT_FAILED;
	}
	TIM_TimeBaseInit(PWM_TIMER, &TIM_TimeBaseinitStructure);

	/* Output configuration */
	TIM_OCInitStructure.TIM_OCMode 		  = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState   = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_OCPolarity 	  = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_Pulse         = 0;
	TIM_OCInitStructure.TIM_OutputNState  = TIM_OutputNState_Disable;
	TIM_OCInitStructure.TIM_OCNPolarity   = TIM_OCPolarity_High;
	TIM_OCInitStructure.TIM_OCIdleState   = TIM_OCIdleState_Reset;
	TIM_OCInitStructure.TIM_OCNIdleState  = TIM_OCIdleState_Reset;

	TIM_OC3Init(PWM_TIMER, &TIM_OCInitStructure);
	TIM_OC3PreloadConfig(PWM_TIMER,TIM_OCPreload_Enable);

	TIM_OC4Init(PWM_TIMER, &TIM_OCInitStructure);
	TIM_OC4PreloadConfig(PWM_TIMER,TIM_OCPreload_Enable);

//	TIM_OC3Init(PWM_TIMER, &TIM_OCInitStructure);
//	TIM_OC3PreloadConfig(PWM_TIMER,TIM_OCPreload_Enable);

//	TIM_OC4Init(PWM_TIMER, &TIM_OCInitStructure);
//	TIM_OC4PreloadConfig(PWM_TIMER,TIM_OCPreload_Enable);

	/* PWM Timer enable */
	TIM_Cmd(PWM_TIMER,ENABLE);
	TIM_CtrlPWMOutputs(PWM_TIMER, ENABLE);

	/* PWM_TIMER as trigger output for another peripheries */
	//TIM_SelectOutputTrigger(PWM_TIMER, TIM_TRGOSource_Update);

	/* Enables TIM update interrupts */
	//TIM_ITConfig(PWM_TIMER, TIM_IT_Update, ENABLE);
	return 0;
}


int8_t pwm_setDutyCycle(pwm_t* pPwm, pwm_ch_t pwmChannel, float32_t dutyCycle)
{
	switch (pwmChannel) {
#ifdef PWM_CH1_EN
		case PWM_CH1:
			if(pwmChanDisable[0]) {
				pPwm->saveDutyCycle[0] = dutyCycle*pPwm->resolution;
			}
			else {
				TIM_SetCompare3(PWM_TIMER,dutyCycle*pPwm->resolution);
				pPwm->saveDutyCycle[0] = dutyCycle*pPwm->resolution;
			}
			break;
#endif
#ifdef PWM_CH2_EN
		case PWM_CH2:
			if(pwmChanDisable[1]) {
				pPwm->saveDutyCycle[0] = dutyCycle*pPwm->resolution;
			}
			else {
				TIM_SetCompare4(PWM_TIMER,dutyCycle*pPwm->resolution);
				pPwm->saveDutyCycle[1] = dutyCycle*pPwm->resolution;
			}
			break;
#endif
#ifdef PWM_CH3_EN
		case PWM_CH3:
			TIM_SetCompare3(PWM_TIMER,dutyCycle*pPwm->resolution);
			pPwm->saveDutyCycle[2] = dutyCycle*pPwm->resolution;
			break;
#endif
#ifdef PWM_CH4_EN
		case PWM_CH4:
			TIM_SetCompare4(PWM_TIMER,dutyCycle*pPwm->resolution);
			pPwm->saveDutyCycle[3] = dutyCycle*pPwm->resolution;
			break;
#endif
		default:
		return PWM_SETDUTYCYCLE_FAILED;
	}
	return 0;
}

int8_t pwm_enable(pwm_t* pPwm, pwm_ch_t pwmChannel)
{
	switch(pwmChannel) {
#ifdef PWM_CH1_EN
	case PWM_CH1:
		pwmChanDisable[0] = 0;
		TIM_SetCompare3(PWM_TIMER, (pPwm->saveDutyCycle[0]));
		break;
#endif
#ifdef PWM_CH2_EN
	case PWM_CH2:
		pwmChanDisable[1] = 0;
		TIM_SetCompare4(PWM_TIMER, (pPwm->saveDutyCycle[1]));
		break;
#endif
#ifdef PWM_CH3_EN
	case PWM_CH3:
		TIM_SetCompare3(PWM_TIMER, (pPwm->saveDutyCycle[2]));
		break;
#endif
#ifdef PWM_CH4_EN
	case PWM_CH4:
		TIM_SetCompare4(PWM_TIMER, (pPwm->saveDutyCycle[3]));
		break;
#endif
	default:
		return PWM_ENABLE_FAILED;
	}

	return 0;
}

int8_t pwm_disable(pwm_ch_t pwmChannel)
{
	switch(pwmChannel) {
#ifdef PWM_CH1_EN
	case PWM_CH1:
		pwmChanDisable[0] = 1;
		TIM_SetCompare3(PWM_TIMER, 0);
		break;
#endif
#ifdef PWM_CH2_EN
	case PWM_CH2:
		pwmChanDisable[1] = 1;
		TIM_SetCompare4(PWM_TIMER, 0);
		break;
#endif
#ifdef PWM_CH3_EN
	case PWM_CH3:
		TIM_SetCompare3(PWM_TIMER, 0);
		break;
#endif
#ifdef PWM_CH4_EN
	case PWM_CH4:
		TIM_SetCompare4(PWM_TIMER, 0);
		break;
#endif
	default:
		return PWM_DISABLE_FAILED;
	}

	return 0;
}

#endif /* PWM_EN*/
