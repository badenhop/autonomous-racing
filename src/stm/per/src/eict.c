/** *****************************************************************************************
 * Unit in charge:
 * @file	eict.h
 * @author	Yoga
 * $Date:: 2015-05-05 13:31:20 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev::                   $
 * $Author::           $
 *
 * ----------------------------------------------------------
 *
 * @brief This unit configures an input capture timer triggering on the both edges of the wheel encoder signal.
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "app/config.h"
#include "per/eict.h"
#include "per/irq.h"


/************* Private typedefs ***********************************************************/
/************* Macros and constants *******************************************************/
#define SR_INC_MAX       100

/************* Globale Variable *******************************************************/

/************* Private static variables ***************************************************/
static uint16_t frontLeftCapture = 0;
static uint16_t frontRightCapture = 0;
static uint16_t rearLeftCapture = 0;
static uint16_t rearRightCapture = 0;

/************* Private function prototypes ************************************************/
static void gpio_config(void);

static void nvic_config(void);

/************* Public functions ***********************************************************/
void eict_init(void)
{

#ifdef WENC_V3
	uint16_t polarity = TIM_ICPolarity_Falling;
#else
	uint16_t polarity = TIM_ICPolarity_Rising;
#endif

	TIM_ICInitTypeDef TIM_ICStructure;

	/* GPIO configuration */
	gpio_config();

	/* nvic configuration */
	nvic_config();

#ifdef FOUR_WHEEL_ODOM
	/* TIMER3 channel 1(PB4) Configuration:  Input Capture mode */
	TIM_ICStructure.TIM_Channel     = TIM_Channel_1;
	TIM_ICStructure.TIM_ICFilter    = 0x0;
	TIM_ICStructure.TIM_ICPolarity  = polarity;    /* rising and falling edges are used as active edge */
	TIM_ICStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;			  /* triggering on each edge */
	TIM_ICStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInit(WENC_FRONT_TIMER, &TIM_ICStructure);

	/* TIMER3 channel 2(PB5) Configuration:  Input Capture mode */
	TIM_ICStructure.TIM_Channel     = TIM_Channel_2;
	TIM_ICStructure.TIM_ICFilter    = 0x0;
	TIM_ICStructure.TIM_ICPolarity  = polarity;    /* rising and falling edges are used as active edge */
	TIM_ICStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;			  /* triggering on each edge */
	TIM_ICStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInit(WENC_FRONT_TIMER, &TIM_ICStructure);
#endif

	/* TIMER2 channel 4(PB11) Configuration:  Input Capture mode */
	TIM_ICStructure.TIM_Channel     = TIM_Channel_4;
	TIM_ICStructure.TIM_ICFilter    = 0x0;
	TIM_ICStructure.TIM_ICPolarity  = polarity;    /* rising and falling edges are used as active edge */
	TIM_ICStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;			  /* triggering on each edge */
	TIM_ICStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInit(WENC_REAR_TIMER, &TIM_ICStructure);

	/* TIMER2 channel 2(PB3) Configuration:  Input Capture mode */
	TIM_ICStructure.TIM_Channel     = TIM_Channel_2;
	TIM_ICStructure.TIM_ICFilter    = 0x0;
	TIM_ICStructure.TIM_ICPolarity  = polarity;    /* rising and falling edges are used as active edge */
	TIM_ICStructure.TIM_ICPrescaler = TIM_ICPSC_DIV1;			  /* triggering on each edge */
	TIM_ICStructure.TIM_ICSelection = TIM_ICSelection_DirectTI;
	TIM_ICInit(WENC_REAR_TIMER, &TIM_ICStructure);
#ifdef FOUR_WHEEL_ODOM
    /* TIMER3 enable counter */
	TIM_Cmd (WENC_FRONT_TIMER, ENABLE);

	/* Timer 3 enable the CC Interrupt request */
	TIM_ITConfig(WENC_FRONT_TIMER, TIM_IT_CC1 | TIM_IT_CC2, ENABLE);

	/* Timer 3 clear CC Flag */
	TIM_ClearFlag(WENC_FRONT_TIMER, TIM_IT_CC1 | TIM_IT_CC2);
#endif

	 /* TIMER2 enable counter */
	TIM_Cmd (WENC_REAR_TIMER, ENABLE);

	/* Timer 2 enable the CC Interrupt request */
	TIM_ITConfig(WENC_REAR_TIMER,TIM_IT_CC4 | TIM_IT_CC2, ENABLE);

	/* Timer 2 clear CC Flag */
	TIM_ClearFlag(WENC_REAR_TIMER, TIM_IT_CC4 | TIM_IT_CC2);
}

void eict_getCounter(uint16_t *frontLeftEnc, uint16_t *frontRightEnc, uint16_t *rearLeftEnc, uint16_t *rearRightEnc)
{

	irq_disable();

	*frontLeftEnc  = frontLeftCapture;
	*frontRightEnc = frontRightCapture;
	*rearLeftEnc   = rearLeftCapture;
	*rearRightEnc  = rearRightCapture;

	/* clear capture Counter */
	frontLeftCapture = 0;
	frontRightCapture = 0;
	rearLeftCapture = 0;
	rearRightCapture = 0;

	irq_enable();
}

#ifdef FOUR_WHEEL_ODOM
void TIM3_IRQHandler()
{
	if(TIM_GetITStatus(WENC_FRONT_TIMER, TIM_IT_CC1)) {
		TIM_ClearITPendingBit(WENC_FRONT_TIMER, TIM_IT_CC1);
		frontLeftCapture++;

	}
	if(TIM_GetITStatus(WENC_FRONT_TIMER, TIM_IT_CC2)) {
		TIM_ClearITPendingBit(WENC_FRONT_TIMER, TIM_IT_CC2);
		frontRightCapture++;
	}
}
#endif

void TIM2_IRQHandler()
{
	if(TIM_GetITStatus(WENC_REAR_TIMER, TIM_IT_CC4)) {
		TIM_ClearITPendingBit(WENC_REAR_TIMER, TIM_IT_CC4);
		rearLeftCapture++;
	}
	if(TIM_GetITStatus(WENC_REAR_TIMER, TIM_IT_CC2)) {
		TIM_ClearITPendingBit(WENC_REAR_TIMER, TIM_IT_CC2);
		rearRightCapture++;
	}
}

/************* Private function ************************************************/
static void gpio_config(void)
{

	GPIO_InitTypeDef GPIO_InitStructure;

	/* GPIOB clock enable */
	RCC_AHB1PeriphClockCmd(WENC_PIN_CLOCK, ENABLE);

	/* Pin configuration for Timer 2 and 3 */
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Pin = WENC_RR_CHAN_2_PIN | WENC_FL_CHAN_1_PIN | WENC_FR_CHAN_2_PIN | WENC_RL_CHAN_4_PIN;
	GPIO_Init(WENC_PORT, &GPIO_InitStructure);

	/* Configure the the GPIO pin as alternate function */
	GPIO_PinAFConfig(WENC_PORT, WENC_FL_CHAN_1_AF_PIN, WENC_FL_CHAN_1_AF);
	GPIO_PinAFConfig(WENC_PORT, WENC_FR_CHAN_2_AF_PIN, WENC_FR_CHAN_2_AF);
	GPIO_PinAFConfig(WENC_PORT, WENC_RL_CHAN_4_AF_PIN, WENC_RL_CHAN_4_AF);
	GPIO_PinAFConfig(WENC_PORT, WENC_RR_CHAN_2_AF_PIN, WENC_RR_CHAN_2_AF);
}

static void nvic_config(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;

	/* Timer 2 peripheral clock enable */
	RCC_APB1PeriphClockCmd (WENC_REAR_TIM_CLOCK, ENABLE);

	/* Timer 3 peripheral clock enable */
	RCC_APB1PeriphClockCmd (WENC_FRONT_TIM_CLOCK, ENABLE);

#ifdef FOUR_WHEEL_ODOM
	/* enable the TIM3 global Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x01;
	NVIC_Init(&NVIC_InitStructure);
#endif

	/* enable the TIM2 global Interrupt */
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x00;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x00;
	NVIC_Init(&NVIC_InitStructure);
}

