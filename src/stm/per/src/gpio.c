/** *****************************************************************************************
 * Unit in charge:
 * @file	gpio.c
 * @author	Bjoern Hepner
 * $Date:: 2016-09-02 15:05:56 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2022                  $
 * $Author:: Yoga               $
 * 
 * ----------------------------------------------------------
 *
 * only GPIO port A pins 0-7 initialization
 *
 * Example Code for using gpio as output pin.
 * @code
 *	gpio_t gpio;
 *	gpio.mode = GPIO_MODE_OUT;
 *	gpio.channel  = GPIO_CHAN_1;
 *	gpio_init(&gpio);
 *
 * 	gpio_writeBit(&gpio, 1);
 *
 * 	if(gpio_readBit(&gpio) == SET) {
 * 		gpio_toggleBit(&gpio);
 * 	}
 * @endcode
 *
 ******************************************************************************************/

 /************* Includes *******************************************************************/
#include "per/gpio.h"

/************* Private typedefs ***********************************************************/


/************* Macros and constants *******************************************************/
/**
 * \defgroup errMess gpio error messages
 */
/*@{*/
#define GPIO_MODE_FAILED 		-1	/**< It has not been selected mode for the pin. */
#define GPIO_PIN_FAILED  		-2	/**< Initialization of the pins failed. */
#define GPIO_WRITE_BIT_FAILED 	-3	/**< Writing a bit is failed. */
#define GPIO_READ_BIT_FAILED	-4	/**< Reading a bit is failed. */
#define GPIO_SET_BIT_FAILED 	-5	/**< Set a bit is failed. */
#define GPIO_CLEAR_BIT_FAILED 	-6	/**< Clear a bit is failed. */
#define GPIO_TOGGLE_BIT_FAILED	-7	/**< Toggle a bit is failed. */
/*@}*/

/************* Private static variables ***************************************************/


/************* Private function prototypes ************************************************/


/************* Public functions ***********************************************************/
int8_t gpio_init(gpio_t *pGpio)
{
	GPIO_InitTypeDef GPIO_InitStructure;

	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;

	switch (pGpio->mode) {
	case GPIO_MODE_IN:
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
		break;
	case GPIO_MODE_OUT:
		GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
		break;
	default:
		return GPIO_MODE_FAILED;
	}

	switch (pGpio->channel) {
	case GPIO_CHAN_0:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_0_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_0_RCC, ENABLE);
		GPIO_Init(GPIO_CH_0_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_1:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_1_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_1_RCC, ENABLE);
		GPIO_Init(GPIO_CH_1_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_2:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_2_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_2_RCC, ENABLE);
		GPIO_Init(GPIO_CH_2_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_3:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_3_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_3_RCC, ENABLE);
		GPIO_Init(GPIO_CH_3_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_4:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_4_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_4_RCC, ENABLE);
		GPIO_Init(GPIO_CH_4_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_5:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_5_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_5_RCC, ENABLE);
		GPIO_Init(GPIO_CH_5_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_6:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_6_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_6_RCC, ENABLE);
		GPIO_Init(GPIO_CH_6_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_7:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_7_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_7_RCC, ENABLE);
		GPIO_Init(GPIO_CH_7_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_8:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_8_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_8_RCC, ENABLE);
		GPIO_Init(GPIO_CH_8_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_9:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_9_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_9_RCC, ENABLE);
		GPIO_Init(GPIO_CH_9_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_10:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_10_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_10_RCC, ENABLE);
		GPIO_Init(GPIO_CH_10_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_12:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_12_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_12_RCC, ENABLE);
		GPIO_Init(GPIO_CH_12_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_13:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_13_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_13_RCC, ENABLE);
		GPIO_Init(GPIO_CH_13_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_14:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_14_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_14_RCC, ENABLE);
		GPIO_Init(GPIO_CH_14_PORT, &GPIO_InitStructure);
		break;
	case GPIO_CHAN_15:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_15_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_15_RCC, ENABLE);
		GPIO_Init(GPIO_CH_15_PORT, &GPIO_InitStructure);
		break;
#ifdef Board
	case GPIO_CHAN_15:
		GPIO_InitStructure.GPIO_Pin = GPIO_CH_15_PIN;
		RCC_AHB1PeriphClockCmd(GPIO_CH_15_RCC, ENABLE);
		GPIO_Init(GPIO_CH_15_PORT, &GPIO_InitStructure);
		break;
#endif
	default:
		return GPIO_PIN_FAILED;
	}

	return 0;
}


int8_t gpio_writeBit(gpio_t *pGpio, uint8_t bit)
{
	switch (pGpio->channel) {
	case GPIO_CHAN_0:
		GPIO_WriteBit(GPIO_CH_0_PORT, GPIO_CH_0_PIN, bit);
		break;
	case GPIO_CHAN_1:
		GPIO_WriteBit(GPIO_CH_1_PORT, GPIO_CH_1_PIN, bit);
		break;
	case GPIO_CHAN_2:
		GPIO_WriteBit(GPIO_CH_2_PORT, GPIO_CH_2_PIN, bit);
		break;
	case GPIO_CHAN_3:
		GPIO_WriteBit(GPIO_CH_3_PORT, GPIO_CH_3_PIN, bit);
		break;
	case GPIO_CHAN_4:
		GPIO_WriteBit(GPIO_CH_4_PORT, GPIO_CH_4_PIN, bit);
		break;
	case GPIO_CHAN_5:
		GPIO_WriteBit(GPIO_CH_5_PORT, GPIO_CH_5_PIN, bit);
		break;
	case GPIO_CHAN_6:
		GPIO_WriteBit(GPIO_CH_6_PORT, GPIO_CH_6_PIN, bit);
		break;
	case GPIO_CHAN_7:
		GPIO_WriteBit(GPIO_CH_7_PORT, GPIO_CH_7_PIN, bit);
		break;
	case GPIO_CHAN_8:
		GPIO_WriteBit(GPIO_CH_8_PORT, GPIO_CH_8_PIN, bit);
		break;
	case GPIO_CHAN_9:
		GPIO_WriteBit(GPIO_CH_9_PORT, GPIO_CH_9_PIN, bit);
		break;
	case GPIO_CHAN_10:
		GPIO_WriteBit(GPIO_CH_10_PORT, GPIO_CH_10_PIN, bit);
		break;
	case GPIO_CHAN_12:
		GPIO_WriteBit(GPIO_CH_12_PORT, GPIO_CH_12_PIN, bit);
		break;
	case GPIO_CHAN_13:
		GPIO_WriteBit(GPIO_CH_13_PORT, GPIO_CH_13_PIN, bit);
		break;
	case GPIO_CHAN_14:
		GPIO_WriteBit(GPIO_CH_14_PORT, GPIO_CH_14_PIN, bit);
		break;
	case GPIO_CHAN_15:
		GPIO_WriteBit(GPIO_CH_15_PORT, GPIO_CH_15_PIN, bit);
		break;

#ifdef Board
	case GPIO_CHAN_15:
		GPIO_WriteBit(GPIO_CH_15_PORT, GPIO_CH_15_PIN, bit);
		break;
#endif
	default:
		return GPIO_WRITE_BIT_FAILED;
	}

	return GPIO_STATE_ON;
}


int8_t gpio_readBit(gpio_t *pGpio)
{
	switch (pGpio->channel) {
	case GPIO_CHAN_0:
		return GPIO_ReadInputDataBit(GPIO_CH_0_PORT, GPIO_CH_0_PIN);
	case GPIO_CHAN_1:
		return GPIO_ReadInputDataBit(GPIO_CH_1_PORT, GPIO_CH_1_PIN);
	case GPIO_CHAN_2:
		return GPIO_ReadInputDataBit(GPIO_CH_2_PORT, GPIO_CH_2_PIN);
	case GPIO_CHAN_3:
		return GPIO_ReadInputDataBit(GPIO_CH_3_PORT, GPIO_CH_3_PIN);
	case GPIO_CHAN_4:
		return GPIO_ReadInputDataBit(GPIO_CH_4_PORT, GPIO_CH_4_PIN);
	case GPIO_CHAN_5:
		return GPIO_ReadInputDataBit(GPIO_CH_5_PORT, GPIO_CH_5_PIN);
	case GPIO_CHAN_6:
		return GPIO_ReadInputDataBit(GPIO_CH_6_PORT, GPIO_CH_6_PIN);
	case GPIO_CHAN_7:
		return GPIO_ReadInputDataBit(GPIO_CH_7_PORT, GPIO_CH_7_PIN);
	case GPIO_CHAN_8:
		return GPIO_ReadInputDataBit(GPIO_CH_8_PORT, GPIO_CH_8_PIN);
	case GPIO_CHAN_9:
		return GPIO_ReadInputDataBit(GPIO_CH_9_PORT, GPIO_CH_9_PIN);
	case GPIO_CHAN_10:
		return GPIO_ReadInputDataBit(GPIO_CH_10_PORT, GPIO_CH_10_PIN);
	case GPIO_CHAN_12:
		return GPIO_ReadInputDataBit(GPIO_CH_12_PORT, GPIO_CH_12_PIN);
	case GPIO_CHAN_13:
		return GPIO_ReadInputDataBit(GPIO_CH_13_PORT, GPIO_CH_13_PIN);
	case GPIO_CHAN_14:
		return GPIO_ReadInputDataBit(GPIO_CH_14_PORT, GPIO_CH_14_PIN);
	case GPIO_CHAN_15:
		return GPIO_ReadInputDataBit(GPIO_CH_15_PORT, GPIO_CH_15_PIN);

#ifdef Board
	case GPIO_CHAN_15:
		return GPIO_ReadInputDataBit(GPIO_CH_15_PORT, GPIO_CH_15_PIN);
#endif
	default:
		return GPIO_READ_BIT_FAILED;
	}
}


int8_t gpio_setBit(gpio_t *pGpio)
{
	switch (pGpio->channel) {
		case GPIO_CHAN_0:
			GPIO_SetBits(GPIO_CH_0_PORT, GPIO_CH_0_PIN);
			break;
		case GPIO_CHAN_1:
			GPIO_SetBits(GPIO_CH_1_PORT, GPIO_CH_1_PIN);
			break;
		case GPIO_CHAN_2:
			GPIO_SetBits(GPIO_CH_2_PORT, GPIO_CH_2_PIN);
			break;
		case GPIO_CHAN_3:
			GPIO_SetBits(GPIO_CH_3_PORT, GPIO_CH_3_PIN);
			break;
		case GPIO_CHAN_4:
			GPIO_SetBits(GPIO_CH_4_PORT, GPIO_CH_4_PIN);
			break;
		case GPIO_CHAN_5:
			GPIO_SetBits(GPIO_CH_5_PORT, GPIO_CH_5_PIN);
			break;
		case GPIO_CHAN_6:
			GPIO_SetBits(GPIO_CH_6_PORT, GPIO_CH_6_PIN);
			break;
		case GPIO_CHAN_7:
			GPIO_SetBits(GPIO_CH_7_PORT, GPIO_CH_7_PIN);
			break;
		case GPIO_CHAN_8:
			GPIO_SetBits(GPIO_CH_8_PORT, GPIO_CH_8_PIN);
			break;
		case GPIO_CHAN_9:
			GPIO_SetBits(GPIO_CH_9_PORT, GPIO_CH_9_PIN);
			break;
		case GPIO_CHAN_10:
			GPIO_SetBits(GPIO_CH_10_PORT, GPIO_CH_10_PIN);
			break;
		case GPIO_CHAN_12:
			GPIO_SetBits(GPIO_CH_12_PORT, GPIO_CH_12_PIN);
			break;
		case GPIO_CHAN_13:
			GPIO_SetBits(GPIO_CH_13_PORT, GPIO_CH_13_PIN);
			break;
		case GPIO_CHAN_14:
			GPIO_SetBits(GPIO_CH_14_PORT, GPIO_CH_14_PIN);
			break;
		case GPIO_CHAN_15:
			GPIO_SetBits(GPIO_CH_15_PORT, GPIO_CH_15_PIN);
			break;

#ifdef Board
		case GPIO_CHAN_15:
			GPIO_SetBits(GPIO_CH_15_PORT, GPIO_CH_15_PIN);
			break;
#endif
		default:
			return GPIO_SET_BIT_FAILED;
			break;
		}
	return 1;
}


int8_t gpio_clearBit(gpio_t *pGpio)
{
	switch (pGpio->channel) {
		case GPIO_CHAN_0:
			GPIO_ResetBits(GPIO_CH_0_PORT, GPIO_CH_0_PIN);
			break;
		case GPIO_CHAN_1:
			GPIO_ResetBits(GPIO_CH_1_PORT, GPIO_CH_1_PIN);
			break;
		case GPIO_CHAN_2:
			GPIO_ResetBits(GPIO_CH_2_PORT, GPIO_CH_2_PIN);
			break;
		case GPIO_CHAN_3:
			GPIO_ResetBits(GPIO_CH_3_PORT, GPIO_CH_3_PIN);
			break;
		case GPIO_CHAN_4:
			GPIO_ResetBits(GPIO_CH_4_PORT, GPIO_CH_4_PIN);
			break;
		case GPIO_CHAN_5:
			GPIO_ResetBits(GPIO_CH_5_PORT, GPIO_CH_5_PIN);
			break;
		case GPIO_CHAN_6:
			GPIO_ResetBits(GPIO_CH_6_PORT, GPIO_CH_6_PIN);
			break;
		case GPIO_CHAN_7:
			GPIO_ResetBits(GPIO_CH_7_PORT, GPIO_CH_7_PIN);
			break;
		case GPIO_CHAN_8:
			GPIO_ResetBits(GPIO_CH_8_PORT, GPIO_CH_8_PIN);
			break;
		case GPIO_CHAN_9:
			GPIO_ResetBits(GPIO_CH_9_PORT, GPIO_CH_9_PIN);
			break;
		case GPIO_CHAN_10:
			GPIO_ResetBits(GPIO_CH_10_PORT, GPIO_CH_10_PIN);
			break;
		case GPIO_CHAN_12:
			GPIO_ResetBits(GPIO_CH_12_PORT, GPIO_CH_12_PIN);
			break;
		case GPIO_CHAN_13:
			GPIO_ResetBits(GPIO_CH_13_PORT, GPIO_CH_13_PIN);
			break;
		case GPIO_CHAN_14:
			GPIO_ResetBits(GPIO_CH_14_PORT, GPIO_CH_14_PIN);
			break;
		case GPIO_CHAN_15:
			GPIO_ResetBits(GPIO_CH_15_PORT, GPIO_CH_15_PIN);
			break;

#ifdef Board
		case GPIO_CHAN_15:
			GPIO_ResetBits(GPIO_CH_15_PORT, GPIO_CH_15_PIN);
			break;
#endif
		default:
			return GPIO_CLEAR_BIT_FAILED;
		}
	return 0;
}


int8_t gpio_toggleBit(gpio_t *pGpio)
{
	switch (pGpio->channel) {
		case GPIO_CHAN_0:
			GPIO_ToggleBits(GPIO_CH_0_PORT, GPIO_CH_0_PIN);
			break;
		case GPIO_CHAN_1:
			GPIO_ToggleBits(GPIO_CH_1_PORT, GPIO_CH_1_PIN);
			break;
		case GPIO_CHAN_2:
			GPIO_ToggleBits(GPIO_CH_2_PORT, GPIO_CH_2_PIN);
			break;
		case GPIO_CHAN_3:
			GPIO_ToggleBits(GPIO_CH_3_PORT, GPIO_CH_3_PIN);
			break;
		case GPIO_CHAN_4:
			GPIO_ToggleBits(GPIO_CH_4_PORT, GPIO_CH_4_PIN);
			break;
		case GPIO_CHAN_5:
			GPIO_ToggleBits(GPIO_CH_5_PORT, GPIO_CH_5_PIN);
			break;
		case GPIO_CHAN_6:
			GPIO_ToggleBits(GPIO_CH_6_PORT, GPIO_CH_6_PIN);
			break;
		case GPIO_CHAN_7:
			GPIO_ToggleBits(GPIO_CH_7_PORT, GPIO_CH_7_PIN);
			break;
		case GPIO_CHAN_8:
			GPIO_ToggleBits(GPIO_CH_8_PORT, GPIO_CH_8_PIN);
			break;
		case GPIO_CHAN_9:
			GPIO_ToggleBits(GPIO_CH_9_PORT, GPIO_CH_9_PIN);
			break;
		case GPIO_CHAN_10:
			GPIO_ToggleBits(GPIO_CH_10_PORT, GPIO_CH_10_PIN);
			break;
		case GPIO_CHAN_12:
			GPIO_ToggleBits(GPIO_CH_12_PORT, GPIO_CH_12_PIN);
			break;
		case GPIO_CHAN_13:
			GPIO_ToggleBits(GPIO_CH_13_PORT, GPIO_CH_13_PIN);
			break;
		case GPIO_CHAN_14:
			GPIO_ToggleBits(GPIO_CH_14_PORT, GPIO_CH_14_PIN);
			break;
#ifdef RC_CONTROL_EN
		case GPIO_CHAN_15:
			GPIO_ToggleBits(GPIO_CH_15_PORT, GPIO_CH_15_PIN);
			break;
#endif

#ifdef Board
		case GPIO_CHAN_15:
			GPIO_ToggleBits(GPIO_CH_15_PORT, GPIO_CH_15_PIN);
			break;
#endif
		default:
			return GPIO_TOGGLE_BIT_FAILED;
			break;
		}
	return 0;
}

/************* Private functions **********************************************************/


