/** *****************************************************************************************
 * Unit in charge:
 * @file	adc.c
 * @author	Stefan Sonski
 * $Date:: 2014-07-04 09:32:32 #$
 * 
 * $Date:: 2014-07-04 09:32:32 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 585                   $
 * $Author:: wandji-kwuntchou   $
 * 
 * ----------------------------------------------------------
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include <stdint.h>

#include "per/adc.h"
#include "brd/startup/stm32f4xx.h"
#include "brd/stm32f4xx_adc.h"
#include "brd/stm32f4xx_gpio.h"
#include "brd/stm32f4xx_rcc.h"
#include "brd/stm32f4xx_dma.h"

/************* Private typedefs ***********************************************************/


/************* Macros and constants *******************************************************/
#ifdef ADC_EN

/************* Private static variables ***************************************************/
/* Results of the convertions are stored in this array depending on the rank of the channel */
static uint16_t convertedValues[ADC_NUM_CHANNELS];
/* Lookup table for the rank of a channel */
static uint8_t channelRankLookup[ADC_NUM_CHANNELS];
/* The number of initialized channels */
static uint8_t nbrOfConversions = 0;

/************* Private function prototypes ************************************************/


/************* Public functions ***********************************************************/
int8_t adc_initDevice(void)
{
	ADC_DeInit();
	ADC_TypeDef* pADC = ADC_DEV1;
	uint32_t rccAdcClock = ADC_ADC1_CLOCK;
	DMA_Stream_TypeDef* pStream = ADC_DMA1_STREAM;
	uint32_t dmaChannel = ADC_DMA1_CHAN;

	ADC_CommonInitTypeDef 	ADCCommonInitStructure;
	DMA_InitTypeDef DMAInitStructure;

	RCC_APB2PeriphClockCmd(rccAdcClock, ENABLE);
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_DMA2, ENABLE);

	DMAInitStructure.DMA_Channel = dmaChannel;
	DMAInitStructure.DMA_PeripheralBaseAddr = (uint32_t)&pADC->DR;
	DMAInitStructure.DMA_Memory0BaseAddr = (uint32_t)(convertedValues);
	DMAInitStructure.DMA_DIR = DMA_DIR_PeripheralToMemory;
	DMAInitStructure.DMA_BufferSize = 0;
	DMAInitStructure.DMA_PeripheralInc = DMA_PeripheralInc_Disable;
	DMAInitStructure.DMA_MemoryInc = DMA_MemoryInc_Enable;
	DMAInitStructure.DMA_PeripheralDataSize = DMA_PeripheralDataSize_HalfWord;
	DMAInitStructure.DMA_MemoryDataSize = DMA_MemoryDataSize_HalfWord;
	DMAInitStructure.DMA_Mode = DMA_Mode_Circular;
	DMAInitStructure.DMA_Priority = DMA_Priority_High;
	DMAInitStructure.DMA_FIFOMode = DMA_FIFOMode_Disable;
	DMAInitStructure.DMA_FIFOThreshold = DMA_FIFOThreshold_HalfFull;
	DMAInitStructure.DMA_MemoryBurst = DMA_MemoryBurst_Single;
	DMAInitStructure.DMA_PeripheralBurst = DMA_PeripheralBurst_Single;
	DMA_Init(pStream, &DMAInitStructure);

	ADCCommonInitStructure.ADC_Mode = ADC_Mode_Independent;
	ADCCommonInitStructure.ADC_Prescaler = ADC_Prescaler_Div2;
	ADCCommonInitStructure.ADC_DMAAccessMode = ADC_DMAAccessMode_Disabled;
	ADCCommonInitStructure.ADC_TwoSamplingDelay = ADC_TwoSamplingDelay_5Cycles;
	ADC_CommonInit(&ADCCommonInitStructure);

	return 0;

}

int8_t adc_initChannel(adc_channel_t channel)
{
	ADC_TypeDef* pADC = ADC_DEV1;
	DMA_Stream_TypeDef* pStream = ADC_DMA1_STREAM;
	GPIO_TypeDef* pPort;
	uint32_t gpioPin;
	uint32_t rccPinClock;
	uint8_t adcChannel;

	switch (channel) {
#ifdef ADC_CH1_EN
		case ADC_CH1:
			pPort = ADC_PORT1;
			gpioPin = ADC_CH1_PIN;
			rccPinClock = ADC_PIN_CLOCK1;
			adcChannel = ADC_CH1_CHAN;
			break;
#endif
#ifdef ADC_CH2_EN
		case ADC_CH2:
			pPort = ADC_PORT1;
			gpioPin = ADC_CH2_PIN;
			rccPinClock = ADC_PIN_CLOCK1;
			adcChannel = ADC_CH2_CHAN;
			break;
#endif
#ifdef ADC_CH3_EN
		case ADC_CH3:
			pPort = ADC_PORT1;
			gpioPin = ADC_CH3_PIN;
			rccPinClock = ADC_PIN_CLOCK1;
			adcChannel = ADC_CH3_CHAN;
			break;
#endif
#ifdef ADC_CH4_EN
		case ADC_CH4:
			pPort = ADC_PORT1;
			gpioPin = ADC_CH4_PIN;
			rccPinClock = ADC_PIN_CLOCK1;
			adcChannel = ADC_CH4_CHAN;
			break;
#endif
#ifdef ADC_CH5_EN
		case ADC_CH5:
			pPort = ADC_PORT1;
			gpioPin = ADC_CH5_PIN;
			rccPinClock = ADC_PIN_CLOCK1;
			adcChannel = ADC_CH5_CHAN;
			break;
#endif
#ifdef ADC_CH6_EN
		case ADC_CH6:
			pPort = ADC_PORT1;
			gpioPin = ADC_CH6_PIN;
			rccPinClock = ADC_PIN_CLOCK1;
			adcChannel = ADC_CH6_CHAN;
			break;
#endif
#ifdef ADC_CH7_EN
		case ADC_CH7:
			pPort = ADC_PORT1;
			gpioPin = ADC_CH7_PIN;
			rccPinClock = ADC_PIN_CLOCK1;
			adcChannel = ADC_CH7_CHAN;
			break;
#endif
#ifdef ADC_CH8_EN
		case ADC_CH8:
			pPort = ADC_PORT2;
			gpioPin = ADC_CH8_PIN;
			rccPinClock = ADC_PIN_CLOCK2;
			adcChannel = ADC_CH8_CHAN;
			break;
#endif
#ifdef ADC_CH9_EN
		case ADC_CH9:
			pPort = ADC_PORT2;
			gpioPin = ADC_CH9_PIN;
			rccPinClock = ADC_PIN_CLOCK2;
			adcChannel = ADC_CH9_CHAN;
			break;
#endif
		default:
			return -1;
	}


	ADC_InitTypeDef 		ADCInitStructure;
	GPIO_InitTypeDef 		GPIOInitStructre;

	RCC_AHB1PeriphClockCmd(rccPinClock, ENABLE);

	GPIOInitStructre.GPIO_Pin = gpioPin;
	GPIOInitStructre.GPIO_Mode = GPIO_Mode_AN;
	GPIOInitStructre.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(pPort, &GPIOInitStructre);

	ADCInitStructure.ADC_DataAlign = ADC_DataAlign_Right;
	ADCInitStructure.ADC_Resolution = ADC_Resolution_12b;
	ADCInitStructure.ADC_ContinuousConvMode = ENABLE;
	ADCInitStructure.ADC_ExternalTrigConv = ADC_EXTTRIG_TIMER;
	ADCInitStructure.ADC_ExternalTrigConvEdge = ADC_TRIGGER_EDGE;
	nbrOfConversions += 1;
	/* Save the rank of the channel, which is also the actual number of conversions. */
	channelRankLookup[channel] = nbrOfConversions;
	ADCInitStructure.ADC_NbrOfConversion = nbrOfConversions;
	ADCInitStructure.ADC_ScanConvMode = ENABLE;
	ADC_Init(pADC, &ADCInitStructure);

	/* Workaround to increase the buffer size of the DMA target memory, so the result of a channel is always in the same space. */
	pStream->NDTR = nbrOfConversions;

	ADC_RegularChannelConfig(pADC, adcChannel, nbrOfConversions, ADC_SampleTime_15Cycles);

	return 0;
}

int8_t adc_start(void)
{
	DMA_Cmd(ADC_DMA1_STREAM, ENABLE);
	ADC_TypeDef* pADC = ADC_DEV1;
	ADC_DMARequestAfterLastTransferCmd(pADC, ENABLE);
	ADC_DMACmd(pADC, ENABLE);
	ADC_Cmd(pADC, ENABLE);
	ADC_SoftwareStartConv(pADC);
	return 0;
}

int8_t adc_getValue(adc_channel_t channel, uint16_t* pValue)
{
	/* Use the channel rank to find the position in the result array */
	*pValue = convertedValues[channelRankLookup[channel] - 1];
	return 0;
}

int8_t adc_stop(void)
{
	ADC_TypeDef* pADC = ADC_DEV1;
	ADC_DMACmd(pADC, DISABLE);
	ADC_Cmd(pADC, DISABLE);
	DMA_Cmd(ADC_DMA1_STREAM, DISABLE);
	return 0;
}

/************* Private functions **********************************************************/

#endif /* ADC_EN */
