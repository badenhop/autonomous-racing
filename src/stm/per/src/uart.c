/** *****************************************************************************************
 * Unit in charge:
 * @file	uart.c
 * @author	Bjoern Hepner
 * $Date:: 2015-07-03 16:29:47 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1594                  $
 * $Author:: Yoga               $
 * 
 * ----------------------------------------------------------
 *
 * @brief The module uart provides a serial communication interface.
 *
 * Module for uart communication interface.
 *
 * UART1_CH -> USART1
 *
 * UART2_CH -> USART2
 *
 * UART3_CH -> USART3
 *
 * UART4_CH -> USART6
 *
 * Example Code for using USART and terminal.
 * @code
 *	int8_t data, n;
 *  int8_t recBuf[12], sendBuf[12];
 *  uint32_t recBufSize = 12, sendBufSize = 12;
 *	uart_t uart;
 *	uart.baudrate = 9600;
 *	uart.channel = UART_CH4;
 *
 *	uart_init(&uart,recBuf,recBufSize,sendBuf,sendBufSize);
 *
 *	while(1) {
 *	 n = uart_readByte(&uart, &data);
 *		 if (n > 0) {
 *			uart_writeByte(&uart, data);
 *		 }
 *	}
 * @endcode
 *
 ******************************************************************************************/

 /************* Includes *******************************************************************/
#include "per/uart.h"
#include "brd/stm32f4xx_usart.h"
#include "per/gpio.h"
#include "dev/rx24f/rx24f.h"

/************* Private typedefs ***********************************************************/


/************* Macros and constants *******************************************************/


/************* Private static variables ***************************************************/
#ifdef Board
static uart_t* pUart1;
static uart_t* pUart2;
	static uart_t* pUart4;
#endif
static uart_t* pUart1;
static uart_t* pUart3;
#ifdef Auto
	static uart_t* pUart5;
#endif

/************* Private function prototypes ************************************************/
/**
* @brief Enable the Transmit Interrupt Routine.
*
* @param [in] *pUart - pointer to an UART interface.
*
* @return void
*/
void enableTransmitIsr(uart_t* pUart);

/**
* @brief Disable the Transmit Interrupt Routine.
*
* @param [in] *pUart - pointer to an UART interface.
*
* @return void
*/
void disbaleTransmitIsr(uart_t* pUart);

/**
* @brief Get a uart channel
*
* @param [in] *pUart - pointer to an UART interface.
*
* @return USART_TypeDef
*/
static USART_TypeDef* uart_getChannel(uart_t* pUart);

/************* Public functions ***********************************************************/
int8_t uart_init(uart_t *pUart, int8_t* pRecBuf, uint32_t recBufSize, int8_t* pSendBuf, uint32_t sendBufSize, uint32_t *pSendBufTimestamp, uint32_t *pRecBufTimestamp )
{

	USART_TypeDef* chan = NULL;
	uint32_t uartClock	= 0;
	uint8_t af			= 0;
	uint8_t interrupt	= 0;

	GPIO_TypeDef* txPort = NULL;
	uint32_t txPin	     = 0;
	uint32_t txClock  	 = 0;
	uint16_t txAfPin 	 = 0;

	GPIO_TypeDef* rxPort = NULL;
	uint32_t rxPin	 	 = 0;
	uint32_t rxClock 	 = 0;
	uint16_t rxAfPin 	 = 0;

	switch(pUart->channel) {
#ifdef Board
		case UART_CH1:
			pUart1 = pUart;

			chan = UART1_CHAN;
			uartClock = UART1_CLOCK;
			af = UART1_AF;
			interrupt = UART1_INTERRUPT;

			txPort = UART1_TX_PORT;
			txPin = UART1_TX_PIN;
			txClock = UART1_TX_CLOCK;
			txAfPin = UART1_TX_AF_PIN;

			rxPort = UART1_RX_PORT;
			rxPin = UART1_RX_PIN;
			rxClock = UART1_RX_CLOCK;
			rxAfPin = UART1_RX_AF_PIN;
			break;

		case UART_CH2:
			pUart2 = pUart;

			chan = UART2_CHAN;
			uartClock = UART2_CLOCK;
			af = UART2_AF;
			interrupt = UART2_INTERRUPT;

			txPort = UART2_TX_PORT;
			txPin = UART2_TX_PIN;
			txClock = UART2_TX_CLOCK;
			txAfPin = UART2_TX_AF_PIN;

			rxPort = UART2_RX_PORT;
			rxPin = UART2_RX_PIN;
			rxClock = UART2_RX_CLOCK;
			rxAfPin = UART2_RX_AF_PIN;
			break;
		case UART_CH4:
			pUart4 = pUart;

			chan = UART4_CHAN;
			uartClock = UART4_CLOCK;
			af = UART4_AF;
			interrupt = UART4_INTERRUPT;

			txPort = UART4_TX_PORT;
			txPin = UART4_TX_PIN;
			txClock = UART4_TX_CLOCK;
			txAfPin = UART4_TX_AF_PIN;

			rxPort = UART4_RX_PORT;
			rxPin = UART4_RX_PIN;
			rxClock = UART4_RX_CLOCK;
			rxAfPin = UART4_RX_AF_PIN;
			break;
#endif
		case UART_CH3:
			pUart3 = pUart;

			chan = UART3_CHAN;
			uartClock = UART3_CLOCK;
			af = UART3_AF;
			interrupt = UART3_INTERRUPT;

			txPort = UART3_TX_PORT;
			txPin = UART3_TX_PIN;
			txClock = UART3_TX_CLOCK;
			txAfPin = UART3_TX_AF_PIN;

			rxPort = UART3_RX_PORT;
			rxPin = UART3_RX_PIN;
			rxClock = UART3_RX_CLOCK;
			rxAfPin = UART3_RX_AF_PIN;
			break;

		// RX24F
		case UART_CH1:
			pUart1 = pUart;

			chan = UART1_CHAN;
			uartClock = UART1_CLOCK;
			af = UART1_AF;
			interrupt = UART1_INTERRUPT;

			txPort = UART1_TX_PORT;
			txPin = UART1_TX_PIN;
			txClock = UART1_TX_CLOCK;
			txAfPin = UART1_TX_AF_PIN;

			rxPort = UART1_RX_PORT;
			rxPin = UART1_RX_PIN;
			rxClock = UART1_RX_CLOCK;
			rxAfPin = UART1_RX_AF_PIN;
			break;
#ifdef Auto
		case UART_CH5:
			pUart5 = pUart;



			chan = UART5_CHAN;
			uartClock = UART5_CLOCK;
			af = UART5_AF;
			interrupt = UART5_INTERRUPT;





			txPort = UART5_TX_PORT;
			txPin = UART5_TX_PIN;
			txClock = UART5_TX_CLOCK;
			txAfPin = UART5_TX_AF_PIN;





			rxPort = UART5_RX_PORT;
			rxPin = UART5_RX_PIN;
			rxClock = UART5_RX_CLOCK;
			rxAfPin = UART5_RX_AF_PIN;
			break;
#endif
		default:
			break;
	}

	ringbuf_init_t  initSendRingUart1, initRecRingUart1;

	initRecRingUart1.size 		= recBufSize;
	initRecRingUart1.memory 	= pRecBuf;
	initRecRingUart1.timestamp  = pRecBufTimestamp;
	initSendRingUart1.size 		= sendBufSize;
	initSendRingUart1.memory 	= pSendBuf;
	initSendRingUart1.timestamp  = pSendBufTimestamp;

	ringbuf_init(&pUart->sendring, &initSendRingUart1);
	ringbuf_init(&pUart->recring, &initRecRingUart1);

	GPIO_InitTypeDef GPIO_initStructure;

	RCC_AHB1PeriphClockCmd(txClock, ENABLE);
	RCC_AHB1PeriphClockCmd(rxClock, ENABLE);
	switch ((int)chan) {
		case (int)USART1:
		case (int)USART6:
			RCC_APB2PeriphClockCmd(uartClock, ENABLE);
			break;
		default:
			RCC_APB1PeriphClockCmd(uartClock, ENABLE);
			break;
	}

	GPIO_initStructure.GPIO_PuPd 	= GPIO_PuPd_UP;
	GPIO_initStructure.GPIO_Speed	= GPIO_Speed_100MHz;
	GPIO_initStructure.GPIO_Mode 	= GPIO_Mode_AF;
	GPIO_initStructure.GPIO_OType 	= GPIO_OType_PP;

	GPIO_initStructure.GPIO_Pin 	= txPin; // TX ;
	GPIO_Init(txPort, &GPIO_initStructure);
	GPIO_initStructure.GPIO_Pin 	= rxPin; // RX ;
	GPIO_Init(rxPort, &GPIO_initStructure);

	GPIO_PinAFConfig(txPort, txAfPin, af);
	GPIO_PinAFConfig(rxPort, rxAfPin, af);

	USART_InitTypeDef uart_initStructure;

	uart_initStructure.USART_BaudRate 				= pUart->baudrate;
	uart_initStructure.USART_HardwareFlowControl	= USART_HardwareFlowControl_None;
	uart_initStructure.USART_Mode 					= USART_Mode_Rx | USART_Mode_Tx;
	uart_initStructure.USART_Parity 				= USART_Parity_No;
	uart_initStructure.USART_StopBits 				= USART_StopBits_1;
	uart_initStructure.USART_WordLength 			= USART_WordLength_8b;

	USART_Init(chan, &uart_initStructure);
	USART_Cmd(chan, ENABLE);

	NVIC_InitTypeDef NVIC_initStructure;

	USART_ITConfig(chan, USART_IT_RXNE, ENABLE);

	NVIC_initStructure.NVIC_IRQChannel 						= interrupt;
	NVIC_initStructure.NVIC_IRQChannelCmd					= ENABLE;
	NVIC_initStructure.NVIC_IRQChannelPreemptionPriority 	= 0x00;
	NVIC_initStructure.NVIC_IRQChannelSubPriority		 	= 0x00;

	NVIC_Init(&NVIC_initStructure);

	return 0;
}

int8_t uart_writeByte(uart_t* pUart, int8_t data)
{
	int8_t ret = -1;
	if (1 == ringbuf_write(&pUart->sendring, data)) {
		ret = 1;
		enableTransmitIsr(pUart);
	}
	return ret;
}

int8_t uart_writeString(uart_t* pUart, int8_t* pData, uint16_t size)
{
	int8_t ret = -1;
	int32_t tmpSize;

	if(pData != '\0') {
		tmpSize = ringbuf_writeString(&pUart->sendring, pData, size);
		if (0 <= tmpSize) {		/* geschriebene gr��e in den RB -> ret */
			ret = tmpSize;
		}
		if (0 < tmpSize) {		/* Wenn was geschrieben < 0 dann Enable Interrupt */
			enableTransmitIsr(pUart);
		}
	}
	return ret;
}


int8_t uart_readByte(uart_t *pUart, int8_t *pData)
{
	return ringbuf_read(&pUart->recring, pData);
}

int8_t uart_readByte_timed(uart_t *pUart, timed_byte_t *pData)
{
	return ringbuf_read_timed(&pUart->recring, pData);
}

int8_t uart_readString(uart_t *pUart, int8_t *pData, uint16_t size)
{
	return (int8_t)(ringbuf_readString(&pUart->recring, pData, size));
}

/************* Private functions **********************************************************/
/**
 * @brief enable the transmit interrupt UART
 *
 * @param [in,out] *pUart - pointer to a structure uart_t.
 *
 */
void enableTransmitIsr(uart_t* pUart)
{
	USART_ITConfig(uart_getChannel(pUart), USART_IT_TXE, ENABLE);
}

/**
 * @brief disable the transmit interrupt UART
 *
 * @param [in,out] *pUart - pointer to a structure uart_t.
 *
 */
void disbaleTransmitIsr(uart_t* pUart)
{
	USART_ITConfig(uart_getChannel(pUart), USART_IT_TXE, DISABLE);
}
#ifdef Board

/**
 * @brief Interrupt serviece request from USART1.
 * If set receive flag give out the same character on the terminal.
 * If set transmit flag read a byte from the ring buffer and give it out.
 *
 */
void USART1_IRQHandler(void)
{

	int8_t data;
	if(USART_GetFlagStatus(UART1_CHAN, USART_FLAG_RXNE) == SET) {

		ringbuf_write ( &(pUart1->recring), USART_ReceiveData(UART1_CHAN));

	} else if (USART_GetFlagStatus(UART1_CHAN, USART_FLAG_TXE) == SET) {

		if ((pUart1->sendring).count != 0) {

			ringbuf_read(&(pUart1->sendring), &data); //byte

			USART_SendData(UART1_CHAN, data);
		} else {
			disbaleTransmitIsr(pUart1);
		}
	}
	USART_ClearITPendingBit(UART1_CHAN, USART_IT_RXNE | USART_IT_TXE);

}

/**
 * @brief Interrupt service request from USART2.
 * If set receive flag give out the same character on the terminal.
 * If set transmit flag read a byte from the ring buffer and give it out.
 *
 */
void USART2_IRQHandler(void)
{

	int8_t data;
	if(USART_GetFlagStatus(UART2_CHAN, USART_FLAG_RXNE) == SET) {

		ringbuf_write(&(pUart2->recring), USART_ReceiveData(UART2_CHAN));

	} else if (USART_GetFlagStatus(UART2_CHAN, USART_FLAG_TXE) == SET) {

		if ((pUart2->sendring).count != 0) {

			ringbuf_read(&(pUart2->sendring), &data); //byte

			USART_SendData(UART2_CHAN, data);
		} else {
			disbaleTransmitIsr(pUart2);
		}
	}
	USART_ClearITPendingBit(UART2_CHAN, USART_IT_RXNE | USART_IT_TXE);

}

/**
 * @brief Interrupt service request from USART6.
 * If set receive flag give out the same character on the terminal.
 * If set transmit flag read a byte from the ring buffer and give it out.
 *
 */
void USART6_IRQHandler(void)
{

	int8_t data;
	if(USART_GetFlagStatus(UART4_CHAN, USART_FLAG_RXNE) == SET) {

		ringbuf_write(&(pUart4->recring), USART_ReceiveData(UART4_CHAN));

	} else if (USART_GetFlagStatus(UART4_CHAN, USART_FLAG_TXE) == SET) {

		if ((pUart4->sendring).count != 0) {

			ringbuf_read(&(pUart4->sendring), &data); //byte

			USART_SendData(UART4_CHAN, data);
		} else {
			disbaleTransmitIsr(pUart4);
		}
	}
	USART_ClearITPendingBit(UART4_CHAN, USART_IT_RXNE | USART_IT_TXE);

}
#endif

/**
 * @brief Interrupt service request from USART3.
 * If set receive flag give out the same character on the terminal.
 * If set transmit flag read a byte from the ring buffer and give it out.
 *
 */

void USART1_IRQHandler(void)
{

	int8_t data;
	uartSendBufferEmpty = 0;

	if(USART_GetFlagStatus(UART1_CHAN, USART_FLAG_RXNE) == SET) {

		uartFirstReceivedByte = 1;
		ringbuf_write ( &(pUart1->recring), USART_ReceiveData(UART1_CHAN));

	} else if (USART_GetFlagStatus(UART1_CHAN, USART_FLAG_TXE) == SET) {

		if ((pUart1->sendring).count != 0) {

			ringbuf_read(&(pUart1->sendring), &data); //byte

			USART_SendData(UART1_CHAN, data);
		} else {

			uartSendBufferEmpty = 1;
			disbaleTransmitIsr(pUart1);
		}
	}
	USART_ClearITPendingBit(UART1_CHAN, USART_IT_RXNE);

}

void USART3_IRQHandler(void)
{
	int8_t data;
	if(USART_GetFlagStatus(UART3_CHAN, USART_FLAG_RXNE) == SET) {
		ringbuf_write(&(pUart3->recring), USART_ReceiveData(UART3_CHAN));
	}
	else if (USART_GetFlagStatus(UART3_CHAN, USART_FLAG_TXE) == SET) {
		if ((pUart3->sendring).count != 0) {
			ringbuf_read(&(pUart3->sendring), &data);
			USART_SendData(UART3_CHAN, data);
		}
		else {
			disbaleTransmitIsr(pUart3);
		}
	}
	USART_ClearITPendingBit(UART3_CHAN, USART_IT_RXNE | USART_IT_TXE);
}

#ifdef Auto
void UART5_IRQHandler(void) {
	int8_t data;
	if(USART_GetFlagStatus(UART5_CHAN, USART_FLAG_RXNE) == SET) {


		ringbuf_write(&(pUart5->recring), USART_ReceiveData(UART5_CHAN));


	} else if (USART_GetFlagStatus(UART5_CHAN, USART_FLAG_TXE) == SET) {


		if ((pUart5->sendring).count != 0) {


			ringbuf_read(&(pUart5->sendring), &data); //byte


			USART_SendData(UART5_CHAN, data);

		} else {
			disbaleTransmitIsr(pUart5);

		}
	}
	USART_ClearITPendingBit(UART5_CHAN, USART_IT_RXNE | USART_IT_TXE);

}
#endif

USART_TypeDef* uart_getChannel(uart_t* pUart) {
	USART_TypeDef* usart = 0;
	switch(pUart->channel) {
#ifdef Board
		case UART_CH1:
			usart = UART1_CHAN;
			break;
		case UART_CH2:
			usart = UART2_CHAN;
			break;
		case UART_CH4:
			usart = UART4_CHAN;
			break;
#endif
		case UART_CH3:
			usart = UART3_CHAN;
			break;
#ifdef Auto
		case UART_CH5:
			usart = UART5_CHAN;
			break;
#endif
		default:
			break;
	}
	return usart;
}
