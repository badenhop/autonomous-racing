/** *****************************************************************************************
 * Unit in charge:
 * @file	irq.c
 * @author	Hauke Petersen <mail@haukepetersen.de>
 * $Date:: 2015-02-19 14:03:20 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 1312                  $
 * $Author:: neumerkel          $
 * 
 * ----------------------------------------------------------
 *
 * @brief 	Abstraction layer for the controllers interrupt system.
 *
 ******************************************************************************************/


/************* Includes *******************************************************************/
#include "per/irq.h"

#include "brd/startup/stm32f4xx.h"

/************* Private typedefs ***********************************************************/


/************* Macros and constants *******************************************************/


/************* Private static variables ***************************************************/


/************* Private function prototypes ************************************************/


/************* Public functions ***********************************************************/
void irq_enable(void)
{
	__enable_irq();
//	NVIC_EnableIRQ(SysTick_IRQn);    			/**< Enable the SysTick_IRQn used by the scheduler*/
//	NVIC_EnableIRQ(TIM2_IRQn);				 		 /**< Enable the TIM2_IRQn used by the systime*/
//	NVIC_EnableIRQ(ADC_IRQn);			   		   /**< Enable the ADC_IRQn used by the foccomplex*/
//	NVIC_EnableIRQ(TIM1_BRK_TIM9_IRQn);	    /**< Enable the TIM1_BRK_TIM9_IRQn used by the ict(rc)*/
//	NVIC_EnableIRQ(USART3_IRQn );	    /**< Enable the UART3_IRQn used by the terminal/io/uart_chan3 */
//	NVIC_EnableIRQ(UART5_IRQn);	    /**< Enable the UART5_IRQn used by the terminal/io/uart_chan5 */
}

void irq_disable(void)
{
	__disable_irq();
//	NVIC_DisableIRQ(SysTick_IRQn);    			/**< Disable the SysTick_IRQn used by the scheduler*/
//	NVIC_DisableIRQ(TIM2_IRQn);				 		 /**< Disable the TIM2_IRQn used by the systime*/
//	NVIC_DisableIRQ(ADC_IRQn);			   		   /**< Disable the ADC_IRQn used by the foccomplex*/
//	NVIC_DisableIRQ(TIM1_BRK_TIM9_IRQn);	    /**< Disable the TIM1_BRK_TIM9_IRQn used by the ict(rc)*/
//	NVIC_DisableIRQ(UART5_IRQn);	    /**< Disable the UART5_IRQn used by the terminal/io/uart_chan5 */
//	NVIC_DisableIRQ(USART3_IRQn );	    /**< Enable the UART3_IRQn used by the terminal/io/uart_chan3 */
}

/************* Private functions **********************************************************/
