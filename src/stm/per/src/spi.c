/** *****************************************************************************************
 * Unit in charge:
 * @file	spi.c
 * @author	Adrian
 * $Date:: 2017-06-27 11:34:20 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev::                   $
 * $Author::           $
 *
 * ----------------------------------------------------------
 *
 * @brief
 *
 ******************************************************************************************/

/************* Includes *******************************************************************/
#include <stdint.h>
#include "brd/startup/stm32f4xx.h"
#include "brd/stm32f4xx_spi.h"
/************* Macros and constants *******************************************************/

/************* Private static variables ***************************************************/

/************* Private function prototypes ************************************************/

/************* Public functions ***********************************************************/
int8_t SPI2_init() {
	GPIO_InitTypeDef GPIO_InitStruct;
	SPI_InitTypeDef SPI_InitStruct;

	// enable clock for used IO pins
	RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);

	/* configure pins used by SPI1
	 * PB13 = SCK
	 * PB14 = MISO
	 * PB15 = MOSI
	 */
	GPIO_InitStruct.GPIO_Pin = GPIO_Pin_13 | GPIO_Pin_14 | GPIO_Pin_15;
	GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStruct.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStruct.GPIO_PuPd = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOB, &GPIO_InitStruct);

	// connect SPI1 pins to SPI alternate function
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource13, GPIO_AF_SPI2);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource14, GPIO_AF_SPI2);
	GPIO_PinAFConfig(GPIOB, GPIO_PinSource15, GPIO_AF_SPI2);

	// enable peripheral clock
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_SPI2, ENABLE);

	/* configure SPI2 in Mode 0
	 * CPOL = 0 --> clock is low when idle
	 * CPHA = 0 --> data is sampled at the first edge
	 */
	SPI_InitStruct.SPI_Direction = SPI_Direction_2Lines_FullDuplex; // set to full duplex mode, seperate MOSI and MISO lines
	SPI_InitStruct.SPI_Mode = SPI_Mode_Master; // transmit in master mode, NSS pin has to be always high
	SPI_InitStruct.SPI_DataSize = SPI_DataSize_8b; // one packet of data is 8 bits wide
	SPI_InitStruct.SPI_CPOL = SPI_CPOL_Low; // clock is low when idle
	SPI_InitStruct.SPI_CPHA = SPI_CPHA_1Edge; // data sampled at first edge
	SPI_InitStruct.SPI_NSS = SPI_NSS_Soft | SPI_NSSInternalSoft_Set; // set the NSS management to internal and pull internal NSS high
	SPI_InitStruct.SPI_BaudRatePrescaler = SPI_BaudRatePrescaler_64; // SPI frequency is APB2 frequency / 4
	SPI_InitStruct.SPI_FirstBit = SPI_FirstBit_MSB; // data is transmitted MSB first
	SPI_Init(SPI2, &SPI_InitStruct);

	SPI_Cmd(SPI2, ENABLE); // enable SPI1
	return 0;
}

void SPI2_sendByte(uint8_t data) {
	SPI2->DR = data; // write data to be transmitted to the SPI data register
	while (!(SPI2->SR & SPI_I2S_FLAG_TXE)); // wait until transmit complete
	while (!(SPI2->SR & SPI_I2S_FLAG_RXNE)); // wait until receive complete
	while (SPI2->SR & SPI_I2S_FLAG_BSY); // wait until SPI is not busy anymore
	data = SPI2->DR; // dummy
}

uint8_t SPI2_sendBuffer(uint8_t* txBuffer, uint8_t bufferLength) {
	uint8_t i = 0;
	for (i = 0; i < bufferLength; i++) {
		SPI2_sendByte(txBuffer[i]);
	}
	return i;
}

uint8_t SPI2_readByte() {
	SPI2->DR = 0xFF;	/* dummy write to buffer to start SPI communication*/
	while (!(SPI2->SR & SPI_I2S_FLAG_TXE)); // wait until transmit complete
	while (!(SPI2->SR & SPI_I2S_FLAG_RXNE)); // wait until receive complete
	while (SPI2->SR & SPI_I2S_FLAG_BSY); // wait until SPI is not busy anymore
	return SPI2->DR; // return received data from SPI data register
}
