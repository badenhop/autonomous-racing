/** *****************************************************************************************
 * Unit in charge:
 * @file	gpio.h
 * @author	Bjoern Hepner
 * $Date:: 2016-09-02 15:05:56 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2022                  $
 * $Author:: Yoga               $
 * 
 * ----------------------------------------------------------
 *
 ******************************************************************************************/

#ifndef GPIO_H_
#define GPIO_H_

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "hwallocation.h"
#include "app/config.h"

/************* Public typedefs ************************************************************/
/**
 * @enum gpio_chan_t
 *
 * Selection of the GPIO pins to be used.
 */
typedef enum {
	GPIO_CHAN_0,
	GPIO_CHAN_1,
	GPIO_CHAN_2,
	GPIO_CHAN_3,
	GPIO_CHAN_4,
	GPIO_CHAN_5,
	GPIO_CHAN_6,
	GPIO_CHAN_7,
	GPIO_CHAN_8,
	GPIO_CHAN_9,
	GPIO_CHAN_10,
	GPIO_CHAN_11,
	GPIO_CHAN_12,
	GPIO_CHAN_13,
	GPIO_CHAN_14,
	GPIO_CHAN_15 /* TODO need to be removed only for test of rcmgr modul */
#ifdef Board
	GPIO_CHAN_15
#endif
} gpio_chan_t;

/**
 * @enum gpio_mode_t
 *
 * As what the GPIO pin to be operated? Input or output ?
 */
typedef enum {
	GPIO_MODE_IN,
	GPIO_MODE_OUT
} gpio_mode_t;

/**
 * @enum gpio_state_t
 *
 * GPIO_STATE_OFF = 0
 * GPIO_STATE_ON  = 1
 */
typedef enum {
	GPIO_STATE_OFF,
	GPIO_STATE_ON
} gpio_state_t;

/**
 * @struct gpio_t
 *
 * Parameter structure for gpio.
 */
typedef struct {
	gpio_chan_t		channel;	/**< Which channel is used ?  See enum gpio_chan_t . */
	gpio_mode_t		mode;		/**< Input pin or output pin*/
	gpio_state_t    state;
} gpio_t;

/************* Macros and constants *******************************************************/


/************* Public function prototypes *************************************************/
/**
 * @brief Initialized gpio Port A.
 *
 *
 * automatic parameter are : Port A, Disbale Pullup and Pulldown, Push-Pul
 *
 * select parameter : Pin 0 to 7 and input or ouput mode
 *
 * @param [in,out] *pGpio - pointer to a structure gpio_t.
 *
 * @return GPIO_MODE_FAILED
 * @return GPIO_PIN_FAILED
 * @return 0 - all right
 */
int8_t gpio_init(gpio_t *pGpio);

/**
 * @brief write the selected gpio pin
 *
 * @param [in,out] *pGpio - pointer to a structure gpio_t.
 * @param [in] bit - value for the pin
 *
 * @return GPIO_WRITE_BIT_FAILED
 * @return GPIO_STATE_ON
 */
int8_t gpio_writeBit(gpio_t *pGpio, uint8_t bit);

/**
 * @brief read the selected gpio pin
 *
 * @param [in,out] *pGpio - pointer to a structure gpio_t.
 *
 * @return read value
 * @return GPIO_READ_BIT_FAILED
 */
int8_t gpio_readBit(gpio_t *pGpio);

/**
 * @brief sets a bit in the selected gpio pin
 *
 * @param [in,out] *pGpio - pointer to a structure gpio_t.
 *
 * @return GPIO_SET_BIT_FAILED
 * @return 1 - bit is set
 */
int8_t gpio_setBit(gpio_t *pGpio);

/**
 * @brief clear a bit in the selected gpio pin
 *
 * @param [in,out] *pGpio - pointer to a structure gpio_t.
 *
 * @return GPIO_CLEAR_BIT_FAILED
 * @return 0 - bit is clear
 */
int8_t gpio_clearBit(gpio_t *pGpio);

/**
 * @brief toggle a bit in the selected gpio pin
 *
 * @param [in,out] *pGpio - pointer to a structure gpio_t.
 *
 * @return GPIO_TOGGLE_BIT_FAILED
 * @return 0 - bit is changed
 */
int8_t gpio_toggleBit(gpio_t *pGpio);



#endif /* GPIO_H_ */
