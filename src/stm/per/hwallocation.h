/** *****************************************************************************************
 * Unit in charge:
 * @file	hwallocation.h
 * @author	Engin Hinze, Robert Ledwig
 * $Date:: 2017-12-05 12:29:39 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 2213                  $
 * $Author:: mbrieske         $
 *
 * ----------------------------------------------------------
 *
 * @brief Parameter for driver
 *
 ******************************************************************************************/

#ifndef HWALLOCATION_H_
#define HWALLOCATION_H_

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "brd/stm32f4xx_gpio.h"
#include "app/config.h"

/************* Public typedefs ************************************************************/
/************* Macros and constants *******************************************************/
//Auto
#ifdef Auto

/**
 * \defgroup hwAllocation_FocCpx_ADC Foccomplex ADC peripheral configuration
 */
/*@{*/

#define FOCCPX_ADC_DEV1                  	ADC1								/**< Foccomplex ADC device 1 uses the ADC 1. */
#define FOCCPX_ADC_ADC1_CLOCK            	RCC_APB2Periph_ADC1					/**< Foccomplex ADC 1 requires the APB2 bus clock.*/

#define FOCCPX_ADC_DMA_STREAM           	DMA2_Stream0						/**< Foccomplex ADC 1 is connected to the DMA 2 Stream 0 */
#define FOCCPX_ADC_DMA_CHAN             	DMA_Channel_0						/**< DMA channel 0 for the ADC 1 */
#define FOCCPX_DMA_CLOCK					RCC_AHB1Periph_DMA2					/**< DMA 2 bus clock*/
#define FOCCPX_ADC_EXTTRIG_TIMER	        ADC_ExternalTrigConv_T1_CC1		    /**< Timer 1 Capture Compare as external trigger conversion */

#define FOCCPX_ADC_DEV2                  	ADC2								/**< Foccomplex ADC device 2 uses the ADC 2. */
#define FOCCPX_ADC_ADC2_CLOCK            	RCC_APB2Periph_ADC2                 /**< Foccomplex ADC 2 requires the APB2 bus clock.*/

#define FOCCPX_ADC_DEV3                  	ADC3								/**< Foccomplex ADC device 2 uses the ADC 3. */
#define FOCCPX_ADC_ADC3_CLOCK            	RCC_APB2Periph_ADC3                 /**< Foccomplex ADC 3 requires the APB2 bus clock.*/

#define FOCCPX_ADC_PIN_CLOCK1  				RCC_AHB1Periph_GPIOA				/**< Foccomplex ADC channel 1 to 3 uses  pins on port A. This port requires the AHB1 bus clock.*/
#define FOCCPX_ADC_PIN_CLOCK3   			RCC_AHB1Periph_GPIOC				/**< Foccomplex ADC channel 4 to 7 uses pins on port C. This port requires the AHB1 bus clock.*/
#define FOCCPX_ADC_PORT1              		GPIOA								/**< Foccomplex ADC GPIO port A */
#define FOCCPX_ADC_PORT3             		GPIOC								/**< Foccomplex ADC GPIO port C */

//Traktion Ph Cur u
#define FOCCPX_ADC_CH1_PIN            		GPIO_Pin_1							/**< The Foccomplex ADC channel 1 as the GPIO pin 1 on Port A*/
#define FOCCPX_ADC_CH1_CHAN              	ADC_Channel_1						/**< Foccomplex ADC channel 1 */

//Traktion Ph Cur w
#define FOCCPX_ADC_CH2_PIN            		GPIO_Pin_2							/**< The Foccomplex ADC channel 2 as the GPIO pin 2 on Port A*/
#define FOCCPX_ADC_CH2_CHAN              	ADC_Channel_2						/**< Foccomplex ADC channel 2 */

//Traktion Ph Cur v
#define FOCCPX_ADC_CH3_PIN            		GPIO_Pin_3							/**< The Foccomplex ADC channel 3 as the GPIO pin 3 on Port A*/
#define FOCCPX_ADC_CH3_CHAN              	ADC_Channel_3                       /**< Foccomplex ADC channel 3 */

//Traktion Pow El Temp
#define FOCCPX_ADC_CH4_PIN        			GPIO_Pin_4							/**< The Foccomplex ADC channel 14 as the GPIO pin 4 on Port C*/
#define FOCCPX_ADC_CH4_CHAN             	ADC_Channel_14                      /**< Foccomplex ADC channel 13 */

//Lageregler (Cosinus)
#define FOCCPX_ADC_CH5_PIN        			GPIO_Pin_5							/**< The Foccomplex ADC channel 15 as the GPIO pin 5 on Port C*/
#define FOCCPX_ADC_CH5_CHAN             	ADC_Channel_15                      /**< Foccomplex ADC channel 15 */

//Bat Voltage
#define FOCCPX_ADC_CH6_PIN        			GPIO_Pin_1							/**< The Foccomplex ADC channel 11 as the GPIO pin 1 on Port C*/
#define FOCCPX_ADC_CH6_CHAN             	ADC_Channel_11                      /**< Foccomplex ADC channel 11 */

//Lageregler (Sinus)
#define FOCCPX_ADC_CH7_PIN        			GPIO_Pin_2							/**< The Foccomplex ADC channel 12 as the GPIO pin 2 on Port C*/
#define FOCCPX_ADC_CH7_CHAN             	ADC_Channel_12                      /**< Foccomplex ADC channel 12 */

//Diag Cur Ph u
#define FOCCPX_ADC_CH8_PIN        			GPIO_Pin_5							/**< The Foccomplex ADC channel 5 as the GPIO pin 5 on Port A*/
#define FOCCPX_ADC_CH8_CHAN             	ADC_Channel_5                       /**< Foccomplex ADC channel 5 */

//Diag Cur Ph v
#define FOCCPX_ADC_CH9_PIN        			GPIO_Pin_4							/**< The Foccomplex ADC channel 4 as the GPIO pin 4 on Port A*/
#define FOCCPX_ADC_CH9_CHAN             	ADC_Channel_4                       /**< Foccomplex ADC channel 4 */

//Diag Cur Ph w
#define FOCCPX_ADC_CH10_PIN        			GPIO_Pin_7							/**< The Foccomplex ADC channel 7 as the GPIO pin 7 on Port A*/
#define FOCCPX_ADC_CH10_CHAN             	ADC_Channel_7                       /**< Foccomplex ADC channel 7 */
/*@}*/

/**
 * \defgroup hwAllocation_FocCpx_PWM Foccomplex PWM peripheral configuration
 */
/*@{*/
#define FOCCPX_PWM_PORT				   		GPIOE					    		/**< Foccomplex PWM channel 1,2 and 3 use port E. */
#define FOCCPX_PWM_PIN_CLOCK	            RCC_AHB1Periph_GPIOE	    		/**< PWM Port requires the AHB1 bus clock.*/
#define FOCCPX_PWM_TIMER			        TIM1					    		/**< To generate the PWM signal on the 3 channels, timer 1 is taken.*/
#define FOCCPX_PWM_TIM_CLOCK		        RCC_APB2Periph_TIM1 				/**< Foccomplex PWM channel 1,2 and 3 use timer 1. This timer requires the APB2 bus clock.*/


// Foccomplex PWM channel 1
/**
 * \addtogroup pwmFocCpxConfig1 Foccomplex PWM channel 1 uses timer 1 channel 1 - PWM Traktion u
 */
/*@{*/
#define FOCCPX_PWM_CHAN_1_PIN				GPIO_Pin_9							/**< The Foccomplex PWM signal from channel 1 is output on pin 9.*/
#define FOCCPX_PWM_CHAN_1_AF_PIN			GPIO_PinSource9						/**< GPIO pin 9 as alternate function initialized.*/
#define FOCCPX_PWM_CHAN_1_AF				GPIO_AF_TIM1						/**< Pin connect with timer 1. */
/*@}*/

// Foccomplex PWM Channel 2
/**
 * \addtogroup pwmFocCpxConfig2 Foccomplex PWM channel 2 uses timer 1 channel 2 - PWM Traktion v
 */
/*@{*/
#define FOCCPX_PWM_CHAN_2_PIN				GPIO_Pin_11							/**< The Foccomplex PWM signal from channel 2 is output on pin 11.*/
#define FOCCPX_PWM_CHAN_2_AF_PIN			GPIO_PinSource11					/**< GPIO pin 11 as alternate function initialized.*/
#define FOCCPX_PWM_CHAN_2_AF				GPIO_AF_TIM1						/**< Pin connect with timer 1. */
/*@}*/


// Foccomplex PWM Channel 3
/**
 * \addtogroup pwmFocCpxConfig3 Foccomplex PWM channel 3 uses timer 3 channel 3 - PWM Traktion w
 */
/*@{*/
#define FOCCPX_PWM_CHAN_3_PIN				GPIO_Pin_13							/**< The Foccomplex PWM signal from channel 3 is output on pin 13.*/
#define FOCCPX_PWM_CHAN_3_AF_PIN			GPIO_PinSource13					/**< GPIO pin 13 as alternate function initialized.*/
#define FOCCPX_PWM_CHAN_3_AF				GPIO_AF_TIM1						/**< Pin connect with timer 1. */
/*@}*/

/*@}*/


/**
 * \defgroup hwAllocation_ADC ADC peripheral configuration
 */
/*@{*/
#define ADC_DEV1                  	ADC1								/**< ADC device 1 uses the ADC 1. */
#define ADC_ADC1_CLOCK            	RCC_APB2Periph_ADC1					/**< ADC 1 requires the APB2 bus clock.*/

#define ADC_DMA_STREAM           	DMA2_Stream0						/**< ADC 1 is connected to the DMA 2 Stream 0 */
#define ADC_DMA_CHAN             	DMA_Channel_0						/**< DMA channel 0 for the ADC 1 */
#define DMA_CLOCK					RCC_AHB1Periph_DMA2					/**< DMA 2 bus clock*/
#define ADC_EXTTRIG_TIMER	        ADC_ExternalTrigConv_T1_CC1		    /**< Timer 1 Capture Compare as external trigger conversion */
#define ADC_TRIGGER_EDGE  			ADC_ExternalTrigConvEdge_Rising		/**< conversion edge is rising */

#define ADC_DEV2                  	ADC2								/**< ADC device 2 uses the ADC 2. */
#define ADC_ADC2_CLOCK            	RCC_APB2Periph_ADC2                 /**< ADC 2 requires the APB2 bus clock.*/

#define ADC_PIN_CLOCK1  			RCC_AHB1Periph_GPIOA				/**< ADC channel 1 to 7 uses  pins on port A. This port requires the AHB1 bus clock.*/
#define ADC_PIN_CLOCK2   			RCC_AHB1Periph_GPIOB				/**< ADC channel 8 and 9 uses pins on port B. This port requires the AHB1 bus clock.*/
#define ADC_PIN_CLOCK3   			RCC_AHB1Periph_GPIOC				/**< ADC channel 10 to 15 uses pins on port C. This port requires the AHB1 bus clock.*/
#define ADC_PORT1              		GPIOA								/**< ADC GPIO port A */
#define ADC_PORT2              		GPIOB								/**< ADC GPIO port B */
#define ADC_PORT3             		GPIOC								/**< ADC GPIO port C */

// Configuration for the standard ADC driver
#define ADC_DMA1_STREAM           	DMA2_Stream0                        /**< ADC is connected to the DMA2 Stream 0 */
#define ADC_DMA1_CHAN             	DMA_Channel_0                       /**< DMA channel0 for the ADC */


#define ADC_CH1_PIN            		GPIO_Pin_4							/**< The ADC channel 1 as the GPIO pin 4 on Port A*/
#define ADC_CH1_CHAN              	ADC_Channel_4                       /**< Channel 1 uses ADC channel 4 */

#define ADC_CH2_PIN            		GPIO_Pin_5							/**< The channel 2 as the GPIO pin 5 on Port A*/
#define ADC_CH2_CHAN              	ADC_Channel_5                       /**< Channel 2 uses ADC channel 5 */

#define ADC_CH3_PIN                	GPIO_Pin_6							/**< The channel 3 as the GPIO pin 6 on Port A*/
#define ADC_CH3_CHAN             	ADC_Channel_6                       /**< Channel 3 uses ADC channel 6 */

#define ADC_CH4_PIN                	GPIO_Pin_7							/**< The channel 4 as the GPIO pin 7 on Port A*/
#define ADC_CH4_CHAN              	ADC_Channel_7                       /**< Channel 4 uses ADC channel 7 */

//Aux ADC 2
#define ADC_CH5_PIN                	GPIO_Pin_0							/**< The channel 5 as the GPIO pin 0 on Port B*/
#define ADC_CH5_CHAN              	ADC_Channel_8                       /**< Channel 5 uses ADC channel 8 */

//Aux ADC 1
#define ADC_CH6_PIN                	GPIO_Pin_1							/**< The channel 6 as the GPIO pin 1 on Port B*/
#define ADC_CH6_CHAN              	ADC_Channel_9                       /**< Channel 6 uses ADC channel 9 */

#define ADC_CH7_PIN        			GPIO_Pin_0							/**< The channel 7 as the GPIO pin 0 on Port C*/
#define ADC_CH7_CHAN             	ADC_Channel_10                      /**< Channel 7 uses ADC channel 10 */

#define ADC_CH8_PIN        			GPIO_Pin_3							/**< The channel 8 as the GPIO pin 3 on Port C*/
#define ADC_CH8_CHAN             	ADC_Channel_13                      /**< Channel 8 uses ADC channel 13 */

/**
 * \defgroup hwAllocation_PCM PCM peripheral configuration
 */
/*@{*/

// PCM Channel 3
/**
 * \addtogroup pcmConfig1 PCM channel 3 uses timer 8
 */
/*@{*/
#define PWM_PORT					GPIOC					    		/**< PCM channel 3 uses port E. */
#define PWM_PIN_CLOCK      			RCC_AHB1Periph_GPIOC	    		/**< This port requires the AHB1 bus clock.*/
#define PWM_TIMER					TIM8					    		/**< To generate the PCM signal, timer 8 is taken.*/
#define PWM_TIM_CLOCK			    RCC_APB2Periph_TIM8 				/**< The timer requires the APB2 bus clock.*/
#define PWM_CHAN_1_PIN				GPIO_Pin_8							/**< The PCM signal from channel 3 is output on pin 8.*/
#define PWM_CHAN_1_AF_PIN			GPIO_PinSource8						/**< GPIO pin 8 as alternate function initialized.*/
#define PWM_CHAN_1_AF				GPIO_AF_TIM8						/**< Pin connect with timer 8. */
/*@}*/


// PCM Channel 4
/**
 * \addtogroup pcmConfig1 PCM channel 4 uses timer 8
 */
/*@{*/
#define PWM_CHAN_2_PIN				GPIO_Pin_9							/**< The PCM signal from channel 4 is output on pin 9.*/
#define PWM_CHAN_2_AF_PIN			GPIO_PinSource9						/**< GPIO pin 9 as alternate function initialized.*/
#define PWM_CHAN_2_AF				GPIO_AF_TIM8						/**< Pin connect with timer 8. */
/*@}*/

/*@}*/

/**
 * \defgroup hwAllocation_CPPM CPPM peripheral configuration
 */
/*@{*/

// CPPM IN
/**
 * \addtogroup cppmConfig1 CPPM channel 1 uses timer 9
 */
/*@{*/
#define CPPM_PORT					GPIOE					    		/**< CPPM input uses port E. */
#define CPPM_PIN_CLOCK        		RCC_AHB1Periph_GPIOE	    		/**< This port requires the AHB1 bus clock.*/
#define CPPM_TIMER					TIM9					    		/**< To decode the CPPM signal, timer 9 is taken.*/
#define CPPM_CLOCK		    		RCC_APB2Periph_TIM9 				/**< The timer requires the APB2 bus clock.*/
#define CPPM_PIN					GPIO_Pin_5							/**< The CPPM signal input is on pin 5.*/
#define CPPM_AF_PIN					GPIO_PinSource5						/**< GPIO pin 5 as alternate function initialized.*/
#define CPPM_AF						GPIO_AF_TIM9						/**< Pin connect with timer 9. */
/*@}*/

/*@}*/

/**
 * \defgroup hwAllocation_WENC Wheel Encoder peripheral configuration
 */
/*@{*/

/**
 * \addtogroup Wheel Encoder Port
 */
/*@{*/
#define WENC_PORT		            GPIOB					    		/**< The Wheel Encoder Front left channel 1 uses port B. */
#define WENC_PIN_CLOCK	            RCC_AHB1Periph_GPIOB	    		/**< This port requires the AHB1 bus clock.*/
/*@}*/

/**
 * \addtogroup Wheel Encoder Front
 */
/*@{*/
#define WENC_FRONT_TIMER		    TIM3					    		/**< To generate the signal, timer 3 is taken.*/
#define WENC_FRONT_TIM_CLOCK	    RCC_APB1Periph_TIM3 				/**< This timer requires the APB1 bus clock.*/
/*@}*/

/**
 * \addtogroup Wheel Encoder Rear
 */
/*@{*/
#define WENC_REAR_TIMER		        TIM2					    		/**< To generate the signal, timer 2 is taken.*/
#define WENC_REAR_TIM_CLOCK	        RCC_APB1Periph_TIM2 				/**< This timer requires the APB1 bus clock.*/
/*@}*/
/**
 * \addtogroup Wheel Encoder Front left
 */
/*@{*/
#define WENC_FL_CHAN_1_PIN			GPIO_Pin_4							/**< The Wheel Encoder Front Left signal from channel 1 is output on pin 4.*/
#define WENC_FL_CHAN_1_AF_PIN		GPIO_PinSource4				     	/**< GPIO pin 4 as alternate function initialized.*/
#define WENC_FL_CHAN_1_AF			GPIO_AF_TIM3						/**< Pin connect with timer 3. */
/*@}*/

/**
 * \addtogroup Wheel Encoder Front right
 */
/*@{*/
#define WENC_FR_CHAN_2_PIN			GPIO_Pin_5							/**< The Wheel Encoder Front right signal from channel 2 is output on pin 5.*/
#define WENC_FR_CHAN_2_AF_PIN		GPIO_PinSource5				     	/**< GPIO pin 5 as alternate function initialized.*/
#define WENC_FR_CHAN_2_AF			GPIO_AF_TIM3						/**< Pin connect with timer 3. */
/*@}*/

/**
 * \addtogroup Wheel Encoder Rear left
 */
/*@{*/
#define WENC_RL_CHAN_4_PIN			GPIO_Pin_11							/**< The Wheel Encoder rear Left signal from channel 2 is output on pin 11.*/
#define WENC_RL_CHAN_4_AF_PIN		GPIO_PinSource11				     	/**< GPIO pin 11 as alternate function initialized.*/
#define WENC_RL_CHAN_4_AF			GPIO_AF_TIM2						/**< Pin connect with timer 2. */
/*@}*/

/**
 * \addtogroup Wheel Encoder Rear right
 */
/*@{*/
#define WENC_RR_CHAN_2_PIN			GPIO_Pin_3							/**< The Wheel Encoder rear right signal from channel 2 is output on pin 3.*/
#define WENC_RR_CHAN_2_AF_PIN		GPIO_PinSource3				     	/**< GPIO pin 3 as alternate function initialized.*/
#define WENC_RR_CHAN_2_AF			GPIO_AF_TIM2						/**< Pin connect with timer 2. */
/*@}*/

/*@}*/

/**
 * \defgroup hwAllocation_GPIO GPIO periphial configuration
 */
/*@{*/

// GPIO_CHAN_0
/**
 * \addtogroup gpioConfig0 GPIO 0
 */
/*@{*/
#define GPIO_CH_0_PIN		GPIO_Pin_0									/**< GPIO Channel 0 uses pin 0.*/
#define GPIO_CH_0_PORT	GPIOA										/**< GPIO Channel 0 uses port A. */
#define GPIO_CH_0_RCC		RCC_AHB1Periph_GPIOA						/**< GPIO Channel 0 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_1
/**
 * \addtogroup gpioConfig1 GPIO 1
 */
/*@{*/
#define GPIO_CH_1_PIN		GPIO_Pin_1									/**< GPIO Channel 1 uses pin 1.*/
#define GPIO_CH_1_PORT	GPIOA										/**< GPIO Channel 1 uses port A. */
#define GPIO_CH_1_RCC		RCC_AHB1Periph_GPIOA						/**< GPIO Channel 1 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_2
/**
 * \addtogroup gpioConfig2 GPIO 2
 */
/*@{*/
#define GPIO_CH_2_PIN		GPIO_Pin_2									/**< GPIO Channel 2 uses pin 2.*/
#define GPIO_CH_2_PORT	GPIOA										/**< GPIO Channel 2 uses port A. */
#define GPIO_CH_2_RCC		RCC_AHB1Periph_GPIOA						/**< GPIO Channel 2 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_3
/**
 * \addtogroup gpioConfig3 GPIO 3
 */
/*@{*/
#define GPIO_CH_3_PIN		GPIO_Pin_3									/**< GPIO Channel 3 uses pin 3.*/
#define GPIO_CH_3_PORT	GPIOA										/**< GPIO Channel 3 uses port A. */
#define GPIO_CH_3_RCC		RCC_AHB1Periph_GPIOA						/**< GPIO Channel 3 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_4
/**
 * \addtogroup gpioConfig4 GPIO 4
 */
/*@{*/
#define GPIO_CH_4_PIN		GPIO_Pin_4									/**< GPIO Channel 4 uses pin 4.*/
#define GPIO_CH_4_PORT	GPIOA										/**< GPIO Channel 4 uses port A. */
#define GPIO_CH_4_RCC		RCC_AHB1Periph_GPIOA						/**< GPIO Channel 4 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_5
/**
 * \addtogroup gpioConfig5 GPIO 5
 */
/*@{*/
#define GPIO_CH_5_PIN		GPIO_Pin_5									/**< GPIO Channel 5 uses pin 5.*/
#define GPIO_CH_5_PORT	GPIOA										/**< GPIO Channel 5 uses port A. */
#define GPIO_CH_5_RCC		RCC_AHB1Periph_GPIOA						/**< GPIO Channel 5 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_6
/**
 * \addtogroup gpioConfig6 GPIO 6
 */
/*@{*/
#define GPIO_CH_6_PIN		GPIO_Pin_6									/**< GPIO Channel 6 uses pin 6.*/
#define GPIO_CH_6_PORT	GPIOA										/**< GPIO Channel 6 uses port A. */
#define GPIO_CH_6_RCC		RCC_AHB1Periph_GPIOA						/**< GPIO Channel 6 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_7
/**
 * \addtogroup gpioConfig7 GPIO 7
 */
/*@{*/
#define GPIO_CH_7_PIN		GPIO_Pin_7									/**< GPIO Channel 7 uses pin 7.*/
#define GPIO_CH_7_PORT	GPIOA										/**< GPIO Channel 7 uses port A. */
#define GPIO_CH_7_RCC		RCC_AHB1Periph_GPIOA						/**< GPIO Channel 7 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_8
/**
 * \addtogroup gpioConfig8 GPIO 8
 */
/*@{*/
#define GPIO_CH_8_PIN		GPIO_Pin_8									/**< GPIO Channel 8 uses pin 8.*/
#define GPIO_CH_8_PORT	GPIOA										/**< GPIO Channel 8 uses port A. */
#define GPIO_CH_8_RCC		RCC_AHB1Periph_GPIOA						/**< GPIO Channel 8 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_9
/**
 * \addtogroup gpioConfig9 GPIO 9
 */
/*@{*/
#define GPIO_CH_9_PIN		GPIO_Pin_9									/**< GPIO Channel 9 uses pin 9.*/
#define GPIO_CH_9_PORT	GPIOA										/**< GPIO Channel 9 uses port A. */
#define GPIO_CH_9_RCC		RCC_AHB1Periph_GPIOA						/**< GPIO Channel 9 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_10
/**
 * \addtogroup gpioConfig10 GPIO 10
 */
/*@{*/
#define GPIO_CH_10_PIN		GPIO_Pin_10								/**< GPIO Channel 10 uses pin 10.*/
#define GPIO_CH_10_PORT		GPIOA									/**< GPIO Channel 10 uses port A. */
#define GPIO_CH_10_RCC		RCC_AHB1Periph_GPIOA					/**< GPIO Channel 10 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_11
/**
 * \addtogroup gpioConfig10 GPIO 11
 */
/*@{*/
#define GPIO_CH_11_PIN		GPIO_Pin_11								/**< GPIO Channel 11 uses pin 10.*/
#define GPIO_CH_11_PORT		GPIOA									/**< GPIO Channel 11 uses port A. */
#define GPIO_CH_11_RCC		RCC_AHB1Periph_GPIOA					/**< GPIO Channel 11 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_12
/**
 * \addtogroup gpioConfig12 GPIO 12
 */
/*@{*/
#define GPIO_CH_12_PIN		GPIO_Pin_12								/**< GPIO Channel 12 uses pin 10.*/
#define GPIO_CH_12_PORT		GPIOD									/**< GPIO Channel 12 uses port D. */
#define GPIO_CH_12_RCC		RCC_AHB1Periph_GPIOD					/**< GPIO Channel 12 uses port D. This port requires the AHB1 bus clock. */
/*@}*/


// GPIO_CHAN_13
/**
 * \addtogroup gpioConfig13 GPIO 13
 */
/*@{*/
#define GPIO_CH_13_PIN		GPIO_Pin_13								/**< GPIO Channel 13 uses pin 13.*/
#define GPIO_CH_13_PORT		GPIOD									/**< GPIO Channel 13 uses port D. */
#define GPIO_CH_13_RCC		RCC_AHB1Periph_GPIOD					/**< GPIO Channel 13 uses port D. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_14
/**
 * \addtogroup gpioConfig14 GPIO 14
 */
/*@{*/
#define GPIO_CH_14_PIN		GPIO_Pin_14								/**< GPIO Channel 14 uses pin 14.*/
#define GPIO_CH_14_PORT		GPIOD									/**< GPIO Channel 14 uses port D. */
#define GPIO_CH_14_RCC		RCC_AHB1Periph_GPIOD					/**< GPIO Channel 14 uses port D. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_15
/**
 * \addtogroup gpioConfig15 GPIO 15
 */
/*@{*/
#define GPIO_CH_15_PIN		GPIO_Pin_15								/**< GPIO Channel 15 uses pin 15.*/
#define GPIO_CH_15_PORT		GPIOD									/**< GPIO Channel 15 uses port D. */
#define GPIO_CH_15_RCC		RCC_AHB1Periph_GPIOD					/**< GPIO Channel 15 uses port D. This port requires the AHB1 bus clock. */
/*@}*/

/*@}*/



/**
 * \defgroup hwAllocation_I2C I2C peripheral configuration
 */
/*@{*/

// I2C channel 1
/**
 * \addtogroup i2cConfig I2C_CH1
 */
#define I2C1_DEV			I2C1										/**< The I2C Device.*/
#define I2C1_CLK			RCC_APB1Periph_I2C1							/**< The certain clock the I2C Device uses.*/
#define I2C1_EVT_IRQ		I2C1_EV_IRQn								/**< I2C1 Event Interrupt.*/
#define I2C1_ERR_IRQ		I2C1_ER_IRQn								/**< I2C1 Error Interrupt.*/
#define I2C1_AF				GPIO_AF_I2C1								/**< I2C1 Alternate Function mapping.*/
//I2C Ultrasonic
#define I2C1_SDA_PIN		GPIO_Pin_7									/**< I2C1 Data PIN.*/
#define I2C1_SDA_PORT		GPIOB										/**< The used GPIO Port of the I2C1 Data PIN.*/
#define I2C1_SDA_CLK		RCC_AHB1Periph_GPIOB						/**< The port requires the AHB1 bus clock.*/
#define I2C1_SDA_AF_PIN		GPIO_PinSource7								/**< The used Pin source.*/
//I2C Gyro
#define I2C1_SCL_PIN		GPIO_Pin_8									/**< I2C1 Clock PIN.*/
#define I2C1_SCL_PORT		GPIOB										/**< The used GPIO Port of the I2C1 Clock PIN.*/
#define I2C1_SCL_CLK		RCC_AHB1Periph_GPIOB						/**< The port requires the AHB1 bus clock.*/
#define I2C1_SCL_AF_PIN		GPIO_PinSource8								/**< The used Pin source.*/
/*@}*/

// I2C channel 2
/**
 * \addtogroup i2cConfig I2C_CH1
 */
#define I2C2_DEV			I2C2										/**< The I2C Device.*/
#define I2C2_CLK			RCC_APB1Periph_I2C2							/**< The certain clock the I2C Device uses.*/
#define I2C2_EVT_IRQ		I2C2_EV_IRQn								/**< I2C2 Event Interrupt.*/
#define I2C2_ERR_IRQ		I2C2_ER_IRQn								/**< I2C2 Error Interrupt.*/
#define I2C2_AF				GPIO_AF_I2C2								/**< I2C2 Alternate Function mapping.*/

#define I2C2_SDA_PIN		GPIO_Pin_11									/**< I2C2 Data PIN.*/
#define I2C2_SDA_PORT		GPIOB										/**< The used GPIO Port of the I2C2 Data PIN.*/
#define I2C2_SDA_CLK		RCC_AHB1Periph_GPIOB						/**< The port requires the AHB1 bus clock.*/
#define I2C2_SDA_AF_PIN		GPIO_PinSource11								/**< The used Pin source.*/

#define I2C2_SCL_PIN		GPIO_Pin_10									/**< I2C2 Clock PIN.*/
#define I2C2_SCL_PORT		GPIOB										/**< The used GPIO Port of the I2C2 Clock PIN.*/
#define I2C2_SCL_CLK		RCC_AHB1Periph_GPIOB						/**< The port requires the AHB1 bus clock.*/
#define I2C2_SCL_AF_PIN		GPIO_PinSource10								/**< The used Pin source.*/
/*@}*/

/*@}*/


/**
 * \defgroup hwAllocation_CAN CAN peripheral configuration
 */
/*@{*/

/**
 * \addtogroup CAN1
 */
/*@{*/
#define CAN1_PORT			GPIOD										/**< The used GPIO Port of CAN1.*/
#define CAN1_CLK			RCC_APB1Periph_CAN1							/**< The port requires the AHB1 bus clock.*/
#define CAN1_TX_PIN			GPIO_Pin_1									/**< CAN1 Transmit PIN.*/
#define CAN1_RX_PIN			GPIO_Pin_0									/**< CAN1 Receive PIN.*/
#define CAN1_AF				GPIO_AF_CAN1								/**< CAN1 Alternate Function mapping.*/
/*@}*/

/*@}*/


/**
 * \defgroup hwAllocation_SPI SPI peripheral configuration
 */
/*@{*/

/**
 * \addtogroup SPI2
 */
/*@{*/

#define SPI2_PORT			GPIOB										/**< The used GPIO Port of SPI2.*/
#define SPI2_SCK			RCC_APB1Periph_SPI2							/**< The port requires the AHB1 bus clock.*/
#define SPI2_SCK_PIN		GPIO_Pin_13									/**< SPI2 Clock PIN.*/
#define SPI2_MOSI_PIN		GPIO_Pin_15									/**< SPI2 Master Out Slave In PIN.*/
#define SPI2_MISO_PIN		GPIO_Pin_14									/**< SPI2 Master In Slave Out PIN.*/
#define SPI2_AF				GPIO_AF_SPI2								/**< SPI2 Alternate Function mapping.*/
/*@}*/

/*@}*/


/**
 * \defgroup hwAllocation_UART UART peripheral configuration
 */
/*@{*/

// UART 1
/**
 * \addtogroup uartConfig1 UART1
 */
/*@{*/
#define UART1_CHAN			USART1									/**< UART Channel 1 uses USART1 on board. */
#define UART1_CLOCK			RCC_APB2Periph_USART1					/**< UART Channel 1 uses USART1. This port requires the APB2 bus clock.*/
#define UART1_AF			GPIO_AF_USART1							/**< Pin connect with USART1. */
#define UART1_INTERRUPT		USART1_IRQn								/**< Interrupt service routine from USART1. */

#define UART1_TX_CLOCK		RCC_AHB1Periph_GPIOB					/**< UART Channel 1 uses port B for transmit. This port requires the AHB1 bus clock.*/
#define UART1_TX_PORT		GPIOB									/**< UART Channel 1 uses port B for transmit. */
#define UART1_TX_PIN		GPIO_Pin_6								/**< The GPIO pin 6 is the transmit pin. */
#define UART1_TX_AF_PIN		GPIO_PinSource6							/**< GPIO pin 6 as alternate function and as transmit pin initialized.*/

#define UART1_RX_CLOCK		RCC_AHB1Periph_GPIOB					/**< UART Channel 1 uses port B for receive. This port requires the AHB1 bus clock.*/
#define UART1_RX_PORT		GPIOB									/**< UART Channel 1 uses port B for receive. */
#define UART1_RX_PIN		GPIO_Pin_7								/**< The GPIO pin 7 is the receive pin. */
#define UART1_RX_AF_PIN		GPIO_PinSource7							/**< GPIO pin 7 as alternate function and as receive pin initialized.*/

#define UART1_RINGBUF_SIZE	32										/**< The ring buffer size for the UART channel 1. */
/*@}*/


// UART 2
/**
 * \addtogroup uartConfig2 UART2
 */
/*@{*/
#define UART2_CHAN			USART2									/**< UART Channel 2 uses USART2 on board. */
#define UART2_CLOCK			RCC_APB1Periph_USART2					/**< UART Channel 2 uses USART2. This port requires the APB1 bus clock.*/
#define UART2_AF			GPIO_AF_USART2							/**< Pin connect with USART2. */
#define UART2_INTERRUPT		USART2_IRQn								/**< Interrupt service routine from USART2. */

#define UART2_TX_CLOCK		RCC_AHB1Periph_GPIOD					/**< UART Channel 2 uses port D for transmit. This port requires the AHB1 bus clock.*/
#define UART2_TX_PORT		GPIOD									/**< UART Channel 2 uses port D for transmit. */
#define UART2_TX_PIN		GPIO_Pin_5								/**< The GPIO pin 5 is the transmit pin. */
#define UART2_TX_AF_PIN		GPIO_PinSource5							/**< GPIO pin 5 as alternate function and as transmit pin initialized.*/

#define UART2_RX_CLOCK		RCC_AHB1Periph_GPIOD					/**< UART Channel 2 uses port D for receive. This port requires the AHB1 bus clock.*/
#define UART2_RX_PORT		GPIOD									/**< UART Channel 2 uses port D for receive. */
#define UART2_RX_PIN		GPIO_Pin_6								/**< The GPIO pin 6 is the receive pin. */
#define UART2_RX_AF_PIN		GPIO_PinSource6							/**< GPIO pin 6 as alternate function and as receive pin initialized.*/

#define UART2_RINGBUF_SIZE	32										/**< The ring buffer size for the UART channel 2. */
/*@}*/


// UART 3
/**
 * \addtogroup uartConfig3 UART3 //Serial Master
 */
/*@{*/
#define UART3_CHAN			USART3									/**< UART Channel 3 uses USART3 on board. */
#define UART3_CLOCK			RCC_APB1Periph_USART3					/**< UART Channel 3 uses USART3. This port requires the APB1 bus clock.*/
#define UART3_AF			GPIO_AF_USART3							/**< Pin connect with USART3. */
#define UART3_INTERRUPT		USART3_IRQn								/**< Interrupt service routine from USART33. */

#define UART3_TX_CLOCK		RCC_AHB1Periph_GPIOD					/**< UART Channel 3 uses port D for transmit. This port requires the AHB1 bus clock.*/
#define UART3_TX_PORT		GPIOD									/**< UART Channel 3 uses port D for transmit. */
#define UART3_TX_PIN		GPIO_Pin_8								/**< The GPIO pin 8 is the transmit pin. */
#define UART3_TX_AF_PIN		GPIO_PinSource8							/**< GPIO pin 8 as alternate function and as transmit pin initialized.*/

#define UART3_RX_CLOCK		RCC_AHB1Periph_GPIOD					/**< UART Channel 3 uses port D for receive. This port requires the AHB1 bus clock.*/
#define UART3_RX_PORT		GPIOD									/**< UART Channel 3 uses port D for receive. */
#define UART3_RX_PIN		GPIO_Pin_9								/**< The GPIO pin 9 is the receive pin. */
#define UART3_RX_AF_PIN		GPIO_PinSource9							/**< GPIO pin 9 as alternate function and as receive pin initialized.*/

#define UART3_RINGBUF_SIZE	32										/**< The ring buffer size for the UART channel 3. */
/*@}*/


//UART 4 only for io functions
/**
 * \addtogroup uartConfig4 UART4 only for io functions
 */
/*@{*/
#define UART4_CHAN			USART6									/**< UART Channel 4 uses USART6 on board. */
#define UART4_CLOCK			RCC_APB2Periph_USART6					/**< UART Channel 4 uses USART6. This port requires the APB2 bus clock.*/
#define UART4_AF			GPIO_AF_USART6							/**< Pin connect with USART6. */
#define UART4_INTERRUPT		USART6_IRQn								/**< Interrupt service routine from USART6. */

#define UART4_TX_CLOCK		RCC_AHB1Periph_GPIOC					/**< UART Channel 4 uses port C for transmit. This port requires the AHB1 bus clock.*/
#define UART4_TX_PORT		GPIOC									/**< UART Channel 4 uses port C for transmit. */
#define UART4_TX_PIN		GPIO_Pin_6								/**< The GPIO pin 6 is the transmit pin. */
#define UART4_TX_AF_PIN		GPIO_PinSource6							/**< GPIO pin 6 as alternate function and as transmit pin initialized.*/

#define UART4_RX_CLOCK		RCC_AHB1Periph_GPIOC					/**< UART Channel 4 uses port C for receive. This port requires the AHB1 bus clock.*/
#define UART4_RX_PORT		GPIOC									/**< UART Channel 4 uses port C for receive. */
#define UART4_RX_PIN		GPIO_Pin_7								/**< The GPIO pin 7 is the receive pin. */
#define UART4_RX_AF_PIN		GPIO_PinSource7							/**< GPIO pin 7 as alternate function and as receive pin initialized.*/

#define UART4_RINGBUF_SIZE	128										/**< The ring buffer size for the UART channel 4. Especially for the ring buffer selected so large!!!!!*/
/*@}*/

// UART 5
/**
 * \addtogroup uartConfig5 UART5 //Terminal
 */
/*@{*/
#define UART5_CHAN			UART5									/**< UART Channel 5 uses USART5 on board. */
#define UART5_CLOCK			RCC_APB1Periph_UART5					/**< UART Channel 5 uses USART5. This port requires the APB1 bus clock.*/
#define UART5_AF			GPIO_AF_UART5							/**< Pin connect with USART5. */
#define UART5_INTERRUPT		UART5_IRQn								/**< Interrupt service routine from USART5. */

#define UART5_TX_CLOCK		RCC_AHB1Periph_GPIOD					/**< UART Channel 5 uses port D for transmit. This port requires the AHB1 bus clock.*/
#define UART5_TX_PORT		GPIOD									/**< UART Channel 5 uses port D for transmit. */
#define UART5_TX_PIN		GPIO_Pin_2								/**< The GPIO pin 2 is the transmit pin. */
#define UART5_TX_AF_PIN		GPIO_PinSource2							/**< GPIO pin 2 as alternate function and as transmit pin initialized.*/

#define UART5_RX_CLOCK		RCC_AHB1Periph_GPIOC					/**< UART Channel 5 uses port D for receive. This port requires the AHB1 bus clock.*/
#define UART5_RX_PORT		GPIOC									/**< UART Channel 5 uses port D for receive. */
#define UART5_RX_PIN		GPIO_Pin_12								/**< The GPIO pin 12 is the receive pin. */
#define UART5_RX_AF_PIN		GPIO_PinSource12						/**< GPIO pin 12 as alternate function and as receive pin initialized.*/

#define UART5_RINGBUF_SIZE	32										/**< The ring buffer size for the UART channel 5. */
/*@}*/

/*@}*/

#endif




//#define Board

#ifdef Board

#define ADC_DEV1                  	ADC1
#define ADC_ADC1_CLOCK            	RCC_APB2Periph_ADC1
#define ADC_DMA1_STREAM           	DMA2_Stream0
#define ADC_DMA1_CHAN             	DMA_Channel_0
#define ADC_TRIGGER1              	ADC_ExternalTrigConv_T1_TRGO
#define ADC_TRIGGER1_EDGE  			ADC_ExternalTrigConvEdge_None

#define ADC_DEV2                  	ADC2
#define ADC_ADC2_CLOCK            	RCC_APB2Periph_ADC2
#define ADC_DMA2_STREAM           	DMA2_Stream0
#define ADC_DMA2_CHAN             	DMA_Channel_0
#define ADC_TRIGGER2              	ADC_ExternalTrigConv_T1_TRGO
#define ADC_TRIGGER2_EDGE  			ADC_ExternalTrigConvEdge_None

#define ADC_DEV3                  	ADC3
#define ADC_ADC3_CLOCK            	RCC_APB2Periph_ADC2
#define ADC_DMA3_STREAM           	DMA2_Stream0
#define ADC_DMA3_CHAN             	DMA_Channel_0
#define ADC_TRIGGER3              	ADC_ExternalTrigConv_T1_TRGO
#define ADC_TRIGGER3_EDGE  			ADC_ExternalTrigConvEdge_None


#define ADC_CH0_PORT              	GPIOA
#define ADC_CH0_PIN            		GPIO_Pin_0
#define ADC_CH0_CHAN              	ADC_Channel_0
#define ADC_CH0_PIN_CLOCK  			RCC_AHB1ENR_GPIOAEN

#define ADC_CH1_PORT              	GPIOA
#define ADC_CH1_PIN            		GPIO_Pin_1
#define ADC_CH1_CHAN              	ADC_Channel_1
#define ADC_CH1_PIN_CLOCK  			RCC_AHB1ENR_GPIOAEN

#define ADC_CH2_PORT              	GPIOA
#define ADC_CH2_PIN            		GPIO_Pin_2
#define ADC_CH2_CHAN              	ADC_Channel_2
#define ADC_CH2_PIN_CLOCK  			RCC_AHB1ENR_GPIOAEN

#define ADC_CH3_PORT              	GPIOA
#define ADC_CH3_PIN            		GPIO_Pin_3
#define ADC_CH3_CHAN              	ADC_Channel_3
#define ADC_CH3_PIN_CLOCK  			RCC_AHB1ENR_GPIOAEN

#define ADC_CH4_PORT              	GPIOA
#define ADC_CH4_PIN            		GPIO_Pin_4
#define ADC_CH4_CHAN              	ADC_Channel_4
#define ADC_CH4_PIN_CLOCK  			RCC_AHB1ENR_GPIOAEN

#define ADC_CH5_PORT              	GPIOA
#define ADC_CH5_PIN            		GPIO_Pin_5
#define ADC_CH5_CHAN              	ADC_Channel_5
#define ADC_CH5_PIN_CLOCK  			RCC_AHB1ENR_GPIOAEN

#define ADC_CH6_PORT              	GPIOA
#define ADC_CH6_PIN                	GPIO_Pin_6
#define ADC_CH6_CHAN             	ADC_Channel_6
#define ADC_CH6_PIN_CLOCK  			RCC_AHB1ENR_GPIOAEN

#define ADC_CH7_PORT              	GPIOA
#define ADC_CH7_PIN                	GPIO_Pin_7
#define ADC_CH7_CHAN              	ADC_Channel_7
#define ADC_CH7_PIN_CLOCK  			RCC_AHB1ENR_GPIOAEN

#define ADC_CH8_PORT              	GPIOB
#define ADC_CH8_PIN                	GPIO_Pin_0
#define ADC_CH8_CHAN              	ADC_Channel_8
#define ADC_CH8_PIN_CLOCK  			RCC_AHB1ENR_GPIOBEN

#define ADC_CH9_PORT              	GPIOB
#define ADC_CH9_PIN                	GPIO_Pin_1
#define ADC_CH9_CHAN              	ADC_Channel_9
#define ADC_CH9_PIN_CLOCK  			RCC_AHB1ENR_GPIOBEN

#define ADC_CH10_PORT             	GPIOC
#define ADC_CH10_PIN        		GPIO_Pin_0
#define ADC_CH10_CHAN             	ADC_Channel_10
#define ADC_CH10_PIN_CLOCK 			RCC_AHB1ENR_GPIOCEN


#define ADC_CH11_PORT             	GPIOC
#define ADC_CH11_PIN        		GPIO_Pin_1
#define ADC_CH11_CHAN             	ADC_Channel_11
#define ADC_CH11_PIN_CLOCK 			RCC_AHB1ENR_GPIOCEN

#define ADC_CH12_PORT             	GPIOC
#define ADC_CH12_PIN        		GPIO_Pin_2
#define ADC_CH12_CHAN             	ADC_Channel_12
#define ADC_CH12_PIN_CLOCK 			RCC_AHB1ENR_GPIOCEN

#define ADC_CH13_PORT             	GPIOC
#define ADC_CH13_PIN        		GPIO_Pin_3
#define ADC_CH13_CHAN             	ADC_Channel_13
#define ADC_CH13_PIN_CLOCK 			RCC_AHB1ENR_GPIOCEN

#define ADC_CH14_PORT             	GPIOC
#define ADC_CH14_PIN        		GPIO_Pin_4
#define ADC_CH14_CHAN             	ADC_Channel_14
#define ADC_CH14_PIN_CLOCK 			RCC_AHB1ENR_GPIOCEN

#define ADC_CH15_PORT             	GPIOC
#define ADC_CH15_PIN        		GPIO_Pin_5
#define ADC_CH15_CHAN             	ADC_Channel_15
#define ADC_CH15_PIN_CLOCK 			RCC_AHB1ENR_GPIOCEN

/**
 * \defgroup hwAllocation_GPIO GPIO periphial configuration
 */
/*@{*/

// GPIO_CHAN_0
/**
 * \addtogroup gpioConfig0 GPIO 0
 */
/*@{*/
#define GPIO_CHAN_0_PIN		GPIO_Pin_0					/**< GPIO Channel 0 uses pin 0.*/
#define GPIO_CHAN_0_PORT	GPIOA						/**< GPIO Channel 0 uses port A. */
#define GPIO_CHAN_0_RCC		RCC_AHB1Periph_GPIOA		/**< GPIO Channel 0 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_1
/**
 * \addtogroup gpioConfig1 GPIO 1
 */
/*@{*/
#define GPIO_CHAN_1_PIN		GPIO_Pin_1					/**< GPIO Channel 1 uses pin 1.*/
#define GPIO_CHAN_1_PORT	GPIOA						/**< GPIO Channel 1 uses port A. */
#define GPIO_CHAN_1_RCC		RCC_AHB1Periph_GPIOA		/**< GPIO Channel 1 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_2
/**
 * \addtogroup gpioConfig2 GPIO 2
 */
/*@{*/
#define GPIO_CHAN_2_PIN		GPIO_Pin_2					/**< GPIO Channel 2 uses pin 2.*/
#define GPIO_CHAN_2_PORT	GPIOA						/**< GPIO Channel 2 uses port A. */
#define GPIO_CHAN_2_RCC		RCC_AHB1Periph_GPIOA		/**< GPIO Channel 2 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_3
/**
 * \addtogroup gpioConfig3 GPIO 3
 */
/*@{*/
#define GPIO_CHAN_3_PIN		GPIO_Pin_3					/**< GPIO Channel 3 uses pin 3.*/
#define GPIO_CHAN_3_PORT	GPIOA						/**< GPIO Channel 3 uses port A. */
#define GPIO_CHAN_3_RCC		RCC_AHB1Periph_GPIOA		/**< GPIO Channel 3 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_4
/**
 * \addtogroup gpioConfig4 GPIO 4
 */
/*@{*/
#define GPIO_CHAN_4_PIN		GPIO_Pin_4					/**< GPIO Channel 4 uses pin 4.*/
#define GPIO_CHAN_4_PORT	GPIOA						/**< GPIO Channel 4 uses port A. */
#define GPIO_CHAN_4_RCC		RCC_AHB1Periph_GPIOA		/**< GPIO Channel 4 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_5
/**
 * \addtogroup gpioConfig5 GPIO 5
 */
/*@{*/
#define GPIO_CHAN_5_PIN		GPIO_Pin_5					/**< GPIO Channel 5 uses pin 5.*/
#define GPIO_CHAN_5_PORT	GPIOA						/**< GPIO Channel 5 uses port A. */
#define GPIO_CHAN_5_RCC		RCC_AHB1Periph_GPIOA		/**< GPIO Channel 5 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_6
/**
 * \addtogroup gpioConfig6 GPIO 6
 */
/*@{*/
#define GPIO_CHAN_6_PIN		GPIO_Pin_6					/**< GPIO Channel 6 uses pin 6.*/
#define GPIO_CHAN_6_PORT	GPIOA						/**< GPIO Channel 6 uses port A. */
#define GPIO_CHAN_6_RCC		RCC_AHB1Periph_GPIOA		/**< GPIO Channel 6 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_7
/**
 * \addtogroup gpioConfig7 GPIO 7
 */
/*@{*/
#define GPIO_CHAN_7_PIN		GPIO_Pin_7					/**< GPIO Channel 7 uses pin 7.*/
#define GPIO_CHAN_7_PORT	GPIOA						/**< GPIO Channel 7 uses port A. */
#define GPIO_CHAN_7_RCC		RCC_AHB1Periph_GPIOA		/**< GPIO Channel 7 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_8
/**
 * \addtogroup gpioConfig8 GPIO 8
 */
/*@{*/
#define GPIO_CHAN_8_PIN		GPIO_Pin_8					/**< GPIO Channel 8 uses pin 8.*/
#define GPIO_CHAN_8_PORT	GPIOA						/**< GPIO Channel 8 uses port A. */
#define GPIO_CHAN_8_RCC		RCC_AHB1Periph_GPIOA		/**< GPIO Channel 8 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_9
/**
 * \addtogroup gpioConfig9 GPIO 9
 */
/*@{*/
#define GPIO_CHAN_9_PIN		GPIO_Pin_9					/**< GPIO Channel 9 uses pin 9.*/
#define GPIO_CHAN_9_PORT	GPIOA						/**< GPIO Channel 9 uses port A. */
#define GPIO_CHAN_9_RCC		RCC_AHB1Periph_GPIOA		/**< GPIO Channel 9 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_10
/**
 * \addtogroup gpioConfig10 GPIO 10
 */
/*@{*/
#define GPIO_CHAN_10_PIN		GPIO_Pin_10				/**< GPIO Channel 10 uses pin 10.*/
#define GPIO_CHAN_10_PORT		GPIOA					/**< GPIO Channel 10 uses port A. */
#define GPIO_CHAN_10_RCC		RCC_AHB1Periph_GPIOA	/**< GPIO Channel 10 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_11
/**
 * \addtogroup gpioConfig10 GPIO 11
 */
/*@{*/
#define GPIO_CHAN_11_PIN		GPIO_Pin_11				/**< GPIO Channel 11 uses pin 10.*/
#define GPIO_CHAN_11_PORT		GPIOA					/**< GPIO Channel 11 uses port A. */
#define GPIO_CHAN_11_RCC		RCC_AHB1Periph_GPIOA	/**< GPIO Channel 11 uses port A. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_12
/**
 * \addtogroup gpioConfig12 GPIO 12
 */
/*@{*/
#define GPIO_CHAN_12_PIN		GPIO_Pin_12				/**< GPIO Channel 12 uses pin 10.*/
#define GPIO_CHAN_12_PORT		GPIOD					/**< GPIO Channel 12 uses port D. */
#define GPIO_CHAN_12_RCC		RCC_AHB1Periph_GPIOD	/**< GPIO Channel 12 uses port D. This port requires the AHB1 bus clock. */
/*@}*/


// GPIO_CHAN_13
/**
 * \addtogroup gpioConfig13 GPIO 13
 */
/*@{*/
#define GPIO_CHAN_13_PIN		GPIO_Pin_13				/**< GPIO Channel 13 uses pin 13.*/
#define GPIO_CHAN_13_PORT		GPIOD					/**< GPIO Channel 13 uses port D. */
#define GPIO_CHAN_13_RCC		RCC_AHB1Periph_GPIOD	/**< GPIO Channel 13 uses port D. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_14
/**
 * \addtogroup gpioConfig14 GPIO 14
 */
/*@{*/
#define GPIO_CHAN_14_PIN		GPIO_Pin_14				/**< GPIO Channel 14 uses pin 14.*/
#define GPIO_CHAN_14_PORT		GPIOD					/**< GPIO Channel 14 uses port D. */
#define GPIO_CHAN_14_RCC		RCC_AHB1Periph_GPIOD	/**< GPIO Channel 14 uses port D. This port requires the AHB1 bus clock. */
/*@}*/

// GPIO_CHAN_15
/**
 * \addtogroup gpioConfig15 GPIO 15
 */
/*@{*/
#define GPIO_CHAN_15_PIN		GPIO_Pin_15				/**< GPIO Channel 15 uses pin 15.*/
#define GPIO_CHAN_15_PORT		GPIOD					/**< GPIO Channel 15 uses port D. */
#define GPIO_CHAN_15_RCC		RCC_AHB1Periph_GPIOD	/**< GPIO Channel 15 uses port D. This port requires the AHB1 bus clock. */
/*@}*/
/*@}*/


/**
 * \defgroup hwAllocation_I2C I2C periphial configuration
 */
/*@{*/

// I2C channel 1
/**
 * \addtogroup i2cConfig I2C_CH1
 */
#define I2C1_DEV			I2C1
#define I2C1_CLK			RCC_APB1Periph_I2C1
#define I2C1_EVT_IRQ		I2C1_EV_IRQn
#define I2C1_ERR_IRQ		I2C1_ER_IRQn
#define I2C1_AF				GPIO_AF_I2C1
#define I2C1_SDA_PIN		GPIO_Pin_9
#define I2C1_SDA_PORT		GPIOB
#define I2C1_SDA_CLK		RCC_AHB1Periph_GPIOB
#define I2C1_SDA_AF_PIN		GPIO_PinSource9
#define I2C1_SCL_PIN		GPIO_Pin_8
#define I2C1_SCL_PORT		GPIOB
#define I2C1_SCL_CLK		RCC_AHB1Periph_GPIOB
#define I2C1_SCL_AF_PIN		GPIO_PinSource8
/*@}*/

// I2C channel 2
/**
 * \addtogroup i2cConfig I2C_CH1
 */
#define I2C2_DEV			I2C2
#define I2C2_CLK			RCC_APB1Periph_I2C2
#define I2C2_EVT_IRQ		I2C2_EV_IRQn
#define I2C2_ERR_IRQ		I2C2_ER_IRQn
#define I2C2_AF				GPIO_AF_I2C2
#define I2C2_SDA_PIN		GPIO_Pin_11
#define I2C2_SDA_PORT		GPIOB
#define I2C2_SDA_CLK		RCC_AHB1Periph_GPIOB
#define I2C2_SDA_AF_PIN		GPIO_PinSource11
#define I2C2_SCL_PIN		GPIO_Pin_10
#define I2C2_SCL_PORT		GPIOB
#define I2C2_SCL_CLK		RCC_AHB1Periph_GPIOB
#define I2C2_SCL_AF_PIN		GPIO_PinSource10
/*@}*/

// I2C channel 3
/**
 * \addtogroup i2cConfig I2C_CH1
 */
#define I2C3_DEV			I2C3
#define I2C3_CLK			RCC_APB1Periph_I2C3
#define I2C3_EVT_IRQ		I2C3_EV_IRQn
#define I2C3_ERR_IRQ		I2C3_ER_IRQn
#define I2C3_AF				GPIO_AF_I2C3
#define I2C3_SDA_PIN		GPIO_Pin_9
#define I2C3_SDA_PORT		GPIOC
#define I2C3_SDA_CLK		RCC_AHB1Periph_GPIOC
#define I2C3_SDA_AF_PIN		GPIO_PinSource9
#define I2C3_SCL_PIN		GPIO_Pin_8
#define I2C3_SCL_PORT		GPIOA
#define I2C3_SCL_CLK		RCC_AHB1Periph_GPIOA
#define I2C3_SCL_AF_PIN		GPIO_PinSource8
/*@}*/
/*@}*/




/**
 * \defgroup hwAllocation_PWM PWM periphial configuration
 */
/*@{*/
#define PWM_PORT				    GPIOE					    /**< PWM channel 1,2,3 and 4 uses port E. */
#define PWM_PIN_CLOCK	            RCC_AHB1Periph_GPIOE	    /**< PWM channel 1,2,3 and 4 uses port E. This port requires the AHB1 bus clock.*/
#define PWM_TIMER			        TIM1					    /**< To generate the PWM signal on the 4 channels , timer 1 is taken.*/
#define PWM_TIM_CLOCK		        RCC_APB2Periph_TIM1 		/**< PWM channel 1,2,3 and 4 uses timer 1. This timer requires the APB2 bus clock.*/


// PWM channel 1
/**
 * \addtogroup pwmConfig1 PWM channel 1 uses timer 1 channel 1
 */
/*@{*/
															/**< PWM channel 1 uses port E. */
#define PWM_CHAN_1_PIN				GPIO_Pin_9				/**< The PWM signal from channel 1 is output on pin 9.*/
#define PWM_CHAN_1_AF_PIN			GPIO_PinSource9			/**< GPIO pin 9 as alternate function initialized.*/
#define PWM_CHAN_1_AF				GPIO_AF_TIM1			/**< Pin connect with timer 1. */
/*@}*/

// PWM Channel 2
/**
 * \addtogroup pwmConfig2 PWM channel 2 uses timer 1 channel 2
 */
/*@{*/
															/**< PWM channel 11 uses port E. */
#define PWM_CHAN_2_PIN				GPIO_Pin_11				/**< The PWM signal from channel 2 is output on pin 11.*/
#define PWM_CHAN_2_AF_PIN			GPIO_PinSource11		/**< GPIO pin 11 as alternate function initialized.*/
#define PWM_CHAN_2_AF				GPIO_AF_TIM1			/**< Pin connect with timer 1. */
/*@}*/


// PWM Channel 3
/**
 * \addtogroup pwmConfig3 PWM channel 1 uses timer 3 channel 3
 */
/*@{*/
															/**< PWM channel 13 uses port E. */
#define PWM_CHAN_3_PIN				GPIO_Pin_13				/**< The PWM signal from channel 3 is output on pin 13.*/
#define PWM_CHAN_3_AF_PIN			GPIO_PinSource13		/**< GPIO pin 13 as alternate function initialized.*/
#define PWM_CHAN_3_AF				GPIO_AF_TIM1			/**< Pin connect with timer 1. */
/*@}*/


// PWM Channel 4
/**
 * \addtogroup pwmConfig4 PWM channel 4 uses timer 1 channel 4
 */
/*@{*/
															/**< PWM channel 4 uses port E. */
#define PWM_CHAN_4_PIN				GPIO_Pin_14				/**< The PWM signal from channel 4 is output on pin 14.*/
#define PWM_CHAN_4_AF_PIN			GPIO_PinSource14		/**< GPIO pin 14 as alternate function initialized.*/
#define PWM_CHAN_4_AF				GPIO_AF_TIM1			/**< Pin connect with timer 1. */
/*@}*/
/*@}*/

/**
 * \defgroup hwAllocation_UART UART periphial configuration
 */
/*@{*/

// UART 1
/**
 * \addtogroup uartConfig1 UART1
 */
/*@{*/
#define UART1_CHAN			USART1					/**< UART Channel 1 uses USART1 on board. */
#define UART1_CLOCK			RCC_APB2Periph_USART1	/**< UART Channel 1 uses USART1. This port requires the APB2 bus clock.*/
#define UART1_AF			GPIO_AF_USART1			/**< Pin connect with USART1. */
#define UART1_INTERRUPT		USART1_IRQn				/**< Interrupt service routine from USART1. */

#define UART1_TX_CLOCK		RCC_AHB1Periph_GPIOB	/**< UART Channel 1 uses port B for transmit. This port requires the AHB1 bus clock.*/
#define UART1_TX_PORT		GPIOB					/**< UART Channel 1 uses port B for transmit. */
#define UART1_TX_PIN		GPIO_Pin_6				/**< The GPIO pin 6 is the transmit pin. */
#define UART1_TX_AF_PIN		GPIO_PinSource6			/**< GPIO pin 6 as alternate function and as transmit pin initialized.*/

#define UART1_RX_CLOCK		RCC_AHB1Periph_GPIOB	/**< UART Channel 1 uses port B for receive. This port requires the AHB1 bus clock.*/
#define UART1_RX_PORT		GPIOB					/**< UART Channel 1 uses port B for receive. */
#define UART1_RX_PIN		GPIO_Pin_7				/**< The GPIO pin 7 is the receive pin. */
#define UART1_RX_AF_PIN		GPIO_PinSource7			/**< GPIO pin 7 as alternate function and as receive pin initialized.*/

#define UART1_RINGBUF_SIZE	32						/**< The ring buffer size for the UART channel 1. */
/*@}*/


// UART 2
/**
 * \addtogroup uartConfig2 UART2
 */
/*@{*/
#define UART2_CHAN			USART2					/**< UART Channel 2 uses USART2 on board. */
#define UART2_CLOCK			RCC_APB1Periph_USART2	/**< UART Channel 2 uses USART2. This port requires the APB1 bus clock.*/
#define UART2_AF			GPIO_AF_USART2			/**< Pin connect with USART2. */
#define UART2_INTERRUPT		USART2_IRQn				/**< Interrupt service routine from USART2. */

#define UART2_TX_CLOCK		RCC_AHB1Periph_GPIOD	/**< UART Channel 2 uses port D for transmit. This port requires the AHB1 bus clock.*/
#define UART2_TX_PORT		GPIOD					/**< UART Channel 2 uses port D for transmit. */
#define UART2_TX_PIN		GPIO_Pin_5				/**< The GPIO pin 5 is the transmit pin. */
#define UART2_TX_AF_PIN		GPIO_PinSource5			/**< GPIO pin 5 as alternate function and as transmit pin initialized.*/

#define UART2_RX_CLOCK		RCC_AHB1Periph_GPIOD	/**< UART Channel 2 uses port D for receive. This port requires the AHB1 bus clock.*/
#define UART2_RX_PORT		GPIOD					/**< UART Channel 2 uses port D for receive. */
#define UART2_RX_PIN		GPIO_Pin_6				/**< The GPIO pin 6 is the receive pin. */
#define UART2_RX_AF_PIN		GPIO_PinSource6			/**< GPIO pin 6 as alternate function and as receive pin initialized.*/

#define UART2_RINGBUF_SIZE	32						/**< The ring buffer size for the UART channel 2. */
/*@}*/


// UART 3
/**
 * \addtogroup uartConfig3 UART3
 */
/*@{*/
#define UART3_CHAN			USART3					/**< UART Channel 3 uses USART3 on board. */
#define UART3_CLOCK			RCC_APB1Periph_USART3	/**< UART Channel 3 uses USART3. This port requires the APB1 bus clock.*/
#define UART3_AF			GPIO_AF_USART3			/**< Pin connect with USART3. */
#define UART3_INTERRUPT		USART3_IRQn				/**< Interrupt service routine from USART33. */

#define UART3_TX_CLOCK		RCC_AHB1Periph_GPIOD	/**< UART Channel 3 uses port D for transmit. This port requires the AHB1 bus clock.*/
#define UART3_TX_PORT		GPIOD					/**< UART Channel 3 uses port D for transmit. */
#define UART3_TX_PIN		GPIO_Pin_8				/**< The GPIO pin 8 is the transmit pin. */
#define UART3_TX_AF_PIN		GPIO_PinSource8			/**< GPIO pin 8 as alternate function and as transmit pin initialized.*/

#define UART3_RX_CLOCK		RCC_AHB1Periph_GPIOD	/**< UART Channel 3 uses port D for receive. This port requires the AHB1 bus clock.*/
#define UART3_RX_PORT		GPIOD					/**< UART Channel 3 uses port D for receive. */
#define UART3_RX_PIN		GPIO_Pin_9				/**< The GPIO pin 9 is the receive pin. */
#define UART3_RX_AF_PIN		GPIO_PinSource9			/**< GPIO pin 9 as alternate function and as receive pin initialized.*/

#define UART3_RINGBUF_SIZE	32						/**< The ring buffer size for the UART channel 3. */
/*@}*/


//UART 4 only for io functions
/**
 * \addtogroup uartConfig4 UART4 only for io functions
 */
/*@{*/
#define UART4_CHAN			USART6					/**< UART Channel 4 uses USART6 on board. */
#define UART4_CLOCK			RCC_APB2Periph_USART6	/**< UART Channel 4 uses USART6. This port requires the APB2 bus clock.*/
#define UART4_AF			GPIO_AF_USART6			/**< Pin connect with USART6. */
#define UART4_INTERRUPT		USART6_IRQn				/**< Interrupt service routine from USART6. */

#define UART4_TX_CLOCK		RCC_AHB1Periph_GPIOC	/**< UART Channel 4 uses port C for transmit. This port requires the AHB1 bus clock.*/
#define UART4_TX_PORT		GPIOC					/**< UART Channel 4 uses port C for transmit. */
#define UART4_TX_PIN		GPIO_Pin_6				/**< The GPIO pin 6 is the transmit pin. */
#define UART4_TX_AF_PIN		GPIO_PinSource6			/**< GPIO pin 6 as alternate function and as transmit pin initialized.*/

#define UART4_RX_CLOCK		RCC_AHB1Periph_GPIOC	/**< UART Channel 4 uses port C for receive. This port requires the AHB1 bus clock.*/
#define UART4_RX_PORT		GPIOC					/**< UART Channel 4 uses port C for receive. */
#define UART4_RX_PIN		GPIO_Pin_7				/**< The GPIO pin 7 is the receive pin. */
#define UART4_RX_AF_PIN		GPIO_PinSource7			/**< GPIO pin 7 as alternate function and as receive pin initialized.*/

#define UART4_RINGBUF_SIZE	128						/**< The ring buffer size for the UART channel 4. Especially for the ring buffer selected so large!!!!!*/




#endif


/*@}*/
/*@}*/

/************* Public function prototypes *************************************************/

#endif /* HWALLOCATION_H_ */
