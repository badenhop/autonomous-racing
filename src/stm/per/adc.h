/** *****************************************************************************************
 * Unit in charge:
 * @file	adc.h
 * @author	Stefan Sonski
 * $Date:: 2014-05-16 13:48:47 #$
 * 
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 473                   $
 * $Author:: wandji-kwuntchou   $
 * 
 * ----------------------------------------------------------
 *
 * @brief	The module provides an interface for the adc. Provides functions to initialize the ADC, the ADC channels and convert values.
 *
 * To use the ADC module, first the ADC device has to be initialized, which is done by adc_init_device(). To achieve this
 * the macro ADC_EN has to be defined in app/config.h. After initializing the ADC itself, the channels need to be initialized, which is done
 * by adc_init_channel(). Additionally the individual ADC channels have to be enabled by defining ADC_CHx_EN in app/config.h, where x
 * is the index of the channel. It has to be noticed that all channels needed, have to be initialized, before starting the ADC. To start the
 * ADC, the function adc_start() is used. If the ADC is shall be stopped, this module provides the function #adc_stop(). To receive the latest
 * converted value adc_getValue() is used.
 *
 * The ADC samples continuously. Therefore when the ADC has sampled one value, the next sampling starts. Because DMA is used, the CPU is not
 * stressed. Because of the fact of multiple channels, that are sampled alternately, the sampling rate depends on the amount of channels, that are
 * used. Every channel uses 15 cycles to sample the values. If n channels are used, \f$n \times 15\f$ cycles are needed to sample every channel once.
 * The actual cycle depends on the APB2 clock.
 *
 * Example Code for using the ADC.
 * @code
 *	adc_init_device();
 *	adc_init_channel(ADC_CH1);
 *	adc_start();
 *
 *	while(1) {
 *		uint16 result;
 *		adc_getValue(ADC_CH1, &result);
 *		doSomething(result);
 *	}
 *
 *	adc_stop(ADC_CH1);
 * @endcode
 *
 ******************************************************************************************/

#ifndef ADC_H_
#define ADC_H_

/************* Includes *******************************************************************/
#include "brd/startup/stm32f4xx.h"
#include "brd/types.h"
#include "per/hwallocation.h"
#include "app/config.h"


#ifdef ADC_EN

/************* Public typedefs ************************************************************/
/**
 * @enum adc_channel_t
 *
 * The different ADC channels. The GPIO pins, which are used for the input of the different ADC channels can be looked up in hwallocation.h.
 *
 */
typedef enum {
#ifdef ADC_CH1_EN
	ADC_CH1 = 0,		/**< ADC Channel 1 */
#endif
#ifdef ADC_CH2_EN
	ADC_CH2,			/**< ADC Channel 2 */
#endif
#ifdef ADC_CH3_EN
	ADC_CH3,			/**< ADC Channel 3 */
#endif
#ifdef ADC_CH4_EN
	ADC_CH4,			/**< ADC Channel 4 */
#endif
#ifdef ADC_CH5_EN
	ADC_CH5,			/**< ADC Channel 5 */
#endif
#ifdef ADC_CH6_EN
	ADC_CH6,			/**< ADC Channel 6 */
#endif
#ifdef ADC_CH7_EN
	ADC_CH7,			/**< ADC Channel 7 */
#endif
#ifdef ADC_CH8_EN
	ADC_CH8,			/**< ADC Channel 8 */
#endif
#ifdef ADC_CH9_EN
	ADC_CH9,			/**< ADC Channel 9 */
#endif
	ADC_NUM_CHANNELS	/**< Used to calculate the amount of enabled channels */
} adc_channel_t;

/************* Macros and constants *******************************************************/


/************* Public function prototypes *************************************************/
/**
 * @brief Initializes the ADC device.
 *
 * @return Returns 0 if the initialization was successfully. In case of an error -1 is returned.
 */
int8_t adc_initDevice(void);

/**
 * @brief Initializes a ADC channel. The ADC device has to be initialized before calling this function.
 *
 * @param [in] channel - The channel that shall be initialized.
 *
 * @return Returns 0 if the initialization was successfully. In case of an error -1 is returned.
 */
int8_t adc_initChannel(adc_channel_t channel);

/**
 * @brief Starts the ADC device.
 *
 * The ADC device and all ADC channels that are needed have to be initialized before calling this function.
 *
 * @return Returns 0 if the start of the ADC device was successfully. In case of an error -1 is returned.
 */
int8_t adc_start(void);

/**
 * @brief Get the latest converted analog value of a initialized ADC channel.
 *
 * @param [in] channel		-	The started channel from which the latest converted value shall be read.
 *
 * @param [out] *pValue	-	The target variable for the converted value.
 *
 * @return Returns 0 if the value has been read correctly. In case of an error -1 is returned.
 */
int8_t adc_getValue(adc_channel_t channel, uint16_t* pValue);

/**
 * @brief Stops the continuous conversion of the ADC device.
 *
 * @return Returns 0 if the stop of the ADC device was successfully. In case of an error -1 is returned.
 */
int8_t adc_stop(void);

#endif /* ADC_EN */
#endif /* ADC_H_ */
