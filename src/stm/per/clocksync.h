/** *****************************************************************************************
 * Unit in charge:
 * @file	clocksync.h
 * @author	Malte Brieske
 * $Date:: 2017-06-08 13:48:47 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 473                   $
 * $Author:: wandji-kwuntchou   $
 *
 * ----------------------------------------------------------
 *
 * @brief	summary
 *
 *
 ******************************************************************************************/

#ifndef CLOCKSYNC_H_
#define CLOCKSYNC_H_

#include "sys/systime/systime.h"
#include "brd/startup/stm32f4xx.h"
#include "brd/stm32f4xx_exti.h"
#include "brd/stm32f4xx_syscfg.h"

//#ifdef CLOCKSYNC_EN




/************* Includes *******************************************************************/


/************* Public typedefs ************************************************************/


/************* Macros and constants *******************************************************/


/************* Public function prototypes *************************************************/

int8_t clocksync_initDevice(void);

//#endif /* CLOCKSYNC_EN */
#endif /* CLOCKSYNC_H_ */
