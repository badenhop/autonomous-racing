/** *****************************************************************************************
 * Unit in charge:
 * @file	i2c.h
 * @author	Hauke Petersen (mail@haukepetersen.de) Robert Ledwig (robert.ledwig@berner-mattner.de)
 * $Date:: 2014-05-08 18:05:37 #$
 *
 * ----------------------------------------------------------
 * SVN information of last commit:
 * $Rev:: 449                   $
 * $Author:: wandji-kwuntchou   $
 *
 * ----------------------------------------------------------
 *
 * @brief 		Driver for the stm32f4 i2c interfaces.
 *
 * 				The driver runs the i2c interfaces of stm32f4 controllers. The driver
 *				supports the target to be run in master as well as in slave mode. In
 *				slave mode it is up to the application designer to implement the slave
 *				behavior, e.g. simulate register wise data access. The driver
 * 				is designed to to run synchronous calls, which means that write and
 * 				read operations are blocking until finished. The blocking time depends
 * 				hereby heavily on the selected bus frequency and the number of bytes to
 * 				be read or written.
 *
 ******************************************************************************************/


#ifndef I2C_H_
#define I2C_H_

/************* Includes *******************************************************************/
#include <stdint.h>
#include "app/config.h"

/************* Public typedefs ************************************************************/
typedef enum
{
#ifdef I2C_CH1_EN
	I2C_CH1,
#endif
#ifdef I2C_CH2_EN
I2C_CH2,
#endif
#ifdef I2C_CH3_EN
I2C_CH3
#endif
} i2c_channel_t;

typedef enum
{
I2C_MASTER, I2C_SLAVE
} i2c_mode_t;

typedef struct
{
i2c_channel_t channel;		/* select the channel to initialize */
i2c_mode_t mode;			/* run i2c as master or slave */
uint8_t address;			/* own address in slave mode, its set to zero in master mode */
uint32_t frequency;			/* the bus frequency in Hz */
} i2c_t;

/************* Macros and constants *******************************************************/
#define I2C_OK		0		/* don't panic, everything is fine */
#define I2C_ERR		-1		/* error on null pointer to device struct */

/************* Public function prototypes *************************************************/
/**
 * @brief	Initialize the chosen I2C channel
 *
 * @param[in,out] i2c_t* i2c contains the channel, mode, address and frequency to initialize
 *
 * @return I2C_OK if everything went fine, I2C_ERR on error
 */
int8_t i2c_init(i2c_t* i2c);

/**
 * @brief	Enable the given i2c channel
 *
 * @param[in] i2c_channel_t channel contains the i2c channel to enable
 *
 * @return I2C_OK if everything went fine, I2C_ERR on error
 *
 */
int8_t i2c_enable(i2c_channel_t channel);

/**
 * @brief	Disable the given i2c channel
 *
 * @param[in] i2c_channel_t channel contains the i2c channel to disable
 *
 * @return I2C_OK if everything went fine, I2C_ERR on error
 *
 */
int8_t i2c_disable(i2c_channel_t channel);

/**
 * @brief	Read one or more bytes of data from a addressed slave.
 *
 * @param[in,out] uint8_t* pData is the pointer of the data
 * @param[in] i2c_channel_t channel is the i2c channel
 * @param[in] uint8_t address is the slave address
 * @param[in] uint16_t numOfBytes is the number of bytes to be read
 *
 * @return I2C_OK if everything went fine, I2C_ERR on error
 */
int32_t i2c_readRaw(i2c_channel_t channel, uint8_t address, uint8_t* pData, uint16_t numOfBytes);

/**
 * @brief	Read one or more bytes of data from a certain register of a addressed slave.
 *
 * @param[in,out] uint8_t* pData is the pointer of the data
 * @param[in] i2c_channel_t channel is the i2c channel
 * @param[in] uint8_t address is the slave address
 * @param[in] uint8_t reg is the register to be read from
 * @param[in] uint16_t numOfBytes is the number of bytes to be read
 *
 * @return I2C_OK if everything went fine, I2C_ERR on error
 */
int32_t i2c_readReg(i2c_channel_t channel, uint8_t address, uint8_t reg, uint8_t* pData, uint16_t numOfBytes);

/**
 * @brief	Write one or more bytes of data to a addressed slave.
 *
 * @param[in,out] uint8_t* pData is the pointer of the data to be written
 * @param[in] i2c_channel_t channel is the i2c channel
 * @param[in] uint8_t address is the slave address
 * @param[in] uint16_t numOfBytes is the number of bytes to be written
 *
 * @return I2C_OK if everything went fine, I2C_ERR on error
 */
int32_t i2c_writeRaw(i2c_channel_t channel, uint8_t address, uint8_t* pData, uint16_t numOfBytes);

/**
 * @brief	Write one or more bytes of data to a certain register of a addressed slave.
 *
 * @param[in,out] uint8_t* pData is the pointer of the data to be written
 * @param[in] i2c_channel_t channel is the i2c channel
 * @param[in] uint8_t address is the slave address
 * @param[in] uint8_t reg is the register written to
 * @param[in] uint16_t numOfBytes is the number of bytes to be read
 *
 * @return I2C_OK if everything went fine, I2C_ERR on error
 */
int32_t i2c_writeReg(i2c_channel_t channel, uint8_t address, uint8_t reg, uint8_t* pData, uint16_t numOfBytes);

#endif /* I2C_H_ */



