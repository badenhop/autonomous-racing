# Exploring hyperparameters in deep learning

## Data augumentation

Without data augumentation the loss converges too quickly and the model overfits.
However less data augumentation is more.

Increasing the contrast helped recognizing the lane in the corridor run.

## Color format

May not be so significant but produced quite different saliency maps.

## Dropout

It appears to be that dropout decreases performance

## Batch normalization

By adding a batch normalization layer after the input layer, the loss finally startet to decrease constantly. The saliency maps showed that it learns lanes across different brighness levels. The more it trained the more it learned to ignore reflections as well.

## Learning rate

After having reached serious improvements with batch normalization, it's even more ridiciulous with setting the learning rate from 1.0e-4 to 1.0e-3.
The saliency maps become cristal clear, completely ignoring reflections by abstracting the lane markings.

## Summary of 3 Conv-Layer net

Playing around with learning rate:
Too high learning rate: saliency map completely random.
Too low leanring rate: saliency map recognizes lanes but very noisy and unable to abstract (seeing reflections).
Moderate learning rate: lanes are recognizable but with low intensity and still noisy. It's lacking the punch.

## Nondeterminism

I could not reproduce the nice results so easily since keras is not deterministic.