Entscheidungen und Probleme
===========================

In diesem Dokument werden Entscheidungen jeglicher Art festgehalten.

Anlegen der Projektstruktur
---------------------------

Folgende Libs werden übernommen:

    - NetworkingLib
    - MessageLib
    - VeloxProtocolLib
    - PC2CarLib (wird noch modifiziert)
    - wiringPi

Folgende Libs werden nicht übernommen:

    - PlatoonProtocolLib

Folgende Nodes werden übernommen:

    - Logging
    - MavLink
    - Ultrasonic
    - Camera (wird noch modifiziert)

Folgende Nodes wurden nicht übernommen:

    - MainNode
    - LaneKeeping
    - Environment

Des Weiteren wird auf jegliches Preprocessing im Mavlink-Node (Umrechnen in cm/s und Anwenden des StreamMedianFilter auf die Geschwindigkeit) verzichtet.

Stm Node
--------

Das Nodelet "mavLink"  wurde nun in "stm" umbenannt (einfach weil ich finde, das es eher das beschreibt, was es macht).
Außerdem wurden enige Messages neu definiert:
Statt "stmData" wurden nun zwei Message-Typen currSpeedMsg und currAngleMsg eingeführt.
Statt "ccData" wurden nun zwei MessageTypen setSpeedMsg und setAngleMsg eingeführt.
Die Motivation ist, mehr von der modularen ROS-Struktur gebrauch zu machen, sodass sich einzelne Nodes
flexibler entscheiden können, welche Daten sie genau abgreifen wollen.

Remote-Control
--------------

Es wurde nun ein Nodelet "remoteControl" angelegt, der über UDP Geschwindigkeit und Lenkwinkel übermittelt bekommt.
Dadurch erzielen wir nun eine deutlich genauere Lenkung, was sich vor allem beim Steuern mit einem Joystick bemerkbar macht.

Recorder
--------

Es wurde das Nodelet "recorder" hinzugefügt, das die Aufgabe hat, jegliche Daten, die das Fahrzeug bzw. der Nutzer erzeugen, aufzunehmen.
Dabei werden folgende Topics aboniert:

    - currSpeed
    - currAngle
    - setSpeed
    - setAngle
    - ussData

Die Kameradaten werden momentan noch direkt ausgelesen, ohne dabei von einem Topic gelesen zu werden.
Wir wussten an dieser Stelle nicht, wie schnell die Daten verarbeitet werden und hatten keine Zeit mehr,
den Vergleich zwischen "über's Topic" vs Direktauslesen zu machen.
Diese Fragestellung sollten wir jedoch noch in zukunft machen.

Das Format der Datenspeicherung ist CSV.
Das Nodelet "recorder" fängt an, Daten aufzunehmen und abzuspeichern, sobald es gestartet wird.
Die Aufnahme wird durch eine ROS-Message an das Topic "stopRecording" beendet.
ACHTUNG: Die Aufnahme sollte nicht durch Abbruch des ROS-Programms vollzogen werden, da dadurch Daten verloren gehen!
Die Message zum Beenden der Aufnahme kann durch den Befehl:

    rostopic pub stopRecording car/stopRecordingMsg '{}'

erfolgen.
Für jede Aufnahme wird ein neuer Ordner angelegt.
Dies hat den Grund, dass wenn man bestimmte Aufnahmen verhaut, diese dann einfach wieder löschen kann.
Am Ende kann man durch Post-Processing eine .csv Datei erstellen, die dann alle Aufnahmen vereinen.
Die Aufnahmedaten befinden sich im Ordner /root/recording.

Message-Renaming
----------------

Die ROS-Messages beginnen nun mit einem Großbuchstaben und enden nicht mehr auf "Msg".
Grund: Dadurch, dass aus den Messages Klassen generiert werden, brachen die alten Messagesnamen unseren Code/Naming-Style. Ros-Messagenamen fangen auch üblicherweise mit einem Großbuchstaben an.

Neuer Config-Mechanismus
------------------------

Es gibt jetzt einen festen Ordner für Daten des Autos:

    ~/.car/

Hinweis: Der Ordner, in dem die Trainingsdaten aufgenommen werden ist nun auch hierhin gezogen und heißt nun 'recordings'.

Außerdem gibt es jetzt die Datei 'config.json', die alle Konfigurationen in einer Datei beinhalten soll. Hier verwenden wir das JSON Format, weil es extrem einfach zu Parsen ist.
Zum Parsen benutzt man das json Objekt aus dem Header include/utils/Json.h.
Mit der utility-Funktion in include/utils/Config.h holt man sich eine Instanz zum Parsen der ~/.car/config.json Datei:

    #include "utils/Config.h"
    auto j = config::open();

Für weitere Informationen zum Parsen von json-Dateien mit C++ gibt es hier die Doku:
https://github.com/nlohmann/json

Geschwindigkeitskontrolle
-------------------------

In der config.json Datei befindet sich das Attribut 'trottleGain'.
Dies ist ein Faktor, der mit im STM-Modul mit dem Wert aus der Message 'SetThrottle' multipliziert wird. Setzt man diesen Wert also auf 0.1, dann fährt das Auto nur noch maximal 10& der Maximalgeschwindigkeit.

Kameraaufnahme
--------------

Am Anfang haben wir zur Aufnahme von Kamerabildern immer opencv's VideoCapture Klasse benutzt.
Es hat sich herausgestellt, dass hierdurch teilweise 4 Frames hintereinander dasselbe Bild aufgenommen wurde.
Die Kamera nimmt also nur mit einem Bruchteil der eigentlichen Framerate auf.
Die Lösung war an dieser Stelle die GStreamer Library zu benutzen. 
Bilder sollen ab nun nicht mehr mit VideoCapture, sondern mit der von mir geschriebenen Klasse 'AsyncCapture' aufgenommen werden.
Der Vorteil an AsyncCapture ist zudem, dass die Bilder in einem anderen Thread aufgenommen werden.
Beim Aufruf der Methode 'start()' wird ein Callback übergeben, welches aufgerufen wird, sobald ein neuer Frame (vom Typ cv::Mat) aufgenommen wurde.


 