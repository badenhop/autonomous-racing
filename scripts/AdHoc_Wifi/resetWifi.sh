# Switch from AdHoc Network to eduroam

IFACE="wlan0"

# Reset wifi card
sudo ifconfig IFACE down
sudo iw IFACE set type managed
sudo ifconfig IFACE up
sleep 0.5

# Connect to wifi and obtain IP address
sudo wpa_cli
sudo dhclient -r
sudo dhclient
