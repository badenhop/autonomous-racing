# Addresses:
# Odroid 10.42.43.1
#

IFACE="wlan0"

if [ -z "$1" ]
  then
    echo Missing last digit of IP-Adress
    exit 1
fi

echo start

echo sudo service network-manager stop
sudo service network-manager stop
sleep 0.5

echo sudo ifconfig $IFACE down
sudo ifconfig $IFACE down
sleep 0.5

echo sudo iw $IFACE set type ibss
sudo iw $IFACE set type ibss
sleep 0.5

echo sudo ifconfig $IFACE up
sudo ifconfig $IFACE up
sleep 0.5

echo sudo iw $IFACE ibss join SPHAF 2432
sudo iw $IFACE ibss join SPHAF 2432
sleep 0.5

echo sudo ifconfig $IFACE 10.42.43.$1
sudo ifconfig $IFACE 10.42.43.$1
sleep 0.5

echo done
